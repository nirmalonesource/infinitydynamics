//
//  ShowAllJobViewController.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/04/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import "ShowAllJobViewController.h"
#import "Recentjob.h"
#import "myModel.h"
#import "Constants.h"
#import "Recentjobcell.h"
#import "UIImageView+Haneke.h"
#import "JobDetails.h"
#import "NSPredicate+Search.h"
#import "ViewController.h"
@interface ShowAllJobViewController ()

@end

@implementation ShowAllJobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Recentjobcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Recentjobcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
        return marrRecentjob.count;
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(Recentjobcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"CompanyName"] ];
    
    cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"JobTitle"] ];
    cell.lblshiptype.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"ShipType"] ];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    // [cell.imageview hnk_setImageFromURL:url];
    [cell.imageview hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];
    [APP_DELEGATE hideLoadingView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
//    s.detail = [marrRecentjob objectAtIndex:indexPath.section] ;
//    
//    [self.navigationController pushViewController:s animated:NO];
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"You need to Sign-In / Sign-Up to apply for this Job."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Accept"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self.navigationController popViewControllerAnimated:NO];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Decline"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    //candidate_id:1
    //keyword:oil
    //searchtype:3
    //[parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:@"" forKey:@"candidate_id"];
    [parameters setValue:@"" forKey:@"keyword"];
    [parameters setValue:@"" forKey:@"searchtype"];
    NSLog(@"PARAMETERS: %@",parameters);

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpRecentjob  = [[HttpWrapper alloc] init];
        httpRecentjob.delegate=self;
        httpRecentjob.getbool=NO;
        
        [httpRecentjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@search_job",MainUrl] param:[parameters copy]];
    });
    
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpRecentjob && httpRecentjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrRecentjob = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrRecentjob);
            [self.tableview reloadData];
        }
        else{
            
            
        }
        [APP_DELEGATE hideLoadingView];

    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)back_clickl:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
@end
