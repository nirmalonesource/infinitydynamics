//
//  Termsandcondition.m
//  Infinity Dynamics
//
//  Created by My Mac on 20/06/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Termsandcondition.h"
#import "Constants.h"
#import "UIImageView+Haneke.h"
#import "SWRevealViewController.h"
@interface Termsandcondition ()

@end

@implementation Termsandcondition

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
      self.navigationController.navigationBarHidden=YES;
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"terms" ofType:@"pdf"];
//    NSURL *url = [NSURL fileURLWithPath:path];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"infinitytnc" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:path];
    
   // NSURL *url = [[NSBundle mainBundle] URLForResource:@"infinitytnc" withExtension:@"html"];
    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    [self customSetup];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
