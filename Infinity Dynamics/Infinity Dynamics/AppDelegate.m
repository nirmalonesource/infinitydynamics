//
//  AppDelegate.m
//  Infinity Dynamics
//
//  Created by My Mac on 18/01/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "AppDelegate.h"
#import "DGActivityIndicatorView.h"
#import "UIColor+CL.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "SPWaterProgressIndicatorView.h"
#import "UIImage+animatedGIF.h"
#import <OneSignal/OneSignal.h>
#import "NotificationVC.h"
@interface AppDelegate ()
@property (nonatomic, strong) SPWaterProgressIndicatorView *waterView;
@property (nonatomic) BOOL isAnimating;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [NSThread sleepForTimeInterval:2.000];
    IQKeyboardManager.sharedManager.enable = true;
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
    [IQKeyboardManager sharedManager].previousNextDisplayMode = IQPreviousNextDisplayModeAlwaysShow;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setObject:@"" forKey:@"CountryIDSelected"];
    [CountryDataSave setObject:@"" forKey:@"CountryNameSelected"];
    [CountryDataSave setObject:@"NO" forKey:@"isHomeButtonFlag"];
    [CountryDataSave synchronize];
    
    // (Optional) - Create block the will fire when a notification is recieved while the app is in focus.
    id notificationRecievedBlock = ^(OSNotification *notification) {
        NSLog(@"Received Notification - %@", notification.payload.notificationID);
    };
    
    // (Optional) - Create block that will fire when a notification is tapped on.
    id notificationOpenedBlock = ^(OSNotificationOpenedResult *result) {
        OSNotificationPayload* payload = result.notification.payload;
        
        NSString* messageTitle = @"OneSignal Example";
        NSString* fullMessage = [payload.body copy];
        NSLog(@"PAYLOADBODY:%@",result.notification.payload.body);
        NSDictionary * additionalData = result.notification.payload.additionalData;
        NSLog(@"PAYLOADDATA:%@",additionalData);
//        if (payload.additionalData) {
//            //
//            if ([[additionalData valueForKey:@"type"]isEqualToString:@"chat"])
//            {
//
//            }
//
//        }
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"email"]== NULL) {
            
        }
        else{
            NotificationVC *VC = (NotificationVC *)[[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"NotificationVC"];
            self.window.rootViewController = VC;
        }
    };
    
    // (Optional) - Configuration options for OneSignal settings.
    id oneSignalSetting = @{kOSSettingsKeyInFocusDisplayOption : @(OSNotificationDisplayTypeNotification), kOSSettingsKeyAutoPrompt : @YES};
    
    
    // NSDictionary * setting = @{kOSSettingsKeyAutoPrompt : @false}; 2a5ba702-5f50-4bb2-b3d6-d3290f4940d1
    [OneSignal initWithLaunchOptions:launchOptions
                               appId:@"509d6d17-622c-4673-90ab-3abebc65e7f1"
          handleNotificationReceived:notificationRecievedBlock
            handleNotificationAction:notificationOpenedBlock
                            settings:oneSignalSetting];
    //  [OneSignal initWithLaunchOptions:launchOptions appId:@"e8c07ba8-2c00-4f64-b0ee-b4514e2ab412" handleNotificationAction:nil settings:oneSignalSetting];
    
    OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
    
    [OneSignal registerForProvisionalAuthorization:^(BOOL accepted) {
        //handle authorization
    }];
    // Recommend moving the below line to prompt for push after informing the user about
    //   how your app will use them.
    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
        NSLog(@"User accepted notifications: %d", accepted);
        
    }];
    
    NSLog(@"PLAYERID: %@",status.subscriptionStatus.userId);
    [[NSUserDefaults standardUserDefaults] setObject:status.subscriptionStatus.userId forKey:@"PushToken"];
    
    
    
//    // (Optional) - Configuration options for OneSignal settings.
//     id oneSignalSetting = @{kOSSettingsKeyInFocusDisplayOption : @(OSNotificationDisplayTypeNotification), kOSSettingsKeyAutoPrompt : @YES};
//    id notificationRecievedBlock = ^(OSNotification *notification) {
//        NSLog(@"Received Notification - %@", notification.payload.notificationID);
//    };
//
//    // (Optional) - Create block that will fire when a notification is tapped on.
//    id notificationOpenedBlock = ^(OSNotificationOpenedResult *result) {
//        OSNotificationPayload* payload = result.notification.payload;
//
////        NSString* messageTitle = @"OneSignal Example";
////        NSString* fullMessage = [payload.body copy];
//        NSLog(@"PAYLOADBODY:%@",result.notification.payload.body);
//        NSDictionary * additionalData = result.notification.payload.additionalData;
//        NSLog(@"PAYLOADDATA:%@",additionalData);
//        if (payload.additionalData) {
//            //
//
//        }
//    };
//
//    // NSDictionary * setting = @{kOSSettingsKeyAutoPrompt : @false};
//    [OneSignal initWithLaunchOptions:launchOptions
//                               appId:@"509d6d17-622c-4673-90ab-3abebc65e7f1"
//          handleNotificationReceived:notificationRecievedBlock
//            handleNotificationAction:notificationOpenedBlock
//                            settings:oneSignalSetting];
//
//    OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
//    OneSignal.inFocusDisplayType = OSNotificationDisplayTypeNotification;
//
//    [OneSignal registerForProvisionalAuthorization:^(BOOL accepted) {
//        //handle authorization
//        NSLog(@"User accepted notifications: %d", accepted);
//    }];
//    // Recommend moving the below line to prompt for push after informing the user about
//    //   how your app will use them.
//    [OneSignal promptForPushNotificationsWithUserResponse:^(BOOL accepted) {
//        NSLog(@"User accepted notifications: %d", accepted);
//    }];
//
//    NSLog(@"PLAYERID: %@",status.subscriptionStatus.userId);
//    [[NSUserDefaults standardUserDefaults] setObject:status.subscriptionStatus.userId forKey:@"PushToken"];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Infinity_Dynamics"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
-(void)showLoadingView:(NSString *)strMSG
{
    [self hideLoadingView];
    UIView *view = [[UIView alloc] initWithFrame:self.window.bounds];
    [view setBackgroundColor:[UIColor blackColor]];
    view.tag = 4444;
    [view setAlpha:0.6];
    //    DGActivityIndicatorView *act = [[DGActivityIndicatorView alloc] initWithType:(DGActivityIndicatorAnimationType)DGActivityIndicatorAnimationTypeBallScaleRippleMultiple tintColor:[UIColor colorWithHex:0xF68B1E]];
    //    [act startAnimating];
    NSUInteger percent = 50;
    [self.waterView updateWithPercentCompletion:percent];
    self.waterView = [[SPWaterProgressIndicatorView alloc] initWithFrame:view.bounds];
    self.waterView.center = view.center;
    [view addSubview:self.waterView];
    //   [view addSubview:act];
    //  act.center = view.center;
    //  NSURL *  url = [[NSBundle mainBundle] URLForResource:@"steering" withExtension:@"gif"];
    //    self.Image.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    //  self.Image.center = view.center;
    //    [view addSubview:self.Image];
    
    [self.window addSubview:view];
}
-(void)hideLoadingView
{
    for(UIView *view in self.window.subviews)
    {
        if(view.tag==4444)
        {
            [view removeFromSuperview];
        }
    }
}
+(AppDelegate *)sharedDelegate
{
    return  (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
-(NSString *)applicationDocumentDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

-(void)AlertView:(NSString *)Title :(NSString *)Message
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:Title message:Message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed Ok, button");
                                   // call method whatever u need
                               }];
    [alert addAction:okButton];
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}

-(void)AlertViewWithCancel:(NSString *)Title :(NSString *)Message
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:Title message:Message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    NSLog(@"you pressed OK, button");
                                    
                                    // call method whatever u need
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"Cancel"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed Cancel, button");
                                   // call method whatever u need
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
}
@end
