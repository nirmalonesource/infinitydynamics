//
//  AppDelegate.h
//  Infinity Dynamics
//
//  Created by My Mac on 18/01/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    AppDelegate*appdelegate;
}


+(AppDelegate *)sharedDelegate;
-(void)showLoadingView:(NSString *)strMSG;
-(void)hideLoadingView;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IBOutlet UIImageView * Image;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property(nonatomic,retain)NSString *totalcandidate;
@property(nonatomic,retain)NSString *totalLatestcandidate;
@property(nonatomic,retain)NSString *totaljob;
@property(nonatomic,retain)NSString *totaljobapplication;
@property(nonatomic,retain)NSString *totalshorejobResume;
@property(nonatomic,retain)NSMutableArray * loginUserData;

- (void)saveContext;
-(NSString *)applicationDocumentDirectoryString;

-(void)AlertView:(NSString *)Title :(NSString *)Message;
-(void)AlertViewWithCancel:(NSString *)Title :(NSString *)Message;
@end

