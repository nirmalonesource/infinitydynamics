//
//  ViewController.m
//  Infinity Dynamics
//
//  Created by My Mac on 18/01/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "ViewController.h"
#import "myModel.h"
#import "Constants.h"
#import "Signupoption.h"
#import "HomeScreen.h"
#import "ComanyHome.h"
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import <OneSignal/OneSignal.h>
#import "EditProfile.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self performSegueWithIdentifier:@"My" sender:nil];
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setObject:@"NO" forKey:@"isHomeButtonFlag"];
    [CountryDataSave synchronize];
    
     _viewterms.hidden = YES;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"infinitytnc" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:path];
    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    // Do any additional setup after loading the view, typically from a nib.
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationController.navigationBarHidden=YES;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapWebView:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.delegate = self;
    [self.view addGestureRecognizer:singleTap];
    
    UIColor *color = [UIColor whiteColor];
    _txtemail.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Your Email"
        attributes:@{NSForegroundColorAttributeName: color}];
    _txtpassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Your Password" attributes:@{NSForegroundColorAttributeName: color}];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"email"]== NULL) {
        
    }
    else{
        _txtemail.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
        _txtpassword.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"Password"];
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.01];
        
    }
    
}
- (void)singleTapWebView:(UITapGestureRecognizer *)gesture {
    [self dimBackgroundViewCancel];
    [forgotEmailidTextField resignFirstResponder];
    forgotPasswordView.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
-(BOOL) validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    
    if ([emailTest evaluateWithObject:forgotEmailidTextField.text] == YES)
    {
        forgotEmailFlag =YES;
    }
    else
    {
        showAlert(AlertTitle,@"Please enter a valid email address.");
        
        [forgotEmailidTextField becomeFirstResponder];
        forgotEmailFlag =NO;
    }
    return isValid;
}
- (IBAction)btnForgetpassword:(id)sender {
    [self dimBackgroundView];
    forgotPasswordView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-140, 150,280,200)];
    forgotPasswordView.backgroundColor=[UIColor whiteColor];
    forgotPasswordView.layer.borderWidth=2.0f;
    forgotPasswordView.layer.cornerRadius=4;
    forgotPasswordView.layer.borderColor=[UIColor clearColor].CGColor;
    [self.view addSubview:forgotPasswordView];
    forgotPasswordLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0,20, 280, 20)];
    forgotPasswordLabel2.textAlignment = NSTextAlignmentCenter;
    forgotPasswordLabel2.backgroundColor=[UIColor clearColor];
    [forgotPasswordView addSubview:forgotPasswordLabel2];
    [forgotPasswordView addSubview:forgotPasswordLabel1];
    forgotPasswordLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0,20, 280, 40)];
    forgotPasswordLabel1.textAlignment = NSTextAlignmentCenter;
    forgotPasswordLabel1.backgroundColor=[UIColor clearColor];
    forgotPasswordLabel1.layer.cornerRadius=4.0;
    forgotPasswordLabel1.clipsToBounds=YES;
    forgotPasswordLabel1.text=@"Forgot Password?";
    forgotPasswordLabel1.textColor=[UIColor blackColor];
    [forgotPasswordLabel1 setFont: [UIFont fontWithName:@"Montserrat-Regular" size:14.0]];
    [forgotPasswordView addSubview:forgotPasswordLabel1];
    forgotPasswordView1=[[UIView alloc]initWithFrame:CGRectMake(0,80,280,110)];
    forgotPasswordView1.backgroundColor=[UIColor whiteColor];
    forgotPasswordView1.backgroundColor=[UIColor whiteColor];
    forgotPasswordView1.layer.cornerRadius=10;
    [forgotPasswordView addSubview:forgotPasswordView1];
    forgotEmailidTextField=[[UITextField alloc]initWithFrame:CGRectMake(10, 5, 260, 40)];
    forgotEmailidTextField.delegate=self;
    forgotEmailidTextField.backgroundColor=[UIColor whiteColor];
    forgotEmailidTextField.placeholder=@"Email";
    [forgotEmailidTextField setFont: [UIFont fontWithName:@"Montserrat-Regular" size:13.0]];
  //  [forgotEmailidTextField setValue:[UIColor colorWithRed:0.50 green:0.50 blue:0.50 alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
    forgotEmailidTextField.textColor = [UIColor colorWithRed:0.50 green:0.50 blue:0.50 alpha:1.0];
    forgotEmailidTextField.layer.borderWidth=1.0f;
    forgotEmailidTextField.autocorrectionType=UITextAutocorrectionTypeNo;
    forgotEmailidTextField.layer.cornerRadius=4;
    forgotEmailidTextField.layer.borderColor=[UIColor colorWithRed:0.50 green:0.50 blue:0.50 alpha:1.0].CGColor;
    forgotEmailidTextField.layer.sublayerTransform=CATransform3DMakeTranslation(5, 0, 0);
    [forgotPasswordView1 addSubview:forgotEmailidTextField];
    sendButton =[[UIButton alloc]initWithFrame:CGRectMake(10,55, 260, 40)];
    sendButton.backgroundColor=[UIColor clearColor];
    [sendButton addTarget:self action:@selector(sendButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    sendButton.layer.cornerRadius=4;
    [forgotPasswordView1 addSubview:sendButton];
    sendLabel = [[UILabel alloc]initWithFrame:CGRectMake(10,55,260,40)];
    sendLabel.backgroundColor=[UIColor colorWithHex:0x142361];
    sendLabel.text=@"SEND";
    sendLabel.layer.cornerRadius=4.0;
    sendLabel.clipsToBounds=YES;
    sendLabel.textAlignment=NSTextAlignmentCenter;
    sendLabel.textColor=[UIColor whiteColor];
    [sendLabel setFont: [UIFont fontWithName:@"Montserrat-Regular" size:15.0]];
    [forgotPasswordView1 addSubview:sendLabel];
    [forgotEmailidTextField becomeFirstResponder];
}

-(void)sendButtonClicked
{
    [forgotEmailidTextField resignFirstResponder];
    if ([forgotEmailidTextField.text length] <=0)
    {
        showAlert(AlertTitle,@"Please enter email Id.");
    }
    else
    {
        [self validateEmail:forgotEmailidTextField.text];
    }
    
    if (forgotEmailFlag)
    {
        [self dimBackgroundViewCancel];
        if (![self connected])
        {
            //            [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
            
            showAlert(AlertTitle,@"Internet is not available.");
        }
        else
        {
            [self forgotPasswordJsonParsing];
        }
    }
}
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
-(void)forgotPasswordJsonParsing{
    
//    dicOfferList1 = [[NSMutableArray alloc] init];
//    //   change_password?userid=&old_password=&new_password=&confirm_new_password=
//
//    str = [NSString stringWithFormat:@"%@forgot_password?email=%@",MainUrl,forgotEmailidTextField.text];
//
//    NSString *encodedString = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//
//    dicOfferList1 =[myModel getDictResponse1:encodedString RequestData:@""] ;
//    NSString * success = [NSString stringWithFormat:@"%@",[dicOfferList1 valueForKey:@"status"]];
//    if ([success isEqualToString:@"1"]) {
//        NSString * message = [NSString stringWithFormat:@"%@",[dicOfferList1 valueForKey:@"message"]];
//        showAlert(AlertTitle,message);
//
//        [self dimBackgroundViewCancel];
//        [forgotEmailidTextField resignFirstResponder];
//        forgotPasswordView.hidden=YES;
//
//
//
//    } else{
//        // newsarray1 = nil;
//        NSString * message = [NSString stringWithFormat:@"%@",[dicOfferList1 valueForKey:@"message"]];
//        showAlert(AlertTitle,message);
//        [forgotEmailidTextField resignFirstResponder];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:forgotEmailidTextField.text forKey:@"email"];
    
   // [parameters setValue:_txtpassword.text forKey:@"password"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpforgotpassword  = [[HttpWrapper alloc] init];
        httpforgotpassword.delegate=self;
        httpforgotpassword.getbool=NO;
        
        [httpforgotpassword requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@forgot_password",MainUrl] param:[parameters copy]];
    });
    
    }

#pragma mark background view blurry

-(void)dimBackgroundView
{
    dimView = [[UIView alloc]initWithFrame:self.view.frame];
    dimView.backgroundColor = [UIColor blackColor];
    dimView.alpha = 0;
    [self.view addSubview:dimView];
    [self.view bringSubviewToFront:dimView];
    [UIView animateWithDuration:0.3
                     animations:^{
                         dimView.alpha = 0.3;
                     }];
}

-(void)dimBackgroundViewCancel
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         dimView.alpha = 0;
                     }];
    [dimView removeFromSuperview];
    dimView = nil;
}
-(void)cancelButtonClicked
{
    [self dimBackgroundViewCancel];
    [forgotEmailidTextField resignFirstResponder];
    forgotPasswordView.hidden=YES;
}

- (IBAction)btnLogin:(id)sender {
    if ([_txtemail.text isEqualToString:@""]||[_txtpassword.text isEqualToString:@""]){
        showAlert(AlertTitle,@"Fill all the detail");
    }else{
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
    }
    // [self performSelector:@selector(sigininClicked1:) withObject:nil afterDelay:0.01];
}

-(void)Loginin{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:_txtemail.text forKey:@"email"];
    [AddPost setValue:_txtpassword.text forKey:@"password"];
    
    //-----------------------------------------------------------------------------------------------------
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@login",MainUrl] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        NSMutableDictionary *Moreinfo=[[NSMutableDictionary alloc]init];
        Moreinfo=[responseObject valueForKey:@"more_info"];
        
        appdelegate.loginUserData = (NSMutableArray *)[responseObject valueForKey:@"more_info"];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
        if ([[responseObject valueForKey:@"status"] integerValue] == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login" message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            //[alert show];
            [APP_DELEGATE hideLoadingView];
        }
        else
        {
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            [standardUserDefaults setObject:[dic valueForKey:@"companyid"] forKey:@"UserID"];
            [standardUserDefaults setObject:[dic valueForKey:@"mobile"] forKey:@"mobile"];
            [standardUserDefaults setObject:[dic valueForKey:@"ImageURL"] forKey:@"ImageURL"];
            
            [standardUserDefaults setObject:[dic valueForKey:@"companyid"] forKey:@"companyid"];
            [standardUserDefaults setObject:[dic valueForKey:@"username"] forKey:@"username"];
            [standardUserDefaults setObject:[dic valueForKey:@"usertype"] forKey:@"usertype"];
            [standardUserDefaults setObject:[dic valueForKey:@"email"] forKey:@"email"];
            
            ///moreinfo
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"Address"] forKey:@"Address"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"CCode"] forKey:@"CCode"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"LogoURL"] forKey:@"LogoURL"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"CandidateID"] forKey:@"CandidateID"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"CityName"] forKey:@"CityName"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"Created"] forKey:@"Created"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"DOB"] forKey:@"DOB"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"EmailID"] forKey:@"EmailID"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"EmailID2"] forKey:@"EmailID2"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"FirstName"] forKey:@"FirstName"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"ImageUrl"] forKey:@"profileImageUrl"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"Indos"] forKey:@"Indos"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"IsActive"] forKey:@"IsActive"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"LastName"] forKey:@"LastName"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"MaritalStatus"] forKey:@"MaritalStatus"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"MiddleName"] forKey:@"MiddleName"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"MobileNo"] forKey:@"MobileNo"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"Nationality"] forKey:@"Nationality"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"NoofChildren"] forKey:@"NoofChildren"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"OtherMobileNo"] forKey:@"OtherMobileNo"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"RankID"] forKey:@"RankID"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"RankName"] forKey:@"RankName"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"TelephoneNo"] forKey:@"TelephoneNo"];
            [standardUserDefaults setObject:[Moreinfo valueForKey:@"Zipcode"] forKey:@"Zipcode"];
            NSString *   type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
            if ([type isEqualToString:@"Candidate"]) {
                
                
                HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:s animated:NO];
            }else{
                ComanyHome *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:s animated:NO];
            }
            
            [APP_DELEGATE hideLoadingView];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}
#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    //Post Method Request
     OSPermissionSubscriptionState* status = [OneSignal getPermissionSubscriptionState];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:_txtemail.text forKey:@"email"];
    
    [parameters setValue:_txtpassword.text forKey:@"password"];
    [parameters setValue:@"IOS" forKey:@"device"];
    [parameters setValue:status.subscriptionStatus.userId forKey:@"TokenID"];
     NSLog(@"PLAYERID: %@",status.subscriptionStatus.userId);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplogin  = [[HttpWrapper alloc] init];
        httplogin.delegate=self;
        httplogin.getbool=NO;
        
        [httplogin requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@login",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httplogin && httplogin != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
            [APP_DELEGATE hideLoadingView];
        }
        else{
            dic=[dicsResponse valueForKey:@"data"];
            NSMutableDictionary *Moreinfo=[[NSMutableDictionary alloc]init];
            Moreinfo=[dicsResponse valueForKey:@"more_info"];
            
            appdelegate.loginUserData = (NSMutableArray *)[dicsResponse valueForKey:@"more_info"];
            
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            [standardUserDefaults setObject:_txtpassword.text forKey:@"Password"];
            [standardUserDefaults setObject:[[dic  valueForKey:@"companyid"] objectAtIndex:0]forKey:@"UserID"];
            [standardUserDefaults setObject:[[dic  valueForKey:@"mobile"] objectAtIndex:0] forKey:@"mobile"];
            [standardUserDefaults setObject:[[dic  valueForKey:@"ImageURL"] objectAtIndex:0] forKey:@"ImageURL"];
            [standardUserDefaults setObject:[[dic valueForKey:@"companyid"]objectAtIndex:0] forKey:@"companyid"];
            [standardUserDefaults setObject:[[dic valueForKey:@"username"]objectAtIndex:0] forKey:@"username"];
            [standardUserDefaults setObject:[[dic valueForKey:@"usertype"]objectAtIndex:0] forKey:@"usertype"];
            [standardUserDefaults setObject:[[dic valueForKey:@"email"]objectAtIndex:0] forKey:@"email"];
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]);
            
            if ([[NSString stringWithFormat:@"%@",Moreinfo] isEqual: @"<null>"] ) {
                
            }else{
                ///moreinfo
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Address"] forKey:@"Address"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"CCode"] forKey:@"CCode"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"CandidateID"] forKey:@"CandidateID"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"CityName"] forKey:@"CityName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"LogoURL"] forKey:@"LogoURL"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Created"] forKey:@"Created"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"DOB"] forKey:@"DOB"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"EmailID"] forKey:@"EmailID"];
                 [standardUserDefaults setObject:[Moreinfo valueForKey:@"id"] forKey:@"id"];
                if ([[NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"EmailID2"]] isEqual: @"<null>"] )
                {
                    [standardUserDefaults setObject:@"" forKey:@"EmailID2"];
                }
                else
                {
                    [standardUserDefaults setObject:[Moreinfo valueForKey:@"EmailID2"] forKey:@"EmailID2"];
                }
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"FirstName"] forKey:@"FirstName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"ImageUrl"] forKey:@"profileImageUrl"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Indos"] forKey:@"Indos"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"IsActive"] forKey:@"IsActive"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"LastName"] forKey:@"LastName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"MaritalStatus"] forKey:@"MaritalStatus"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"MiddleName"] forKey:@"MiddleName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"MobileNo"] forKey:@"MobileNo"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Nationality"] forKey:@"Nationality"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"NoofChildren"] forKey:@"NoofChildren"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"OtherMobileNo"] forKey:@"OtherMobileNo"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"RankID"] forKey:@"RankID"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"RankName"] forKey:@"RankName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"TelephoneNo"] forKey:@"TelephoneNo"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Zipcode"] forKey:@"Zipcode"];
            }
            
            appdelegate.totaljob = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_job"]];
            appdelegate.totalcandidate = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_candidate"]];
            appdelegate.totalLatestcandidate = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_latest_candidate"]];
            appdelegate.totaljobapplication = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_job_application"]];
            appdelegate.totalshorejobResume = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_shorejob"]];
            
            NSString *   type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
            if ([type isEqualToString:@"Candidate"]) {
                HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:s animated:NO];
            }else{
                ComanyHome *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:s animated:NO];
            }
            long DateOfAvaliability = [[[dic valueForKey:@"DateOfAvaliability"] objectAtIndex:0] longValue];
            if (DateOfAvaliability == 1)
            {
                
                UIAlertController * alert = [UIAlertController
                                                alertControllerWithTitle:@"Alert"
                                                message:@"Your given date of availability is more than 30 days old - Update?"
                                                preferredStyle:UIAlertControllerStyleAlert];
                   
                   UIAlertAction* yesButton = [UIAlertAction
                                               actionWithTitle:@"Yes"
                                               style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Handle your yes please button action here
                                                   EditProfile *s =[self.storyboard instantiateViewControllerWithIdentifier:@"EditProfile"];
                       s.profilecandidatebool = YES;
                       s.isfrompopup = YES;
                                                   // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                                                   [self.navigationController pushViewController:s animated:NO];
                                               }];
                   
                   UIAlertAction* noButton = [UIAlertAction
                                              actionWithTitle:@"No"
                                              style:UIAlertActionStyleDefault
                                              handler:^(UIAlertAction * action) {
                                                  //Handle no, thanks button
                                              }];
                   
                   [alert addAction:yesButton];
                   [alert addAction:noButton];
                   
                   [self presentViewController:alert animated:YES completion:nil];
            }
            
            
        }
        [APP_DELEGATE hideLoadingView];
    }
    if(wrapper == httpforgotpassword && httpforgotpassword != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
       // NSString * success = [NSString stringWithFormat:@"%@",[dicOfferList1 valueForKey:@"status"]];
            if ([status isEqualToString:@"1"]) {
                NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
                showAlert(AlertTitle,message);
        
                [self dimBackgroundViewCancel];
                [forgotEmailidTextField resignFirstResponder];
                forgotPasswordView.hidden=YES;
            
            } else{
                // newsarray1 = nil;
                NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
                showAlert(AlertTitle,message);
                [forgotEmailidTextField resignFirstResponder];
    }
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

- (IBAction)btnSingUp:(id)sender {
   
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Alert"
                                 message:@"Personal / Maritime Certification, and Sailing Experience details are required for registration. \n \nYou agree to receive calls from Shipping Companies registered on this portal seeking candidates for their requirements."
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Accept"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    Signupoption *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Signupoption"];
                                    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                                    [self.navigationController pushViewController:s animated:NO];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Decline"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)btnCloseterms:(id)sender {
    _viewterms.hidden = YES;
    //_btnaccept.userInteractionEnabled = YES;
}
- (IBAction)BtnTermsnConditions:(id)sender {
    _viewterms.hidden = NO;
   // _btnaccept.userInteractionEnabled = YES;
}

- (IBAction)viewjobs_click:(UIButton *)sender {
    
    UIViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ShowAllJobViewController"];
    [self.navigationController pushViewController:s animated:NO];
}
@end
