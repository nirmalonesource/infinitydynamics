//
//  ViewController.h
//  Infinity Dynamics
//
//  Created by My Mac on 18/01/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowAlert.h"
#import "TextFieldValidator.h"
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import <WebKit/WebKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate,HttpWrapperDelegate>{
    HttpWrapper *httplogin;
    HttpWrapper *httpforgotpassword;

    AppDelegate *appdelegate;
    NSDictionary *dicSignIn;
    ////Forgetpassword
    bool forgotEmailFlag;
    UIAlertView *forgotPasswordAlertView;
    UIView *forgotPasswordView;
    UILabel *forgotPasswordLabel1;
    UILabel *forgotPasswordLabel2;
    UIView *forgotPasswordView1;
    UITextField *forgotEmailidTextField;
    UIButton *sendButton;
    UILabel *sendLabel;
    UIButton *cancelButton;
    UILabel *cancelLabel;
    NSMutableArray *dicOfferList;
    NSMutableArray *dicOfferList1;
    NSString *str;
    
    // to make background invisible or blurry
    UIView *dimView;
}
@property (strong, nonatomic) IBOutlet UITextField *txtemail;
@property (strong, nonatomic) IBOutlet UITextField *txtpassword;
- (IBAction)btnForgetpassword:(id)sender;
- (IBAction)btnLogin:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnSignup;
- (IBAction)btnSingUp:(id)sender;
@property (strong, nonatomic) IBOutlet WKWebView *webview;

//@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UIView *viewterms;
@property (strong, nonatomic) IBOutlet UIView *containerView;

- (IBAction)btnCloseterms:(id)sender;

- (IBAction)BtnTermsnConditions:(id)sender;
- (IBAction)viewjobs_click:(UIButton *)sender;

@end

