//
//  SideMenu.h
//  Infinity Dynamics
//
//  Created by My Mac on 08/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface SideMenu : UIViewController{
     NSMutableArray* arrname;
    NSMutableArray*  arrimg;
    AppDelegate *appdelegate;

    NSMutableArray* arrcompanyname;
    NSMutableArray*  arrcompanyimg;
    NSString * type;
     NSString* profilepicname ;
    
}
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)BtnLogout:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblCountry;

@end
