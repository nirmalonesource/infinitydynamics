//
//  SideMenu.m
//  Infinity Dynamics
//
//  Created by My Mac on 08/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "SideMenu.h"
#import "SWRevealViewController.h"
#import "Homeviewcell.h"
#import "ViewController.h"
#import "NewsScreen.h"
#import "MyProfileCandidate.h"
#import "UIImageView+WebCache.h"
#import "Companies.h"
#import "Aboutus.h"
#import "Support.h"
#import "Constants.h"
#import "ComanyHome.h"
#import "Termsandcondition.h"
#import "NotificationVC.h"
#import "Managecompanyjoblist.h"
#import "Candidatejobs.h"
#import "CandidateJob.h"
#import "AppDelegate.h"

@interface SideMenu ()

@end

@implementation SideMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
 //@"About us",@"News",
   arrname =  [[NSMutableArray alloc]initWithObjects:@"DashBoard",@"My Profile",@"Find Ship Jobs",@"Find Shore Jobs",@"Companies",@"Institutes",@"Contact Us",@"Change Password",@"Terms and Condition",@"Notification", nil];
   // ,@"about_us",@"news"
//    UIImage *shoreimg = [UIImage imageNamed:@"ShoreJobResume.png"];
//    [shoreimg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    arrimg=[[NSMutableArray alloc]initWithObjects:@"dashboard.png",@"user",@"ship",@"ShoreJobResumeblack.png",@"company.png",@"institute.png",@"ContactUs.png",@"changepass.png",@"terms.png",@"Notifications.png", nil];
    
    arrcompanyname =  [[NSMutableArray alloc]initWithObjects:@"DashBoard",@"My Plan",@"Publish Ship Job Vacancies",@"Publish Shore Job Vacancies",@"All Candidates",@"Ship Job Applications Received",@"Shore Job Applications Received",@"Downloaded Resume",@"Change Password",@"My Account",@"Notification", nil];
    
    arrcompanyimg=[[NSMutableArray alloc]initWithObjects:@"dashboard.png",@"MyPlans.png",@"publishvacancy.png",@"publishvacancy.png",@"all-candidates.png",@"Applicationreceived.png",@"Applicationreceived.png",@"DownloadedResume.png",@"changepass.png",@"user",@"Notifications.png", nil];
    if ( IsIphone6Plus ) {
        _tableview.frame = CGRectMake(self.tableview.frame.origin.x, self.tableview.frame.origin.y, self.view.frame.size.width-60, self.tableview.frame.size.height);
        //self.tableview.frame = self.view.frame.size.width-228;
    }
    else{
    _tableview.frame = CGRectMake(self.tableview.frame.origin.x, self.tableview.frame.origin.y, self.view.frame.size.width-60, self.tableview.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
     [super viewWillAppear:YES];
    //image reolad
       type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
       _lblName.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
    //_lblCountry.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"Address"];
    NSString *   type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
   
    if ([type isEqualToString:@"Candidate"]) {
         profilepicname = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ImageURL"]];
        if ([profilepicname  isEqualToString:@"http://test.infinitydynamics.in/uploads/user_profile_image/"]) {
            
            _profileimage.image = [UIImage imageNamed: @"user-1"];
        }else{
            NSString *fullpath = [NSString stringWithFormat:@"%@",profilepicname];
            [_profileimage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",fullpath]] placeholderImage:[UIImage imageNamed:@"user-1"]];
            //   _Profileimage.image = [self getImageFromURL:fullpath];
        }
      
        _profileimage.layer.cornerRadius = _profileimage.frame.size.width/2;
          _profileimage.layer.masksToBounds = YES;
        
    }else{
          profilepicname = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LogoURL"]];
        if ([profilepicname  isEqualToString:@"http://clientsdemoarea.com/projects/mariexeltime/uploads/company_logo/"]) {
            
            _profileimage.image = [UIImage imageNamed: @"user-1"];
        }else{
            NSString *fullpath = [NSString stringWithFormat:@"%@",profilepicname];
            [_profileimage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",fullpath]] placeholderImage:[UIImage imageNamed:@"500x200"]];
            //   _Profileimage.image = [self getImageFromURL:fullpath];
        }
        _profileimage.layer.cornerRadius = _profileimage.frame.size.width/2;
        _profileimage.layer.masksToBounds = YES;
    }
    
    [self.tableview reloadData];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Homeviewcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Homeviewcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([type isEqualToString:@"Candidate"]) {
         return arrname.count;
    }
    else{
         return arrcompanyname.count;
    }
 
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(Homeviewcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([type isEqualToString:@"Candidate"]) {
    cell.LblName.text = [NSString stringWithFormat:@"%@",[arrname objectAtIndex:indexPath.section]];

    cell.imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrimg objectAtIndex:indexPath.section]]];
    }else{
        cell.LblName.text = [NSString stringWithFormat:@"%@",[arrcompanyname objectAtIndex:indexPath.section]];
        
        cell.imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",[arrcompanyimg objectAtIndex:indexPath.section]]];
        
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger row = indexPath.row;
    // Grab a handle to the reveal controller, as if you'd do with a navigtion controller via self.navigationController.
    SWRevealViewController *revealController = self.revealViewController;
      if ([type isEqualToString:@"Candidate"]) {
          UIViewController *newFrontController = nil;
          if (indexPath.section == 0 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }else  if (indexPath.section == 1 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 2 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"FindJob"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 3 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"shorejob"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 4 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"Companies"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 5 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"InstitutesVC"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 6 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"Aboutus"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 7 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"Changepassword"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 8 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"Termsandcondition"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 9 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
          [revealController pushFrontViewController:navigationController animated:YES];
      }
      else
      {
         //////company menu
          UIViewController *newFrontController = nil;
          if (indexPath.section == 0 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }else  if (indexPath.section == 1 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPlanView"];
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 2 ) {
            //  newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"Managecompanyjoblist"];
             Managecompanyjoblist *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"Managecompanyjoblist"];
              nvc.isfrom = @"shipjob";
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController = nvc;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 3 ) {
              Managecompanyjoblist *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"Managecompanyjoblist"];
              nvc.isfrom = @"shorejob";
               newFrontController = nvc;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 4 ) {
              NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
              [CountryDataSave setObject:@"NO" forKey:@"isHomeButtonFlag"];
              [CountryDataSave synchronize];
              
             // newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"CandidateJob"];
              //Candidatejobs *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"Candidatejobs"];
              CandidateJob *nvc =[self.storyboard instantiateViewControllerWithIdentifier:@"CandidateJob"];
              //    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
              nvc.total_shorejob_candidate = [NSString stringWithFormat:@"%@",appdelegate.totalshorejobResume];
              nvc.loginUserDataArray = (NSMutableArray *)appdelegate.loginUserData;
              nvc.HeaderTitleStr = [NSString stringWithFormat:@"All Candidates"];
              //[self.navigationController pushViewController:s animated:NO];
              newFrontController = nvc;
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 5 ) {
              Candidatejobs *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"Candidatejobs"];
              nvc.isfrom = @"shipapp";
               newFrontController = nvc;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 6 ) {
              Candidatejobs *nvc = [self.storyboard instantiateViewControllerWithIdentifier:@"Candidatejobs"];
              nvc.isfrom = @"shoreapp";
              nvc.isdashboard = @"1";
              newFrontController = nvc;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 7 ) {
             
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"resumedownloadedList"];
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 8 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"Changepassword"];
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 9 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"CompanyAccount"];
              //   critic.fromMyCriti = @"1";
              //   newFrontController = critic;
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          else  if (indexPath.section == 10 ) {
              newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
              newFrontController.hidesBottomBarWhenPushed = YES;
          }
          UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
          [revealController pushFrontViewController:navigationController animated:YES];
      }
}
- (IBAction)BtnLogout:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Logout Alert"
                                                      message:@"Do You Want To Logout?"
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:@"Cancel",nil];
    message.tag = 0;
    [message show];
}
- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 0)
    {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            [self resetDefaults];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"logged_in"];
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"EmailID"]);
            SWRevealViewController *revealController = self.revealViewController;
            
            UIViewController *newFrontController = nil;
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            newFrontController.hidesBottomBarWhenPushed = YES;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
            
            [revealController pushFrontViewController:navigationController animated:YES];
        }
        else
        {
            NSLog(@"Cancell button");
            //Do something else
        }
    }
    else{
        if (buttonIndex == 0) {
            
        }
    }
}
@end
