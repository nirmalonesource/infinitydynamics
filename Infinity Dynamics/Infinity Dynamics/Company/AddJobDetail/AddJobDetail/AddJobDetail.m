//
//  AddJobDetail.m
//  Infinity Dynamics
//
//  Created by My Mac on 26/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "AddJobDetail.h"
#import "Constants.h"
#import "WYPopoverController.h"
#import "HcdDateTimePickerView.h"
@interface AddJobDetail ()<WYPopoverControllerDelegate>
{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
    HcdDateTimePickerView * dateTimePickerView;
}

@end

@implementation AddJobDetail


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 550);
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        _lblship.hidden = YES;
        _shipvw.hidden = YES;
        _checkvw.hidden = YES;
        _ranktop.constant = 10;
        _lblrank.text =@"Shore Job Designation";
        _txtRank.placeholder = @"Select Designation";
    }
    if (_Edit == YES) {
        //_txtDescription.text = @"Enter Job Description";
        _txtDescription.textColor = [UIColor blackColor];
        _txtDescription.delegate = self;
        if ([_isfrom isEqualToString:@"shipjob"])
        {
            _txtshipType.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShipType"]];
            _txtRank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Rank"]];
        }
        else if ([_isfrom isEqualToString:@"shorejob"])
        {
            _txtRank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"DesignationName"]];
        }
      
        
        NSString * date = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"JobEndDate"]];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        
        
         _txtLeaveingdate.text = [NSString stringWithFormat:@"%@",JoiningDate];

        _txtDescription.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Description"]];
        shore = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"IsShore"]];
         fetured = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"IsFeatured"]];
         diplay = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"IsFront"]];
        
        if ([diplay isEqualToString:@"No"]) {
              [_btndistancejob setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        }
        else{
             [_btndistancejob setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        }
        if ([fetured isEqualToString:@"No"]) {
            [_btnfeaturejob setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        }
        else{
            [_btnfeaturejob setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        }
        if ([shore isEqualToString:@"No"]) {
            [_btnshorejob setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        }
        else{
            [_btnshorejob setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        }
        
        experiencerangeformid= [NSString stringWithFormat:@"%@",[_detail valueForKey:@"FromExperience"]];
      experiencerangetoid= [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ToExperience"]];
        
        
        if ([experiencerangeformid isEqualToString:@""]) {
            _txtExperiencerange.text = @"";
        }
        else{
            if (experiencerangeformid == [NSString stringWithFormat:@"1" ] ) {
              _txtExperiencerange.text = [NSString stringWithFormat:@"On Promotion"]  ;
            }
            else if ( experiencerangeformid== [NSString stringWithFormat:@"6" ]){
               
                _txtExperiencerange.text = [NSString stringWithFormat:@"6 Months"];
            }
            else if (experiencerangeformid== [NSString stringWithFormat:@"12" ]){
                _txtExperiencerange.text = [NSString stringWithFormat:@"12 Months"];
            }
            else if ( experiencerangeformid == [NSString stringWithFormat:@"18" ]){
               
                _txtExperiencerange.text = [NSString stringWithFormat:@"18 Months"];
            }
            else if ( experiencerangeformid == [NSString stringWithFormat:@"24" ]){
               
                _txtExperiencerange.text = [NSString stringWithFormat:@"24 Months"];
            }
            else if (experiencerangeformid== [NSString stringWithFormat:@"36" ]){
               _txtExperiencerange.text = [NSString stringWithFormat:@"36 Months"] ;
            }
        }
        if ([experiencerangetoid  isEqual: @""]) {
          _txtexperienceto.text = [NSString stringWithFormat:@""]  ;
        }
        else{
            if (experiencerangetoid== [NSString stringWithFormat:@"6" ]){
                _txtexperienceto.text = [NSString stringWithFormat:@"6 Months"];
            }
            else if ( experiencerangetoid== [NSString stringWithFormat:@"12" ]){
                _txtexperienceto.text = [NSString stringWithFormat:@"12 Months"];
            }
            else if ( experiencerangetoid== [NSString stringWithFormat:@"18" ]){
                _txtexperienceto.text = [NSString stringWithFormat:@"18 Months"];
            }
            else if ( experiencerangetoid == [NSString stringWithFormat:@"24" ]){
                _txtexperienceto.text = [NSString stringWithFormat:@"24 Months"];
            }
            else if ( experiencerangetoid == [NSString stringWithFormat:@"24+" ]){
                _txtexperienceto.text = [NSString stringWithFormat:@"24+ Months"];
                
            }
            
        }
        _txtExperiencerange.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"FromExperience"]];
        _txtexperienceto.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ToExperience"]];

       
    }
    else
    {
        _txtDescription.text = @"Enter Job Description";
        _txtDescription.textColor = [UIColor lightGrayColor];
        _txtDescription.delegate = self;
    }
   
}
- (BOOL) textViewShouldBeginEditing:(UITextView *)textView {
    _txtDescription.text = @"";
    _txtDescription.textColor = [UIColor blackColor];
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView {
    
    if(_txtDescription.text.length == 0) {
        _txtDescription.textColor = [UIColor lightGrayColor];
        _txtDescription.text = @"Enter Job Description";
        [_txtDescription resignFirstResponder];
    }
}

- (void)textViewDidEndEditing:(UITextView *) textView {
    
    if(_txtDescription.text.length == 0) {
        _txtDescription.textColor = [UIColor lightGrayColor];
        _txtDescription.text = @"Enter Job Description";
        [_txtDescription resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnLeavingDate:(id)sender {
    __block AddJobDetail *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger year = [components year];
    [dateTimePickerView setMinYear:1900];
    [dateTimePickerView setMaxYear:year];
    dateTimePickerView.maxYear = year;
    dateTimePickerView.topViewColor = [UIColor darkGrayColor];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];
    
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        //        dateofb=datetimeStr;
        
        //--------DATE CONVERT------------------
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:datetimeStr];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString *finaldt=[formatedt stringFromDate:fulldate];
        
        weakSelf.txtLeaveingdate.text = finaldt;
    };
    
    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }
    
}

- (IBAction)btnShiptype:(id)sender {
      if (_Edit == YES)
      {
          
      }
    else
    {
        isshiptype = YES;
        isenginetype = NO;
        isrank = NO;
        [_txtshipType setTag:7];
        currentTextField = _txtshipType;
        // amountype = YES;
        [self showPopover:sender];
    }
}

- (IBAction)btnRank:(id)sender {
    
    if (_Edit == YES)
    {
        
    }
    else
    {
        isshiptype = NO;
        isenginetype = NO;
        isrank = YES;
        [_txtRank setTag:0];
        currentTextField = _txtRank;
        // amountype = YES;
        [self showPopover:sender];
    }
  
}

- (IBAction)btnUpdateexp:(id)sender
{
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        if ([_txtshipType.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Select Ship Type");
        }
        else if ([_txtRank.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Select Rank");
        }
        else if ([_txtLeaveingdate.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Expiry Date");
        }
        else if ([_txtExperiencerange.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Experience From");
        }
        else if ([_txtexperienceto.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Experience Upto");
        }
        else if ([_txtDescription.text isEqualToString:@"Enter Job Description"])
        {
            showAlert(AlertTitle, @"Please Enter Job Description");
        }
        
        
       
        else
        {
            if (_Edit == YES) {
                [APP_DELEGATE showLoadingView:@""];
                [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
            }
            else{
                 if (isfetured == false && isshore == false && isdiplay == false)
                {
                    showAlert(AlertTitle, @"Please Select any one Job Section");
                }
                else
                {
                    [APP_DELEGATE showLoadingView:@""];
                    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
                }
               
                
            }
        }

    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
//        if ([_txtshipType.text isEqualToString:@""])
//        {
//            showAlert(AlertTitle, @"Please Select Ship Type");
//        }
         if ([_txtRank.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Select Designation");
        }
        else if ([_txtLeaveingdate.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Expiry Date");
        }
        else if ([_txtExperiencerange.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Experience From");
        }
        else if ([_txtexperienceto.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Experience Upto");
        }
        else if ([_txtDescription.text isEqualToString:@"Enter Job Description"])
        {
            showAlert(AlertTitle, @"Please Enter Job Description");
        }
        
        
//        else if (isfetured == false && isshore == false && isdiplay == false)
//        {
//            showAlert(AlertTitle, @"Please Select any one Job Type");
//        }
        else
        {
            if (_Edit == YES) {
                [APP_DELEGATE showLoadingView:@""];
                [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
            }
            else{
                [APP_DELEGATE showLoadingView:@""];
                [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
                
            }
        }

    }
}


-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
   
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        [self performSelector:@selector(Shiptype) withObject:nil afterDelay:0.1];
        [self performSelector:@selector(enginetype) withObject:nil afterDelay:0.1];
        [self performSelector:@selector(Ranklist) withObject:nil afterDelay:0.1];
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        [self performSelector:@selector(enginetype) withObject:nil afterDelay:0.1];
        [self performSelector:@selector(Designation) withObject:nil afterDelay:0.1];
    }

    
    
}
-(void)enginetype
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpenginetype  = [[HttpWrapper alloc] init];
        httpenginetype.delegate=self;
        httpenginetype.getbool=NO;
        
        [httpenginetype requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_engine_type",MainUrl] param:@""];
    });
    
    
}
-(void)Shiptype
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpshiptype  = [[HttpWrapper alloc] init];
        httpshiptype.delegate=self;
        httpshiptype.getbool=NO;
        
        [httpshiptype requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_ship_type",MainUrl] param:@""];
    });
    
    
}
-(void)Ranklist
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprank  = [[HttpWrapper alloc] init];
        httprank.delegate=self;
        httprank.getbool=NO;
        
        [httprank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:@""];
    });
    
    
}
-(void)Designation
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpdesignation  = [[HttpWrapper alloc] init];
        httpdesignation.delegate=self;
        httpdesignation.getbool=NO;
        
        [httpdesignation requestWithMethod:@"GET" url:[NSString stringWithFormat:@"%@get_designation_list",MainUrl] param:@""];
    });
    
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpdesignation && httpdesignation != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrdesignation = [dicsResponse valueForKey:@"data"];
        // NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    if(wrapper == httpshiptype && httpshiptype != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrShiptype = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    else if(wrapper == httprank && httprank != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrranklist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
    else if(wrapper == httpenginetype && httpenginetype != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrEnginetype = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
    else if(wrapper == httpaddData && httpaddData != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrupdate = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        else
        {
              NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
             showAlert(AlertTitle,message);
        }
    }
    else if(wrapper == httpupdatedata && httpupdatedata != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrEdit = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            [self.navigationController popViewControllerAnimated:NO];
            
        }
    }
    
    
    
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        if (isshiptype == YES) {
            settingsViewController.marrshiptype = marrShiptype;
            settingsViewController.index = currentTextField.tag;
        }
        else if (isrank == YES){
            if ([_isfrom isEqualToString:@"shipjob"])
            {
                settingsViewController.marrData = marrranklist;
                settingsViewController.isfrom = _isfrom;
                settingsViewController.index = currentTextField.tag;
            }
            else if ([_isfrom isEqualToString:@"shorejob"])
            {
                settingsViewController.marrData = marrdesignation;
                settingsViewController.isfrom = _isfrom;
                settingsViewController.index = currentTextField.tag;
            }
           
        }
        
        else if (isenginetype == YES){
            settingsViewController.marrenginetype = marrEnginetype;
            settingsViewController.index = currentTextField.tag;
        }
        else{
            settingsViewController.index = currentTextField.tag;
            settingsViewController.isfrom = @"AddVacancy";
            // settingsViewController.marrnationality = marrnationality;
        }
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            //            if (amountype == YES) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
            //            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
            //            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    if ([_txtExperiencerange.text isEqualToString:@""]) {
        experiencerangeformid = @"";
    }
    else{
        if ([_txtExperiencerange.text isEqualToString:@"On Promotion"]) {
            experiencerangeformid= [NSString stringWithFormat:@"1" ] ;
        }
        else if ([_txtExperiencerange.text isEqualToString:@"6 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"6" ];
        }
        else if ([_txtExperiencerange.text isEqualToString:@"12 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"12" ];
        }
        else if ([_txtExperiencerange.text isEqualToString:@"18 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"18" ];
        }
        else if ([_txtExperiencerange.text isEqualToString:@"24 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"24" ];
        }
        else if ([_txtExperiencerange.text isEqualToString:@"36 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"36" ];
        }
    }
    if ([_txtexperienceto.text isEqualToString:@""]) {
        experiencerangetoid = @"";
    }
    else{
        if ([_txtexperienceto.text isEqualToString:@"6 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"6" ];
        }
        else if ([_txtexperienceto.text isEqualToString:@"12 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"12" ];
        }
        else if ([_txtexperienceto.text isEqualToString:@"18 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"18" ];
        }
        else if ([_txtexperienceto.text isEqualToString:@"24 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"24" ];
        }
        else if ([_txtexperienceto.text isEqualToString:@"24+ Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"24+" ];
        }
        
    }
    
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnExperiencerangeFrom:(id)sender {
    isshiptype = NO;
    isrank = NO;
       isenginetype = NO;
    [_txtExperiencerange setTag:13];
    currentTextField = _txtExperiencerange;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnExperiencerangeTo:(id)sender {
    isshiptype = NO;
    isrank = NO;
    isenginetype = NO;
    [_txtexperienceto setTag:14];
    currentTextField = _txtexperienceto;
    // amountype = YES;
    [self showPopover:sender];
}
-(void)CallMyMethod
{
//    job_id
//    company_id
//    rank_id
//    ship_id
//    job_title
//    experience
//    job_start_date
//    job_end_date
//    description
//    is_featured ('Yes', 'No')
//    is_shore ('Yes', 'No')
//    is_front ('Yes', 'No')
//    update = 1
    
    
   
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        NSString *find = _txtshipType.text;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
        NSArray *results = [marrShiptype filteredArrayUsingPredicate:predicate];
        shiptypeid= [NSString stringWithFormat:@"%@", [results  valueForKey:@"ShipID" ]] ;
        
        NSString * Uid1 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
        
        [parameters setValue:experiencerangeformid forKey:@"from_experience"];
        [parameters setValue:experiencerangetoid forKey:@"to_experience"];
        NSString *strdescription = _txtDescription.text;
        
//        NSDictionary *documentAttributes = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType};
//        
//        NSData *htmlData = [strdescription dataFromRange:NSMakeRange(0, strdescription.length) documentAttributes:documentAttributes error:NULL];
//        NSString *str = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
//        NSLog(@"%@", str);
      //  NSString *htmltxt = [self parseText:_txtDescription.text];
        [parameters setValue:strdescription forKey:@"description"];
        NSString * date = [NSString stringWithFormat:@"%@",_txtLeaveingdate.text];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        [parameters setValue:JoiningDate forKey:@"job_end_date"];
        [parameters setValue:[_detail valueForKey:@"JobStartDate"] forKey:@"job_start_date"];
        
        [parameters setValue:fetured forKey:@"is_featured"];
        [parameters setValue:shore forKey:@"is_shore"];
        [parameters setValue:diplay forKey:@"is_front"];
        
        
        
        
        [parameters setValue:trimmed forKey:@"ship_id"];
        [parameters setValue:[_detail valueForKey:@"JobID"] forKey:@"job_id"];
        
        [parameters setValue:@"1" forKey:@"update"];
        NSString *find2 = _txtRank.text;
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find2];
        NSArray *results2= [marrranklist filteredArrayUsingPredicate:predicate2];
        rankid= [NSString stringWithFormat:@"%@", [results2  valueForKey:@"RankID" ]] ;
        
        
        NSString * Uid12 =   [[rankid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed2 = [Uid12 stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
         [parameters setValue:trimmed2 forKey:@"rank_id"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpupdatedata  = [[HttpWrapper alloc] init];
            httpupdatedata.delegate=self;
            httpupdatedata.getbool=NO;
            
            [httpupdatedata requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_job",MainUrl] param:[parameters copy]];
        });
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
//        NSString *find = _txtshipType.text;
//
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
//        NSArray *results = [marrShiptype filteredArrayUsingPredicate:predicate];
//        shiptypeid= [NSString stringWithFormat:@"%@", [results  valueForKey:@"ShipID" ]] ;
//
//        NSString * Uid1 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
//        NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
//                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
        
        [parameters setValue:experiencerangeformid forKey:@"from_experience"];
        [parameters setValue:experiencerangetoid forKey:@"to_experience"];
        [parameters setValue:_txtDescription.text forKey:@"description"];
        NSString * date = [NSString stringWithFormat:@"%@",_txtLeaveingdate.text];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        [parameters setValue:JoiningDate forKey:@"job_end_date"];
       // [parameters setValue:[_detail valueForKey:@"JobStartDate"] forKey:@"job_start_date"];
        
//        [parameters setValue:fetured forKey:@"is_featured"];
//        [parameters setValue:shore forKey:@"is_shore"];
//        [parameters setValue:diplay forKey:@"is_front"];
        
        
        
        
      //  [parameters setValue:trimmed forKey:@"ship_id"];
        [parameters setValue:[_detail valueForKey:@"JobID"] forKey:@"job_id"];
        
        [parameters setValue:@"1" forKey:@"update"];
        NSString *find2 = _txtRank.text;
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find2];
        NSArray *results2= [marrdesignation filteredArrayUsingPredicate:predicate2];
        rankid= [NSString stringWithFormat:@"%@", [results2  valueForKey:@"DesignationID" ]] ;
        
        
        NSString * Uid12 =   [[rankid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed2 = [Uid12 stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [parameters setValue:trimmed2 forKey:@"designation_id"];
         [parameters setValue:[_detail valueForKey:@"ShoreJobID"] forKey:@"job_id"];
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpupdatedata  = [[HttpWrapper alloc] init];
            httpupdatedata.delegate=self;
            httpupdatedata.getbool=NO;
            
            [httpupdatedata requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_shorejob",MainUrl] param:[parameters copy]];
        });
    }
   
    
    
}
-(void)parseText:(NSString *)appendedArticleText
{
   // appendedArticleText = @"";
    NSString *articleText = _txtDescription.text;
    NSString *temp = nil;
    NSScanner *theScanner = [NSScanner scannerWithString:articleText];
    int count = 0;
    
    while (![theScanner isAtEnd]) {
        count = count +1;
        NSString *htmlTag = [NSString stringWithFormat:@"<p class=\"pt\"data-seq=\"%d\">",count];
        [theScanner scanUpToCharactersFromSet:[NSCharacterSet newlineCharacterSet] intoString:&temp];
        
        NSString *taggedArticleText = [htmlTag stringByAppendingString:temp];
        NSString *formattedText = [taggedArticleText stringByAppendingString:@"</p>"];
        appendedArticleText = [appendedArticleText stringByAppendingString:formattedText];
    }
    
    NSLog(@"%@",appendedArticleText);
    
}
-(void)CallMyMethod1
{
    //    candidate_id
    //    company_name
    //    ship_name
    //    joining_date (yyyy-mm-dd)
    //    leaving_date (yyyy-mm-dd)
    //    ship_type
    //    engine_type
    //    rank
    //    grt
    //    total_sea_duration
    //    update = 0
    
   
    
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        NSString *find = _txtshipType.text;
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
        NSArray *results = [marrShiptype filteredArrayUsingPredicate:predicate];
        shiptypeid= [NSString stringWithFormat:@"%@", [results  valueForKey:@"ShipID" ]] ;
        
        
        NSString * Uid1 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSString *find2 = _txtRank.text;
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find2];
        NSArray *results2= [marrranklist filteredArrayUsingPredicate:predicate2];
        rankid= [NSString stringWithFormat:@"%@", [results2  valueForKey:@"RankID" ]] ;
        
        
        NSString * Uid12 =   [[rankid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed2 = [Uid12 stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
        
        [parameters setValue:_txtExperiencerange.text forKey:@"from_experience"];
        [parameters setValue:_txtexperienceto.text forKey:@"to_experience"];
        [parameters setValue:_txtDescription.text forKey:@"description"];
        NSString * date = [NSString stringWithFormat:@"%@",_txtLeaveingdate.text];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        [parameters setValue:JoiningDate forKey:@"job_end_date"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *string = [formatter stringFromDate:[NSDate date]];
        [parameters setValue:string forKey:@"job_start_date"];
        if ([fetured length] == 0 ) {
            fetured = [NSString stringWithFormat:@"No"];
        }
        if ([shore length] == 0 ) {
            shore = [NSString stringWithFormat:@"No"];
        }
        if ([diplay length] == 0 ) {
            diplay = [NSString stringWithFormat:@"No"];
        }
        [parameters setValue:fetured forKey:@"is_featured"];
        [parameters setValue:shore forKey:@"is_shore"];
        [parameters setValue:diplay forKey:@"is_front"];
        [parameters setValue:trimmed2 forKey:@"rank_id"];
        [parameters setValue:trimmed forKey:@"ship_id"];
        
        [parameters setValue:@"0" forKey:@"update"];
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpaddData  = [[HttpWrapper alloc] init];
            httpaddData.delegate=self;
            httpaddData.getbool=NO;
            
            [httpaddData requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_job",MainUrl] param:[parameters copy]];
        });
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        
        NSString *find2 = _txtRank.text;
        
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find2];
        NSArray *results2= [marrdesignation filteredArrayUsingPredicate:predicate2];
        rankid= [NSString stringWithFormat:@"%@", [results2  valueForKey:@"DesignationID" ]] ;
        
        
        NSString * Uid12 =   [[rankid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed2 = [Uid12 stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
        
        [parameters setValue:_txtExperiencerange.text forKey:@"from_experience"];
        [parameters setValue:_txtexperienceto.text forKey:@"to_experience"];
        [parameters setValue:_txtDescription.text forKey:@"description"];
        NSString * date = [NSString stringWithFormat:@"%@",_txtLeaveingdate.text];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        [parameters setValue:JoiningDate forKey:@"job_end_date"];
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.dateFormat = @"yyyy-MM-dd";
//        NSString *string = [formatter stringFromDate:[NSDate date]];
       // [parameters setValue:string forKey:@"job_start_date"];
       
        [parameters setValue:trimmed2 forKey:@"designation_id"];
        
        [parameters setValue:@"0" forKey:@"update"];
        [parameters setValue:[_detail valueForKey:@"ShoreJobID"] forKey:@"job_id"];
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpaddData  = [[HttpWrapper alloc] init];
            httpaddData.delegate=self;
            httpaddData.getbool=NO;
            
            [httpaddData requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_shorejob",MainUrl] param:[parameters copy]];
        });
    }
    
    
}

- (IBAction)BtnFeaturejob:(id)sender {
    if (isfetured) {
        [_btnfeaturejob setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        isfetured = false;
        fetured = [NSString stringWithFormat:@"No"];
        NSLog(@"UNCHEK");
    }else{
        [_btnfeaturejob setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        isfetured= true;
        fetured = [NSString stringWithFormat:@"Yes"];
        NSLog(@"CHEK");
    }
    
}
- (IBAction)btnShoreJob:(id)sender {
    if (isshore) {
        [_btnshorejob setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        isshore = false;
        shore = [NSString stringWithFormat:@"No"];
        NSLog(@"UNCHEK");
    }else{
        [_btnshorejob setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        isshore= true;
        shore = [NSString stringWithFormat:@"Yes"];
        NSLog(@"CHEK");
    }
}

- (IBAction)btnDisplayJob:(id)sender {
    if (isdiplay) {
        [_btndistancejob setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        isdiplay = false;
        diplay = [NSString stringWithFormat:@"No"];
        NSLog(@"UNCHEK");
    }else{
        [_btndistancejob setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        isdiplay= true;
        diplay = [NSString stringWithFormat:@"Yes"];
        NSLog(@"CHEK");
    }
}
@end
