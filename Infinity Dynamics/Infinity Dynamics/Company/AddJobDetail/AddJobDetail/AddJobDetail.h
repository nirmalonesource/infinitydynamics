//
//  AddJobDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 26/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
@interface AddJobDetail : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate>{
    HttpWrapper *httprank,*httpshiptype,*httpenginetype,*httpupdatedata,*httpaddData,*httpdesignation;
    NSMutableArray *marrranklist;
    NSMutableArray *marrShiptype;
    NSMutableArray *marrEnginetype;
    NSMutableArray *marrupdate;
    NSMutableArray *marrEdit;
    NSMutableArray *marrdesignation;
    NSString * shiptypeid;
    NSString * rankid;
    NSString * engineid;
    BOOL isenginetype;
    BOOL isrank;
    BOOL isshiptype;
    UITextField *currentTextField;
    
    NSString * experiencerangeformid;
    NSString * experiencerangetoid;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    
    /////fetured
      BOOL isfetured;
     NSString * fetured;
    
    /////shore
    BOOL isshore;
    NSString * shore;
    
    /////diplay
    BOOL isdiplay;
    NSString * diplay;
    
}

- (IBAction)btnBack:(id)sender;
- (IBAction)btnExperiencerangeFrom:(id)sender;
- (IBAction)btnExperiencerangeTo:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtexperienceto;

@property (strong, nonatomic) IBOutlet UITextField *txtLeaveingdate;
- (IBAction)btnLeavingDate:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtshipType;
- (IBAction)btnShiptype:(id)sender;
- (IBAction)btnRank:(id)sender;
- (IBAction)btnUpdateexp:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtRank;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UITextField *txtExperiencerange;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;

//@property (strong, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UITextView *txtDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnfeaturejob;
- (IBAction)BtnFeaturejob:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnshorejob;
- (IBAction)btnShoreJob:(id)sender;
- (IBAction)btnDisplayJob:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btndistancejob;
@property (strong, nonatomic) IBOutlet UIView *checkvw;
@property (strong, nonatomic) NSString *isfrom;
@property (strong, nonatomic) IBOutlet UILabel *lblship;
@property (strong, nonatomic) IBOutlet UIView *shipvw;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ranktop;
@property (strong, nonatomic) IBOutlet UILabel *lblrank;




@end
