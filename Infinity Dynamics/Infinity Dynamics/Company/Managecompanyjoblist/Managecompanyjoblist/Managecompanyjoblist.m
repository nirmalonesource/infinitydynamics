//
//  Managecompanyjoblist.m
//  Infinity Dynamics
//
//  Created by My Mac on 25/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Managecompanyjoblist.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "Constants.h"
#import "Managecompanyjoblistcell.h"
#import "AddJobDetail.h"
#import "ViewCandidatelist.h"
#import "Candidatejobs.h"
@interface Managecompanyjoblist ()
{
    NSIndexPath *myindexPath;
}
@end

@implementation Managecompanyjoblist

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [self customSetup];
     self.navigationController.navigationBarHidden=YES;
     self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    NSLog(@"ISFROM:%@",_isfrom);
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
   
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Managecompanyjoblistcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Managecompanyjoblistcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return marrdetaillist.count;
    
    
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_isfrom isEqualToString:@"shipjob"])
    {
         return 185;
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
         return 160;
    }
   else
   {
       return 0;
   }
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(Managecompanyjoblistcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        cell.lblshipetype.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"ShipType"]];
        cell.lblrank.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"Rank"]];
        cell.lblshipetype.hidden = NO;
        cell.lblshiptyp.hidden = NO;

    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        cell.lblshipetype.hidden = YES;
        cell.lblshiptyp.hidden = YES;
         cell.btnmatchedcandidates.hidden = YES;
        cell.lblrnk.text = @"Designation";
        cell.lblrank.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"DesignationName"]];
    }
    cell.lblCompany.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section]valueForKey:@"ToExperience"] ];
    
     cell.lbljobtitle.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"FromExperience"]];
     cell.lblapplycandidate.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"AppliedCandidates"]];
    
    NSString * date = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"JobEndDate"]];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    NSString * JoiningDate = [formatedt stringFromDate:fulldate];
 
     cell.lblenddate.text = [NSString stringWithFormat:@"%@",JoiningDate];
    
    [cell.btnviewCandidate setTag:indexPath.section];
    [ cell.btnviewCandidate addTarget:self action:@selector(viewcandidate:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnmatchedcandidates setTag:indexPath.section];
    [ cell.btnmatchedcandidates addTarget:self action:@selector(viewmatchedcandidate:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnedit setTag:indexPath.section];
    [ cell.btnedit addTarget:self action:@selector(switchpress:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btndelete setTag:indexPath.section];
    [ cell.btndelete addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
}
- (IBAction)viewcandidate:(UIButton *)sender {
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        NSLog(@"here :: %@",[marrdetaillist objectAtIndex:indexPath.row]  );
        ViewCandidatelist *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewCandidatelist"];
        
        //NSLog(@"[marrdetaillist objectAtIndex:indexPath.row] = %@",[marrdetaillist objectAtIndex:indexPath.row]);
        
        s.PublishVacanciesArray = (NSMutableArray *)[marrdetaillist objectAtIndex:indexPath.row];
        s.jobid = [[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"JobID"];
        s.isfrom = _isfrom;
        [self.navigationController pushViewController:s animated:NO];
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        Candidatejobs *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatejobs"];
         s.isfrom = @"shoreapp";
        s.shorejobid = [[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"ShoreJobID"];
        [self.navigationController pushViewController:s animated:NO];
    }
}
- (IBAction)viewmatchedcandidate:(UIButton *)sender {
    myindexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    NSLog(@"here :: %@",[marrdetaillist objectAtIndex:myindexPath.row]  );
    [APP_DELEGATE showLoadingView:@""];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:@"" forKey:@"job_id"];
    [parameters setValue:@"" forKey:@"from_age"];
    [parameters setValue:@"" forKey:@"to_age"];
    [parameters setValue:@"" forKey:@"to_experience"];
    [parameters setValue:@"1" forKey:@"days"];
    [parameters setValue:@"" forKey:@"company_id"];
    [parameters setValue:@"" forKey:@"country_id"];
    
    NSString *experiencefrom = [[marrdetaillist objectAtIndex:myindexPath.row] valueForKey:@"FromExperience"];
    
    if ([experiencefrom isEqualToString:@"On Promotion"]) {
        experiencefrom= [NSString stringWithFormat:@"1" ] ;
    }
    else if ([experiencefrom isEqualToString:@"6 Months"]){
        experiencefrom= [NSString stringWithFormat:@"6" ];
    }
    else if ([experiencefrom isEqualToString:@"12 Months"]){
        experiencefrom= [NSString stringWithFormat:@"12" ];
    }
    else if ([experiencefrom isEqualToString:@"18 Months"]){
        experiencefrom= [NSString stringWithFormat:@"18" ];
    }
    else if ([experiencefrom isEqualToString:@"24 Months"]){
        experiencefrom= [NSString stringWithFormat:@"24" ];
    }
    else if ([experiencefrom isEqualToString:@"36 Months"]){
        experiencefrom= [NSString stringWithFormat:@"36" ];
    }

    [parameters setValue:[[marrdetaillist objectAtIndex:myindexPath.row] valueForKey:@"RankID"] forKey:@"rank_id"];
    [parameters setValue:[[marrdetaillist objectAtIndex:myindexPath.row] valueForKey:@"ShipID"] forKey:@"ship_id"];
    [parameters setValue:experiencefrom forKey:@"from_experience"];
    [parameters setValue:@"1" forKey:@"IsMatch"];
    
    NSLog(@"parameters = %@",parameters);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcandidatelist  = [[HttpWrapper alloc] init];
        httpcandidatelist.delegate=self;
        httpcandidatelist.getbool=NO;
        
        [httpcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_list",MainUrl] param:[parameters copy]];
    });
//    ViewCandidatelist *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewCandidatelist"];
//
//    //NSLog(@"[marrdetaillist objectAtIndex:indexPath.row] = %@",[marrdetaillist objectAtIndex:indexPath.row]);
//
//    s.PublishVacanciesArray = (NSMutableArray *)[marrdetaillist objectAtIndex:indexPath.row];
//    s.jobid = [[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"JobID"];
//
//    [self.navigationController pushViewController:s animated:NO];
  //  [self CallMyMethod2];
}
- (IBAction)switchpress:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    NSLog(@"here :: %@",[marrdetaillist objectAtIndex:indexPath.row]  );
    AddJobDetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"AddJobDetail"];
    s.detail = [marrdetaillist objectAtIndex:indexPath.row] ;
    s.isfrom = _isfrom;
    s.Edit = YES;
    [self.navigationController pushViewController:s animated:NO];
    
    
}
- (IBAction)buttonPressed:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag]inSection:0];
    NSLog(@"here :: %@",[[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"SeamanDetailID"] );
    //DesignationID
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        seaman_id = [NSString stringWithFormat:@"%@",[[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"JobID"] ];
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        seaman_id = [NSString stringWithFormat:@"%@",[[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"DesignationID"] ];
    }
   
    [self performSelector:@selector(delete) withObject:nil afterDelay:0.1];
}
-(void)delete
{
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@""
                                 message:@"Want to Delete This Record?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    if ([_isfrom isEqualToString:@"shipjob"])
                                    {
                                        [APP_DELEGATE showLoadingView:@""];
                                        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
                                        
                                        [parameters setValue:seaman_id forKey:@"job_id"];
                                        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                                            httpdelete  = [[HttpWrapper alloc] init];
                                            httpdelete.delegate=self;
                                            httpdelete.getbool=NO;
                                            
                                            [httpdelete requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@delete_job",MainUrl] param:[parameters copy]];
                                        });
                                    }
                                    else if ([_isfrom isEqualToString:@"shorejob"])
                                    {
                                        [APP_DELEGATE showLoadingView:@""];
                                        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
                                        
                                        [parameters setValue:seaman_id forKey:@"shore_job_id"];
                                        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                                            httpdelete  = [[HttpWrapper alloc] init];
                                            httpdelete.delegate=self;
                                            httpdelete.getbool=NO;
                                            
                                            [httpdelete requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@delete_shorejob",MainUrl] param:[parameters copy]];
                                        });
                                    }
                                  
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"CANCEL"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
   
    
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    if ([_isfrom isEqualToString:@"shipjob"])
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httdetailist  = [[HttpWrapper alloc] init];
            httdetailist.delegate=self;
            httdetailist.getbool=NO;
            
            [httdetailist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_job_list",MainUrl] param:[parameters copy]];
        });
    }
    else if ([_isfrom isEqualToString:@"shorejob"])
    {
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httdetailist  = [[HttpWrapper alloc] init];
            httdetailist.delegate=self;
            httdetailist.getbool=NO;
            
            [httdetailist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_shorejob_list",MainUrl] param:[parameters copy]];
        });
    }
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httdetailist && httdetailist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrdetaillist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
            [self.tableview reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
        
    }
    if(wrapper == httpdelete && httpdelete != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrdetaillist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
        }
        else{
            
              [APP_DELEGATE hideLoadingView];
        }
    }
    if(wrapper == httpcandidatelist && httpcandidatelist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcandidatelist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrcandidatelist);
            [APP_DELEGATE hideLoadingView];
            ViewCandidatelist *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewCandidatelist"];
            if (marrcandidatelist.count>0)
            {
                s.PublishVacanciesArray = [[NSMutableArray alloc] init];
               // [s.PublishVacanciesArray insertObject:(NSMutableArray *)[marrcandidatelist objectAtIndex:myindexPath.row] atIndex:0];
                 s.PublishVacanciesArray = marrcandidatelist;
                //= (NSMutableArray *)[marrcandidatelist objectAtIndex:myindexPath.row];
                //s.jobid = [[marrcandidatelist objectAtIndex:myindexPath.row] valueForKey:@"JobID"];
            }
            s.isviewmatchedjob = YES;;
            [self.navigationController pushViewController:s animated:NO];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
    }
    
  
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}
- (IBAction)addjob:(id)sender {
    AddJobDetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"AddJobDetail"];
    s.isfrom = _isfrom;
    [self.navigationController pushViewController:s animated:NO];
}
@end
