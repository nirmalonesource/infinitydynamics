//
//  Locationcell.m
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import "Managecompanyjoblistcell.h"
#import "UIColor+CL.h"
#import "UIImageView+Haneke.h"
@implementation Managecompanyjoblistcell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       
    }
    return self;
}
-(void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];



}
- (void)prepareForReuse
{
    [self.imageView hnk_cancelSetImage];
    self.imageView.image = nil;
}

@end
