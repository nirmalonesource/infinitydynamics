//
//  CandidateJob.m
//  Infinity Dynamics
//
//  Created by My Mac on 26/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "CandidateJob.h"
#import "myModel.h"
#import "Constants.h"
#import "CandidateJobcell.h"
#import "UIImageView+Haneke.h"
#import "JobDetails.h"
#import "SWRevealViewController.h"
#import "WYPopoverController.h"
#import "Candidatedetail.h"
#import "CompanyAccount.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "AppDelegate.h"
#import "DropDownListView.h"

//#import <UIActivityIndicator-for-SDWebImage/UIImageView+UIActivityIndicatorForSDWebImage.h>

/*
 - (void)setImageWithURL:(NSURL *)url usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
 - (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
 - (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
 - (void)setImageWithURL:(NSURL *)url completed:(SDWebImageCompletionBlock)completedBlock usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
 - (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder completed:(SDWebImageCompletionBlock)completedBlock usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
 - (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options completed:(SDWebImageCompletionBlock)completedBlock usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
 - (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options progress:(SDWebImageDownloaderProgressBlock)progressBlock completed:(SDWebImageCompletionBlock)completedBlock usingActivityIndicatorStyle:(UIActivityIndicatorViewStyle)activityStyle;
*/

@interface CandidateJob ()
<WYPopoverControllerDelegate,kDropDownListViewDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
    UILabel *count;
    DropDownListView * Dropobj;

}
@end

@implementation CandidateJob

@synthesize  Search_View,filterview,total_shorejob_candidate,loginUserDataArray,HeaderTitleStr,isCandidate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _txtavailablefrom.text =  @"All Available";
//    NSString * total_shorejob_candidate1 = [NSString stringWithFormat:@"%@",total_shorejob_candidate];
//    NSString * HeaderTitleStr1 = [NSString stringWithFormat:@"%@",HeaderTitleStr];
//    NSString * isShoreJobResumeFlag1 = [NSString stringWithFormat:@"%@",_isShoreJobResumeFlag];
//    NSString * ShoreJobResumeFlag1 = [NSString stringWithFormat:@"%@",_ShoreJobResumeFlag];
//    NSMutableArray * LoginArray = (NSMutableArray *)loginUserDataArray;
    
    
    NSLog(@"isShoreJobResumeFlag = %@",total_shorejob_candidate);
    NSLog(@"loginUserDataArray = %@", loginUserDataArray);
    NSLog(@"HeaderTitleStr = %@",HeaderTitleStr);
    NSLog(@"isShoreJobResumeFlag = %@",_isShoreJobResumeFlag);
    NSLog(@"ShoreJobResumeFlag = %@",_ShoreJobResumeFlag);
    
    [_HeaderBTNChangeTitle setTitle:HeaderTitleStr forState:UIControlStateNormal];
    
    self.navigationController.navigationBarHidden=YES;
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    Search_View.hidden = NO;
    filterview.hidden = YES;
  UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(20, 0, Search_View.frame.size.width-70, Search_View.frame.size.height+5)];    searchBar.autoresizingMask =0;
    searchBar.delegate = self;
    searchBar.placeholder = @"Ship Type";
    searchBar.showsScopeBar = YES;
    
    [searchBar setBackgroundColor:[UIColor clearColor]];
    searchBar.layer.borderWidth = 1.0f;
    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
    [searchBar setBarTintColor:[UIColor whiteColor]]; //this is what you want
    [searchBar setShowsCancelButton:YES animated:YES];
    
     UIButton *Filter = [[UIButton alloc] initWithFrame:CGRectMake(Search_View.frame.size.width-38, 15, 25, 25)];
    [Filter setImage:[UIImage imageNamed:@"bluefilter"] forState : UIControlStateNormal];
    [Filter addTarget:self action:@selector(Filteroption:) forControlEvents:UIControlEventTouchUpInside];
    [Search_View addSubview:Filter];
    
    if ([HeaderTitleStr isEqualToString:@"Shore Job Resume"])
    {
       // Filter.hidden = YES;
    }
    else
    {
        //Filter.hidden = NO;
    }
    
    [Search_View addSubview:searchBar];
    
    count = [[UILabel alloc] initWithFrame:CGRectMake(0, 18, 35, 20)];
    count.font = [UIFont systemFontOfSize:12];
    count.textColor = [UIColor lightGrayColor];
    count.layer.borderWidth = 1.0f;
    count.layer.borderColor = [UIColor darkGrayColor].CGColor;
    count.layer.cornerRadius = 5.0f;
    count.textAlignment = UITextAlignmentCenter;
    [Search_View addSubview:count];
   // [searchBar becomeFirstResponder];
    
    UITextField *searchField = [searchBar valueForKey:@"searchField"];
    
    // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
    
    // To change text color
    searchField.textColor = [UIColor blackColor];
    searchField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
    // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search Candidate"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
       [self customSetup];
    
//        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapWeb:)];
//        singleTap.numberOfTapsRequired = 1;
//    [self.view addGestureRecognizer:singleTap];
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(rank) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(Shiptype) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(Country) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(Designation) withObject:nil afterDelay:0.1];
     availableid = @"1";
    _shoreresumefiltervw.hidden = YES;
}

- (void)singleTapWeb:(UITapGestureRecognizer *)gesture {
    filterview.hidden = YES;
     _shoreresumefiltervw.hidden = YES;
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(IBAction)Filteroption:(id)sender
{
   
    if ([HeaderTitleStr isEqualToString:@"Shore Job Resume"])
    {
         filterview.hidden = YES;
        _shoreresumefiltervw.hidden = NO;
    }
    else
    {
         filterview.hidden = NO;
         _shoreresumefiltervw.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CandidateJobcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CandidateJobcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isFilterd == YES)
    {
         count.text = [NSString stringWithFormat:@"%lu",(unsigned long)filterdarray.count];
        return filterdarray.count;
    }
    else
    {
         count.text = [NSString stringWithFormat:@"%lu",(unsigned long)marrcandidatelist.count];
        return marrcandidatelist.count;
    }// in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(CandidateJobcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isFilterd == YES)
    {
        if ([HeaderTitleStr isEqualToString:@"Shore Job Resume"])
        {
            cell.lblnam.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"CandidateName"] ];
            cell.lblamount.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"AppliedRank"] ];
        }
        else
        {
            cell.lblamount.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"AppliedRankName"] ];
            cell.lblnam.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"CanidateName"] ];
        }
//        cell.lblnam.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"CanidateName"] ];
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"CandidateCode"] ];
        
        NSString * date = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"LastVisited"] ];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        cell.lbldate.text = [NSString stringWithFormat:@"%@",date];
  // 5     cell.lbldate.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"last_login"] ];
        
        
        NSString* profilepicname = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"ImageURL"]];
        NSURL *url = [NSURL URLWithString:profilepicname];
        NSLog(@"IMAGE URL = %@",url);
       
        //[cell.imageview hnk_setImageFromURL:url];
        
        //New Async Image
        cell.imageview.layer.cornerRadius = (cell.imageview.frame.size.width / 2);
        cell.imageview.clipsToBounds = YES;
        //cell.imageview.layer.borderWidth = 2.0f;
        cell.imageview.layer.borderColor = [UIColor whiteColor].CGColor;
        [cell.imageview setImageWithURL:url placeholderImage:[UIImage imageNamed:@"avt1"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //New Async Image
        
        
        [APP_DELEGATE hideLoadingView];
        [cell.btntouch setTag:indexPath.section];
        
        [cell.btntouch addTarget:self action:@selector(checkBoxClicked:event:)forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
       
        if ([HeaderTitleStr isEqualToString:@"Shore Job Resume"])
        {
             cell.lblnam.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"CandidateName"] ];
            cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"AppliedRank"] ];
        }
        else
        {
             cell.lblnam.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"CanidateName"] ];
            cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"AppliedRankName"] ];
           
        }
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"CandidateCode"] ];
        NSString * date = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"LastVisited"] ];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        cell.lbldate.text = [NSString stringWithFormat:@"%@",date];
 //       cell.lbldate.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"last_login"] ];
       
       // cell.lblshiptype.text = @"Available";
        NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"ImageURL"]];
        NSURL *url = [NSURL URLWithString:profilepicname];
        NSLog(@"IMAGE URL = %@",url);
   
        //[cell.imageview hnk_setImageFromURL:url];
        
        
        //New Async Image
        cell.imageview.layer.cornerRadius = (cell.imageview.frame.size.width / 2);
        cell.imageview.clipsToBounds = YES;
        //cell.imageview.layer.borderWidth = 2.0f;
        cell.imageview.layer.borderColor = [UIColor whiteColor].CGColor;
        [cell.imageview setImageWithURL:url placeholderImage:[UIImage imageNamed:@"avt1"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //New Async Image
        
        
        [APP_DELEGATE hideLoadingView];
        [cell.btntouch setTag:indexPath.section];
        
        [cell.btntouch addTarget:self action:@selector(checkBoxClicked:event:)forControlEvents:UIControlEventTouchUpInside];
    }
    
    
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    {
       if ( !error )
       {
            UIImage *image = [[UIImage alloc] initWithData:data];
            completionBlock(YES,image);
        } else{
            completionBlock(NO,nil);
        }
    }];
}

- (void)checkBoxClicked:(id)sender event:(id)event
{
    
    NSLog(@"isShoreJobResumeFlag = %@",total_shorejob_candidate);
    NSLog(@"loginUserDataArray = %@", loginUserDataArray);
    NSLog(@"HeaderTitleStr = %@",HeaderTitleStr);
    NSLog(@"isShoreJobResumeFlag = %@",_isShoreJobResumeFlag);
    NSLog(@"ShoreJobResumeFlag = %@",_ShoreJobResumeFlag);
    
    UIButton *btn=sender;
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
    
    
    
    if (isFilterd == YES)
    {
        isFilterd = NO;
        
        NSLog(@"isShoreJobResumeFlag = %@",total_shorejob_candidate);
        NSLog(@"loginUserDataArray = %@", loginUserDataArray);
        NSLog(@"HeaderTitleStr = %@",HeaderTitleStr);
        NSLog(@"isShoreJobResumeFlag = %@",_isShoreJobResumeFlag);
        NSLog(@"ShoreJobResumeFlag = %@",_ShoreJobResumeFlag);
        
        Candidatedetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatedetail"];
        s.detail = [filterdarray objectAtIndex:indexPath.row] ;
        
        s.total_shorejob_candidate = [NSString stringWithFormat:@"%@",total_shorejob_candidate];
        s.isShoreJobResumeFlag = [NSString stringWithFormat:@"%@",_isShoreJobResumeFlag];
        s.HeaderTitleStr = [NSString stringWithFormat:@"%@",HeaderTitleStr];
        s.ShoreJobResumeFlag = [NSString stringWithFormat:@"%@",_ShoreJobResumeFlag];
        s.loginUserDataArray = (NSMutableArray *)loginUserDataArray;
        [self.navigationController pushViewController:s animated:NO];

        
//        @property (nonatomic, retain) NSString * total_shorejob_candidate;
//        @property (nonatomic, retain) NSString * isShoreJobResumeFlag;
//        @property (nonatomic, retain) NSString * HeaderTitleStr;
//        @property (nonatomic, retain) NSString * ShoreJobResumeFlag;
//        @property (nonatomic, retain) NSMutableArray * loginUserDataArray;
        
    }else{
        
        
        //   http://clientsdemoarea.com/projects/flexcash/web_services/refund_request?userid=1&joinid=21&flexid=1&flexuserid=3
        //  [APP_DELEGATE showLoadingView:@""];
        Candidatedetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatedetail"];
        s.detail = [marrcandidatelist objectAtIndex:indexPath.row] ;
       
        s.total_shorejob_candidate = [NSString stringWithFormat:@"%@",total_shorejob_candidate];
        s.isShoreJobResumeFlag = [NSString stringWithFormat:@"%@",_isShoreJobResumeFlag];
        s.HeaderTitleStr = [NSString stringWithFormat:@"%@",HeaderTitleStr];
        s.ShoreJobResumeFlag = [NSString stringWithFormat:@"%@",_ShoreJobResumeFlag];
        s.loginUserDataArray = (NSMutableArray *)loginUserDataArray;
        [self.navigationController pushViewController:s animated:NO];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Candidatedetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatedetail"];
    s.detail = [marrcandidatelist objectAtIndex:indexPath.section] ;
    [self.navigationController pushViewController:s animated:NO];
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}



///searchbar
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isFilterd = NO;
    [self SearchBarDismiss];
    [self.tableview reloadData];
}

//SearchBarDismiss
#pragma mark - SearchBar Delegate

- (void)SearchBarDismiss
{
    isFilterd = NO;
    [self.view endEditing:YES];
    //  Btn_Search.hidden = NO;
    // Search_View.hidden = YES;
}

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        isFilterd = NO;
    }
    else
    {
        isFilterd = YES;
//
//        filterdarray = [[NSMutableArray alloc]init];
//
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@ ",searchText];
//        filterdarray = [NSMutableArray arrayWithArray:[[marrcandidatelist valueForKey:@"RankName"] filteredArrayUsingPredicate:predicate]];
//
//           //filterdarray = [NSMutableArray arrayWithArray:[[marrcandidatelist valueForKey:@"EmailID"] filteredArrayUsingPredicate:predicate]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"CandidateCode contains[c] %@",searchText];
        
        filterdarray = [[NSMutableArray alloc] initWithArray: [[marrcandidatelist filteredArrayUsingPredicate:predicate] copy]];
        
//        filterdarraybeforearray = [[NSMutableArray alloc]init];
//
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
//        filterdarraybeforearray = [NSMutableArray arrayWithArray:[[marrcandidatelist valueForKey:@"CandidateCode"] filteredArrayUsingPredicate:predicate]];
//        NSString * find;
//        if (filterdarraybeforearray.count <= 0) {
//
//        }
//        else{
//            find = [NSString stringWithFormat:@"%@",[filterdarraybeforearray objectAtIndex:0]];
//        }
//        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
//        NSArray *results = [marrcandidatelist filteredArrayUsingPredicate:predicate1];
//
//        filterdarray = [[NSMutableArray alloc]init];
//
//        filterdarray = [results copy];
    }
    [self.tableview reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  isFilterd = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    // isFilterd = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
}

- (IBAction)btnsearch:(id)sender {
    //////////for searching
    //Create SearchBar
    Search_View.hidden = NO;
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, Search_View.frame.size.width+0, Search_View.frame.size.height+5)];
    searchBar.autoresizingMask =0;
    searchBar.delegate = self;
    searchBar.placeholder = @"Ship Type";
    searchBar.showsScopeBar = YES;
    
    [searchBar setBackgroundColor:[UIColor clearColor]];
    searchBar.layer.borderWidth = 1.0f;
    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
    [searchBar setBarTintColor:[UIColor whiteColor]]; //this is what you want
    [searchBar setShowsCancelButton:YES animated:YES];
    
    [Search_View addSubview:searchBar];
   [searchBar becomeFirstResponder];
    
    UITextField *searchField = [searchBar valueForKey:@"searchField"];
    
    // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
    
    // To change text color
    searchField.textColor = [UIColor blackColor];
    searchField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
    // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Company Name"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
   
}
-(void)CallMyMethod1
{
  
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcandidatelist  = [[HttpWrapper alloc] init];
        httpcandidatelist.delegate=self;
        httpcandidatelist.getbool=NO;
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
     //   [parameters setValue:total_shorejob_candidate forKey:@"shore_job_resume"];
       // [parameters setValue:[PublishVacanciesArray valueForKey:@"JobID"] forKey:@"job_id"];
        //  [parameters setValue:@"" forKey:@"country_id"];
        //[parameters setValue:@"" forKey:@"from_experience"];
        [parameters setValue:@"" forKey:@"from_age"];
        // [parameters setValue:@"" forKey:@"latest_available"];
        [parameters setValue:@"" forKey:@"to_age"];
        [parameters setValue:@"" forKey:@"to_experience"];
        [parameters setValue:@"1" forKey:@"days"];
        [parameters setValue:@"" forKey:@"company_id"];
        
        [parameters setValue:@"" forKey:@"rank_id"];
        [parameters setValue:@"" forKey:@"ship_id"];
        //  [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
        //  [parameters setValue:@"yes" forKey:@"latest_available"];
        [parameters setValue:@"" forKey:@"country_id"];
        [parameters setValue:@"" forKey:@"from_experience"];
        if ([_isShoreJobResumeFlag isEqualToString:@"1"] && [_ShoreJobResumeFlag isEqualToString:@"YES"])
        {
            [parameters setValue:@"yes" forKey:@"shore_job_resume"];
        }
        
        NSLog(@"parameters = %@",parameters);
        if ([HeaderTitleStr isEqualToString:@"Shore Job Resume"])
        {
          [httpcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_shorejob_resume_list",MainUrl] param:parameters];
        }
        else
        {
          [httpcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_list",MainUrl] param:parameters];
        }
       
    });
    
    
}
-(void)Country
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpCountry  = [[HttpWrapper alloc] init];
        httpCountry.delegate=self;
        httpCountry.getbool=NO;
        
        [httpCountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country_list",MainUrl] param:@""];
    });
    
    
}
-(void)selectrank
{
    if ([HeaderTitleStr isEqualToString:@"Shore Job Resume"])
    {
        
    }
    else
    {
        NSString *find = _txt_country.text;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
        NSString * Uid2 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed1 = [Uid2 stringByTrimmingCharactersInSet:
                              [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        [parameters setValue:@"" forKey:@"job_id"];
        [parameters setValue:criteriafrom forKey:@"from_age"];
        [parameters setValue:@"" forKey:@"to_age"];
        [parameters setValue:@"" forKey:@"to_experience"];
        [parameters setValue:availableid forKey:@"days"];
        [parameters setValue:@"" forKey:@"company_id"];
        
        [parameters setValue:trimmed forKey:@"rank_id"];
        [parameters setValue:trimmed1 forKey:@"ship_id"];
        
        [parameters setValue:countryId forKey:@"country_id"];
        [parameters setValue:experiencerangeformid forKey:@"from_experience"];
        
        NSLog(@"parameters = %@",parameters);
        
        filterview.hidden = YES;
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httprankcandidatelist  = [[HttpWrapper alloc] init];
            httprankcandidatelist.delegate=self;
            httprankcandidatelist.getbool=NO;
            
            [httprankcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_list",MainUrl] param:[parameters copy]];
        });
    }
   
}
-(void)rank
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprank  = [[HttpWrapper alloc] init];
        httprank.delegate=self;
        httprank.getbool=NO;
        
        [httprank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:@""];
    });
    
    
}
-(void)Shiptype
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpshiptype  = [[HttpWrapper alloc] init];
        httpshiptype.delegate=self;
        httpshiptype.getbool=NO;
        
        [httpshiptype requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_ship_type",MainUrl] param:@""];
    });
    
    
}
-(void)Designation
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpdesignation  = [[HttpWrapper alloc] init];
        httpdesignation.delegate=self;
        httpdesignation.getbool=NO;
        
        [httpdesignation requestWithMethod:@"GET" url:[NSString stringWithFormat:@"%@get_designation_list",MainUrl] param:@""];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpdesignation && httpdesignation != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrdesignation = [dicsResponse valueForKey:@"data"];
        // NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    if(wrapper == httpcandidatelist && httpcandidatelist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcandidatelist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrcandidatelist);
            [self.tableview reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
    }
    if(wrapper == httpCountry && httpCountry != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrCountry = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    if(wrapper == httpshiptype && httpshiptype != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrShiptype = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    if(wrapper == httprankcandidatelist && httprankcandidatelist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcandidatelist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrcandidatelist);
            [self.tableview reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
    }
    
    if(wrapper == httprank && httprank != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrranklist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
           [APP_DELEGATE hideLoadingView];
            
        }
           [APP_DELEGATE hideLoadingView];
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


- (IBAction)btnSelectRank:(id)sender {
    isrank = YES;
    isshiptype = NO;
    [_txtselectrank setTag:0];
    currentTextField = _txtselectrank;
    // amountype = YES;
    [self showPopover:sender];
    
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
     
        if (isshiptype == YES) {
            settingsViewController.marrshiptype = marrShiptype;
            settingsViewController.index = currentTextField.tag;
        }
        if (isrank == YES) {
            settingsViewController.marrData = marrranklist;
            settingsViewController.index = currentTextField.tag;
        }
        if (isrank == NO && isshiptype == NO) {
            settingsViewController.index = currentTextField.tag;
        }
        if (iscountry == YES) {
            settingsViewController.marrcountry = marrCountry;
            settingsViewController.index = currentTextField.tag;
        }
        if (isdesignation == YES) {
            settingsViewController.marrshiptype = marrdesignation;
            settingsViewController.isfrom = @"shoreapp";
        }
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            //            if (amountype == YES) {
            settingsViewController.preferredContentSize = CGSizeMake(250, 200);
            //            }
            //            else{
            //                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
            //            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(250, 200);
            //            }
            //            else{
            //                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
            //            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
  //  filterview.hidden = YES;
    
    NSString *find = _txtselectrank.text;
    if ([_txtselectrank.text isEqualToString:@""]) {
        rankId = @"";
    }
    else{
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
        NSArray *results = [marrranklist filteredArrayUsingPredicate:predicate];
        
        rankId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"RankID" ]] ;
    }
    
    NSString *find1 = _txtshipType.text;
    if ([_txtshipType.text isEqualToString:@""]) {
        shiptypeid = @"";
    }
    else{
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find1];
        NSArray *results1 = [marrShiptype filteredArrayUsingPredicate:predicate1];
        shiptypeid= [NSString stringWithFormat:@"%@", [results1  valueForKey:@"ShipID" ]] ;
    }
    
    if ([_txtexperiencerangefrom.text isEqualToString:@""]) {
        experiencerangeformid = @"";
    }
    else{
        if ([_txtexperiencerangefrom.text isEqualToString:@"On Promotion"]) {
            experiencerangeformid= [NSString stringWithFormat:@"1" ] ;
        }
        else if ([_txtexperiencerangefrom.text isEqualToString:@"Less than 6 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"2" ];
        }
        else if ([_txtexperiencerangefrom.text isEqualToString:@"6 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"6" ];
        }
        else if ([_txtexperiencerangefrom.text isEqualToString:@"12 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"12" ];
        }
        else if ([_txtexperiencerangefrom.text isEqualToString:@"18 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"18" ];
        }
        else if ([_txtexperiencerangefrom.text isEqualToString:@"24 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"24" ];
        }
        else if ([_txtexperiencerangefrom.text isEqualToString:@"36 Months"]){
            experiencerangeformid= [NSString stringWithFormat:@"36" ];
        }
    }
    if ([_txtexperiencerangeto.text isEqualToString:@""]) {
        experiencerangetoid = @"";
    }
    else{
        if ([_txtexperiencerangeto.text isEqualToString:@"6 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"6" ];
        }
        else if ([_txtexperiencerangeto.text isEqualToString:@"12 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"12" ];
        }
        else if ([_txtexperiencerangeto.text isEqualToString:@"18 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"18" ];
        }
        else if ([_txtexperiencerangeto.text isEqualToString:@"24 Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"24" ];
        }
        else if ([_txtexperiencerangeto.text isEqualToString:@"24+ Months"]){
            experiencerangetoid= [NSString stringWithFormat:@"24+" ];
        }
        
    }
    if ([_txtavailablefrom.text isEqualToString:@"Available Immediately"]) {
        availableid = @"0";
    }
    else{
        if ([_txtavailablefrom.text isEqualToString:@"15 Days-30 Days"]){
            availableid= [NSString stringWithFormat:@"15" ];
        }
        else if ([_txtavailablefrom.text isEqualToString:@"30 Days-45 Days"]){
            availableid= [NSString stringWithFormat:@"30" ];
        }
        else if ([_txtavailablefrom.text isEqualToString:@"45 Days-60 Days"]){
            availableid= [NSString stringWithFormat:@"45" ];
        }
//        else if ([_txtavailablefrom.text isEqualToString:@"60 Days"]){
//            availableid = [NSString stringWithFormat:@"60" ];
//        }
        else if ([_txtavailablefrom.text isEqualToString:@"All Available"]){
            availableid = [NSString stringWithFormat:@"1" ];
        }
        else
        {
           // availableid = @"0";
        }
    }
    
 
    
    //filterview.hidden = YES;
//    [APP_DELEGATE showLoadingView:@""];
//    [self performSelector:@selector(selectrank) withObject:nil afterDelay:0.1];
//    if ([_txtselectrank.text isEqualToString:@""]&& [_txtexperiencerangefrom.text isEqualToString:@""]&&[_txtexperiencerangeto.text isEqualToString:@""]) {
//        filterview.hidden = YES;
//        [APP_DELEGATE showLoadingView:@""];
//        [self performSelector:@selector(selectrank) withObject:nil afterDelay:0.1];
//    }
//    else{
//        NSLog(@"check");
//
//        if (![_txtselectrank.text isEqualToString:@""]&& ![_txtexperiencerangefrom.text isEqualToString:@""]&& ![_txtexperiencerangeto.text isEqualToString:@""]) {
//            filterview.hidden = YES;
//            [APP_DELEGATE showLoadingView:@""];
//            [self performSelector:@selector(selectrank) withObject:nil afterDelay:0.1];
//        }
//        else{
//            if ([_txtselectrank.text isEqualToString:@""]){
//                showAlert(AlertTitle, @"Please select rank")
//                [_txtexperiencerangeto.text isEqualToString:@""];
//                [_txtexperiencerangefrom.text isEqualToString:@""];
//            }
//            else if ([_txtexperiencerangeto.text isEqualToString:@""]){
//                showAlert(AlertTitle, @"Please select experience range ""To""")
//                [_txtselectrank.text isEqualToString:@""];
//                [_txtexperiencerangefrom.text isEqualToString:@""];
//            }
//            else{
//                [_txtselectrank.text isEqualToString:@""];
//                [_txtexperiencerangeto.text isEqualToString:@""];
//                showAlert(AlertTitle, @"Please select experience range ""From""")
//            }
//
//        }
//    }
   
}
- (IBAction)btnShiptype:(id)sender {
    isshiptype = YES;
    isrank = NO;
    [_txtshipType setTag:7];
    currentTextField = _txtshipType;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnExperiencerangefrom:(id)sender {
    isshiptype = NO;
    isrank = NO;
    [_txtexperiencerangefrom setTag:13];
    currentTextField = _txtexperiencerangefrom;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnExperiencerangeto:(id)sender {
    isshiptype = NO;
    isrank = NO;
    [_txtexperiencerangeto setTag:14];
    currentTextField = _txtexperiencerangeto;
    // amountype = YES;
    [self showPopover:sender];
}
- (IBAction)btnAvailablefrom:(id)sender {
    isshiptype = NO;
    isrank = NO;
    [_txtavailablefrom setTag:15];
    currentTextField = _txtavailablefrom;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnAvailableall:(id)sender {
    
    filterview.hidden = YES;
    [APP_DELEGATE showLoadingView:@""];
    experiencerangeformid =@"";
    experiencerangetoid =@"";
    shiptypeid =@"";
    rankId = @"";
    criteriafrom = @"";
    criteriato = @"";
    _txt_country.text = @"";
    _txtshipType.text = @"";
    _txtselectrank.text = @"";
    _txtagecriteriato.text = @"";
    _txtagecriteriafrom.text = @"";
    _txtexperiencerangeto.text = @"";
    _txtexperiencerangefrom.text = @"";
    _txtavailablefrom.text=@"";
    
    availableid = @"1";
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setValue:@"" forKey:@"CountryIDSelected"];
    [CountryDataSave setValue:@"" forKey:@"CountryNameSelected"];
    [CountryDataSave synchronize];
    [self.view endEditing:YES];
    [self performSelector:@selector(selectrank) withObject:nil afterDelay:0.1];
    
//    experiencerangeformid =@"";
//    experiencerangetoid =@"";
//    availableid =@"";
//    shiptypeid =@"";
//    rankId = @"";
//    criteriafrom = @"";
//    criteriato = @"";
//    _txtshipType.text = @"";
//    _txtselectrank.text = @"";
//    _txtagecriteriato.text = @"";
//    _txtagecriteriafrom.text = @"";
//    _txtexperiencerangeto.text = @"";
//    _txtexperiencerangefrom.text = @"";
//    _txtavailablefrom.text=@"";
//    filterview.hidden = YES;
//
//    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
//    [CountryDataSave setValue:@"" forKey:@"CountryIDSelected"];
//    [CountryDataSave setValue:@"" forKey:@"CountryNameSelected"];
//    [CountryDataSave synchronize];
//
//    [APP_DELEGATE showLoadingView:@""];
//    [self performSelector:@selector(selectrank) withObject:nil afterDelay:0.1];
}



- (IBAction)txtagecriteriafrom:(id)sender {
    if ([_txtagecriteriato.text isEqualToString:@""]) {
        
    }
    else if ([_txtagecriteriafrom.text intValue] > 70){
        showAlert(AlertTitle, @"Maximum age limit is 70");
        _txtavailablefrom.text = @"";
    }
    else{
        //filterview.hidden = YES;
       
        criteriafrom = [NSString stringWithFormat:@"%@",_txtagecriteriafrom.text];
//        [APP_DELEGATE showLoadingView:@""];
//        [self performSelector:@selector(selectrank) withObject:nil afterDelay:0.1];
    }
}
- (IBAction)btnmyaccount:(id)sender {
    CompanyAccount *s =[self.storyboard instantiateViewControllerWithIdentifier:@"CompanyAccount"];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)Btn_Country:(id)sender {
    
//    filterview.hidden = NO;
//    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
//    [CountryDataSave setValue:@"" forKey:@"CountryIDSelected"];
//    [CountryDataSave setValue:@"" forKey:@"CountryNameSelected"];
//    [CountryDataSave synchronize];
//
//    isshiptype = NO;
    iscountry = YES;
//    isrank = NO;
//    [_txt_country setTag:19];
//    currentTextField = _txt_country;
//    // amountype = YES;
//    [self showPopover:sender];
    [Dropobj fadeOut];
    // [self showPopUpWithTitle:@"Select Country" withOption:[marrCountry valueForKey:@"CountryName"] xy:CGPointMake(35, 58) size:CGSizeMake(300, 330) isMultiple:YES];
    [self showPopUpWithTitle:@"Select Country" withOption:[marrCountry valueForKey:@"CountryName"] xy:CGPointMake(10, 100) size:CGSizeMake(300, 300) isMultiple:YES];
}
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSMutableArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    if (iscountry == YES) {
        Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
        Dropobj.delegate = self;
        [Dropobj showInView:self.view animated:YES];
        [self.view bringSubviewToFront:Dropobj];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDown_R:20.0 G:35.0 B:97.0 alpha:0.70];
        
    }
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    //    for (int i=0; i<marrranklist.count; i++) {
    //        _txtmultiplerank.text=[[marrrankselectedlist objectAtIndex:i] valueForKey:@"Name"];
    //    }
    //    _txtmultiplerank.text=[[marrrankselectedlist objectAtIndex:anIndex] valueForKey:@"Name"];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (iscountry == YES) {
        if (ArryData.count>0) {
            _txt_country.text=[ArryData componentsJoinedByString:@","];
            NSMutableArray *NewArr = [[NSMutableArray alloc] init];
            NSMutableArray *myarr = [[NSMutableArray alloc] init];
            
            for (int i=0; i<marrCountry.count; i++) {
                [NewArr addObject:[[marrCountry objectAtIndex:i] valueForKey:@"CountryName"]];
            }
            
            for (int i=0; i<ArryData.count; i++) {
                NSUInteger index = [NewArr indexOfObject:ArryData[i]];
                NSLog(@"index %lu",(unsigned long)index);
                [myarr addObject:[[marrCountry objectAtIndex:index] valueForKey:@"CountryID"]];
            }
            NSLog(@"my arr %@",myarr);
            
            countryId = [myarr componentsJoinedByString:@","];
            //
            NSLog(@"countryId %@",countryId);
            
        }
        else{
            countryId = @"";
            _txt_country.text=@"";
        }
    }
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}

- (IBAction)btnok_click:(UIButton *)sender {
    
    filterview.hidden = YES;
    [APP_DELEGATE showLoadingView:@""];
     criteriafrom = [NSString stringWithFormat:@"%@",_txtagecriteriafrom.text];
    [self.view endEditing:YES];
    [self selectrank];
}
- (IBAction)btndesignation_click:(UIButton *)sender {
    
    isdesignation = YES;
    isshiptype = NO;
    [_txt_designation setTag:7];
    currentTextField = _txt_designation;
    // amountype = YES;
    [self showPopover:sender];
}
- (IBAction)resumeok_click:(UIButton *)sender {
    
    _shoreresumefiltervw.hidden = YES;
    NSString *find1 = _txt_designation.text;
    if ([_txt_designation.text isEqualToString:@""]) {
        shiptypeid = @"";
    }
    else{
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find1];
        NSArray *results1 = [marrdesignation filteredArrayUsingPredicate:predicate1];
        shiptypeid= [NSString stringWithFormat:@"%@", [results1  valueForKey:@"DesignationID" ]] ;
    }
    
    
    NSString * Uid2 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed1 = [Uid2 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:trimmed1 forKey:@"designation_id"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    NSLog(@"parameters ====>>>>>>>>>>>>>>>>>> %@",parameters);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprankcandidatelist  = [[HttpWrapper alloc] init];
        httprankcandidatelist.delegate=self;
        httprankcandidatelist.getbool=NO;
        
        [httprankcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_shorejob_resume_list",MainUrl] param:[parameters copy]];
    });
}

- (IBAction)resumeallavailable_click:(UIButton *)sender {
     _shoreresumefiltervw.hidden = YES;
    _txt_designation.text = @"";
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:@"" forKey:@"designation_id"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    NSLog(@"parameters ====>>>>>>>>>>>>>>>>>> %@",parameters);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprankcandidatelist  = [[HttpWrapper alloc] init];
        httprankcandidatelist.delegate=self;
        httprankcandidatelist.getbool=NO;
        
        [httprankcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_shorejob_resume_list",MainUrl] param:[parameters copy]];
    });
}


@end
