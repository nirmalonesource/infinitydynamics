//
//  CandidateJob.h
//  Infinity Dynamics
//
//  Created by My Mac on 26/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface CandidateJob : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>{
      UITextField *currentTextField;
    HttpWrapper *httprank,*httpshiptype,*httpCountry,*httpdesignation;
    NSString * rankId;
     NSMutableArray *marrranklist;
    AppDelegate * appdelegate;
    HttpWrapper *httpcandidatelist;
     HttpWrapper *httprankcandidatelist;
    NSMutableArray *marrcandidatelist;
    BOOL isFilterd;
    //ArrayCreation
    NSMutableArray *filterdarray;
    NSMutableArray * filterdarraybeforearray;
    NSMutableArray *marrdesignation;
      UIView *picker;   /////menu picker view
    
    NSMutableArray *marrShiptype;
    NSString * experiencerangeformid;
    NSString * experiencerangetoid;
    NSString * availableid;
    NSString * shiptypeid;
    NSString * criteriafrom;
    NSString * criteriato;
    BOOL isshiptype;
    BOOL isrank;
    BOOL isdesignation;
    BOOL iscountry;
    NSString * countryId;
    NSMutableArray *marrCountry;
}
@property (weak, nonatomic) IBOutlet UITextField *txt_country;
- (IBAction)Btn_Country:(id)sender;
- (IBAction)btnmyaccount:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *Search_View;


- (IBAction)btnSelectRank:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;

@property (strong, nonatomic) IBOutlet UIView *filterview;
@property (strong, nonatomic) IBOutlet UITextField *txtselectrank;
@property (nonatomic, strong) NSString *jobid;
- (IBAction)btnSelectRank:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtshipType;
- (IBAction)btnShiptype:(id)sender;
- (IBAction)btnExperiencerangefrom:(id)sender;
- (IBAction)btnExperiencerangeto:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtexperiencerangefrom;
@property (strong, nonatomic) IBOutlet UITextField *txtexperiencerangeto;

@property (strong, nonatomic) IBOutlet UITextField *txtagecriteriato;
@property (strong, nonatomic) IBOutlet UITextField *txtagecriteriafrom;
- (IBAction)btnAvailablefrom:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtavailablefrom;
- (IBAction)btnAvailableall:(id)sender;
- (IBAction)txtagecriteriato:(id)sender;
- (IBAction)txtagecriteriafrom:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *HeaderBTNChangeTitle;

@property (nonatomic , retain) NSString * HeaderTitleStr;
@property (nonatomic , retain) NSString * total_shorejob_candidate;
@property (nonatomic , retain) NSMutableArray * loginUserDataArray;
@property (nonatomic , retain) NSString * ShoreJobResumeFlag;
@property (nonatomic , retain) NSString * isShoreJobResumeFlag;
@property (nonatomic , retain) NSString * isCandidate;
- (IBAction)btnok_click:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIView *shoreresumefiltervw;
@property (strong, nonatomic) IBOutlet UITextField *txt_designation;
- (IBAction)btndesignation_click:(UIButton *)sender;

- (IBAction)resumeok_click:(UIButton *)sender;
- (IBAction)resumeallavailable_click:(UIButton *)sender;

@end
