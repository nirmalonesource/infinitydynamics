//
//  candidateBuildResume.m
//  Infinity Dynamics
//
//  Created by My Mac on 06/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "candidateBuildResume.h"
#import "Constants.h"
@interface candidateBuildResume ()

@end

@implementation candidateBuildResume

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_btntitle setTitle:_headertitle forState:UIControlStateNormal];
    _webview.navigationDelegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    if (_downloadlist == YES) {
        NSURL *targetURL = [NSURL URLWithString:_url];
        
        //[[NSBundle mainBundle] URLForResource:@"document" withExtension:@"pdf"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [_webview loadRequest:request];
    }
    else{
        if (_Preview == YES) {
            NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://infinitydynamics.in/web_services/version_2/web_services/preview_build_resume?candidate_id=%@",_candidateid]];
           //  NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://test.infinitydynamics.in/web_services/version_1/web_services/preview_build_resume?candidate_id=%@",_candidateid]];
            //[[NSBundle mainBundle] URLForResource:@"document" withExtension:@"pdf"];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
            [_webview loadRequest:request];
            
           // http://test.infinitydynamics.in/web_services/version_1/web_services/preview_build_resume?candidate_id="+candidateId
        }
        else{
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
        }
    }
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    if (_downloadlist == YES) {
       // _url = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"resume_url"]];
        
        NSURL *targetURL = [NSURL URLWithString:_url];
        
        //[[NSBundle mainBundle] URLForResource:@"document" withExtension:@"pdf"];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [_webview loadRequest:request];
         
    }
    else{
      [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    }
    
    [parameters setValue:_candidateid forKey:@"candidate_id"];
   
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpJobappliedlist  = [[HttpWrapper alloc] init];
        httpJobappliedlist.delegate=self;
        httpJobappliedlist.getbool=NO;
        
        [httpJobappliedlist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@build_resume",MainUrl] param:[parameters copy]];
    });

}
-(void)CallMyMethod2
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
 
       [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
   
    
    [parameters setValue:_candidateid forKey:@"candidate_id"];
     [parameters setValue:@"1" forKey:@"download_again"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpJobappliedlist  = [[HttpWrapper alloc] init];
        httpJobappliedlist.delegate=self;
        httpJobappliedlist.getbool=NO;
        
        [httpJobappliedlist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@build_resume",MainUrl] param:[parameters copy]];
    });
    
}
-(void)CallMyMethod3
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
   // [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    
    
    [parameters setValue:_candidateid forKey:@"candidate_id"];
   // [parameters setValue:@"1" forKey:@"download_again"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpjobprievw  = [[HttpWrapper alloc] init];
        httpjobprievw.delegate=self;
        httpjobprievw.getbool=NO;
        
        [httpjobprievw requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@preview_build_resume",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpJobappliedlist && httpJobappliedlist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrJobappliedlist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
            
            _url = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"resume_url"]];

           // NSURL *targetURL = [NSURL URLWithString:_url];
           // NSString* webStringURL = [_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSString* webStringURL = [_url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            //[[NSBundle mainBundle] URLForResource:@"document" withExtension:@"pdf"];
            
            //NSURLRequest *request = [NSURLRequest requestWithURL:webStringURL];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:webStringURL]];
            [_webview loadRequest:request];
            [APP_DELEGATE hideLoadingView];
        }
        else{
             NSString * message1 = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([message1 isEqualToString:@"This resume is already downloaded by you.Do you Still want to download ?"]) {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Infinity Dynamics"
                                                                  message:message1
                                                                 delegate:self
                                                        cancelButtonTitle:@"YES"
                                                        otherButtonTitles:@"Cancel",nil];
                message.tag = 0;
                [message show];
                // showAlert(AlertTITLE, message);
                // [self.navigationController popViewControllerAnimated:NO];
                [APP_DELEGATE hideLoadingView];
                
            }else{
              showAlert(AlertTITLE, message1);
              [self.navigationController popViewControllerAnimated:NO];
                [APP_DELEGATE hideLoadingView];
           
            }
        }
    }
    if(wrapper == httpjobprievw && httpjobprievw != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrjobprievw = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
            
            _url = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"resume_url"]];
            
            NSURL *targetURL = [NSURL URLWithString:_url];
            
            //[[NSBundle mainBundle] URLForResource:@"document" withExtension:@"pdf"];
            
            NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
            [_webview loadRequest:request];
        }
        else{
            NSString * message1 = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            
            if ([message1 isEqualToString:@"This resume is already downloaded by you.Do you Still want to download ?"]) {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Infinity Dynamics"
                                                                  message:message1
                                                                 delegate:self
                                                        cancelButtonTitle:@"YES"
                                                        otherButtonTitles:@"Cancel",nil];
                message.tag = 0;
                [message show];
                // showAlert(AlertTITLE, message);
                // [self.navigationController popViewControllerAnimated:NO];
                [APP_DELEGATE hideLoadingView];
                
            }else{
                showAlert(AlertTITLE, message1);
                [self.navigationController popViewControllerAnimated:NO];
                [APP_DELEGATE hideLoadingView];
                
            }
        }
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
     [APP_DELEGATE showLoadingView:@""];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
     [APP_DELEGATE hideLoadingView];
}
//- (void)webViewDidStartLoad:(UIWebView *)webView{
//    [APP_DELEGATE showLoadingView:@""];
//
//}
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    [APP_DELEGATE hideLoadingView];
//}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 0)
    {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(CallMyMethod2) withObject:nil afterDelay:0.1];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:NO];
          
            NSLog(@"Cancell button");
            //Do something else
        }
    }
    else{
        if (buttonIndex == 0) {
             NSLog(@"Cancell button");
        }
    }
}
@end
