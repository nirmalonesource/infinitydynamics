//
//  Candidatejobs.h
//  Infinity Dynamics
//
//  Created by My Mac on 27/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface Candidatejobs : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>{
    UITextField *currentTextField;
    HttpWrapper *httprank,*httpshiptype,*httpdesignation;
    NSString * rankId;
    NSMutableArray *marrranklist;
    AppDelegate * appdelegate;
    HttpWrapper *httpcandidatelist;
    HttpWrapper *httprankcandidatelist;
    NSMutableArray *marrcandidatelist;
    BOOL isFilterd;
    
    //ArrayCreation
    NSMutableArray *filterdarray;
    NSMutableArray * filterdarraybeforearray;
    UIView *picker;   /////menu picker view
    NSMutableArray *marrShiptype;
    NSMutableArray *marrdesignation;

    NSString * shiptypeid;
    BOOL isshiptype;
     BOOL isrank;
}
- (IBAction)btnmyaccount:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *Search_View;
@property (strong, nonatomic) IBOutlet UIView *filterview;
@property (strong, nonatomic) IBOutlet UITextField *txtselectrank;
- (IBAction)btnAvailable:(id)sender;

- (IBAction)btnSelectRank:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@property (strong, nonatomic) IBOutlet UITextField *txtshipType;
- (IBAction)btnShiptype:(id)sender;
@property (strong, nonatomic) NSString *isfrom;
@property (strong, nonatomic) NSString *isdashboard;

@property (strong, nonatomic) NSString *shorejobid;
@property (strong, nonatomic) IBOutlet UILabel *lblshiptype;

@end
