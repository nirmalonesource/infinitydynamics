//
//  CompanyAccount.h
//  Infinity Dynamics
//
//  Created by My Mac on 27/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "TextFieldValidator.h"
#import "UzysImageCropperViewController.h"
@interface CompanyAccount : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate,UzysImageCropperDelegate>{
    BOOL ischecked;
    bool isphoto;
    HttpWrapper *httpSignup,*httpgetdata;
    UIView *picker;
    NSMutableArray * getdata;
    NSString * isAccept;
    //    IBOutlet TextFieldValidator *txtConfirmpasword;
    //    IBOutlet TextFieldValidator *txtpassword;
    //    IBOutlet TextFieldValidator *txtphone;
    //    IBOutlet TextFieldValidator *txtemail;
    //    IBOutlet TextFieldValidator *txtname;
    IBOutlet TextFieldValidator *txtCompanyname;
    IBOutlet TextFieldValidator *txtEmailid;
    IBOutlet TextFieldValidator *txtPassword;
    IBOutlet TextFieldValidator *txtconfirmPassword;
    IBOutlet TextFieldValidator *txtWebsite;
    IBOutlet TextFieldValidator *txtMobileno;
    IBOutlet UITextView *txtAddress;
    
    __weak IBOutlet TextFieldValidator *txtContactPerson;
    __weak IBOutlet TextFieldValidator *txtRPSLNo;
    __weak IBOutlet TextFieldValidator *txtValidUntil;
    __weak IBOutlet TextFieldValidator *txtCity;
    __weak IBOutlet TextFieldValidator *txtZipCode;
    __weak IBOutlet TextFieldValidator *txtCountry;
    __weak IBOutlet TextFieldValidator *txtPlan;
}


- (IBAction)BtnImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageview;
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnback;

////////crop
@property (nonatomic,retain) UIImageView *resultImgView;
@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *picker;
//////////
@property (strong, nonatomic) IBOutlet TextFieldValidator *txttelephone;

- (IBAction)btnRegister:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAccepttermsandcondition;
- (IBAction)BtnacceptTermsandCondition:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@end
