//
//  CompanyAccount.m
//  Infinity Dynamics
//
//  Created by My Mac on 27/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "CompanyAccount.h"
#import "myModel.h"
#import "Constants.h"
#import "ViewController.h"
#import "TextFieldValidator.h"
#import "SWRevealViewController.h"
#import "UIImageView+WebCache.h"
#import "ComanyHome.h"
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{5,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{5,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
@interface CompanyAccount (){
    UIActionSheet *actionSheet;
}

@end

@implementation CompanyAccount

- (void)viewDidLoad
{
    [super viewDidLoad];
      self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
    txtAddress.delegate=self;
    txtAddress.text = @"Enter your address";
    txtAddress.textColor = [UIColor lightGrayColor];
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 1000);
    [self setupAlerts];
    [self customSetup];
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
}
-(void)setupAlerts{
    //    [txtname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    //    [txtname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    //    txtname.validateOnResign=NO;
    
    [txtEmailid addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    
    [txtPassword addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password characters limit should be come between 6-20"];
    [txtPassword addRegx:REGEX_PASSWORD withMsg:@"Password must contain alpha numeric characters."];
    
    [txtconfirmPassword addConfirmValidationTo:txtPassword withMsg:@"Confirm password didn't match."];
    
    //    [txtMobileno addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper format (eg. ##########)"];
    //    txtMobileno.isMandatory=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if([txtAddress.text isEqual: @"Enter your address"]){
        txtAddress.text = @"";
        txtAddress.textColor = [UIColor blackColor];
    }
    return YES;
}
-(void) textViewDidChange:(UITextView *)textView
{
    if(txtAddress.text.length == 0){
        txtAddress.textColor = [UIColor lightGrayColor];
        txtAddress.text = @"Enter your address";
        [txtAddress resignFirstResponder];
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (IBAction)BtnacceptTermsandCondition:(id)sender {
    if (ischecked) {
        [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        ischecked = false;
        isAccept = [NSString stringWithFormat:@"0"];
        NSLog(@"UNCHEK");
    }else{
        [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        ischecked= true;
        isAccept = [NSString stringWithFormat:@"1"];
        NSLog(@"CHEK");
    }
}
- (IBAction)btnRegister:(id)sender {
    if ([txtEmailid.text isEqualToString:@""]||[txtWebsite.text isEqualToString:@""]||[txtMobileno.text isEqualToString:@""]||[txtCompanyname.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Fill all the detail");
    }
    else
    {
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    }
}

-(void)CallMyMethod1
{
    //Post Method Request
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
     [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    [parameters setValue:txtEmailid.text forKey:@"email"];
    [parameters setValue:txtPassword.text forKey:@"password"];
    [parameters setValue:txtMobileno.text forKey:@"phone"];
    [parameters setValue:txtWebsite.text forKey:@"website"];
    [parameters setValue:txtCompanyname.text forKey:@"company_name"];
    [parameters setValue:txtAddress.text forKey:@"address"];
    NSData *imageData = UIImageJPEGRepresentation(_imageview.image, 0.5);
    [parameters setValue:imageData forKey:@"company_image"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpSignup  = [[HttpWrapper alloc] init];
        httpSignup.delegate=self;
        httpSignup.getbool=NO;
        
        [httpSignup requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@update_company_data",MainUrl] param:[parameters copy]];
    });
    
}

-(void)getcompanydata
{
    //Post Method Request
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
     [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpgetdata  = [[HttpWrapper alloc] init];
        httpgetdata.delegate=self;
        httpgetdata.getbool=NO;
        
        [httpgetdata requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_company_data",MainUrl] param:[parameters copy]];
    });
}

-(void)viewWillAppear:(BOOL)animated{
    if (isphoto == YES) {
        
    }else{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(getcompanydata) withObject:nil afterDelay:0.1];
}
   
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpSignup && httpSignup != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
            ComanyHome *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
            //            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:s animated:NO];
        }
        else{
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
//            ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
//            [self.navigationController pushViewController:s animated:NO];
            
        }
    }
    if(wrapper == httpgetdata && httpgetdata != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        getdata=[[NSMutableDictionary alloc]init];
        getdata=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
           
              txtAddress.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"Address"]];
                txtAddress.textColor = [UIColor blackColor];
              txtEmailid.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"EmailID"]];
              txtWebsite.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"Website"]];
            
              NSString * TelephoneNoStr = [NSString stringWithFormat:@"%@ %@ %@",[getdata valueForKey:@"TelCountryCode"],[getdata valueForKey:@"STDCode"],[getdata valueForKey:@"Telephone"]];
            
            //_txttelephone.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"Telephone"]];
            
            _txttelephone.text = [NSString stringWithFormat:@"%@",TelephoneNoStr];
            
            NSString * MobileNoStr = [NSString stringWithFormat:@"%@ %@",[getdata valueForKey:@"CCode"],[getdata valueForKey:@"Mobile"]];
            
            txtMobileno.text = [NSString stringWithFormat:@"%@",MobileNoStr];
            
            txtCompanyname.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"CompanyName"]];
            NSString *   profilepicname = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"LogoURL"]];
            
            if ([profilepicname  isEqualToString:@"http://clientsdemoarea.com/projects/mariexeltime/uploads/company_logo/"])
            {
                _imageview.image = [UIImage imageNamed: @"user-1"];
            }
            else
            {
                NSString *fullpath = [NSString stringWithFormat:@"%@",profilepicname];
                
                [_imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",fullpath]] placeholderImage:[UIImage imageNamed:@"500x200"]];
                //   _Profileimage.image = [self getImageFromURL:fullpath];
            }
            
            txtContactPerson.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"ContactPerson"]];
            
            txtRPSLNo.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"RPSLNo"]];
            
            NSString *validuntil = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"ValidUntil"]];
            if (validuntil.length == 0)
            {
                validuntil = @"";
            }
            txtValidUntil.text =
            
            txtCity.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"CityName"]];
            
            txtZipCode.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"Zipcode"]];
            
            txtCountry.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"CountryName"]];
            
            txtPlan.text = [NSString stringWithFormat:@"%@",[getdata valueForKey:@"CountryName"]];
            
            /*
             txtContactPerson
             txtRPSLNo
             txtValidUntil
             txtCity
             txtZipCode
             txtCountry
             txtPlan
             */
            
          
            
        }
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnBack:(id)sender
{
    // [self.navigationController popViewControllerAnimated:NO];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnback addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
- (IBAction)BtnImage:(id)sender {
    [self createActionSheet];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //    [_flexbanner setImage:chosenImage];
    //    _addflexbanner.hidden = YES;
    //    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"original Image Size : %@", NSStringFromCGSize(image.size));
    _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:image andframeSize:picker.view.frame.size andcropSize:CGSizeMake(500, 200)];
    _imgCropperViewController.delegate = self;
    [picker presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
     isphoto = YES;
}
- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    
    // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [_imageview setImage:image];
    //_addflexbanner.hidden = YES;
    //      [picker dismissViewControllerAnimated:YES completion:nil];
    //    if(self.resultImgView)
    //        [self.resultImgView removeFromSuperview];
    //
    //    self.resultImgView = [[[UIImageView alloc]  initWithFrame:CGRectMake(5, 5, 310, 310)] autorelease];
    //    self.resultImgView.image = image;
    //    self.resultImgView.backgroundColor = [UIColor darkGrayColor];
    //    self.resultImgView.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.view addSubview:self.resultImgView];
    //
    //    NSLog(@"cropping Image Size : %@", NSStringFromCGSize(image.size));
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
    [_picker dismissViewControllerAnimated:YES completion:nil];
    isphoto = YES;
}

-(void)createActionSheet
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Camera", @"Library",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Edit Profile");
            [self cameraPictureButtonClicked];
        }
        else if (buttonIndex ==1)
        {
            NSLog(@"Camera");
            [self libraryButtonClicked];
        }
    }
    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}
- (void)cameraPictureButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        //   UIImagePickerControllerCropRect
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
- (void)libraryButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    //picker.cro
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}


@end
