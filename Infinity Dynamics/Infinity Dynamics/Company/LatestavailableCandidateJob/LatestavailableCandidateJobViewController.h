//
//  LatestavailableCandidateJobViewController.h
//  Infinity Dynamics
//
//  Created by My Mac on 02/05/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
#import "DropDownListView.h"

@interface LatestavailableCandidateJobViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>{
    UITextField *currentTextField;
    HttpWrapper *httprank,*httpshiptype,*httpCountry;
    NSString * rankId;
    NSMutableArray *marrranklist;
    AppDelegate * appdelegate;
    HttpWrapper *httpcandidatelist;
    HttpWrapper *httprankcandidatelist;
    NSMutableArray *marrcandidatelist;
    BOOL isFilterd;
    //ArrayCreation
    NSMutableArray *filterdarray;
    NSMutableArray * filterdarraybeforearray;
    UIView *picker;   /////menu picker view
    //NSMutableArray *marrCountry;
    NSMutableArray *marrShiptype;
    NSString * experiencerangeformid;
    NSString * experiencerangetoid;
    NSString * availableid;
    NSString * shiptypeid;
    NSString * criteriafrom;
    NSString * criteriato;
    BOOL isshiptype;
     // BOOL iscountry;
    BOOL isrank;
       BOOL iscountry;
    BOOL isavailability;

    NSString * countryId;
    NSMutableArray *marrCountry;
}
- (IBAction)btnmyaccount:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *Search_View;


- (IBAction)btnSelectRank:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
@property (weak, nonatomic) IBOutlet UITextField *txt_country;
-(void)dismissPopUpViewController;
- (IBAction)Btn_Country:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *filterview;
@property (strong, nonatomic) IBOutlet UITextField *txtselectrank;
@property (nonatomic, strong) NSString *jobid;
- (IBAction)btnSelectRank:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtshipType;
- (IBAction)btnShiptype:(id)sender;
- (IBAction)btnExperiencerangefrom:(id)sender;
- (IBAction)btnExperiencerangeto:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtexperiencerangefrom;
@property (strong, nonatomic) IBOutlet UITextField *txtexperiencerangeto;

@property (strong, nonatomic) IBOutlet UITextField *txtagecriteriato;
@property (strong, nonatomic) IBOutlet UITextField *txtagecriteriafrom;
- (IBAction)btnAvailablefrom:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtavailablefrom;
- (IBAction)btnAvailableall:(id)sender;
- (IBAction)txtagecriteriato:(id)sender;
- (IBAction)txtagecriteriafrom:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtavailability;
- (IBAction)btnavailability_click:(UIButton *)sender;
- (IBAction)btnok_click:(UIButton *)sender;
@end
