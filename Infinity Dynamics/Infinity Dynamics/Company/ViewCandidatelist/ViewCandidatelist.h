//
//  ViewCandidatelist.h
//  Infinity Dynamics
//
//  Created by My Mac on 08/03/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "PopOverView.h"
#import "AppDelegate.h"
@interface ViewCandidatelist : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>{
    UITextField *currentTextField;
    HttpWrapper *httprank,*httpshiptype,*httpCountry;
    NSString * rankId;
    NSMutableArray *marrranklist;
    AppDelegate * appdelegate;
    HttpWrapper *httpcandidatelist;
    HttpWrapper *httprankcandidatelist;
    NSMutableArray *marrcandidatelist;
    NSMutableArray *marrCountry;
    NSString * countryId;

    BOOL isFilterd;
    //ArrayCreation
    NSMutableArray *filterdarray;
    NSMutableArray *  filterdarraybeforearray;
    UIView *picker;   /////menu picker view
    NSMutableArray *marrShiptype;
     NSString * experiencerangeformid;
     NSString * experiencerangetoid;
     NSString * availableid;
    NSString * shiptypeid;
      NSString * criteriafrom;
      NSString * criteriato;
    BOOL isshiptype;
    BOOL isrank;
    BOOL iscountry;
    BOOL isavailability;

    
}
- (IBAction)btnmyaccount:(id)sender;
@property (assign) BOOL isviewmatchedjob;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UIView *Search_View;
@property (strong, nonatomic) IBOutlet UIView *filterview;
@property (strong, nonatomic) IBOutlet UITextField *txtselectrank;
@property (nonatomic, strong) NSString *jobid;
- (IBAction)btnSelectRank:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@property (strong, nonatomic) IBOutlet UITextField *txtshipType;
- (IBAction)btnShiptype:(id)sender;
- (IBAction)btnExperiencerangefrom:(id)sender;
- (IBAction)btnExperiencerangeto:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtexperiencerangefrom;
@property (strong, nonatomic) IBOutlet UITextField *txtexperiencerangeto;

@property (strong, nonatomic) IBOutlet UITextField *txtagecriteriato;
@property (strong, nonatomic) IBOutlet UITextField *txtagecriteriafrom;
- (IBAction)btnAvailablefrom:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtavailablefrom;
- (IBAction)btnAvailableall:(id)sender;
- (IBAction)txtagecriteriato:(id)sender;
- (IBAction)txtagecriteriafrom:(id)sender;
@property (strong, nonatomic) NSMutableArray * PublishVacanciesArray;
- (IBAction)btnok_click:(UIButton *)sender;
@property (strong, nonatomic) NSString *isfrom;

@end
