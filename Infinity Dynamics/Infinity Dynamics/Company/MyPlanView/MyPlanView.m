//
//  MyPlanView.m
//  Infinity Dynamics
//
//  Created by My Mac on 21/06/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "MyPlanView.h"
#import "Constants.h"
#import "SWRevealViewController.h"
@interface MyPlanView ()

@end

@implementation MyPlanView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
-(void)CallMyMethod1
{
    //    company_id
    //    old_password
    //    password
    //    is_company (company=1 ,candidate=0)
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];

        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];

    //  [parameters setValue:_candidateid forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpplan  = [[HttpWrapper alloc] init];
        httpplan.delegate=self;
        httpplan.getbool=NO;
        
        [httpplan requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_company_plan",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpplan && httpplan != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
       
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
             marrplandata = [dicsResponse valueForKey:@"data"];
            _lblplanname.text = [NSString stringWithFormat:@"%@",[marrplandata valueForKey:@"PlanName"]];
              _lbldownloadresume.text = [NSString stringWithFormat:@"%@",[marrplandata valueForKey:@"TotalDownload"]];
              _lblremainingdownload.text = [NSString stringWithFormat:@"%@",[marrplandata valueForKey:@"RemainDownload"]];
              _lblexpirydate.text = [NSString stringWithFormat:@"%@",[marrplandata valueForKey:@"expirydate"]];
              _lblsubcribedate.text = [NSString stringWithFormat:@"%@",[marrplandata valueForKey:@"subscribedate"]];
      
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
    }
     [APP_DELEGATE hideLoadingView];
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
@end
