//
//  MyPlanView.h
//  Infinity Dynamics
//
//  Created by My Mac on 21/06/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface MyPlanView : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httpplan;
    NSMutableArray *marrplandata;
    UIView *picker;   /////menu picker view
}
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UILabel *lblplanname;
@property (strong, nonatomic) IBOutlet UILabel *lbldownloadresume;
@property (strong, nonatomic) IBOutlet UILabel *lblremainingdownload;
@property (strong, nonatomic) IBOutlet UILabel *lblexpirydate;
@property (strong, nonatomic) IBOutlet UILabel *lblsubcribedate;

@end
