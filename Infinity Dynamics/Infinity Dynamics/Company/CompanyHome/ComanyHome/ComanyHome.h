//
//  ComanyHome.h
//  Infinity Dynamics
//
//  Created by My Mac on 25/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"
@interface ComanyHome : UIViewController<HttpWrapperDelegate>{
    AppDelegate *appdelegate;
    HttpWrapper *httplogin;

     UIView *picker;   /////menu picker view
}
@property (strong, nonatomic) IBOutlet UILabel *jobapplication;
- (IBAction)btnmyaccount:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *candidateno;
@property (strong, nonatomic) IBOutlet UILabel *latestcandidateno;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UILabel *postedjobs;
@property (weak, nonatomic) IBOutlet UILabel *ShoreJobResume;
@property (strong, nonatomic) IBOutlet UILabel *publishshorevacancy;
@property (strong, nonatomic) IBOutlet UILabel *shorejobapplnrecv;

- (IBAction)btnMenu:(id)sender;
- (IBAction)btnJobapplication:(id)sender;
- (IBAction)btnPostedjobs:(id)sender;
- (IBAction)btnCandidate:(id)sender;
- (IBAction)btnLatestAvailabel:(id)sender;
- (IBAction)btnShoreJobResume:(id)sender;
-(IBAction)btnpublishshorevacancy_click:(UIButton *)sender;
-(IBAction)shoreapplicationrecv_click:(UIButton *)sender;
@end
