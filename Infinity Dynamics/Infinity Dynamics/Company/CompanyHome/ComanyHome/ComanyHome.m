//
//  ComanyHome.m
//  Infinity Dynamics
//
//  Created by My Mac on 25/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "ComanyHome.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "CandidateJob.h"
#import "Candidatejobs.h"
#import "Managecompanyjoblist.h"
#import "LatestavailableCandidateJobViewController.h"
@interface ComanyHome ()

//SHORE JOB RESUME
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation ComanyHome
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self CallMyMethod];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationController.navigationBarHidden=YES;
    
    self.scrollView.contentSize =CGSizeMake(self.view.frame.size.width, 1580);
    
    [self setvalue];
    [self customSetup];
   
    
     [APP_DELEGATE hideLoadingView];
}
-(void)setvalue
{
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    _candidateno.text = [NSString stringWithFormat:@"%@",appdelegate.totalcandidate];
    _latestcandidateno.text = [NSString stringWithFormat:@"%@",appdelegate.totalLatestcandidate];
    _jobapplication.text = [NSString stringWithFormat:@"%@",appdelegate.totaljobapplication];
    _postedjobs.text = [NSString stringWithFormat:@"%@",appdelegate.totaljob];
    _ShoreJobResume.text = [NSString stringWithFormat:@"%@",appdelegate.totalshorejobResume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
  
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

- (IBAction)btnJobapplication:(id)sender {
    // Publish Vacancies
    
    Candidatejobs *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatejobs"];
    //    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
     s.isfrom = @"shipapp";
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)btnPostedjobs:(id)sender {
    Managecompanyjoblist *s = [self.storyboard instantiateViewControllerWithIdentifier:@"Managecompanyjoblist"];
    s.isfrom = @"shipjob";
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)btnCandidate:(id)sender {
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setObject:@"NO" forKey:@"isHomeButtonFlag"];
    [CountryDataSave synchronize];
    CandidateJob *s =[self.storyboard instantiateViewControllerWithIdentifier:@"CandidateJob"];
    //    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
    s.total_shorejob_candidate = [NSString stringWithFormat:@"%@",appdelegate.totalshorejobResume];
    s.loginUserDataArray = (NSMutableArray *)appdelegate.loginUserData;
    s.HeaderTitleStr = [NSString stringWithFormat:@"All Candidates"];
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)btnLatestAvailabel:(id)sender
{
    // Urgently Available Candidates
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setObject:@"NO" forKey:@"isHomeButtonFlag"];
    [CountryDataSave synchronize];
    LatestavailableCandidateJobViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"LatestavailableCandidateJobViewController"];
    //    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
    
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)btnShoreJobResume:(id)sender
{
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setObject:@"YES" forKey:@"isHomeButtonFlag"];
    [CountryDataSave synchronize];

    CandidateJob *s =[self.storyboard instantiateViewControllerWithIdentifier:@"CandidateJob"];
    s.total_shorejob_candidate = [NSString stringWithFormat:@"%@",appdelegate.totalshorejobResume];
    s.loginUserDataArray = (NSMutableArray *)appdelegate.loginUserData;
    s.HeaderTitleStr = [NSString stringWithFormat:@"Shore Job Resume"];
    s.isShoreJobResumeFlag = [NSString stringWithFormat:@"1"];
    s.ShoreJobResumeFlag = [NSString stringWithFormat:@"YES"];
    s.ShoreJobResumeFlag = [NSString stringWithFormat:@"YES"];
    //[NSString stringWithFormat:@"%@",appdelegate.totalshorejobResume];
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)btnpublishshorevacancy_click:(UIButton *)sender {
    
    Managecompanyjoblist *s = [self.storyboard instantiateViewControllerWithIdentifier:@"Managecompanyjoblist"];
    s.isfrom = @"shorejob";
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)shoreapplicationrecv_click:(UIButton *)sender {
    Candidatejobs *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatejobs"];
    s.isfrom = @"shoreapp";
    s.isdashboard = @"1";
    //    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
    [self.navigationController pushViewController:s animated:NO];
}

#pragma mark - GetAllPosts
-(void)CallMyMethod
{
    //Post Method Request
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
     NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userid = [standardUserDefaults objectForKey:@"email"];
    NSString *password = [standardUserDefaults objectForKey:@"Password"];

    [parameters setValue:userid forKey:@"email"];
    
    [parameters setValue:password forKey:@"password"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplogin  = [[HttpWrapper alloc] init];
        httplogin.delegate=self;
        httplogin.getbool=NO;
        
        [httplogin requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@login",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httplogin && httplogin != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
            [APP_DELEGATE hideLoadingView];
        }
        else{
            dic=[dicsResponse valueForKey:@"data"];
            NSMutableDictionary *Moreinfo=[[NSMutableDictionary alloc]init];
            Moreinfo=[dicsResponse valueForKey:@"more_info"];
            
            appdelegate.loginUserData = (NSMutableArray *)[dicsResponse valueForKey:@"more_info"];
            
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
           // [standardUserDefaults setObject:_txtpassword.text forKey:@"Password"];
            [standardUserDefaults setObject:[[dic  valueForKey:@"companyid"] objectAtIndex:0]forKey:@"UserID"];
            [standardUserDefaults setObject:[[dic  valueForKey:@"mobile"] objectAtIndex:0] forKey:@"mobile"];
            [standardUserDefaults setObject:[[dic  valueForKey:@"ImageURL"] objectAtIndex:0] forKey:@"ImageURL"];
            [standardUserDefaults setObject:[[dic valueForKey:@"companyid"]objectAtIndex:0] forKey:@"companyid"];
            [standardUserDefaults setObject:[[dic valueForKey:@"username"]objectAtIndex:0] forKey:@"username"];
            [standardUserDefaults setObject:[[dic valueForKey:@"usertype"]objectAtIndex:0] forKey:@"usertype"];
            [standardUserDefaults setObject:[[dic valueForKey:@"email"]objectAtIndex:0] forKey:@"email"];
            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]);
            if ([[NSString stringWithFormat:@"%@",Moreinfo] isEqual: @"<null>"] ) {
                
            }else{
                ///moreinfo
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Address"] forKey:@"Address"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"CCode"] forKey:@"CCode"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"CandidateID"] forKey:@"CandidateID"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"CityName"] forKey:@"CityName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"LogoURL"] forKey:@"LogoURL"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Created"] forKey:@"Created"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"DOB"] forKey:@"DOB"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"EmailID"] forKey:@"EmailID"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"EmailID2"] forKey:@"EmailID2"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"FirstName"] forKey:@"FirstName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"ImageUrl"] forKey:@"profileImageUrl"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Indos"] forKey:@"Indos"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"IsActive"] forKey:@"IsActive"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"LastName"] forKey:@"LastName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"MaritalStatus"] forKey:@"MaritalStatus"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"MiddleName"] forKey:@"MiddleName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"MobileNo"] forKey:@"MobileNo"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Nationality"] forKey:@"Nationality"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"NoofChildren"] forKey:@"NoofChildren"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"OtherMobileNo"] forKey:@"OtherMobileNo"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"RankID"] forKey:@"RankID"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"RankName"] forKey:@"RankName"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"TelephoneNo"] forKey:@"TelephoneNo"];
                [standardUserDefaults setObject:[Moreinfo valueForKey:@"Zipcode"] forKey:@"Zipcode"];
            }
            
            appdelegate.totaljob = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_job"]];
            appdelegate.totalcandidate = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_candidate"]];
            appdelegate.totalLatestcandidate = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_latest_candidate"]];
            appdelegate.totaljobapplication = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_job_application"]];
            appdelegate.totalshorejobResume = [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_shorejob"]];
            
            _publishshorevacancy.text =  [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_company_shorejob"]];
            _shorejobapplnrecv.text =  [NSString stringWithFormat:@"%@",[Moreinfo valueForKey:@"total_shorejob_application"]];


            
            NSString *   type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
            [self setvalue];

//            if ([type isEqualToString:@"Candidate"]) {
////                HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
////                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
////                [self.navigationController pushViewController:s animated:NO];
//            }else{
//                ComanyHome *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
//                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
//                [self.navigationController pushViewController:s animated:NO];
//            }
        }
        [APP_DELEGATE hideLoadingView];
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


@end
