//
//  Changepassword.m
//  Infinity Dynamics
//
//  Created by My Mac on 07/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Changepassword.h"
#import "Constants.h"
#import "SWRevealViewController.h"
#import "HomeScreen.h"
#import "ComanyHome.h"
@interface Changepassword ()

@end

@implementation Changepassword

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.navigationController.navigationBarHidden=YES;
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}

-(void)CallMyMethod1
{
//    company_id
//    old_password
//    password
//    is_company (company=1 ,candidate=0)
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
 NSString *      type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
    
    if ([type isEqualToString:@"Candidate"]) {
         [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
         [parameters setValue:_txtoldpassword.text forKey:@"old_password"];
         [parameters setValue:_txtnewpassword.text forKey:@"password"];
         [parameters setValue:@"2" forKey:@"is_company"];
        
    }else{
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
        [parameters setValue:_txtoldpassword.text forKey:@"old_password"];
        [parameters setValue:_txtnewpassword.text forKey:@"password"];
        [parameters setValue:@"1" forKey:@"is_company"];
        
    }
    
  //  [parameters setValue:_candidateid forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpChangepassword  = [[HttpWrapper alloc] init];
        httpChangepassword.delegate=self;
        httpChangepassword.getbool=NO;
        
        [httpChangepassword requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@change_password",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpChangepassword && httpChangepassword != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrchangepassword = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSString *   type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"usertype"]];
            if ([type isEqualToString:@"Candidate"]) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Password"];
 [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
                
                [standardUserDefaults setObject:_txtnewpassword.text forKey:@"Password"];
                
                 NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
                showAlert(AlertTitle,message);
                HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:s animated:NO];
            }else{
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
                
                [standardUserDefaults setObject:_txtnewpassword.text forKey:@"Password"];
                 NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
                showAlert(AlertTitle,message);
                ComanyHome *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
                // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
                [self.navigationController pushViewController:s animated:NO];
            }
            
         
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnSave:(id)sender {
    
    if ([_txtoldpassword.text isEqualToString:@""]) {
        showAlert(AlertTitle,@"Please Enter Old Password");
    }
    else if (![_txtoldpassword.text isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"Password"]])
    {
        showAlert(AlertTitle,@"Incorrect Old Password");
    }
    else if ([_txtnewpassword.text isEqualToString:@""]) {
        showAlert(AlertTitle,@"Please Enter New password");
    }else if ([_txtconfirmpassword.text isEqualToString:@""]) {
        showAlert(AlertTitle,@"Please Re-Enter Password");
    }else {
        if ([_txtoldpassword.text isEqualToString:[[NSUserDefaults standardUserDefaults] valueForKey:@"Password"]]) {
            
            if ([_txtnewpassword.text isEqualToString:_txtconfirmpassword.text]) {
                if ([_txtoldpassword.text isEqualToString:_txtnewpassword.text]) {
                    showAlert(AlertTitle,@"Old Password and New Password cannot be same");
                }else{
                    [APP_DELEGATE showLoadingView:@""];
                    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
                }
            }
            else{
                showAlert(@"Password", @"New Password and Confirm password does not match")
            }
        }
        else{
            showAlert(@"Password", @"Old Password is invalid")
        }
    }
}
@end
