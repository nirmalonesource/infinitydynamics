//
//  Changepassword.h
//  Infinity Dynamics
//
//  Created by My Mac on 07/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface Changepassword : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httpChangepassword;
    NSMutableArray *marrchangepassword;
    UIView *picker;   /////menu picker view
}
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UITextField *txtoldpassword;
@property (strong, nonatomic) IBOutlet UITextField *txtnewpassword;
@property (strong, nonatomic) IBOutlet UITextField *txtconfirmpassword;
- (IBAction)btnSave:(id)sender;

@end
