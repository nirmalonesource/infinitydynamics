//
//  resumedownloadedList.m
//  Infinity Dynamics
//
//  Created by My Mac on 22/07/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "resumedownloadedList.h"
#import "Constants.h"
#import "Candidatedetail1.h"
#import "CandidateJobcell.h"
#import "UIImageView+Haneke.h"
#import "ComanyHome.h"
#import "HcdDateTimePickerView.h"

@interface resumedownloadedList ()
{
     HcdDateTimePickerView * dateTimePickerView;
    BOOL isavailableall;
}

@property (weak, nonatomic) IBOutlet UIButton *FilterByBTN;
@property (weak, nonatomic) IBOutlet UILabel *FilterByLabel;
@property (weak, nonatomic) IBOutlet UIImageView *FilterImage;


@property (weak, nonatomic) IBOutlet UIView *FilterByView;

@property (weak, nonatomic) IBOutlet UITextField *FromDateTextField;
@property (weak, nonatomic) IBOutlet UIButton *SelectFromDateBTN;

@property (weak, nonatomic) IBOutlet UITextField *ToDateTextField;
@property (weak, nonatomic) IBOutlet UIButton *SelectToDateBTN;

@end

@implementation resumedownloadedList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    // self.tableview.se = UITableViewCellSeparatorStyleNone;
    
    //FilterByLabel;
    //FilterImage;
    
    //_FilterByLabel.frame.origin.x = self.view.frame.origin.x;
    
    self.FilterByView.hidden = YES;
    
    _count.font = [UIFont systemFontOfSize:12];
    _count.textColor = [UIColor lightGrayColor];
    _count.layer.borderWidth = 1.0f;
    _count.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _count.layer.cornerRadius = 5.0f;
    _count.textAlignment = UITextAlignmentCenter;
}

//**************************************************************
- (IBAction)FilterByBTNClick:(id)sender
{
    self.FilterByView.hidden = NO;
}

- (IBAction)SelectFromDateBTNClick:(id)sender
{
    __block resumedownloadedList *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger year = [components year];
    [dateTimePickerView setMinYear:1900];
    [dateTimePickerView setMaxYear:year];
    dateTimePickerView.maxYear = year;
    dateTimePickerView.topViewColor = [UIColor darkGrayColor];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];

    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        //        dateofb=datetimeStr;
        
        //--------DATE CONVERT------------------
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:datetimeStr];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString *finaldt=[formatedt stringFromDate:fulldate];
        
        //weakSelf.txtLeaveingdate.text = finaldt;
        weakSelf.FromDateTextField.text = finaldt;
        
    };

    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }
}

- (IBAction)SelectToDateBTNClick:(id)sender
{
    __block resumedownloadedList *weakSelf = self;
    dateTimePickerView = [[HcdDateTimePickerView alloc] initWithDatePickerMode:DatePickerDateMode defaultDateTime:[[NSDate alloc]initWithTimeIntervalSinceNow:1000]];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger year = [components year];
    [dateTimePickerView setMinYear:1900];
    [dateTimePickerView setMaxYear:year];
    dateTimePickerView.maxYear = year;
    
    dateTimePickerView.topViewColor = [UIColor darkGrayColor];
    dateTimePickerView.buttonTitleColor = [UIColor whiteColor];
    
    dateTimePickerView.clickedOkBtn = ^(NSString * datetimeStr){
        NSLog(@"%@", datetimeStr);
        //        dateofb=datetimeStr;
        
        //--------DATE CONVERT------------------
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:datetimeStr];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString *finaldt=[formatedt stringFromDate:fulldate];
        
        weakSelf.ToDateTextField.text = finaldt;
    };
    
    if (dateTimePickerView) {
        [self.view addSubview:dateTimePickerView];
        [dateTimePickerView showHcdDateTimePicker];
    }
}

- (IBAction)AvailableAllClick:(id)sender
{
    self.FilterByView.hidden = YES;
    [APP_DELEGATE showLoadingView:@""];
    isavailableall = YES;
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}

- (IBAction)OkBTNClick:(id)sender
{
    self.FilterByView.hidden = YES;
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}

//**************************************************************

- (IBAction)btnBack:(id)sender
{
    ComanyHome *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ComanyHome"];
    //    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
    //
    [self.navigationController pushViewController:s animated:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CandidateJobcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CandidateJobcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    _count.text = [NSString stringWithFormat:@"%lu",(unsigned long)marrcandidatelist.count];
        return marrcandidatelist.count;
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(CandidateJobcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"CanidateName"] ];
        
        NSString * date = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"DownloadedDate"] ];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
    
        cell.lbldate.text = [NSString stringWithFormat:@"%@",date];
        //      cell.lbldate.text = [NSString stringWithFormat:@"%@",[[filterdarray objectAtIndex:indexPath.section] valueForKey:@"last_login"] ];
        cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"AppliedRankName"] ];
        
        NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrcandidatelist objectAtIndex:indexPath.section] valueForKey:@"ImageURL"]];
        NSURL *url = [NSURL URLWithString:profilepicname];
        [cell.imageview hnk_setImageFromURL:url];
        [APP_DELEGATE hideLoadingView];
    
    [cell.btntouch setTag:indexPath.section];
    
    [cell.btntouch addTarget:self action:@selector(checkBoxClicked:event:)forControlEvents:UIControlEventTouchUpInside];

}
- (void)checkBoxClicked:(id)sender event:(id)event
{
    UIButton *btn=sender;
    NSLog(@"button was clicked for the following row %li", (long)btn.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[btn tag]  inSection:0];
    NSLog(@"indexpath.row %ld", (long)indexPath.row);
    NSLog(@"indexpath.row %ld", (long)indexPath.section);
   
        
        //   http://clientsdemoarea.com/projects/flexcash/web_services/refund_request?userid=1&joinid=21&flexid=1&flexuserid=3
        //  [APP_DELEGATE showLoadingView:@""];
        Candidatedetail1 *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatedetail1"];
        s.detail = [marrcandidatelist objectAtIndex:indexPath.row] ;
          s.downloadlist = YES;
        [self.navigationController pushViewController:s animated:NO];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Candidatedetail1 *s =[self.storyboard instantiateViewControllerWithIdentifier:@"Candidatedetail1"];
    s.detail = [marrcandidatelist objectAtIndex:indexPath.section] ;
    s.downloadlist = YES;
    [self.navigationController pushViewController:s animated:NO];
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}

-(void)CallMyMethod1
{
    NSString * FromDateString;
    NSString * ToDateString;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    if (![_FromDateTextField.text isEqualToString:@""])
    {
        NSString * date1 = [NSString stringWithFormat:@"%@",_FromDateTextField.text];
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSDate *fulldate=[formatedt dateFromString:date1];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        
        FromDateString = [formatedt stringFromDate:fulldate];
        NSLog(@"FromDate = %@",FromDateString);
          [parameters setValue:FromDateString forKey:@"from_date"];
        
       
    }
    if (![_ToDateTextField.text isEqualToString:@""])
        {
            NSString * date2 = [NSString stringWithFormat:@"%@",_ToDateTextField.text];
            NSDateFormatter *formatedt2=[[NSDateFormatter alloc]init];
            [formatedt2 setDateFormat:@"dd-MMM-yyyy"];
            NSDate *fulldate2=[formatedt2 dateFromString:date2];
            [formatedt2 setDateFormat:@"yyyy-MM-dd"];
            
            ToDateString = [formatedt2 stringFromDate:fulldate2];
            NSLog(@"ToDate = %@",ToDateString);
             [parameters setValue:ToDateString forKey:@"to_date"];
        }
    if (isavailableall)
    {
        isavailableall = NO;
        parameters = [[NSMutableDictionary alloc]init];
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"company_id"];
    }
   
    NSLog(@"parameters AAAAAA = %@",parameters);
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcandidatelist  = [[HttpWrapper alloc] init];
        httpcandidatelist.delegate=self;
        httpcandidatelist.getbool=NO;
        
        [httpcandidatelist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_company_download_resume",MainUrl] param:[parameters copy]];
    });
}



-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpcandidatelist && httpcandidatelist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
      NSString  *data=[dicsResponse valueForKey:@"data"];
//        if (dic i) {
//
//        }
        marrcandidatelist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrcandidatelist);
            _lblnodatafound.hidden = YES;
            if ([[dicsResponse valueForKey:@"data"] isKindOfClass:[NSString class]])
            {
            }
            else
            {
                [self.tableview reloadData];

            }

            [APP_DELEGATE hideLoadingView];
        }
        else{
             _lblnodatafound.hidden = NO;
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
    }
 
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

@end
