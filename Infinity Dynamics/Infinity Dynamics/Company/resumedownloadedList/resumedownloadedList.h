//
//  resumedownloadedList.h
//  Infinity Dynamics
//
//  Created by My Mac on 22/07/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "AppDelegate.h"
@interface resumedownloadedList : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>{
  
    HttpWrapper *httpcandidatelist;
   
    NSMutableArray *marrcandidatelist;
    AppDelegate * appdelegate;
 
 
}

@property (strong, nonatomic) IBOutlet UILabel *count;
@property (strong, nonatomic) IBOutlet UILabel *lblnodatafound;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@end
