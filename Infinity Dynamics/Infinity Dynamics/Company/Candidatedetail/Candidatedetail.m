//
//  Candidatedetail.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Candidatedetail.h"
#import "UIImageView+Haneke.h"
#import "candidateBuildResume.h"
#import "WebViewController.h"
@interface Candidatedetail ()
@property (weak, nonatomic) IBOutlet UIView *View1Display;
@property (weak, nonatomic) IBOutlet UIView *View2Display;
@property (strong, nonatomic) IBOutlet UIButton *btncv;
@property (strong, nonatomic) IBOutlet UIButton *btncv1;

@end

@implementation Candidatedetail
@synthesize iSCandidateDetailsFlag;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    if ([[_detail valueForKey:@"CVFile"] isEqualToString:@""])
    {
        _btncv.hidden = YES;
         _btncv1.hidden = YES;
    }
    else
    {
        _btncv.hidden = NO;
        _btncv1.hidden = NO;
    }
    
    NSLog(@"detail = %@",_detail);
    NSLog(@"isShoreJobResumeFlag = %@",_total_shorejob_candidate);
    NSLog(@"loginUserDataArray = %@", _loginUserDataArray);
    NSLog(@"HeaderTitleStr = %@",_HeaderTitleStr);
    NSLog(@"isShoreJobResumeFlag = %@",_isShoreJobResumeFlag);
    NSLog(@"ShoreJobResumeFlag = %@",_ShoreJobResumeFlag);
    
    //&& _loginUserDataArray == nil && _HeaderTitleStr == nil && _isShoreJobResumeFlag == nil && _ShoreJobResumeFlag == nil
    
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    NSString * flag = [NSString stringWithFormat:@"%@",[CountryDataSave valueForKey:@"isHomeButtonFlag"]];
    NSLog(@"flag = %@",flag);
    
    if ([flag isEqualToString:@"YES"])
    {
        _View1Display.hidden = YES;
        _View2Display.hidden = NO;
    }
    else
    {
        _View1Display.hidden = NO;
        _View2Display.hidden = YES;
    }
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[_detail  valueForKey:@"ImageURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [_profileimage hnk_setImageFromURL:url];
    _lblName.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CandidateCode"]];
    
    _lblAppliedrank2.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"AppliedRank"]];
    _lblAppliedrank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"AppliedRankName"]];

    _lblCurrentrank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"RankName"]];
    _lblCurrentrank2.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"RankName"]];
    
    _lblAppliedShiptype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"AppliedShipName"]];
    
    _lblSeatime.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"SeatimeInLastRank"]];
    
    _lblResumeupdated.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Modified"]];
    _lblResumeupdated2.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Modified"]];
    
    _lblLastvisted2.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"last_visited"]];
    _lblLastvisted.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"LastVisited"]];
    
    _profileimage.layer.masksToBounds = YES;
    _profileimage.layer.cornerRadius = _profileimage.frame.size.width/2;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnDownloadresume:(id)sender {
    if (_downloadlist == YES) {
        candidateBuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"candidateBuildResume"];
        s.url = [_detail valueForKey:@"ResumeLink"] ;
        s.candidateid = [_detail valueForKey:@"CandidateID"] ;
        s.downloadlist = YES;
        s.headertitle = @"Built Resume";
        [self.navigationController pushViewController:s animated:NO];
        
    }else{
    
    candidateBuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"candidateBuildResume"];
        s.downloadlist = NO;
    s.candidateid = [_detail valueForKey:@"CandidateID"] ;
        s.headertitle = @"Built Resume";

    [self.navigationController pushViewController:s animated:NO];
    }
}

- (IBAction)btn_preview:(id)sender {
    candidateBuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"candidateBuildResume"];
    s.downloadlist = NO;
    s.Preview = YES;
    s.headertitle = @"Preview Resume";
    s.candidateid = [_detail valueForKey:@"CandidateID"] ;
    
    
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)btnback:(id)sender {
     [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)viewcv_click:(UIButton *)sender {
    WebViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    s.website =   [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CVFile"]];
    s.strheader = @"View Uploaded CV";
    [self.navigationController pushViewController:s animated:NO];
}
@end
