//
//  Candidatedetail1.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Candidatedetail1 : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (nonatomic, assign) BOOL downloadlist;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblMaritialstatus;
@property (weak, nonatomic) IBOutlet UILabel *lblCandidateCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentRank;
@property (strong, nonatomic) IBOutlet UILabel *lblMobileno;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailbelFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblDateofapplication;
@property (weak, nonatomic) IBOutlet UILabel *lblShiptype;

@property (weak, nonatomic) IBOutlet UIButton *btnPreview;
@property (strong, nonatomic) IBOutlet UILabel *lblRank;
- (IBAction)btnDownloadresume:(id)sender;
- (IBAction)btnPrieview:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnBack;
- (IBAction)btnback:(id)sender;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UIView *shipdetailvw;
@property (strong, nonatomic) IBOutlet UIView *shoredetailvw;
@property (strong, nonatomic) IBOutlet UILabel *lblemail;
@property (strong, nonatomic) IBOutlet UILabel *lblmono;
@property (strong, nonatomic) IBOutlet UILabel *lbldesignation;
@property (strong, nonatomic) IBOutlet UILabel *lbldateofapp;
@property (strong, nonatomic) NSString *isfrom;

@end
