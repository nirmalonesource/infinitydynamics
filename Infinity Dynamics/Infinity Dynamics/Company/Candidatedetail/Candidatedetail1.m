//
//  Candidatedetail.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Candidatedetail1.h"
#import "UIImageView+Haneke.h"
#import "candidateBuildResume.h"
#import "WebViewController.h"
@interface Candidatedetail1 ()
@property (strong, nonatomic) IBOutlet UIView *viewdownload;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *mono;
@property (strong, nonatomic) IBOutlet UILabel *appliedrank;
@property (strong, nonatomic) IBOutlet UILabel *currentrank;
@property (strong, nonatomic) IBOutlet UILabel *resumeupdated;
@property (strong, nonatomic) IBOutlet UILabel *datedownloaded;
@property (strong, nonatomic) IBOutlet UIButton *btncv1;
@property (strong, nonatomic) IBOutlet UIButton *btncv2;
@property (strong, nonatomic) IBOutlet UIButton *btncv3;

@end

@implementation Candidatedetail1


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([[_detail valueForKey:@"CVFile"] isEqualToString:@""])
    {
        _btncv1.hidden = YES;
        _btncv2.hidden = YES;
        _btncv3.hidden = YES;
    }
    else
    {
        _btncv1.hidden = NO;
        _btncv2.hidden = NO;
        _btncv3.hidden = NO;
    }
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[_detail  valueForKey:@"ImageURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [_profileimage hnk_setImageFromURL:url];
    _profileimage.layer.masksToBounds = YES;
    _profileimage.layer.cornerRadius = _profileimage.frame.size.width/2;

    if ([_isfrom isEqualToString:@"shipapp"])
    {
        _shoredetailvw.hidden = YES;
        _viewdownload.hidden = YES;
        
        _lblName.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CanidateName"]];
        _lblCandidateCode.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CandidateCode"]];
        _lblEmail.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"EmailID"]];
        _lblMobileno.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"MobileNo"]];
        _lblMaritialstatus.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"MaritalStatus"]];
        _lblCurrentRank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CurrentRank"]];
        _lblAvailbelFrom.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"AvailableFrom"]];
        _lblRank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"RankName"]];
        _lblDateofapplication.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"DateofApplication"]];
        _lblShiptype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShipType"]];
       
    }
   else if (_downloadlist == YES)
    {
        _shoredetailvw.hidden = YES;
        _shipdetailvw.hidden = YES;
        
        _lblName.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CanidateName"]];
        _lblCandidateCode.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CandidateCode"]];
        _email.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"EmailID"]];
        _mono.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"MobileNo"]];
        _appliedrank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"AppliedRankName"]];
        _currentrank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"RankName"]];
        _resumeupdated.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Modified"]];
        _datedownloaded.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"DownloadedDate"]];
        
    }
    else
    {
        _shipdetailvw.hidden = YES;
        _viewdownload.hidden = YES;
        
        _lblCandidateCode.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CandidateCode"]];
        _lblName.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CandidateName"]];
        _lblemail.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"EmailID"]];
        _lblmono.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"MobileNo"]];
        _lbldesignation.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"AppliedRankName"]];
        _lbldateofapp.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"LastVisited"]];
       
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnDownloadresume:(id)sender {
    if (_downloadlist == YES) {
        candidateBuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"candidateBuildResume"];
        s.url = [_detail valueForKey:@"ResumeLink"] ;
        s.candidateid = [_detail valueForKey:@"CandidateID"] ;
        s.downloadlist = YES;
        s.headertitle = @"Built Resume";
        [self.navigationController pushViewController:s animated:NO];
        
    }else{
    
    candidateBuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"candidateBuildResume"];
        s.downloadlist = NO;
        s.candidateid = [_detail valueForKey:@"CandidateID"] ;
        [self.navigationController pushViewController:s animated:NO];
    }
}
- (IBAction)btnPrieview:(id)sender {
    
        
        candidateBuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"candidateBuildResume"];
        s.downloadlist = NO;
        s.Preview = YES;
        s.candidateid = [_detail valueForKey:@"CandidateID"] ;
        s.headertitle = @"Preview Resume";
        [self.navigationController pushViewController:s animated:NO];
    
}
- (IBAction)btnback:(id)sender {
     [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)viewcv_click:(UIButton *)sender {
    WebViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    s.website =   [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CVFile"]];
    s.strheader = @"View Uploaded CV";
    [self.navigationController pushViewController:s animated:NO];
}
@end
