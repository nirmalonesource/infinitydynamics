//
//  Candidatedetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Candidatedetail : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblResumeupdated;
@property (weak, nonatomic) IBOutlet UILabel *lblSeatime;
@property (weak, nonatomic) IBOutlet UILabel *lblAppliedShiptype;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentrank;
@property (weak, nonatomic) IBOutlet UILabel *lblAppliedrank;

@property (weak, nonatomic) IBOutlet UILabel *lblResumeupdated2;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentrank2;
@property (weak, nonatomic) IBOutlet UILabel *lblAppliedrank2;
@property (weak, nonatomic) IBOutlet UILabel *lblLastvisted2;


@property (strong, nonatomic) IBOutlet UIImageView *profileimage;
@property (nonatomic, assign) BOOL downloadlist;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblLastvisted;
- (IBAction)btnDownloadresume:(id)sender;
- (IBAction)btn_preview:(id)sender;

- (IBAction)btnback:(id)sender;
@property (nonatomic, strong) NSMutableArray *detail;
@property (nonatomic, strong) NSString * iSCandidateDetailsFlag;

@property (nonatomic, retain) NSString * total_shorejob_candidate;
@property (nonatomic, retain) NSString * isShoreJobResumeFlag;
@property (nonatomic, retain) NSString * HeaderTitleStr;
@property (nonatomic, retain) NSString * ShoreJobResumeFlag;
@property (nonatomic, retain) NSMutableArray * loginUserDataArray;


@end
