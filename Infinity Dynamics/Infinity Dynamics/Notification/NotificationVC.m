//
//  NotificationVC.m
//  Infinity Dynamics
//
//  Created by My Mac on 13/04/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import "NotificationVC.h"
#import "SWRevealViewController.h"
#import "myModel.h"
#import "Constants.h"
#import "NotificationCell.h"
#import "UIImageView+Haneke.h"
@interface NotificationVC ()
{
    UIView *picker;   /////menu picker view
}
@property (weak, nonatomic) IBOutlet UIButton *btnmenu;

@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
       [self customSetup];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
   return  UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return marrRecentjob.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    if (!cell)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    }
       cell.lbl1.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.row] valueForKey:@"Notification"] ];
    
    NSString *NotificationDate = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.row] valueForKey:@"NotificationDate"] ];
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *fulldate=[formatedt dateFromString:NotificationDate];
    [formatedt setDateFormat:@"dd-MM-yyyy"];
    NSString * JoiningDate = [formatedt stringFromDate:fulldate];
    cell.lbl2.text = JoiningDate;
    
     cell.lbl3.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.row] valueForKey:@"NotificationDesc"] ];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.row] valueForKey:@"ImageURL"]];
    if ([profilepicname isEqualToString:@""])
    {
        cell.imgheight.constant = 1;
        
    }
    else
    {
        cell.imgheight.constant = 200;
    }
    NSURL *url = [NSURL URLWithString:profilepicname];
    // [cell.imageview hnk_setImageFromURL:url];
    [cell.imgv hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(NotificationCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"CompanyName"] ];
//
//    cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"JobTitle"] ];
//    cell.lblshiptype.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"ShipType"] ];
//
//    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
//    NSURL *url = [NSURL URLWithString:profilepicname];
//    // [cell.imageview hnk_setImageFromURL:url];
//    [cell.imageview hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];
//    [APP_DELEGATE hideLoadingView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    //    s.detail = [marrRecentjob objectAtIndex:indexPath.section] ;
    //
    //    [self.navigationController pushViewController:s animated:NO];
    
    
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    //candidate_id:1
    //keyword:oil
    //searchtype:3
    //[parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
//    [parameters setValue:@"" forKey:@"candidate_id"];
//    [parameters setValue:@"" forKey:@"keyword"];
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
   
    [parameters setValue:[CountryDataSave valueForKey:@"id"] forKey:@"user_id"];
    NSLog(@"PARAMETERS: %@",parameters);
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpRecentjob  = [[HttpWrapper alloc] init];
        httpRecentjob.delegate=self;
        httpRecentjob.getbool=NO;
        
        [httpRecentjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_notification_dtl",MainUrl] param:[parameters copy]];
    });
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpRecentjob && httpRecentjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrRecentjob = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrRecentjob);
            [self.tablenotification reloadData];
        }
        else{
            
        }
        [APP_DELEGATE hideLoadingView];
        
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
@end
