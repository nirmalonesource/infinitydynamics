//
//  NotificationCell.h
//  Infinity Dynamics
//
//  Created by My Mac on 06/08/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl1;
@property (strong, nonatomic) IBOutlet UILabel *lbl2;
@property (strong, nonatomic) IBOutlet UILabel *lbl3;
@property (strong, nonatomic) IBOutlet UIImageView *imgv;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imgheight;

@end

NS_ASSUME_NONNULL_END
