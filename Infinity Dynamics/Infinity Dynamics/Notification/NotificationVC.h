//
//  NotificationVC.h
//  Infinity Dynamics
//
//  Created by My Mac on 13/04/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotificationVC : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper *httpRecentjob;
    NSMutableArray *marrRecentjob;
    AppDelegate * appdelegate;
}
@property (strong, nonatomic) IBOutlet UITableView *tablenotification;

@end

NS_ASSUME_NONNULL_END
