//
//  WebViewController.m
//  Infinity Dynamics
//
//  Created by My Mac on 15/03/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_btnheader setTitle:_strheader forState:UIControlStateNormal];
    NSURL *url = [NSURL URLWithString:_website];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [_webview loadRequest:urlRequest];
   // [self.view addSubview:webView];
}


- (IBAction)back_click:(UIButton *)sender {
    
    [[self navigationController] popViewControllerAnimated:YES];
}
@end
