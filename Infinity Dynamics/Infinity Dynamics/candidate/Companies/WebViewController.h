//
//  WebViewController.h
//  Infinity Dynamics
//
//  Created by My Mac on 15/03/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController
@property (strong, nonatomic) NSString *website;
@property (strong, nonatomic) NSString *strheader;
@property (strong, nonatomic) IBOutlet WKWebView *webview;
//@property (weak, nonatomic) IBOutlet UIWebView *webview;
- (IBAction)back_click:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnheader;

@end

NS_ASSUME_NONNULL_END
