//
//  InviteTCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/4/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompaniesCell : UICollectionViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *imageview;
@property (weak, nonatomic) IBOutlet UILabel *Address;
@property (weak, nonatomic) IBOutlet UILabel *CompanyName;




@end
