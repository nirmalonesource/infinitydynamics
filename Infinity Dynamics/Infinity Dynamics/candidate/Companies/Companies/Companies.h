//
//  Companies.h
//  Infinity Dynamics
//
//  Created by My Mac on 10/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface Companies : UIViewController<HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{
      UIView *picker;   /////menu picker view
    HttpWrapper *httpcompanies;
     NSMutableArray *marrcompaniesjob;
}
@property (weak, nonatomic) IBOutlet UIButton *btnheader;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@end
