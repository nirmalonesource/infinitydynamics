//
//  COCDetails.m
//  Infinity Dynamics
//
//  Created by My Mac on 13/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "COCDetails.h"
#import "Constants.h"
#import "COCDetailscell.h"
#import "AddCOCDetail.h"
#import "MyProfileCandidate.h"
@interface COCDetails ()

@end

@implementation COCDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    COCDetailscell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"COCDetailscell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return marrdetaillist.count;
    
    
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(COCDetailscell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.lblcertificatetype.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section]valueForKey:@"CertificateNo"] ];
    cell.lblNumber.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section]valueForKey:@"CertificateType"] ];
    NSString * date = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"Validity"]];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    NSString * JoiningDate = [formatedt stringFromDate:fulldate];
    cell.lblvalidity.text = [NSString stringWithFormat:@"%@",JoiningDate];
    cell.lblPlaceofissue.text = [NSString stringWithFormat:@"%@",[[marrdetaillist  objectAtIndex:indexPath.section] valueForKey:@"Authority"]];
    [cell.btnedit setTag:indexPath.section];
    [ cell.btnedit addTarget:self action:@selector(switchpress:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btndelete setTag:indexPath.section];
    [ cell.btndelete addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
}
- (IBAction)switchpress:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    NSLog(@"here :: %@",[marrdetaillist objectAtIndex:indexPath.row]  );
    AddCOCDetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"AddCOCDetail"];
    s.detail = [marrdetaillist objectAtIndex:indexPath.row] ;
    s.Edit = YES;
    s.indos = indos;
    [self.navigationController pushViewController:s animated:NO];
    
    
}
- (IBAction)buttonPressed:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag]inSection:0];
    NSLog(@"here :: %@",[[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"COCDetailsID"] );
    
    seaman_id = [NSString stringWithFormat:@"%@",[[marrdetaillist objectAtIndex:indexPath.row] valueForKey:@"COCDetailsID"] ];
    [self performSelector:@selector(delete) withObject:nil afterDelay:0.1];
}
-(void)delete
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Delete" message:@"Want to Delete This Record?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    [APP_DELEGATE showLoadingView:@""];

                                    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
                                    
                                    [parameters setValue:seaman_id forKey:@"coc_id"];
                                    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                                        httpdelete  = [[HttpWrapper alloc] init];
                                        httpdelete.delegate=self;
                                        httpdelete.getbool=NO;
                                        
                                        [httpdelete requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@delete_coc",MainUrl] param:[parameters copy]];
                                    });
                                    /** What we write here???????? **/
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"CANCEL"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed Cancel, button");
                                   // call method whatever u need
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
   
    
    
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httdetailist  = [[HttpWrapper alloc] init];
        httdetailist.delegate=self;
        httdetailist.getbool=NO;
        
        [httdetailist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_coc_dtl",MainUrl] param:[parameters copy]];
    });
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httdetailist && httdetailist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrdetaillist = [dicsResponse valueForKey:@"data"];
        indos = [dicsResponse valueForKey:@"Indos"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
            [self.tableview reloadData];
        }
        else{
            
            
        }
    }
    else if (wrapper == httpdelete && httpdelete != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrdetaillist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
            showAlert(AlertTitle, @"Deleted Successfully");

        }
        else{
            
            
        }
    }
    
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (IBAction)btnBack:(id)sender {
    MyProfileCandidate *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
    
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)btnPlusbutton:(id)sender {
    AddCOCDetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"AddCOCDetail"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}

@end
