//
//  STCWDetails.h
//  Infinity Dynamics
//
//  Created by My Mac on 15/03/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"

@interface STCWDetails : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httdetailist,*httpdelete;
    NSMutableArray *marrdetaillist;
    NSString * seaman_id;
}
- (IBAction)btnPlusbutton:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)btnBack:(id)sender;

@end
