//
//  BuildResume.m
//  Infinity Dynamics
//
//  Created by My Mac on 17/03/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "BuildResume.h"
#import "Constants.h"
@interface BuildResume ()

@end

@implementation BuildResume

- (void)viewDidLoad {
    [super viewDidLoad];
    _webview.navigationDelegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnBack:(id)sender {
     [self.navigationController popViewControllerAnimated:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpJobappliedlist  = [[HttpWrapper alloc] init];
        httpJobappliedlist.delegate=self;
        httpJobappliedlist.getbool=NO;
        
        [httpJobappliedlist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@build_resume",MainUrl] param:[parameters copy]];
    });
    
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpJobappliedlist && httpJobappliedlist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrJobappliedlist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            _url = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"resume_url"]];
//            _webview.delegate = self;
//            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:_url]];
//            [_webview loadData:data MIMEType:@"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" textEncodingName:@"utf-8" baseURL:nil];
            
//            NSString *encodedString = [_url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//            _webview.delegate = self;
//            [self.webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:encodedString]]];
        //    [_webview setDataDetectorTypes:UIDataDetectorTypeLink];
            
            
            
         //   NSURL *targetURL = [NSURL URLWithString:_url];
            
            //[[NSBundle mainBundle] URLForResource:@"document" withExtension:@"pdf"];
            
//            NSURLRequest *request = [NSURLRequest requestWithURL:<#(nonnull NSURL *)#> cachePolicy:<#(NSURLRequestCachePolicy)#> timeoutInterval:<#(NSTimeInterval)#>];
//            [_webview loadRequest:request];
            
          //  NSURL *url = [NSURL URLWithString:_url];
            NSString* webStringURL = [_url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            NSURL* url = [NSURL URLWithString:webStringURL];

            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
            [_webview loadRequest:urlRequest];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
    }
    
    
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
//- (void)webViewDidStartLoad:(UIWebView *)webView{
//    [APP_DELEGATE showLoadingView:@""];
//
//}
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    [APP_DELEGATE hideLoadingView];
//}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
     [APP_DELEGATE showLoadingView:@""];
}
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
     [APP_DELEGATE hideLoadingView];
}
@end
