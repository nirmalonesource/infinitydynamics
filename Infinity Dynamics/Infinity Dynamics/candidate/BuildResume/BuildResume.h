//
//  BuildResume.h
//  Infinity Dynamics
//
//  Created by My Mac on 17/03/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import <WebKit/WebKit.h>

@interface BuildResume : UIViewController<HttpWrapperDelegate,WKNavigationDelegate>{
    HttpWrapper *httdetailist,*httpJobappliedlist;
    NSMutableArray *marrJobappliedlist;
    NSString * seaman_id;
}
//@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet WKWebView *webview;
@property (nonatomic, strong) NSString * url;
- (IBAction)btnBack:(id)sender;
@end
