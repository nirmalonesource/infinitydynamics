//
//  HomeScreen.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "HomeScreen.h"
#import "homecollectionCell.h"
#import "Constants.h"
#import "Hometablecell.h"
#import "UIImageView+Haneke.h"
#import "SWRevealViewController.h"
#import "JobDetails.h"
#import "MyProfileCandidate.h"
#import "myModel.h"

@interface HomeScreen ()

@end

@implementation HomeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBarHidden=YES;
       [self customSetup];
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(CallMyMethod2) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(CallMyMethod3) withObject:nil afterDelay:0.1];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return marrhttpFeaturedjob.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    
    homecollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrhttpFeaturedjob valueForKey:@"JobTitle"]objectAtIndex:indexPath.row]];
     cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[marrhttpFeaturedjob valueForKey:@"ShipType"]objectAtIndex:indexPath.row]];
  
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrhttpFeaturedjob objectAtIndex:indexPath.row] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [cell.imageview hnk_setImageFromURL:url];
    [APP_DELEGATE hideLoadingView];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   // [self SelectedMenu:indexPath.row];
    
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [marrhttpFeaturedjob objectAtIndex:indexPath.row] ;
    
    [self.navigationController pushViewController:s animated:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Hometablecell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Hometablecell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return tblarray.count;
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(Hometablecell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_segment.selectedSegmentIndex == 0) {
        
       
    }
    else if (_segment.selectedSegmentIndex == 1) {
        
    }
  
    
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
   // [cell.imageview hnk_setImageFromURL:url];
    [cell.imageview hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];
    if (_segment.selectedSegmentIndex == 0) {
        cell.LblName.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"JobTitle"]];
       cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"ShipType"]];
        cell.lblcompanyname.hidden = YES;
        
        [cell.LblName setFrame:CGRectMake(cell.LblName.frame.origin.x, 25, cell.LblName.frame.size.width, cell.LblName.frame.size.height)];
        [cell.lbldescription setFrame:CGRectMake(cell.lbldescription.frame.origin.x, 40, cell.lbldescription.frame.size.width, cell.lbldescription.frame.size.height)];
    }
    else if(_segment.selectedSegmentIndex == 1) {
        
        [cell.LblName setFrame:CGRectMake(cell.LblName.frame.origin.x, 34, cell.LblName.frame.size.width, cell.LblName.frame.size.height)];
        [cell.lbldescription setFrame:CGRectMake(cell.lbldescription.frame.origin.x, 48, cell.lbldescription.frame.size.width, cell.lbldescription.frame.size.height)];
        
        cell.lblcompanyname.hidden = NO;
        cell.lblcompanyname.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"CompanyName"]];
        cell.LblName.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"JobTitle"]];
        cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"DesignationName"]];
    }
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    
    if (_segment.selectedSegmentIndex == 0) {
        
       [self callcountapi:@"0" companyid:[[tblarray objectAtIndex:indexPath.section] valueForKey:@"JobID"]];
    }
    else if(_segment.selectedSegmentIndex == 1) {
        
       [self callcountapi:@"1" companyid:[[tblarray objectAtIndex:indexPath.section] valueForKey:@"ShoreJobID"]];
    }
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [tblarray objectAtIndex:indexPath.section] ;
    s.isshoreselect =[NSString stringWithFormat:@"%d",(int)_segment.selectedSegmentIndex];
    [self.navigationController pushViewController:s animated:NO];
    
}

-(void)callcountapi:(NSString *)type companyid:(NSString *)companyid
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:type forKey:@"Type"];
    [AddPost setValue:companyid forKey:@"ID"];
    [AddPost setValue:[myModel getIPAddress] forKey:@"IPAddress"];
    
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@ship_shore_job_visit_count",MainUrl] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplatestjob  = [[HttpWrapper alloc] init];
        httplatestjob.delegate=self;
        httplatestjob.getbool=NO;
        
        [httplatestjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_latestjob_list",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)CallMyMethod2
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpFeaturedjob  = [[HttpWrapper alloc] init];
        httpFeaturedjob.delegate=self;
        httpFeaturedjob.getbool=NO;
        
        [httpFeaturedjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_featuredjob_list",MainUrl] param:[parameters copy]];
    });
    
    
    
}
-(void)CallMyMethod3
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpShorejob  = [[HttpWrapper alloc] init];
        httpShorejob.delegate=self;
        httpShorejob.getbool=NO;
        
        [httpShorejob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_shorejob_list",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httplatestjob && httplatestjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrlatestjob = [dicsResponse valueForKey:@"data"];
        tblarray = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
          
              [self.tableview reloadData];
        }
        else{
            
            
        }
    }
    else if (wrapper == httpFeaturedjob && httpFeaturedjob != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrhttpFeaturedjob = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
           
          
            [self.collectionview reloadData];
        }
        else{
            
            
        }
        
    }
    else if (wrapper == httpShorejob && httpShorejob != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrshorejob = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
        }
        else{
            
        }
        
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnMenu:(id)sender {
}
- (IBAction)BtnProfile:(id)sender {
    MyProfileCandidate *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)segmentvaluechanged:(UISegmentedControl *)sender {
    
    if (_segment.selectedSegmentIndex == 0) {
        
        tblarray = [marrlatestjob mutableCopy];
         [self.tableview reloadData];
    }
    else if(_segment.selectedSegmentIndex == 1) {
        
        tblarray = [marrshorejob mutableCopy];
         [self.tableview reloadData];
    }
}

@end
