//
//  HomeScreen.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface HomeScreen : UIViewController<UITextFieldDelegate,HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>{
      HttpWrapper *httplatestjob,*httpFeaturedjob,*httpShorejob;
    NSMutableArray *marrlatestjob;
    NSMutableArray *marrhttpFeaturedjob;
    NSMutableArray *marrshorejob;
    NSMutableArray *tblarray;
     UIView *picker;   /////menu picker view
}
- (IBAction)BtnProfile:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
- (IBAction)btnMenu:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;

@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segment;

@end
