//
//  InviteTCell.h
//  IndieGlobe
//
//  Created by ADMIN-Khushal on 12/4/17.
//  Copyright © 2017 ADMIN-Khushal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface homecollectionCell : UICollectionViewCell

//Outlet
@property (strong, nonatomic) IBOutlet UIImageView *imageview;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lbldescription;
@property (strong, nonatomic) IBOutlet UILabel *lbldays;



@end
