//
//  AddCOCDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 15/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "ViewController.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface AddCOCDetail : ViewController<HttpWrapperDelegate>{
    HttpWrapper *httadddetail,*httpeditdetail,*httpcertificates1,*httpCountry;
    //    NSMutableArray* detail;1
    NSMutableArray *marreditdetail,*marrcountry;
    NSMutableArray *marradddetail;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
     UITextField *currentTextField;
     NSMutableArray *marrProficiency;
    NSString * CertificateID;
    BOOL isCountry;
     NSString * countryId;
}
- (IBAction)btnCountry:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtproficiency;
- (IBAction)btnProficiency:(id)sender;
@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSString *indos;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtnumber;
- (IBAction)btncalender:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtplaceofissue;
@property (strong, nonatomic) IBOutlet UIButton *btncalender;
@property (strong, nonatomic) IBOutlet UIButton *btnCalender;
@property (strong, nonatomic) IBOutlet UITextField *txtvalidity;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveseamandetail;
- (IBAction)btnSaveSeamanDetail:(id)sender;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblindos;
@property (strong, nonatomic) IBOutlet UITextField *txtindos;
- (IBAction)Indos_click:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *validitytopspace;
@property (strong, nonatomic) IBOutlet UIView *vwindos;

-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@end
