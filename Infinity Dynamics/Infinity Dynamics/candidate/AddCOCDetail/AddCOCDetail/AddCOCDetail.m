//
//  AddCOCDetail.m
//  Infinity Dynamics
//
//  Created by My Mac on 15/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "AddCOCDetail.h"
#import "Constants.h"
#import "HttpWrapper.h"
#import "WYPopoverController.h"
#import "EditProfile.h"
@interface AddCOCDetail ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
}

@end

@implementation AddCOCDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_Edit == YES) {
        _txtproficiency.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CertificateType"]];
        _txtplaceofissue.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Authority"]];
        _txtnumber.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CertificateNo"]];
        
        if ([_txtplaceofissue.text isEqualToString:@"India"]) {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:@"“INDOS #” MANDATORY FOR adding Indian COC detail."
                                         preferredStyle:UIAlertControllerStyleAlert];
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            _validitytopspace.constant = 53;
                                            _vwindos.hidden = false;
                                            _lblindos.hidden = false;
                                            _txtindos.text = _indos;
                                        }];
            //Add your buttons to alert controller
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            _validitytopspace.constant = 0;
            _vwindos.hidden = true;
            _lblindos.hidden = true;
        }
        NSString * date = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Validity"]];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        _txtvalidity.text = [NSString stringWithFormat:@"%@",JoiningDate];
    }
    else
    {
        _validitytopspace.constant = 0;
        _vwindos.hidden = true;
        _lblindos.hidden = true;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSaveSeamanDetail:(id)sender {
    [self.view endEditing:YES];
    if ([_txtproficiency.text isEqualToString:@""]){
        
//    }||[_txtnumber.text isEqualToString:@""]||[_txtvalidity.text isEqualToString:@""]||[_txtplaceofissue.text isEqualToString:@""]) {
           showAlert(AlertTitle, @"Please Select Competency / Proficiency");
    }else{
         if ([_txtnumber.text isEqualToString:@""])
         {
             showAlert(AlertTitle, @"Please Enter Certificate Number");
         }
         else
         {
             if ([_txtplaceofissue.text isEqualToString:@""]){
                 showAlert(AlertTitle, @"Please Select Country");
             }
             else if ([_txtplaceofissue.text isEqualToString:@"India"])
             {
                 if ([_txtindos.text isEqualToString:@""]){
                     showAlert(AlertTitle, @"Please Enter Indos");
                 }
                 else
                 {
                     [self nextvalidation];
                 }
             }
             else
             {
                 [self nextvalidation];
             }
         }
         }

}
-(void)nextvalidation
{
    if ([_txtvalidity.text isEqualToString:@""]){
        showAlert(AlertTitle, @"Please Select Validity");
    }else{
        
        if (_Edit == YES) {
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(Updatemethod) withObject:nil afterDelay:0.1];
        }
        else{
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
        }
    }

}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)btncalender:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setDay:3];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
       datePicker.minimumDate = [NSDate date];
        // datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
  [self performSelector:@selector(certificate1) withObject:nil afterDelay:0.1];
     [self performSelector:@selector(Country) withObject:nil afterDelay:0.1];
    
}
-(void)Country
{
    //    candidate_id
    //    number
    //    proficiency
    //    place_of_issue
    //    validity (yyyy-mm-dd)
    //    update = 0
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpCountry  = [[HttpWrapper alloc] init];
        httpCountry.delegate=self;
        httpCountry.getbool=NO;
        
        [httpCountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country_list",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)certificate1
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcertificates1  = [[HttpWrapper alloc] init];
        httpcertificates1.delegate=self;
        httpcertificates1.getbool=NO;
        
        [httpcertificates1 requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_certificate",MainUrl] param:@""];
    });
    
    
}
-(void)datePickerCancelPressed
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MMM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter1 stringFromDate:selectedDate];
        _txtvalidity.text = eventDate;
        if ([_txtvalidity.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([_txtvalidity.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MMM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        
        _txtvalidity.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}
-(void)Updatemethod
{
//    candidate_id
//    number
//    proficiency
//    place_of_issue
//    validity (yyyy-mm-dd)
//    update = 1
//    coc_id
//
    
    NSString *find1 = _txtplaceofissue.text;
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find1];
    NSArray *results1 = [marrcountry filteredArrayUsingPredicate:predicate1];
    
    countryId= [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryID" ]] ;
    NSString * Uid =   [[countryId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed1 = [Uid stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
 
    NSString *find = _txtproficiency.text;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrProficiency filteredArrayUsingPredicate:predicate];
    CertificateID= [NSString stringWithFormat:@"%@", [results  valueForKey:@"CertificateID" ]] ;
   
    
    NSString * Uid1 =   [[CertificateID stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:_txtnumber.text forKey:@"number"];
    NSString * ldate = [NSString stringWithFormat:@"%@",_txtvalidity.text];
    
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
    
    NSDate *leavedate=[formatedt1 dateFromString:ldate];
    [formatedt1 setDateFormat:@"yyyy-MM-dd"];
    NSString * leavedates = [formatedt1 stringFromDate:leavedate];
    [parameters setValue:leavedates forKey:@"validity"];
    [parameters setValue:trimmed1 forKey:@"place_of_issue"];
    [parameters setValue:[_detail valueForKey:@"COCDetailsID"] forKey:@"coc_id"];
     [parameters setValue:trimmed forKey:@"proficiency"];
    
    [parameters setValue:@"1" forKey:@"update"];
    [parameters setValue:_txtindos.text forKey:@"indos"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpeditdetail  = [[HttpWrapper alloc] init];
        httpeditdetail.delegate=self;
        httpeditdetail.getbool=NO;
        
        [httpeditdetail requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_coc_dtl",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)CallMyMethod1
{
//    candidate_id
//    number
//    proficiency
//    place_of_issue
//    validity (yyyy-mm-dd)
//    update = 0
    NSString *find1 = _txtplaceofissue.text;
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find1];
    NSArray *results1 = [marrcountry filteredArrayUsingPredicate:predicate1];
    
    countryId= [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryID" ]] ;
    NSString * Uid =   [[countryId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed1 = [Uid stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *find = _txtproficiency.text;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrProficiency filteredArrayUsingPredicate:predicate];
    CertificateID= [NSString stringWithFormat:@"%@", [results  valueForKey:@"CertificateID" ]] ;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    NSString * Uid1 =   [[CertificateID stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    
    NSString * ldate = [NSString stringWithFormat:@"%@",_txtvalidity.text];
    
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *leavedate=[formatedt1 dateFromString:ldate];
    [formatedt1 setDateFormat:@"yyyy-MM-dd"];
    NSString * leavedates = [formatedt1 stringFromDate:leavedate];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:_txtnumber.text forKey:@"number"];
    [parameters setValue:leavedates forKey:@"validity"];
    [parameters setValue:trimmed1 forKey:@"place_of_issue"];
     [parameters setValue:trimmed forKey:@"proficiency"];
    [parameters setValue:_txtindos.text forKey:@"indos"];
    [parameters setValue:@"0" forKey:@"update"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httadddetail  = [[HttpWrapper alloc] init];
        httadddetail.delegate=self;
        httadddetail.getbool=NO;
        
        [httadddetail requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_coc_dtl",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
  
    
 if (wrapper == httpcertificates1 && httpcertificates1 != nil)
   {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marreditdetail = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
             marrProficiency = [dicsResponse valueForKey:@"data"] ;
 
        }
        else{
            
        }
    }
   else if(wrapper == httadddetail && httadddetail != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marradddetail = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            showAlert(AlertTitle, @"Added Successfully");
            [self.navigationController popViewControllerAnimated:NO];
        }
        else{
            
            
        }
    }
   else if (wrapper == httpCountry && httpCountry != nil){
       NSLog(@"dicresponce %@",dicsResponse);
       NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
       dic=[dicsResponse valueForKey:@"data"];
       marrcountry = [dicsResponse valueForKey:@"data"] ;
       NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
       if ([status isEqualToString:@"1"]) {
           
           
           
       }
       else{
           
           
       }
       
   }
        else if (wrapper == httpeditdetail && httpeditdetail != nil){
            NSLog(@"dicresponce %@",dicsResponse);
            NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
            dic=[dicsResponse valueForKey:@"data"];
            marreditdetail = [dicsResponse valueForKey:@"data"] ;
            NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
            if ([status isEqualToString:@"1"]) {
                showAlert(AlertTitle, @"Updated Successfully");
                  [self.navigationController popViewControllerAnimated:NO];
                
            }
        else{
            
            
        }
        
    }
        else{
            
        }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnProficiency:(id)sender {
    [self.view endEditing:YES];
    [_txtproficiency setTag:5];
    currentTextField = _txtproficiency;
    // amountype = YES;
     isCountry = NO;
    [self showPopover:sender];
}
- (IBAction)btnCountry:(id)sender {
    [_txtplaceofissue setTag:19];
    currentTextField = _txtplaceofissue;
    isCountry = YES;
    [self showPopover:sender];
    
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        if (isCountry == YES) {
            settingsViewController.marrcountry = marrcountry;
            settingsViewController.index = currentTextField.tag;
        }else{
        settingsViewController.index = currentTextField.tag;
        settingsViewController.marrproficiency = marrProficiency;
        }
        
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
- (IBAction)Indos_click:(UIButton *)sender {
}

-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    if (isCountry)
    {
        if ([strValue isEqualToString:@"India"]) {
            // NSString * indos = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Indos"]];
            //        if ([indos isEqualToString:@""]) {
            //            showAlert(AlertTITLE, @"“INDOS #” is MANDATORY FOR adding Indian COC detail.")
            //            EditProfile *s =[self.storyboard instantiateViewControllerWithIdentifier:@"EditProfile"];
            //            s.cocdetail = YES;
            //            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
            //            [self.navigationController pushViewController:s animated:NO];
            //
            //        }
            //        else{
            //
            //        }
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@""
                                         message:@"“INDOS #” MANDATORY FOR adding Indian COC detail."
                                         preferredStyle:UIAlertControllerStyleAlert];
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                            _validitytopspace.constant = 53;
                                            _vwindos.hidden = false;
                                            _lblindos.hidden = false;
                                            _txtindos.text = _indos;
                                        }];
            //Add your buttons to alert controller
            [alert addAction:yesButton];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            _validitytopspace.constant = 0;
            _vwindos.hidden = true;
            _lblindos.hidden = true;
        }
    }
    
    
}

@end
