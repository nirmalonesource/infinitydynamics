//
//  SignupCompany.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "SignupCompany.h"
#import "myModel.h"
#import "Constants.h"
#import "ViewController.h"
#import "TextFieldValidator.h"
#import "SWRevealViewController.h"
#import "PCCPViewController.h"

#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{5,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{5,20}"
#define REGEX_RSPL @"[A-Za-z0-9]"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
#import "WYPopoverController.h"
@interface SignupCompany ()<WYPopoverControllerDelegate,WKNavigationDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
}


@end

@implementation SignupCompany


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    txtAddress.delegate=self;
    txtAddress.text = @"Enter your address";
    txtAddress.textColor = [UIColor lightGrayColor];
     self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 1180);
     [self setupAlerts];

    //Select Country
    SelectCountryId = 1;
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"infinitytnc_com" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:path];
    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    _viewterms.hidden = YES;
    _btnAccepttermsandcondition.userInteractionEnabled = YES;
    _btnaccept.hidden = YES;
     _btnDecline.hidden = YES;
    self.webview.scrollView.delegate = self;
    self.webview.navigationDelegate = self;
   
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
//    CGSize fittingSize = [self.webview sizeThatFits:CGSizeZero];
//
//    CGFloat height1 = scrollView.bounds.origin.y + self.webview.bounds.size.height;
//
//    CGFloat height2 = fittingSize.height;
//
//    int delta = fabs(height1 - height2);
//
//    if (delta < 30) {
//        _btnaccept.hidden = NO;
//        _btnDecline.hidden = NO;
//    }
}
-(void)setupAlerts{
//    [txtname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
//    [txtname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
//    txtname.validateOnResign=NO;
    
    [txtEmailid addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    
    [txtPassword addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password characters limit should be come between 6-20"];
    [txtPassword addRegx:REGEX_PASSWORD withMsg:@"Password must contain alpha numeric characters."];
    
    [txtconfirmPassword addConfirmValidationTo:txtPassword withMsg:@"Confirm password didn't match."];
    
  //  [txtMobileno addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper format (eg. ##########)"];
//    txtMobileno.isMandatory=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if([txtAddress.text isEqual: @"Enter your address"]){
        txtAddress.text = @"";
        txtAddress.textColor = [UIColor blackColor];
    }
    return YES;
}
-(void) textViewDidChange:(UITextView *)textView
{
    if(txtAddress.text.length == 0){
        txtAddress.textColor = [UIColor lightGrayColor];
        txtAddress.text = @"Enter your address";
        [txtAddress resignFirstResponder];
    }
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}



- (IBAction)btnSelectPaln:(id)sender {
    [self.view endEditing:YES];
    [_txtPlan setTag:18];
    currentTextField = _txtPlan;
    iscountry=NO;
    [self showPopover:sender];
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;

}
//-(void)Popvalueselected:(NSString*)strValue
//{
//    NSLog(@"POP:%@",strValue);
//    currentTextField.text = strValue;
//
//    if (SelectCountryId == 1)
//    {
//        SelectCountryIDString = [NSString stringWithFormat:@"%@",strValue];
//        NSLog(@"SelectCountryIDString = %@",SelectCountryIDString);
//    }
//    else  if (SelectCountryId == 2)
//    {
//        SelectCountryNameString = [NSString stringWithFormat:@"%@",strValue];
//        NSLog(@"SelectCountryNameString = %@",SelectCountryNameString);
//        txtCountry.text = [NSString stringWithFormat:@"%@",strValue];
//    }
//    SelectCountryId++;
//
//    [self dismissPopUpViewController];
//
//}

- (IBAction)btnRegister123:(id)sender
{
    NSLog(@"isAccept = %@",isAccept);

    if ([isAccept isEqualToString:@"1"])
    {
    if ([txtEmailid validate]&[txtRPSLNo validate]&[txtPassword validate]&[txtconfirmPassword validate])
    {
        if ([txtCompanyname.text isEqualToString:@""]||[txtWebsite.text isEqualToString:@""]||[_txttelephoneno.text isEqualToString:@""]||[_txtPlan.text isEqualToString:@""])
        {
             showAlert(AlertTitle,@"Fill all the detail");
        }
        else{

            if([_txtValidUntil.text isEqualToString:@""]){
                 showAlert(AlertTitle,@"Select Valid until");
            }
            else
            {

                [APP_DELEGATE showLoadingView:@""];
                [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
            }
        }
    }
//    else if ([_txtmobilecode.text isEqualToString:@"91"] && txtMobileno.text.length != 10)
//    {
//        showAlert(AlertTitle,@"Mobile No should be 10 digits for India");
//    }
    else
    {
         showAlert(AlertTitle,@"Fill all the detail");

    }
    }
    else
    {
        showAlert(AlertTitle,@"Terms & Conditions must be read and accepted");
    [APP_DELEGATE hideLoadingView];
    }
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)validateEmailWithString2:(NSString*)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)btnRegister:(id)sender
{
    if ([txtCompanyname.text isEqualToString:@""])
    {
          showAlert(AlertTitle,@"Please Enter your company name.");
    }else if ([txtContactPerson.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter contact person.");
    }
//    else if ([txtRPSLNo.text isEqualToString:@""])
//    {
//       showAlert(AlertTitle,@"Please Enter your RPSL No.");
//    }
//    else if ([_txtValidUntil.text isEqualToString:@""])
//    {
//        showAlert(AlertTitle,@"Please Enter Select Vaild Until.");
//    }
    else if ([txtEmailid.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter your email address.");
    }
    else if ( [self validateEmailWithString:txtEmailid.text] == NO)
    {
        showAlert(AlertTitle,@"Please Enter Valid email address.");
    }
    else if ([_txtwebsite.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter website.");
    }
    else if ([txtPassword.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter your password.");
    }
    else if ([txtconfirmPassword.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Re-Enter Your Password.");
    }
    else if (![txtPassword.text isEqualToString:txtconfirmPassword.text]) {
        showAlert(@"Password", @"New Password and Confirm password does not match");
    }
    else if ([_txttelephoneno.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter Telephone No.");
    }
    else if ([_txttelephonecode.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter ISD Code.");
    }
    else if ([_txtstdcode.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter STD Code.");
    }
    else if ([_txtmobilecode.text isEqualToString:@"91"] && txtMobileno.text.length != 10)
    {
         showAlert(AlertTitle,@"Mobile No should be 10 digits for India.");
    }
    else if ([txtAddress.text isEqualToString:@""]||[txtAddress.text isEqual: @"Enter your address"])
    {
        showAlert(AlertTitle,@"Please Enter Address.");
    }
    else if ([_txtCity.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter city.");
    }
    else if ([_txtZipCode.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please Enter ZipCode.");
    }else if ([_txtCountry.text isEqualToString:@""])
    {
         showAlert(AlertTitle,@"Please Select Your Country.");
    }else if ([_txtPlan.text isEqualToString:@""])
    {
        showAlert(AlertTitle,@"Please select plan.");
    }else if (!ischecked)
    {
         showAlert(AlertTitle,@"Terms & Conditions must be read and accepted");
    }
    else
    {
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(Plan) withObject:nil afterDelay:0.1];
      [self performSelector:@selector(Country) withObject:nil afterDelay:0.1];
}
-(void)Plan
{
   dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpPlan  = [[HttpWrapper alloc] init];
        httpPlan.delegate=self;
        httpPlan.getbool=NO;
        [httpPlan requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_plan",MainUrl] param:@""];
    });
}
-(void)Country
{
    //    candidate_id
    //    number
    //    proficiency
    //    place_of_issue
    //    validity (yyyy-mm-dd)
    //    update = 0
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpCountry  = [[HttpWrapper alloc] init];
        httpCountry.delegate=self;
        httpCountry.getbool=NO;
        
        [httpCountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country_list",MainUrl] param:nil];
    });
}
-(void)CallMyMethod1
{
    //Post Method Request
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:txtEmailid.text forKey:@"email"];
    NSString * contectperson = [NSString stringWithFormat:@"%@",txtContactPerson.text];
    [parameters setValue:contectperson forKey:@"contact_person"];
    [parameters setValue:txtPassword.text forKey:@"password"];
     [parameters setValue:txtMobileno.text forKey:@"phone"];
     [parameters setValue:txtWebsite.text forKey:@"website"];
     [parameters setValue:txtCompanyname.text forKey:@"company_name"];
     [parameters setValue:txtAddress.text forKey:@"address"];
    [parameters setValue:_txtmobilecode.text forKey:@"mcountry_code"];
    [parameters setValue:_txttelephonecode.text forKey:@"country_code"];
    [parameters setValue:_txtstdcode.text forKey:@"std_code"];
     [parameters setValue:_txttelephoneno.text forKey:@"telephone"];
    [parameters setValue:_txtValidUntil.text forKey:@"validity"];
    [parameters setValue:txtRPSLNo.text forKey:@"rpsl_no"];
    [parameters setValue:_txtCity.text forKey:@"city"];
    [parameters setValue:_txtZipCode.text forKey:@"pincode"];

    NSString * Country = [NSString stringWithFormat:@"%@",_txtCountry.text];
    NSLog(@"Country = %@",Country);
    NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", Country];
    NSArray *results1 = [marrcountry filteredArrayUsingPredicate:predicate12];
    NSString * countryresid = [NSString stringWithFormat:@"%@", [results1 valueForKey:@"CountryID" ]] ;
    NSString * Uid111 =   [[countryresid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed11 = [Uid111 stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [parameters setValue:[NSString stringWithFormat:@"%@",trimmed11] forKey:@"country_id"];
    NSString *find = _txtPlan.text;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrPlan filteredArrayUsingPredicate:predicate];
    planid= [NSString stringWithFormat:@"%@", [results  valueForKey:@"PlanID" ]] ;
    
    
    NSString * Uid1 =   [[planid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    [parameters setValue:trimmed forKey:@"plan_id"];
    NSData *imageData = UIImageJPEGRepresentation(_imageview.image, 0.5);
     [parameters setValue:imageData forKey:@"company_image"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpSignup  = [[HttpWrapper alloc] init];
        httpSignup.delegate=self;
        httpSignup.getbool=NO;

        [httpSignup requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@company_register",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpSignup && httpSignup != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
            ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            [self.navigationController pushViewController:s animated:NO];
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            if ([message isEqualToString:@""])
            {
                message=@"Company Registered Successfully!!";
            }
            showAlert(AlertTitle, message);
        }
    }
    if(wrapper == httpPlan && httpPlan != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
            marrPlan=[dicsResponse valueForKey:@"data"];
           
        }
    }else if (wrapper == httpCountry && httpCountry != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcountry = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"])
        {
            //[self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            //NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            //showAlert(AlertTitle, message);
        }
        
    }
    else if (wrapper == httpCountryTo && httpCountryTo != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcountry = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"])
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnBack:(id)sender
{
      [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)BtnImage:(id)sender {
     [self createActionSheet];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"original Image Size : %@", NSStringFromCGSize(image.size));
    _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:image andframeSize:picker.view.frame.size andcropSize:CGSizeMake(500, 200)];
    _imgCropperViewController.delegate = self;
    [picker presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
}
- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    
    // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [_imageview setImage:image];
    //_addflexbanner.hidden = YES;
    //      [picker dismissViewControllerAnimated:YES completion:nil];
    //    if(self.resultImgView)
    //        [self.resultImgView removeFromSuperview];
    //
    //    self.resultImgView = [[[UIImageView alloc]  initWithFrame:CGRectMake(5, 5, 310, 310)] autorelease];
    //    self.resultImgView.image = image;
    //    self.resultImgView.backgroundColor = [UIColor darkGrayColor];
    //    self.resultImgView.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.view addSubview:self.resultImgView];
    //
    //    NSLog(@"cropping Image Size : %@", NSStringFromCGSize(image.size));
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
    [_picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)createActionSheet
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Camera", @"Library",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Edit Profile");
            [self cameraPictureButtonClicked];
        }
        else if (buttonIndex ==1)
        {
            NSLog(@"Camera");
            [self libraryButtonClicked];
        }
    }
    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}
- (void)cameraPictureButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        //   UIImagePickerControllerCropRect
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
- (void)libraryButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    //picker.cro
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (IBAction)btntelephonecountrycode:(id)sender {
    ismobilecountrycode = NO;
    
    istelephonecountrycode = YES;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}
- (void)updateViewsWithCountryDic:(NSDictionary*)countryDic{
    if (ismobilecountrycode == YES) {
        [_txtmobilecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    else{
        [_txttelephonecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    
    
}
- (IBAction)btnMobilecountrycode:(id)sender {
    ismobilecountrycode = YES;
  
    istelephonecountrycode = NO;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
    
}

- (IBAction)btnCountry:(id)sender
{
    [self.view endEditing:YES];
    iscountry = YES;
    [_txtCountry setTag:19];
    currentTextField = _txtCountry;
    [self showPopover:sender];
}


#pragma mark popover
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        settingsViewController.index = currentTextField.tag;

        if (iscountry == YES) {
                        settingsViewController.marrcountry = marrcountry;
                    }
                    else{
                        settingsViewController.marrPlan = marrPlan;
                    }
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            settingsViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width-20, 150);
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width-20, 150);
 #pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
//#pragma mark popover
//- (void)showPopover:(UIView*)btn;
//{
//    if (popoverController == nil)
//    {
//        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
//        if (iscountry == YES) {
//            settingsViewController.marrcountry = marrcountry;
//            settingsViewController.index = currentTextField.tag;
//        }
//        else{
//            settingsViewController.index = currentTextField.tag;
//            settingsViewController.marrnationality = marrPlan;
//        }
//
//        //settingsViewController.marrData = marrranklist;
//        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
//            settingsViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width-20, 200);
//
//        }
//        else {
//#pragma clang diagnostic pushc
//#pragma GCC diagnostic ignored "-Wdeprecated"
//            //if (amountype == YES) {
//            settingsViewController.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width-20, 200);
//
//#pragma clang diagnostic pop
//        }
//        settingsViewController.delegates = self;
//        [[self navigationController] setNavigationBarHidden:YES animated:YES];
//        settingsViewController.modalInPopover = NO;
//        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
//        popoverController.delegate = self;
//        popoverController.passthroughViews = @[btn];
//        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
//        popoverController.wantsDefaultContentAppearance = YES;
//        [popoverController presentPopoverFromRect:btn.bounds
//                                           inView:btn
//                         permittedArrowDirections:WYPopoverArrowDirectionAny
//                                         animated:YES
//                                          options:WYPopoverAnimationOptionFadeWithScale];
//    }
//}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)datePickerCancelPressed1
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed1
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        _txtValidUntil.text = eventDate;
        if ([_txtValidUntil.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([_txtValidUntil.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        
        _txtValidUntil.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}

- (IBAction)BtnValid:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed1];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setDay:3];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
        // datePicker.minimumDate = [NSDate date];
        // datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed1)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
    
    
    
}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//
//    CGSize fittingSize = [self.webview sizeThatFits:CGSizeZero];
//
//    CGFloat height1 = scrollView.bounds.origin.y + self.webview.bounds.size.height;
//
//    CGFloat height2 = fittingSize.height;
//
//    int delta = fabs(height1 - height2);
//
//    if (delta < 30) {
//        _btnacceptterms.hidden = NO;
//        _btnDecline.hidden = NO;
//    }
//}

- (void) drawCustomPDFContent
{
    //  Put your drawing calls here
    //  Draw a red box
    [[UIColor redColor] set];
    UIRectFill(CGRectMake(20, 20, 100, 100));
    
    //  Example of drawing your view into PDF, note that this will be a rasterized bitmap, including the text.
    //  To get smoother text you'll need to use the NSString draw methods
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    [_viewterms.layer renderInContext:ctx];
}

- (void) createCustomPDF
{
     NSString *path = [[NSBundle mainBundle] pathForResource:@"terms" ofType:@"pdf"];
    
    NSURL* pdfURL = [NSURL URLWithString:path]; /* URL to pdf file */;
    CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfURL);
    
    const size_t numberOfPages = CGPDFDocumentGetNumberOfPages(pdf);
    
    NSMutableData* data = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(data, CGRectZero, nil);
    
    for(size_t page = 1; page <= numberOfPages; page++)
    {
        //  Get the current page and page frame
        CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdf, page);
        const CGRect pageFrame = CGPDFPageGetBoxRect(pdfPage, kCGPDFMediaBox);
        
        UIGraphicsBeginPDFPageWithInfo(pageFrame, nil);
        
        //  Draw the page (flipped)
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx);
        CGContextScaleCTM(ctx, 1, -1);
        CGContextTranslateCTM(ctx, 0, -pageFrame.size.height);
        CGContextDrawPDFPage(ctx, pdfPage);
        CGContextRestoreGState(ctx);
        
        if(page == 1)
        {
          //  [self drawCustomPDFContent];
        }
    }
    
    UIGraphicsEndPDFContext();
    
    CGPDFDocumentRelease(pdf);
    pdf = nil;
    
    //  Do something with data here
    [data writeToFile:path atomically:YES];
}

- (IBAction)btnCloseterms:(id)sender {
    [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
    ischecked= false;
    _viewterms.hidden = YES;
    //_btnaccept.userInteractionEnabled = YES;
}
- (IBAction)BtnTermsnConditions:(id)sender {
    _viewterms.hidden = NO;
    _btnaccept.userInteractionEnabled = YES;
    _btnaccept.hidden = NO;
    _btnDecline.hidden = NO;
}
//- (IBAction)BtnAcceptTerms:(id)sender {
//    _viewterms.hidden = YES;
//    // _btnaccept.userInteractionEnabled = YES;
//    [_btnaccept setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
//    ischecked= true;
//    isAccept = [NSString stringWithFormat:@"1"];
//    NSLog(@"CHEK");
//}

//- (void)webViewDidFinishLoad:(UIWebView *)theWebView
//{
////    NSUInteger contentHeight = [[theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.scrollHeight;"]] intValue];
////    NSUInteger contentWidth = [[theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.body.scrollWidth;"]] intValue];
////    [theWebView setFrame:CGRectMake(0, 0, contentWidth, contentHeight)];
//}

- (IBAction)BtnAcceptTerms:(id)sender {
    _viewterms.hidden = YES;
    // _btnaccept.userInteractionEnabled = YES;
    [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
    ischecked= true;
    isAccept = [NSString stringWithFormat:@"1"];
    NSLog(@"CHEK");
}

- (IBAction)BtnacceptTermsandCondition:(id)sender {
    if (ischecked) {
        [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        ischecked = false;
        isAccept = [NSString stringWithFormat:@"0"];
        NSLog(@"UNCHEK");
    }else{
        [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        ischecked= true;
        isAccept = [NSString stringWithFormat:@"1"];
        NSLog(@"CHEK");
    }
}

@end
