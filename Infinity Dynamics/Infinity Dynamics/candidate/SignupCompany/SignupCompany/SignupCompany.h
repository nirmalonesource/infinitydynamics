//
//  SignupCompany.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "TextFieldValidator.h"
#import "UzysImageCropperViewController.h"
#import "PopOverView.h"
#import <WebKit/WebKit.h>

@interface SignupCompany : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate,UzysImageCropperDelegate>{
       BOOL ischecked;
     BOOL iscountry;
    HttpWrapper *httpSignup,*httpPlan,*httpCountry,*httpCountryTo;
    UIView *picker;
    BOOL ismobilecountrycode;
    NSMutableArray *marrPlan,*marrcountry;
//   NSMutableArray *marrPlan;
    NSString *planid;
    BOOL istelephonecountrycode;
      NSString * isAccept;
//    IBOutlet TextFieldValidator *txtConfirmpasword;
//    IBOutlet TextFieldValidator *txtpassword;
//    IBOutlet TextFieldValidator *txtphone;
//    IBOutlet TextFieldValidator *txtemail;
//    IBOutlet TextFieldValidator *txtname;
//        __weak IBOutlet TextFieldValidator *txtCity;
    
   IBOutlet TextFieldValidator *txtCompanyname;
    IBOutlet TextFieldValidator *txtEmailid;
    IBOutlet TextFieldValidator *txtPassword;
    IBOutlet TextFieldValidator *txtconfirmPassword;
    IBOutlet TextFieldValidator *txtWebsite;
    IBOutlet TextFieldValidator *txtMobileno;
    IBOutlet UITextView *txtAddress;
    
     IBOutlet TextFieldValidator *txtRPSLNo;
    
    __weak IBOutlet TextFieldValidator *txtContactPerson;
   // __weak IBOutlet UITextField *txtCountry;
    UITextField *currentTextField;
    
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    int SelectCountryId;
}
- (IBAction)BtnAcceptTerms:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnacceptterms;
- (IBAction)btnAccept:(id)sender;
- (IBAction)btnCloseterms:(id)sender;
@property (strong, nonatomic) IBOutlet WKWebView *webview;
//@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet UIView *viewterms;
@property (strong, nonatomic) IBOutlet UIButton *btnaccept;
- (IBAction)BtnValid:(id)sender;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtValidUntil;
@property (weak, nonatomic) IBOutlet UISegmentedControl *langSwitch;
- (IBAction)BtnImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageview;
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnback;

////////crop
@property (nonatomic,retain) UIImageView *resultImgView;
@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *picker;
//////////
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtZipCode;
//@property (strong, nonatomic) IBOutletCollection(TextFieldValidator) NSArray *txtCity;
- (IBAction)btnSelectPaln:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtPlan;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtCity;
@property (strong, nonatomic) IBOutlet UITextField *txtCountry;

- (IBAction)btnRegister:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAccepttermsandcondition;
- (IBAction)BtnacceptTermsandCondition:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UITextField *txtmobilecode;
- (IBAction)btntelephonecountrycode:(id)sender;

- (IBAction)btnMobilecountrycode:(id)sender;
- (IBAction)btnCountry:(id)sender;
//@property (weak, nonatomic) IBOutlet UIButton *btnCountry;
@property (strong, nonatomic) IBOutlet UITextField *txtstdcode;
@property (strong, nonatomic) IBOutlet UITextField *txttelephonecode;
@property (strong, nonatomic) IBOutlet UITextField *txttelephoneno;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@property (weak, nonatomic) IBOutlet UIButton *btnDecline;
- (IBAction)BtnDecline:(id)sender;
//- (IBAction)btnMobilecountrycode:(id)sender;
//-(void)Popvalueselected:(NSString*)strValue;
//-(void)Popvalveselected:(NSString*)strValve;
//-(void)dismissPopUpViewController;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtwebsite;

@end
