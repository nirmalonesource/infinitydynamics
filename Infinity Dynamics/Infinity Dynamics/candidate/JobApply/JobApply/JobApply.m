//
//  JobApply.m
//  Infinity Dynamics
//
//  Created by My Mac on 24/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "JobApply.h"
#import "Constants.h"
#import "JobApplied.h"
#import "HomeScreen.h"
@interface JobApply ()

@end

@implementation JobApply

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _txtcompany.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CompanyName"]];
    _txtrank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Rank"]];
    _txtjobtitle.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"JobTitle"]];
    _txtshiptype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShipType"]];
    NSString * IsApplied = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"IsApplied"]];
    if ([IsApplied isEqualToString:@"Applied"]) {
        _btnconfirm.enabled = NO;
        _btnconfirm.userInteractionEnabled = NO;
        _btnconfirm.alpha = 0.5;
    }
    else{
        _btnconfirm.enabled = YES;
        _btnconfirm.userInteractionEnabled = YES;
        _btnconfirm.alpha = 1;
        
    }
    if ([_isshoreselect isEqualToString:@"1"])
    {
        _lbljobtitle.text = @"Designation";
        _txtjobtitle.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"DesignationName"]];
        _lblrank.hidden = YES;
        _rankvw.hidden = YES;
        _lblshiptype.hidden = YES;
        _shiptypevw.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
-(void)CallMyMethod
{
//    candidate_id
//    job_id
//    apply_date (yyyy-mm-dd)
    
   
    if ([_isshoreselect isEqualToString:@"1"])
    {
        jobid = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShoreJobID"]];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
        [parameters setValue:jobid forKey:@"job_id"];
        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        formatter.dateFormat = @"yyyy-MM-dd";
//        NSString *todaydate = [formatter stringFromDate:[NSDate date]];
//
//        [parameters setValue:todaydate forKey:@"apply_date"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpAPPLYjob  = [[HttpWrapper alloc] init];
            httpAPPLYjob.delegate=self;
            httpAPPLYjob.getbool=NO;
            
            [httpAPPLYjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@apply_shorejob",MainUrl] param:[parameters copy]];
        });
    }
    else
    {
        jobid = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"JobID"]];
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
        
        
        [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
        [parameters setValue:jobid forKey:@"job_id"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *todaydate = [formatter stringFromDate:[NSDate date]];
        
        [parameters setValue:todaydate forKey:@"apply_date"];
        
        dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
            httpAPPLYjob  = [[HttpWrapper alloc] init];
            httpAPPLYjob.delegate=self;
            httpAPPLYjob.getbool=NO;
            
            [httpAPPLYjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@apply_job",MainUrl] param:[parameters copy]];
        });
    }
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpAPPLYjob && httpAPPLYjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrrappply = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            showAlert(AlertTITLE, @"Applied successfully")
            if ([_isshoreselect isEqualToString:@"1"])
            {
                HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
                //s.detail = _detail;
                [self.navigationController pushViewController:s animated:NO];
            }
            else
            {
                JobApplied *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobApplied"];
                //s.detail = _detail;
                [self.navigationController pushViewController:s animated:NO];
            }
        }
        else{
            showAlert(AlertTITLE, [dicsResponse valueForKey:@"message"])
        }
    }

 
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnConfirmApplication:(id)sender {
    [self.view endEditing:YES];
     [APP_DELEGATE showLoadingView:@""];
    
        [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
   
}
@end
