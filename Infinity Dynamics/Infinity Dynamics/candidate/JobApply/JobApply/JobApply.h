//
//  JobApply.h
//  Infinity Dynamics
//
//  Created by My Mac on 24/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface JobApply : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate>{
    HttpWrapper *httpAPPLYjob;
     NSMutableArray *marrrappply;
    NSString *jobid;
    
}
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtcompany;
@property (strong, nonatomic) IBOutlet UITextField *txtjobtitle;
@property (strong, nonatomic) IBOutlet UITextField *txtshiptype;
@property (strong, nonatomic) IBOutlet UIButton *btnconfirm;
@property (strong, nonatomic) IBOutlet UITextField *txtrank;
@property (strong, nonatomic)  NSString *isshoreselect;
@property (strong, nonatomic) IBOutlet UILabel *lblshiptype;
@property (strong, nonatomic) IBOutlet UILabel *lblrank;
@property (strong, nonatomic) IBOutlet UIView *rankvw;
@property (strong, nonatomic) IBOutlet UILabel *lbljobtitle;
@property (strong, nonatomic) IBOutlet UIView *jobtitlevw;
@property (strong, nonatomic) IBOutlet UIView *shiptypevw;

- (IBAction)btnConfirmApplication:(id)sender;
- (IBAction)btnBack:(id)sender;
@end
