//
//  InstitutesVC.h
//  Infinity Dynamics
//
//  Created by My Mac on 27/11/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"

@interface InstitutesVC : UIViewController<HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIView *picker;   /////menu picker view
    HttpWrapper *httpcompanies;
    AppDelegate *appdelegate;
    NSMutableArray *marrcompaniesjob;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UIButton *btnheader;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@end
