//
//  InstitutesVC.m
//  Infinity Dynamics
//
//  Created by My Mac on 27/11/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "InstitutesVC.h"
#import "Constants.h"
#import "SWRevealViewController.h"
#import "CompaniesCell.h"
#import "UIImageView+Haneke.h"
#import "CompanyJobList.h"
#import "AppDelegate.h"
#import "WebViewController.h"
#import "myModel.h"

@interface InstitutesVC ()

@end

@implementation InstitutesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    
    NSLog(@"Login Data = %@",appdelegate.loginUserData);
    [self customSetup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return marrcompaniesjob.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    
    CompaniesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSString* CompanyName = [NSString stringWithFormat:@"%@",[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"InstituteName"]];
    cell.CompanyName.text = CompanyName;
    
    NSString* Address = [NSString stringWithFormat:@"%@",[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"Address"]];
    cell.Address.text = Address;
    
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"LogoURL"]];
    if ([profilepicname isEqualToString:@"http://clientsdemoarea.com/projects/mariexeltime/uploads/company_logo/"]) {
        // cell.imageview.image = [UIImage imageNamed: @"no_image"];
        
        [cell.imageview setImage:[UIImage imageNamed:@"no_image"]];
        
    }else{
        NSURL *url = [NSURL URLWithString:profilepicname];
        [cell.imageview hnk_setImageFromURL:url];
        [APP_DELEGATE hideLoadingView];
    }
    return cell;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int numberOfCellInRow = 2;
    CGFloat cellWidth =  ([[UIScreen mainScreen] bounds].size.width/numberOfCellInRow)-20;
    CGFloat cellheight = 145;
    return CGSizeMake(cellWidth, cellheight);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    // [self SelectedMenu:indexPath.row];
    CompanyJobList *s =[self.storyboard instantiateViewControllerWithIdentifier:@"CompanyJobList"];
    s.Companyid =   [NSString stringWithFormat:@"%@",[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"CompanyID"]];
    
    //s.CountryID =   [NSString stringWithFormat:@"%@",[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"CountryID"]];
    
    [self.navigationController pushViewController:s animated:NO];
     */
    
     [self callcountapi:@"1" companyid:[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"InstituteID"]];
    
    WebViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    s.website =   [NSString stringWithFormat:@"%@",[[marrcompaniesjob objectAtIndex:indexPath.row] valueForKey:@"Website"]];
    s.strheader = _btnheader.titleLabel.text;
    [self.navigationController pushViewController:s animated:NO];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}

-(void)callcountapi:(NSString *)type companyid:(NSString *)companyid
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:type forKey:@"Type"];
    [AddPost setValue:companyid forKey:@"ID"];
    [AddPost setValue:[myModel getIPAddress] forKey:@"IPAddress"];
    
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@company_institute_visit_count",MainUrl] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setObject:@"" forKey:@"institute_id"];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcompanies  = [[HttpWrapper alloc] init];
        httpcompanies.delegate=self;
        httpcompanies.getbool=NO;
        
        [httpcompanies requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_institute_list",MainUrl] param:@""];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpcompanies && httpcompanies != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcompaniesjob = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"])
        {
            [self.collectionview reloadData];
        }
        else
        {
            
        }
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
@end
