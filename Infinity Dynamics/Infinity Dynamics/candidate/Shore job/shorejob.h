//
//  shorejob.h
//  Infinity Dynamics
//
//  Created by My Mac on 25/05/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"

NS_ASSUME_NONNULL_BEGIN

@interface shorejob : UIViewController<UITextFieldDelegate,HttpWrapperDelegate>{
    HttpWrapper *httplatestjob,*httpFeaturedjob,*httpShorejob;
    NSMutableArray *marrlatestjob;
    NSMutableArray *marrhttpFeaturedjob;
    NSMutableArray *marrshorejob;
    NSMutableArray *tblarray;
    UIView *picker;   /////menu picker view
}
@property (strong, nonatomic) IBOutlet UITableView *tblshorejob;

@end

NS_ASSUME_NONNULL_END
