//
//  shorejob.m
//  Infinity Dynamics
//
//  Created by My Mac on 25/05/19.
//  Copyright © 2019 Rishi. All rights reserved.
//

#import "shorejob.h"
#import "HomeScreen.h"
#import "homecollectionCell.h"
#import "Constants.h"
#import "Hometablecell.h"
#import "UIImageView+Haneke.h"
#import "SWRevealViewController.h"
#import "JobDetails.h"
#import "MyProfileCandidate.h"
#import "myModel.h"

@interface shorejob ()

@end

@implementation shorejob

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNeedsStatusBarAppearanceUpdate];
    self.tblshorejob.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBarHidden=YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
  
    [self performSelector:@selector(CallMyMethod3) withObject:nil afterDelay:0.1];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Hometablecell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Hometablecell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return tblarray.count;
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(Hometablecell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.lblcompanyname.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"CompanyName"]];
    cell.LblName.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"JobTitle"]];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    // [cell.imageview hnk_setImageFromURL:url];
    [cell.imageview hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];
   
    cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[tblarray objectAtIndex:indexPath.section] valueForKey:@"DesignationName"]];
    
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    
     [self callcountapi:@"1" companyid:[[tblarray objectAtIndex:indexPath.section] valueForKey:@"ShoreJobID"]];
    
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [tblarray objectAtIndex:indexPath.section] ;
    s.isshoreselect = @"1";
    [self.navigationController pushViewController:s animated:NO];
}

-(void)callcountapi:(NSString *)type companyid:(NSString *)companyid
{
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    [AddPost setValue:type forKey:@"Type"];
    [AddPost setValue:companyid forKey:@"ID"];
    [AddPost setValue:[myModel getIPAddress] forKey:@"IPAddress"];
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@ship_shore_job_visit_count",MainUrl] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(void)CallMyMethod3
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpShorejob  = [[HttpWrapper alloc] init];
        httpShorejob.delegate=self;
        httpShorejob.getbool=NO;
        
        [httpShorejob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_shorejob_list",MainUrl] param:[parameters copy]];
    });
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
     if (wrapper == httpShorejob && httpShorejob != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        tblarray = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            [_tblshorejob reloadData];
        }
        else{
            
        }
        
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

- (IBAction)btnBack:(id)sender
{
  //  [self.navigationController popViewControllerAnimated:YES];
    HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
@end
