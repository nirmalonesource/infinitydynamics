//
//  MatchedJob.h
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"
@interface MatchedJob : UIViewController<UITableViewDelegate,UITableViewDataSource,HttpWrapperDelegate>{
    
    AppDelegate * appdelegate;
    HttpWrapper *httpMatchjob;
    NSMutableArray *marrMatchjob;
    BOOL isFilterd;
    //ArrayCreation
    NSMutableArray *filterdarray;
}
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableview;

@end
