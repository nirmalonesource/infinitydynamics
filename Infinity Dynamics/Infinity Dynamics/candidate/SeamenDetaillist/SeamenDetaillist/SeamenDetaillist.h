//
//  SeamenDetaillist.h
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"

@interface SeamenDetaillist : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httdetailist,*httpdelete;
    NSMutableArray *marrdetaillist;
     NSString * seaman_id;
}
- (IBAction)btnPlusbutton:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)btnBack:(id)sender;

@end
