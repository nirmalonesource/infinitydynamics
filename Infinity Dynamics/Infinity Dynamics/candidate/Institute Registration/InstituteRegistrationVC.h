//
//  InstituteRegistrationVC.h
//  Infinity Dynamics
//
//  Created by My Mac on 26/11/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "TextFieldValidator.h"
#import "UzysImageCropperViewController.h"
#import "PopOverView.h"
#import <WebKit/WebKit.h>

@interface InstituteRegistrationVC : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate,UzysImageCropperDelegate>
{

    //HttpWrapper *httadddetail,*httpeditdetail,*httpCountry;
    NSMutableArray *marreditdetail,*marrcountry;
    NSMutableArray *marradddetail;
  
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    UITextField *currentTextField;
    NSString * countryId;


    BOOL ischecked;
    HttpWrapper *httadddetail,*httpeditdetail,*httpCountry,*httpCountryTo;
    UIView *picker;
    BOOL ismobilecountrycode;
    NSMutableArray *marrPlan;
    NSString *planid;
    BOOL istelephonecountrycode;
    NSString * isAccept;
  
    
    IBOutlet TextFieldValidator *txtEmailid;
    IBOutlet TextFieldValidator *txtPassword;
    IBOutlet TextFieldValidator *txtconfirmPassword;
    IBOutlet TextFieldValidator *txtRPSLNo;
    
    IBOutlet TextFieldValidator *txtInstitutename;
    IBOutlet TextFieldValidator *txtcontectperson;
    IBOutlet TextFieldValidator *txtemailaddress;
    IBOutlet TextFieldValidator *txtWebsite;
 
    IBOutlet TextFieldValidator *txtMobileno;
    IBOutlet UITextView *txtAddress;
    
    __weak IBOutlet TextFieldValidator *txtCity;
    
    __weak IBOutlet TextFieldValidator *txtZipCode;
    __weak IBOutlet UITextField *txtCountry;
    
    NSString * SelectCountryId;
    NSString * SelectCountryIDString;
    NSString * SelectCountryNameString;
}
@property (strong, nonatomic) IBOutlet UITextField *txttelephonecode;
@property (strong, nonatomic) IBOutlet UITextField *txtstdcode;
@property (strong, nonatomic) IBOutlet UITextField *txttelephoneno;



- (IBAction)BtnAcceptTerms:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnacceptterms;
- (IBAction)btnAccept:(id)sender;
- (IBAction)btnCloseterms:(id)sender;
//@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet WKWebView *webview;
@property (strong, nonatomic) IBOutlet UIView *viewterms;
@property (strong, nonatomic) IBOutlet UIButton *btnaccept;
- (IBAction)BtnValid:(id)sender;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtValidUntil;
@property (weak, nonatomic) IBOutlet UISegmentedControl *langSwitch;
- (IBAction)BtnImage:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imageview;
@property (strong, nonatomic) IBOutlet UIButton *btnImage;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnback;

////////crop
@property (nonatomic,retain) UIImageView *resultImgView;
@property (nonatomic,retain) UzysImageCropperViewController *imgCropperViewController;
@property (nonatomic,retain) UIImagePickerController *picker;
//////////
- (IBAction)btnSelectPaln:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtPlan;

- (IBAction)btnRegister:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAccepttermsandcondition;
- (IBAction)BtnacceptTermsandCondition:(id)sender;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UITextField *txtmobilecode;
- (IBAction)btntelephonecountrycode:(id)sender;

- (IBAction)btnMobilecountrycode:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@property (weak, nonatomic) IBOutlet UIButton *btnDecline;
- (IBAction)BtnDecline:(id)sender;

@end
