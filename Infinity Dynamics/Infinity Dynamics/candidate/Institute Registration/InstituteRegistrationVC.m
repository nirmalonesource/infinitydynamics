//
//  InstituteRegistrationVC.m
//  Infinity Dynamics
//
//  Created by My Mac on 26/11/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "InstituteRegistrationVC.h"
#import "myModel.h"
#import "Constants.h"
#import "ViewController.h"
#import "TextFieldValidator.h"
#import "SWRevealViewController.h"
#import "PCCPViewController.h"
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{5,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{5,20}"
#define REGEX_RSPL @"[A-Za-z0-9]"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
#import "WYPopoverController.h"
#import "Constants.h"
#import "WYPopoverController.h"

@interface InstituteRegistrationVC ()<WYPopoverControllerDelegate,UIActionSheetDelegate>
{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
}

@end

@implementation InstituteRegistrationVC


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SelectCountryId = @"0";
    SelectCountryIDString = [[NSString alloc] init];
    SelectCountryNameString = [[NSString alloc] init];
    SelectCountryIDString = @"";
    SelectCountryNameString = @"";
    
    isAccept = @"0";
    
    txtAddress.delegate=self;
    txtAddress.text = @"Enter your address";
    txtAddress.textColor = [UIColor lightGrayColor];
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 980);
    [self setupAlerts];
    
    ///ADD TAP
    picker = [[UIView alloc] initWithFrame:self.view.bounds];
    picker.backgroundColor=[UIColor blackColor];
    picker.alpha=0.4;
    [self.view addSubview: picker];
    picker.hidden=YES;
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController
                                            action:@selector(revealToggle:)];
    [picker addGestureRecognizer:singleFingerTap];
    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"terms" ofType:@"pdf"];
//    NSURL *url = [NSURL fileURLWithPath:path];

    NSString *path = [[NSBundle mainBundle] pathForResource:@"infinitytnc" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:path];

    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    _viewterms.hidden = YES;
    //_btnAccepttermsandcondition.userInteractionEnabled = NO;
    _btnacceptterms.hidden = YES;
    _btnDecline.hidden = YES;
    self.webview.scrollView.delegate = self;
    
}
-(void)setupAlerts{
    //    [txtname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    //    [txtname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    //    txtname.validateOnResign=NO;
    
    [txtemailaddress addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    
    //  [txtMobileno addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper format (eg. ##########)"];
    //    txtMobileno.isMandatory=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if([txtAddress.text isEqual: @"Enter your address"]){
        txtAddress.text = @"";
        txtAddress.textColor = [UIColor blackColor];
    }
    return YES;
}
-(void) textViewDidChange:(UITextView *)textView
{
    if(txtAddress.text.length == 0)
    {
        txtAddress.textColor = [UIColor lightGrayColor];
        txtAddress.text = @"Enter your address";
        [txtAddress resignFirstResponder];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (IBAction)BtnacceptTermsandCondition:(id)sender
{
    
    if (ischecked) {
        [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        ischecked = false;
        isAccept = [NSString stringWithFormat:@"0"];
        NSLog(@"UNCHEK");
    }else{
        [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        ischecked= true;
        isAccept = [NSString stringWithFormat:@"1"];
        NSLog(@"CHEK");
    }
}

//- (IBAction)btnSelectPaln:(id)sender {
//    [self.view endEditing:YES];
//    [_txtPlan setTag:18];
//    currentTextField = _txtPlan;
//    // amountype = YES;
//    [self showPopover:sender];
//}


- (IBAction)btnRegister123:(id)sender
{
    NSString * Institutename = [NSString stringWithFormat:@"%@",txtInstitutename.text];
    NSLog(@"Institutename = %@",Institutename);
    
    NSString * contectperson = [NSString stringWithFormat:@"%@",txtcontectperson.text];
    NSLog(@"contectperson = %@",contectperson);
    
    NSString * emailaddress = [NSString stringWithFormat:@"%@",txtemailaddress.text];
    NSLog(@"emailaddress = %@",emailaddress);
    
    NSString * Website = [NSString stringWithFormat:@"%@",txtWebsite.text];
    NSLog(@"Website = %@",Website);
    
    NSString * Telephoneno = [NSString stringWithFormat:@"%@",_txttelephoneno.text];
    NSLog(@"Telephoneno = %@",Telephoneno);

    NSString * Mobileno = [NSString stringWithFormat:@"%@",txtMobileno.text];
    NSLog(@"Mobileno = %@",Mobileno);
    
    NSString * TelephoneNumber = [NSString stringWithFormat:@"%@ %@ %@",_txttelephonecode.text,_txtstdcode.text,_txttelephoneno.text];
    NSLog(@"TelephoneNumber AAAAA ====>>>>>>> %@",TelephoneNumber);
    
    NSString * Mobile = [NSString stringWithFormat:@"%@ %@",_txtmobilecode.text,txtMobileno.text];
    NSLog(@"Mobile AAAAA ====>>>>>>> %@",Mobile);
    
    NSString * Address = [NSString stringWithFormat:@"%@",txtAddress.text];
    NSLog(@"Address = %@",Address);
    
    NSString * City = [NSString stringWithFormat:@"%@",txtCity.text];
    NSLog(@"City = %@",City);
    
    NSString * ZipCode = [NSString stringWithFormat:@"%@",txtZipCode.text];
    NSLog(@"ZipCode = %@",ZipCode);
    
    NSString * Country = [NSString stringWithFormat:@"%@",txtCountry.text];
    NSLog(@"Country = %@",Country);
    
    if ([isAccept isEqualToString:@"1"])
    {
        if ([txtemailaddress validate])
        {
            if ([txtInstitutename.text isEqualToString:@""]||[txtcontectperson.text isEqualToString:@""]||[_txttelephoneno.text isEqualToString:@""])
            {
                showAlert(AlertTitle,@"Fill all the detail");
            }
            else
            {
                if([_txtValidUntil.text isEqualToString:@""])
                {
                    showAlert(AlertTitle,@"Select Valid until");
                }
                else
                {
                    [APP_DELEGATE showLoadingView:@""];
                    
                    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
                    
                    NSString * Institutename = [NSString stringWithFormat:@"%@",txtInstitutename.text];
                    NSLog(@"Institutename = %@",Institutename);
                    
                    NSString * contectperson = [NSString stringWithFormat:@"%@",txtcontectperson.text];
                    NSLog(@"contectperson = %@",contectperson);
                    
                    NSString * emailaddress = [NSString stringWithFormat:@"%@",txtemailaddress.text];
                    NSLog(@"emailaddress = %@",emailaddress);
                    
                    NSString * Website = [NSString stringWithFormat:@"%@",txtWebsite.text];
                    NSLog(@"Website = %@",Website);
                    
                    //=======================================
                    NSString * TelephoneNumber = [NSString stringWithFormat:@"%@ %@ %@",_txttelephonecode.text,_txtstdcode.text,_txttelephoneno.text];
                    NSLog(@"TelephoneNumber AAAAA ====>>>>>>> %@",TelephoneNumber);
                    
                    NSString * Mobile = [NSString stringWithFormat:@"%@ %@",_txtmobilecode.text,txtMobileno.text];
                    NSLog(@"Mobile ====>>>>>>> %@",Mobile);
                    //=======================================
                    
                    [parameters setValue:Institutename forKey:@"institute_name"];
                    [parameters setValue:contectperson forKey:@"contact_person"];
                    [parameters setValue:emailaddress forKey:@"email"];
                    [parameters setValue:_txtmobilecode.text forKey:@"mcountry_code"];
                    [parameters setValue:txtMobileno.text forKey:@"phone"];
                    [parameters setValue:_txttelephonecode.text forKey:@"country_code"];
                    [parameters setValue:_txtstdcode.text forKey:@"std_code"];
                    [parameters setValue:_txttelephoneno.text forKey:@"telephone"];
                    [parameters setValue:txtWebsite.text forKey:@"website"];
                    
                    UIImage *imageSelect = [[UIImage alloc] initWithData:UIImagePNGRepresentation(_imageview.image)];
                    NSData *imageData = UIImageJPEGRepresentation(imageSelect, 0.5);
                    [parameters setValue:imageData forKey:@"institute_image"];
                    
                    NSString * Address = [NSString stringWithFormat:@"%@",txtAddress.text];
                    NSLog(@"Address = %@",Address);
                    [parameters setValue:txtAddress.text forKey:@"address"];
                    
                    NSString * City = [NSString stringWithFormat:@"%@",txtCity.text];
                    NSLog(@"City = %@",City);
                    [parameters setValue:txtCity.text forKey:@"city"];
                    
                    NSString * ZipCode = [NSString stringWithFormat:@"%@",txtZipCode.text];
                    NSLog(@"ZipCode = %@",ZipCode);
                    [parameters setValue:txtZipCode.text forKey:@"pincode"];
                    
                    
                NSString * Country = [NSString stringWithFormat:@"%@",txtCountry.text];
                    NSLog(@"Country = %@",Country);
                
                    NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", Country];
                    NSArray *results1 = [marrcountry filteredArrayUsingPredicate:predicate12];
                    
                    
                    NSString * countryresid = [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryID" ]] ;
                    NSString * Uid111 =   [[countryresid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                    NSString *trimmed11 = [Uid111 stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    [parameters setValue:[NSString stringWithFormat:@"%@",trimmed11] forKey:@"country_id"];
                    
                    NSLog(@"institute_register api parameters = %@",parameters);
                    
                    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                        httpCountryTo  = [[HttpWrapper alloc] init];
                        httpCountryTo.delegate=self;
                        httpCountryTo.getbool=NO;
                        
                        NSLog(@"parameters = %@",parameters);
                        
                        [httpCountryTo requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@institute_register",MainUrl] param:parameters];
                    });
                    
                }
            }
        }
        else
        {
            showAlert(AlertTitle,@"Fill all the detail");
        }
    }
    else
    {
        showAlert(AlertTitle,@"Terms & Conditions must be read and accepted");
        [APP_DELEGATE hideLoadingView];
    }
}


- (IBAction)btnRegister:(id)sender
{
   
        if ([txtInstitutename.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"Please Enter Institute name." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtcontectperson.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter contact person." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtemailaddress.text isEqualToString:@""] && ![txtemailaddress validate])
        {
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"" message:@"Please Enter your email address." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtWebsite.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter your website." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([_txttelephonecode.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Telephone ISD Code." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([_txtstdcode.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Telephone STD Code." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([_txttelephoneno.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter your Telephone No." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
//        else if ([_txtmobilecode.text isEqualToString:@""])
//        {
//            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
//
//            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//
//                //do something when click button
//            }];
//            [alert addAction:okAction];
//            [self presentViewController:alert animated:YES completion:nil];
//
//        }
        else if (![txtMobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
        {
           
                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                
                                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                                    //do something when click button
                                }];
                                [alert addAction:okAction];
                                [self presentViewController:alert animated:YES completion:nil];
                
//                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//
//                    //do something when click button
//                }];
//                [alert addAction:okAction];
//                [self presentViewController:alert animated:YES completion:nil];
           
//                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile No." preferredStyle:UIAlertControllerStyleAlert];
//
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//
//                    //do something when click button
//                }];
//                [alert addAction:okAction];
//                [self presentViewController:alert animated:YES completion:nil];
            
            
        }
        else if ([_txtmobilecode.text isEqualToString:@"91"] && txtMobileno.text.length != 10)
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtAddress.text isEqualToString:@"Enter your address"])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Your Address." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtCity.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Your City." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtZipCode.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Your Zipcode." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([txtCountry.text isEqualToString:@""])
        {
            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Select Your Country." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else if ([isAccept isEqualToString:@"0"])
        {
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"" message:@"Terms & Conditions must be read and accepted" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                
                //do something when click button
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [APP_DELEGATE showLoadingView:@""];
            
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
            
            NSString * Institutename = [NSString stringWithFormat:@"%@",txtInstitutename.text];
            NSLog(@"Institutename = %@",Institutename);
            
            NSString * contectperson = [NSString stringWithFormat:@"%@",txtcontectperson.text];
            NSLog(@"contectperson = %@",contectperson);
            
            NSString * emailaddress = [NSString stringWithFormat:@"%@",txtemailaddress.text];
            NSLog(@"emailaddress = %@",emailaddress);
            
            NSString * Website = [NSString stringWithFormat:@"%@",txtWebsite.text];
            NSLog(@"Website = %@",Website);
            
            //=======================================
            NSString * TelephoneNumber = [NSString stringWithFormat:@"%@ %@ %@",_txttelephonecode.text,_txtstdcode.text,_txttelephoneno.text];
            NSLog(@"TelephoneNumber AAAAA ====>>>>>>> %@",TelephoneNumber);
            
            NSString * Mobile = [NSString stringWithFormat:@"%@ %@",_txtmobilecode.text,txtMobileno.text];
            NSLog(@"Mobile ====>>>>>>> %@",Mobile);
            //=======================================
            
            [parameters setValue:Institutename forKey:@"institute_name"];
            [parameters setValue:contectperson forKey:@"contact_person"];
            [parameters setValue:emailaddress forKey:@"email"];
            [parameters setValue:_txtmobilecode.text forKey:@"mcountry_code"];
            [parameters setValue:txtMobileno.text forKey:@"phone"];
            [parameters setValue:_txttelephonecode.text forKey:@"country_code"];
            [parameters setValue:_txtstdcode.text forKey:@"std_code"];
            [parameters setValue:_txttelephoneno.text forKey:@"telephone"];
            [parameters setValue:txtWebsite.text forKey:@"website"];
            
            UIImage *imageSelect = [[UIImage alloc] initWithData:UIImagePNGRepresentation(_imageview.image)];
            NSData *imageData = UIImageJPEGRepresentation(imageSelect, 0.5);
            [parameters setValue:imageData forKey:@"institute_image"];
            
            NSString * Address = [NSString stringWithFormat:@"%@",txtAddress.text];
            NSLog(@"Address = %@",Address);
            [parameters setValue:txtAddress.text forKey:@"address"];
            
            NSString * City = [NSString stringWithFormat:@"%@",txtCity.text];
            NSLog(@"City = %@",City);
            [parameters setValue:txtCity.text forKey:@"city"];
            
            NSString * ZipCode = [NSString stringWithFormat:@"%@",txtZipCode.text];
            NSLog(@"ZipCode = %@",ZipCode);
            [parameters setValue:txtZipCode.text forKey:@"pincode"];
            
            NSString * Country = [NSString stringWithFormat:@"%@",txtCountry.text];
            NSLog(@"Country = %@",Country);
            
            NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", Country];
            NSArray *results1 = [marrcountry filteredArrayUsingPredicate:predicate12];
            
            
            NSString * countryresid = [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryID" ]] ;
            NSString * Uid111 =   [[countryresid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
            NSString *trimmed11 = [Uid111 stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            [parameters setValue:[NSString stringWithFormat:@"%@",trimmed11] forKey:@"country_id"];
            
            NSLog(@"institute_register api parameters = %@",parameters);
            
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                httpCountryTo  = [[HttpWrapper alloc] init];
                httpCountryTo.delegate=self;
                httpCountryTo.getbool=NO;
                
                NSLog(@"parameters = %@",parameters);
                
                [httpCountryTo requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@institute_register",MainUrl] param:parameters];
            });
        }
}

-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(Country) withObject:nil afterDelay:0.1];
    
}

-(void)CallMyMethod1
{
    //Post Method Request
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:txtInstitutename.text forKey:@"institutename"];
    [parameters setValue:txtcontectperson.text forKey:@"contectperson"];
    [parameters setValue:txtemailaddress.text forKey:@"email"];
    [parameters setValue:txtWebsite.text forKey:@"website"];
    [parameters setValue:txtInstitutename.text forKey:@"company_name"];
    
    NSString * TelephoneNumber = [NSString stringWithFormat:@"%@ %@ %@",_txttelephonecode.text,_txtstdcode.text,_txttelephoneno.text];
    NSLog(@"TelephoneNumber = %@",TelephoneNumber);
    
    NSString * Mobile = [NSString stringWithFormat:@"%@ %@",_txtmobilecode.text,txtMobileno.text];
    NSLog(@"Mobile = %@",Mobile);
    
    [parameters setValue:txtAddress.text forKey:@"address"];
    [parameters setValue:txtCity.text forKey:@"city"];
    [parameters setValue:txtZipCode.text forKey:@"pincode"];
    
    [parameters setValue:txtCountry.text forKey:@"country"];

    //NSData *imageData = UIImageJPEGRepresentation(_imageview.image, 0);
    //[parameters setValue:imageData forKey:@"company_image"];
    
    UIImage *imageSelect = [[UIImage alloc] initWithData:UIImagePNGRepresentation(_imageview.image)];
    NSData *imageData = UIImageJPEGRepresentation(imageSelect, 0.5);
    [parameters setValue:imageData forKey:@"company_image"];
    
//    [parameters setValue:_txttelephoneno.text forKey:@"telephone"];
//    [parameters setValue:_txtValidUntil.text forKey:@"validity"];
//    [parameters setValue:txtRPSLNo.text forKey:@"rpsl_no"];
    
    NSString * Uid1 =   [[planid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpCountry  = [[HttpWrapper alloc] init];
        httpCountry.delegate=self;
        httpCountry.getbool=NO;
        
        [httpCountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@company_register",MainUrl] param:[parameters copy]];
    });

    
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//
//    //Now post
//    NSLog(@"AddPost %@",parameters);
//    NSString *url = [NSString stringWithFormat:@"%@company_register",MainUrl];
//    [manager POST:url parameters:[parameters copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        //add img data one by one
//
//        NSData *imageData = UIImageJPEGRepresentation(_imageview,0.5);
//        [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"company_image"] fileName:[NSString stringWithFormat:@"company_image.jpg"] mimeType:@"image/jpeg"];
//
//    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
//        // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
//        NSLog(@"Success: %@", dicsResponse);
//        NSLog(@"sign up---%@",dicsResponse);
//        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
//
//        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
//            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
//            showAlert(AlertTitle, msg);
//        }
//        else
//        {
//            showAlert(AlertTitle, msg);
//            ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
//            [self.navigationController pushViewController:s animated:NO];
//        }
//    }
//  failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//      NSLog(@"Error: %@", error);
//  }];
    
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httadddetail && httadddetail != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"])
        {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else
        {
            ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
            showAlert(AlertTitle, @"Registered Successfully");
            [self.navigationController pushViewController:s animated:NO];
            
        }
    }
    else if (wrapper == httpCountry && httpCountry != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcountry = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"])
        {
            //[self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            //NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            //showAlert(AlertTitle, message);
        }

    }
    else if (wrapper == httpCountryTo && httpCountryTo != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrcountry = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"])
        {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            if ([message isEqualToString:@""])
            {
                message=@"Institute Registered Successfully!!";
            }
            showAlert(AlertTitle, message);
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        
    }

//    if(wrapper == httpPlan && httpPlan != nil)
//    {
//        NSLog(@"dicresponce %@",dicsResponse);
//        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
//        dic=[dicsResponse valueForKey:@"data"];
//        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
//        if ([status isEqualToString:@"0"]) {
//            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
//            showAlert(AlertTitle, message);
//        }
//        else{
//            marrPlan=[dicsResponse valueForKey:@"data"];
//
//        }
//    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

-(void)Country
{
    //    candidate_id
    //    number
    //    proficiency
    //    place_of_issue
    //    validity (yyyy-mm-dd)
    //    update = 0
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpCountry  = [[HttpWrapper alloc] init];
        httpCountry.delegate=self;
        httpCountry.getbool=NO;
        
        [httpCountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country_list",MainUrl] param:nil];
    });
}



- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)BtnImage:(id)sender {
    [self createActionSheet];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //    [_flexbanner setImage:chosenImage];
    //    _addflexbanner.hidden = YES;
    //    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"original Image Size : %@", NSStringFromCGSize(image.size));
    _imgCropperViewController = [[UzysImageCropperViewController alloc] initWithImage:image andframeSize:picker.view.frame.size andcropSize:CGSizeMake(500, 200)];
    _imgCropperViewController.delegate = self;
    [picker presentViewController:_imgCropperViewController animated:YES completion:nil];
    [_imgCropperViewController release];
}
- (void)imageCropper:(UzysImageCropperViewController *)cropper didFinishCroppingWithImage:(UIImage *)image
{
    
    // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [_imageview setImage:image];
    //_addflexbanner.hidden = YES;
    //      [picker dismissViewControllerAnimated:YES completion:nil];
    //    if(self.resultImgView)
    //        [self.resultImgView removeFromSuperview];
    //
    //    self.resultImgView = [[[UIImageView alloc]  initWithFrame:CGRectMake(5, 5, 310, 310)] autorelease];
    //    self.resultImgView.image = image;
    //    self.resultImgView.backgroundColor = [UIColor darkGrayColor];
    //    self.resultImgView.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.view addSubview:self.resultImgView];
    //
    //    NSLog(@"cropping Image Size : %@", NSStringFromCGSize(image.size));
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imageCropperDidCancel:(UzysImageCropperViewController *)cropper
{
   // [_picker dismissViewControllerAnimated:YES completion:nil];
    [self dismissModalViewControllerAnimated:YES];
}

-(void)createActionSheet
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Camera", @"Library",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Edit Profile");
            [self cameraPictureButtonClicked];
        }
        else if (buttonIndex ==1)
        {
            NSLog(@"Camera");
            [self libraryButtonClicked];
        }
    }
    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}
- (void)cameraPictureButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        //   UIImagePickerControllerCropRect
        picker.delegate = self;
        picker.allowsEditing = NO;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
- (void)libraryButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    //picker.cro
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)btntelephonecountrycode:(id)sender {
    ismobilecountrycode = NO;
    
    istelephonecountrycode = YES;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}

- (void)updateViewsWithCountryDic:(NSDictionary*)countryDic{
    if (ismobilecountrycode == YES)
    {
        [_txtmobilecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    else
    {
        [_txttelephonecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    
}

- (IBAction)btnMobilecountrycode:(id)sender {
    ismobilecountrycode = YES;
    
    istelephonecountrycode = NO;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
    
}

- (IBAction)btnCountry:(id)sender
{
    [txtCountry setTag:19];
    [self showPopover:sender];
}

#pragma mark popover
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        settingsViewController.index = txtCountry.tag;
        settingsViewController.marrcountry = marrcountry;
        
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            //            if (amountype == YES) {
            settingsViewController.preferredContentSize = CGSizeMake(self.view.frame.size.width-20, 3000);
            //            }
            //            else{
            //                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
            //            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width-20, 150);
            //            }
            //            else{
            //                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
            //            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue
{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;

//    if (SelectCountryId == 1)
//    {
//        SelectCountryIDString = [NSString stringWithFormat:@"%@",strValue];
//        NSLog(@"SelectCountryIDString = %@",SelectCountryIDString);
//    }
//    else  if (SelectCountryId == 2)
//    {
//        SelectCountryNameString = [NSString stringWithFormat:@"%@",strValue];
//        NSLog(@"SelectCountryNameString = %@",SelectCountryNameString);
//        txtCountry.text = [NSString stringWithFormat:@"%@",strValue];
//    }
     txtCountry.text = [NSString stringWithFormat:@"%@",strValue];
   
    
    [self dismissPopUpViewController];
    
}
-(void)datePickerCancelPressed1
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed1
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        _txtValidUntil.text = eventDate;
        if ([_txtValidUntil.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([_txtValidUntil.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        
        _txtValidUntil.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}

- (IBAction)BtnValid:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed1];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setDay:3];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
        // datePicker.minimumDate = [NSDate date];
        // datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed1)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
    
    
    
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGSize fittingSize = [self.webview sizeThatFits:CGSizeZero];
    
    CGFloat height1 = scrollView.bounds.origin.y + self.webview.bounds.size.height;
    
    CGFloat height2 = fittingSize.height;
    
    int delta = fabs(height1 - height2);
    
    if (delta < 30) {
        _btnacceptterms.hidden = NO;
        _btnDecline.hidden = NO;
    }
}
- (IBAction)btnCloseterms:(id)sender {
    _viewterms.hidden = YES;
    //_btnaccept.userInteractionEnabled = YES;
}
- (IBAction)BtnTermsnConditions:(id)sender {
    _viewterms.hidden = NO;
    // _btnAccepttermsandcondition.userInteractionEnabled = YES;
}
- (IBAction)BtnAcceptTerms:(id)sender {
    _viewterms.hidden = YES;
    // _btnaccept.userInteractionEnabled = YES;
    [_btnAccepttermsandcondition setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
    ischecked= true;
    isAccept = [NSString stringWithFormat:@"1"];
    NSLog(@"CHEK");
}
- (IBAction)btnAccept:(id)sender {
    
    if (ischecked) {
        [_btnaccept setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        ischecked = false;
        isAccept = [NSString stringWithFormat:@"0"];
        NSLog(@"UNCHEK");
    }else{
        [_btnaccept setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        ischecked= true;
        isAccept = [NSString stringWithFormat:@"1"];
        NSLog(@"CHEK");
    }
    
}
@end
