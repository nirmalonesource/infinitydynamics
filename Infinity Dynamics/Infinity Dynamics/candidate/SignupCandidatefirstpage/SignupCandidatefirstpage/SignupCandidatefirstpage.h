//
//  SignupCandidatefirstpage.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "DropDownListView.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface SignupCandidatefirstpage : UIViewController<UIScrollViewDelegate,UITextViewDelegate,kDropDownListViewDelegate,HttpWrapperDelegate>{
    UITextField *currentTextField;
    UIImage * image;
    
    NSString * isAccept;
    
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    BOOL ismobilecountrycode;
    BOOL ismobile2countrycode;
    BOOL istelephonecountrycode;
      DropDownListView * Dropobj;
    HttpWrapper *httpNationality;
    HttpWrapper *httpemailverification;
    NSMutableDictionary *parameters;

    NSString * NationalityId;
 NSMutableArray *marrnationality;
    IBOutlet UITextField *txtnationality;
}

- (IBAction)BTnNationality:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnNAtionality;
@property (strong, nonatomic) IBOutlet UITextField *txtstdcode;
@property (weak, nonatomic) IBOutlet UISegmentedControl *langSwitch;
@property (strong, nonatomic) IBOutlet UIButton *btnimage;
@property (strong, nonatomic) IBOutlet UIImageView *imgeview;
- (IBAction)btnImage:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtfirstname;
@property (strong, nonatomic) IBOutlet UITextField *txtmiddlename;
@property (strong, nonatomic) IBOutlet UITextField *txtlastname;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtemail;
@property (strong, nonatomic) IBOutlet UITextField *txtemail2;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtpassword;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtconfirmpassword;
@property (strong, nonatomic) IBOutlet UITextField *txtindusno;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txttelephoneno;
@property (strong, nonatomic) IBOutlet UITextField *txttelephonecode;
@property (strong, nonatomic) IBOutlet UITextField *txtmobilecode;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmobileno;
@property (strong, nonatomic) IBOutlet UITextField *txtmobilecode2;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmobileno2;

@property (strong, nonatomic) IBOutlet UITextField *txtdateofbirth;
- (IBAction)btnDateofbirth:(id)sender;

- (IBAction)btnNextStep:(id)sender;

- (IBAction)btnBack:(id)sender;
- (IBAction)btntelephonecountrycode:(id)sender;

- (IBAction)btnMobilecountrycode:(id)sender;
- (IBAction)btnmobile2countrycode:(id)sender;
@end
