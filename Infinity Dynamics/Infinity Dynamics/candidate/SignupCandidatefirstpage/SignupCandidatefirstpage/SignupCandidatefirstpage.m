//
//  SignupCandidatefirstpage.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "SignupCandidatefirstpage.h"
#import "SignupCandidatesecondpage.h"
#import "TextFieldValidator.h"
#import "Constants.h"
#import "PCCPViewController.h"
#import "WYPopoverController.h"
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{5,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{5,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
@interface SignupCandidatefirstpage ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
    
}

@end

@implementation SignupCandidatefirstpage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 930);
    _imgeview.layer.masksToBounds = YES;
    _imgeview.layer.cornerRadius = _imgeview.frame.size.width/2;
    
    _btnimage.layer.masksToBounds = YES;
    _btnimage.layer.cornerRadius = _btnimage.frame.size.width/2;
    
    [self setupAlerts];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupAlerts{
    //       [_txtfirstname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    //       [_txtfirstname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    //   _txtfirstname.validateOnResign=NO;
    //    [_txtlastname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    //    [_txtlastname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    //    _txtlastname.validateOnResign=NO;
    //    [_txtmiddlename addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    //    [_txtmiddlename addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    //    _txtmiddlename.validateOnResign=NO;
    
    [_txtemail addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    //[_txtemail2 addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    
    [_txtpassword addRegx:REGEX_PASSWORD_LIMIT withMsg:@"Password characters limit should be come between 6-20"];
    [_txtpassword addRegx:REGEX_PASSWORD withMsg:@"Password must contain alpha numeric characters."];
    
    [_txtconfirmpassword addConfirmValidationTo:_txtpassword withMsg:@"Confirm password didn't match."];
    
    //   [_txtmobileno addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper format (eg. ##########)"];
    // _txtmobileno.isMandatory=YES;
 //    [_txtmobileno2 addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper format (eg. ##########)"];
    // _txtmobileno2.isMandatory=YES;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)btnDateofbirth:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        //   NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        //        NSDate * currentDate = [NSDate date];
        //        NSDateComponents * comps = [[NSDateComponents alloc] init];
        //        [comps setDay:3];
        //        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
        
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setYear: -18];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        [comps setYear: -70];
        NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        self->datePicker.minimumDate = minDate;
        self->datePicker.maximumDate = maxDate;
        self->datePicker.date = maxDate;
        
        //        // datePicker.minimumDate = [NSDate date];
        //        datePicker.maximumDate = [NSDate date];
        
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
}
-(void)datePickerCancelPressed
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        //  dd-mm-yyyy
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        _txtdateofbirth.text = eventDate;
        if ([_txtdateofbirth.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([_txtdateofbirth.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"yyyy'-'MM'-'dd"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        
        _txtdateofbirth.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}

- (IBAction)btnNextStep:(id)sender {
    if ([_txtfirstname.text isEqualToString:@""]) {
         showAlert(AlertTitle,@"Please Enter First Name");
    }
    else{
        
        if ([_txtlastname.text isEqualToString:@""]) {
            showAlert(AlertTitle,@"Please Enter Last Name");
        }
        else{
            if ([_txtemail validate])
            {
                if([_txtpassword validate]){
                    if ([_txtconfirmpassword.text isEqualToString:@""]) {
                            showAlert(AlertTitle,@"Please re-enter password");
                    }else{
                        if([_txtconfirmpassword validate]){
                            if ([_txtdateofbirth.text isEqualToString:@""]) {
                                showAlert(AlertTitle,@"Please select date of birth");
                            }
                            else{
                                if ([txtnationality.text isEqualToString:@""]) {
                                    showAlert(AlertTitle,@"Please Select Nationality");
                                }
                                else{
                                    if ([_txtmobileno.text isEqualToString:@""]) {//////for mobile no
                                        if ([_txttelephoneno.text isEqualToString:@""])
                                        {
                                            showAlert(AlertTitle,@"Please Enter Telphone or Mobile No.");
                                        }
                                        else
                                        {
                                            if ([_txttelephonecode.text isEqualToString:@""]){
                                                showAlert(AlertTitle,@"Please enter ISD");
                                            }
                                            else{
                                                if ([_txtstdcode.text isEqualToString:@""]){
                                                    showAlert(AlertTitle,@"Enter STD Code");
                                                }
                                               else if (![_txtmobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
                                                {
                                                    UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                                                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                        //do something when click button
                                                    }];
                                                    [alert addAction:okAction];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                }
                                                else if ([_txtmobilecode.text isEqualToString:@"91"] && _txtmobileno.text.length != 10)
                                                {
                                                    UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
                                                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                        //do something when click button
                                                    }];
                                                    [alert addAction:okAction];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                }else if(![_txtmobileno2.text isEqualToString:@""] && [_txtmobilecode2.text isEqualToString:@""]){
                                                    UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile 2 ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                                                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                        //do something when click button
                                                    }];
                                                    [alert addAction:okAction];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                }
                                                else if ([_txtmobilecode2.text isEqualToString:@"91"] && _txtmobileno2.text.length != 10)
                                                {
                                                    UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
                                                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                        //do something when click button
                                                    }];
                                                    [alert addAction:okAction];
                                                    [self presentViewController:alert animated:YES completion:nil];
                                                }
                                                else{
                                                    if ([_txtemail.text isEqualToString:_txtemail2.text]) {
                                                        showAlert(AlertTitle,@"Both Email id should not be same");
                                                    }
                                                    else{
                                                        NSString *find = txtnationality.text;
                                                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
                                                        NSArray *results = [marrnationality filteredArrayUsingPredicate:predicate];
                                                        NationalityId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"NationalID" ]] ;
                                                        NSString * Uid1 =   [[NationalityId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                        NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                                                                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                        parameters = [[NSMutableDictionary alloc]init];
                                                        [parameters setValue:trimmed forKey:@"nationality"];
                                                        [parameters setValue:_txtfirstname.text forKey:@"first_name"];
                                                        [parameters setValue:_txtmiddlename.text forKey:@"middle_name"];
                                                        [parameters setValue:_txtlastname.text forKey:@"last_name"];
                                                        [parameters setValue:_txtemail.text forKey:@"email"];
                                                        [parameters setValue:_txtemail2.text forKey:@"email2"];
                                                        [parameters setValue:_txtpassword.text forKey:@"password"];
                                                        [parameters setValue:_txtindusno.text forKey:@"indos"];
                                                        [parameters setValue:endtime forKey:@"dob"];
                                                        [parameters setValue:_txtmobilecode.text forKey:@"mcountry_code"];
                                                        [parameters setValue:_txtmobilecode2.text forKey:@"mcountry_code2"];
                                                        [parameters setValue:_txttelephonecode.text forKey:@"country_code"];
                                                        [parameters setValue:_txtstdcode.text forKey:@"std_code"];
                                                        [parameters setValue:_txttelephoneno.text forKey:@"phone_no"];
                                                        [parameters setValue:_txtmobileno.text forKey:@"mobile_no"];
                                                        [parameters setValue:_txtmobileno2.text forKey:@"mobile_no2"];
                                                        
                                                        
                                                        [self CallMyMethod2];
                                                       
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (![_txtmobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
                                        {
                                            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                //do something when click button
                                            }];
                                            [alert addAction:okAction];
                                            [self presentViewController:alert animated:YES completion:nil];
                                        }
                                        else if ([_txtmobilecode.text isEqualToString:@"91"] && _txtmobileno.text.length != 10)
                                        {
                                            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                //do something when click button
                                            }];
                                            [alert addAction:okAction];
                                            [self presentViewController:alert animated:YES completion:nil];
                                        }else if(![_txtmobileno2.text isEqualToString:@""] && [_txtmobilecode2.text isEqualToString:@""]){
                                            UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile 2 ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                                //do something when click button
                                            }];
                                            [alert addAction:okAction];
                                            [self presentViewController:alert animated:YES completion:nil];
                                        }
                                        else
                                        {
                                            if ([_txtmobilecode2.text isEqualToString:@"91"] && _txtmobileno2.text.length != 10)
                                            {
                                                showAlert(AlertTitle,@"Mobile No2 should be 10 digits for India");
                                            }
                                            else if ([_txtemail.text isEqualToString:_txtemail2.text]) {
                                                showAlert(AlertTitle,@"Both Email id should not be same");
                                            }
                                            else
                                            {
                                                if ([_txtmobileno.text isEqualToString:_txtmobileno2.text]) {
                                                    showAlert(AlertTitle,@"Both Mobile No. should not be same");
                                                }
                                                else{
                                                    NSString *find = txtnationality.text;
                                                    
                                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
                                                    NSArray *results = [marrnationality filteredArrayUsingPredicate:predicate];
                                                    NationalityId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"NationalID" ]] ;
                                                    
                                                    NSString * Uid1 =   [[NationalityId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                                    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                                                                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                    parameters = [[NSMutableDictionary alloc]init];
                                                    [parameters setValue:trimmed forKey:@"nationality"];
                                                    [parameters setValue:_txtfirstname.text forKey:@"first_name"];
                                                    [parameters setValue:_txtmiddlename.text forKey:@"middle_name"];
                                                    [parameters setValue:_txtlastname.text forKey:@"last_name"];
                                                    [parameters setValue:_txtemail.text forKey:@"email"];
                                                    [parameters setValue:_txtemail2.text forKey:@"email2"];
                                                    [parameters setValue:_txtpassword.text forKey:@"password"];
                                                    [parameters setValue:_txtindusno.text forKey:@"indos"];
                                                    [parameters setValue:endtime forKey:@"dob"];
                                                    [parameters setValue:_txtmobilecode.text forKey:@"mcountry_code"];
                                                    [parameters setValue:_txtmobilecode2.text forKey:@"mcountry_code2"];
                                                    [parameters setValue:_txttelephonecode.text forKey:@"country_code"];
                                                    [parameters setValue:_txtstdcode.text forKey:@"std_code"];
                                                    
                                                    [parameters setValue:_txttelephoneno.text forKey:@"phone_no"];
                                                    [parameters setValue:_txtmobileno.text forKey:@"mobile_no"];
                                                    [parameters setValue:_txtmobileno2.text forKey:@"mobile_no2"];
                                                    SignupCandidatesecondpage *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SignupCandidatesecondpage"];
                                                    s.Image = image;
                                                    s.parameter = parameters;
                                                    [self.navigationController pushViewController:s animated:NO];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            showAlert(AlertTitle,@"Password Does Not Match");
                        }
                    }
                   
            }
                else{
                    showAlert(AlertTitle,@"Please enter Password");
                    
                }
            }
            else{
                showAlert(AlertTitle,@"Please enter email id");
            }

        }
    }
}
- (IBAction)btnImage:(id)sender {
    [self createActionSheet];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [_imgeview setImage:chosenImage];
    //  _img_choose_img.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)createActionSheet
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Camera", @"Library",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Edit Profile");
            [self cameraPictureButtonClicked];
        }
        else if (buttonIndex ==1)
        {
            NSLog(@"Camera");
            [self libraryButtonClicked];
        }
    }
    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}
- (void)cameraPictureButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
- (void)libraryButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}
- (IBAction)btntelephonecountrycode:(id)sender {
    ismobilecountrycode = NO;
    ismobile2countrycode = NO;
    istelephonecountrycode = YES;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}
- (void)updateViewsWithCountryDic:(NSDictionary*)countryDic{
    if (ismobilecountrycode == YES) {
        [_txtmobilecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
        
    }
    else if (ismobile2countrycode == YES){
        [_txtmobilecode2 setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    else{
        [_txttelephonecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    
    
}
- (IBAction)btnMobilecountrycode:(id)sender {
    ismobilecountrycode = YES;
    ismobile2countrycode = NO;
    istelephonecountrycode = NO;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
    
}
- (IBAction)btnmobile2countrycode:(id)sender {
    ismobilecountrycode = NO;
    ismobile2countrycode = YES;
    istelephonecountrycode = NO;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}
-(void)CallMyMethod1
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpNationality  = [[HttpWrapper alloc] init];
        httpNationality.delegate=self;
        httpNationality.getbool=NO;
        
        [httpNationality requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_nationality",MainUrl] param:@""];
    });
    
    
}
-(void)CallMyMethod2
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:_txtemail.text forKey:@"email"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpemailverification  = [[HttpWrapper alloc] init];
        httpemailverification.delegate=self;
        httpemailverification.getbool=NO;
        
        [httpemailverification requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@is_email_available_checked",MainUrl] param:parameters];
    });
    
    
}
- (IBAction)BTnNationality:(id)sender {
    [txtnationality setTag:1];
    currentTextField = txtnationality;
    // amountype = YES;
    [self showPopover:sender];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        settingsViewController.index = currentTextField.tag;
        settingsViewController.marrnationality = marrnationality;
        
        
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpNationality && httpNationality != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrnationality = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
    if(wrapper == httpemailverification && httpemailverification != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            SignupCandidatesecondpage *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SignupCandidatesecondpage"];
            s.Image = image;
            s.parameter = parameters;
            [self.navigationController pushViewController:s animated:NO];
        }
        else{
            showAlert(AlertTITLE, [dicsResponse valueForKey:@"message"])
        }
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}
@end
