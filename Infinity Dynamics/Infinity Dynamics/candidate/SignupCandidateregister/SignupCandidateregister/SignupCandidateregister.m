//
//  SignupCandidateregister.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "SignupCandidateregister.h"
#import "Constants.h"
#import "HttpWrapper.h"
#import "WYPopoverController.h"
#import "ViewController.h"
@interface SignupCandidateregister ()<WYPopoverControllerDelegate,UIDocumentPickerDelegate,UIDocumentMenuDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
    NSData *fileData;
    NSString *pdfname;
}


@end

@implementation SignupCandidateregister

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",_parameter);
    NSString *path = [[NSBundle mainBundle] pathForResource:@"infinitytnc" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:path];
    [_webview loadRequest:[NSURLRequest requestWithURL:url]];
    _viewterms.hidden = YES;
    _btnaccept.userInteractionEnabled = NO;
     _btnacceptterms.hidden = YES;
      _btnDecline.hidden = YES;
    self.webview.scrollView.delegate = self;
    _viewdifferentemail.hidden = YES;
    [self Country];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGSize fittingSize = [self.webview sizeThatFits:CGSizeZero];
    
    CGFloat height1 = scrollView.bounds.origin.y + self.webview.bounds.size.height;
    
    CGFloat height2 = fittingSize.height;
    
    int delta = fabs(height1 - height2);
    
    if (delta < 30) {
        _btnacceptterms.hidden = NO;
          _btnDecline.hidden = NO;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        if (iscountry == YES) {
            settingsViewController.marrcountry = marrCountry;
            settingsViewController.index = currentTextField.tag;
        }
        else{
            settingsViewController.index = currentTextField.tag;
            settingsViewController.marrnationality = marrnationality;
        }
    
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
        
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
      
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    
}

- (IBAction)btnSignin:(id)sender {
  
}

- (IBAction)btnRegister:(id)sender {
  
    
    if ([_txtaddress.text isEqualToString:@""]) {
        showAlert(AlertTitle, @"Please Enter Address");
    }
    else{
        
        if ([_txtcity.text isEqualToString:@""]) {
            showAlert(AlertTitle, @"Please Enter City");
        }
        else{
            if ([_txtzipcode.text isEqualToString:@""]) {
                showAlert(AlertTitle, @"Please Enter ZipCode");
            }
            else{
                if ([_txtCountry.text isEqualToString:@""]) {
                    showAlert(AlertTitle, @"Please select country of residence");
                }
                else{
    
     if ([isAccept isEqualToString:@"1"]) {
           [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
     }
     else{
         showAlert(AlertTitle,@"Terms & Conditions must be read and accepted");
        
     }
                }
        }
        }
    }
}

- (IBAction)btnAccept:(id)sender {
    if (ischecked) {
        [_btnaccept setImage:[UIImage imageNamed:@"filledright"] forState:UIControlStateNormal];
        ischecked = false;
        isAccept = [NSString stringWithFormat:@"0"];
        NSLog(@"UNCHEK");
    }else{
        [_btnaccept setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
        ischecked= true;
        isAccept = [NSString stringWithFormat:@"1"];
        NSLog(@"CHEK");
    }
    
}
- (IBAction)BTnNationality:(id)sender {
     iscountry = NO;
    [txtnationality setTag:1];
    currentTextField = txtnationality;
    // amountype = YES;
    [self showPopover:sender];
}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
//-(void)viewWillAppear:(BOOL)animated{
//    [APP_DELEGATE showLoadingView:@""];
//    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
//
//}
-(void)CallMyMethod
{
    //Post Method Request
    NSString *find = txtnationality.text;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrnationality filteredArrayUsingPredicate:predicate];
    NationalityId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"NationalID" ]] ;
    
    NSString * Uid1 =   [[NationalityId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //
    NSString *find1 = _txtCountry.text;
    
    NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find1];
    NSArray *results1 = [marrCountry filteredArrayUsingPredicate:predicate12];
    
    
    NSString * countryresid = [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryID" ]] ;
    NSString * Uid111 =   [[countryresid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed11 = [Uid111 stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
     [parameters addEntriesFromDictionary:_parameter];
    [parameters setValue:_txtaddress.text forKey:@"address"];
    [parameters setValue:_txtcity.text forKey:@"city_name"];
    [parameters setValue:_txtzipcode.text forKey:@"zip_code"];
     [parameters setValue:trimmed11 forKey:@"CountryofResidenceID"];

    if (_image!=nil) {
     [self uploadfile:parameters];
    }
    else
    {
        if (fileData > 0)
        {
            [self uploadfile:parameters];
        }
        else
        {
        NSLog(@" url is signup");
        [parameters setObject:@"" forKey:@"candidate_image"];
        
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                httpcandidatesignin  = [[HttpWrapper alloc] init];
                httpcandidatesignin.delegate=self;
                httpcandidatesignin.getbool=NO;
        
                [httpcandidatesignin requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@candidate_register",MainUrl] param:[parameters copy]];
            });
        }
    }

}
-(void)uploadfile:(NSMutableDictionary *)parameters
{
    //Create manager
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //Now post
    NSLog(@"AddPost %@",parameters);
    NSString *url = [NSString stringWithFormat:@"%@candidate_register",MainUrl];
    [manager POST:url parameters:[parameters copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //add img data one by one
        if (_image>0) {
            NSData *imageData = UIImageJPEGRepresentation(_image,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"candidate_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
        }
        if (fileData>0)
        {
            [formData appendPartWithFileData:fileData name:[NSString stringWithFormat:@"CV"] fileName:[NSString stringWithFormat:@"%@",pdfname] mimeType:@"application/pdf"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
        // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
        NSLog(@"Success: %@", dicsResponse);
        NSLog(@"sign up---%@",dicsResponse);
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            [APP_DELEGATE hideLoadingView];
            showAlert(AlertTitle, message);
            
        }
        else
        {
            showAlert(AlertTitle, msg);
            ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
            [APP_DELEGATE hideLoadingView];
            [self.navigationController pushViewController:s animated:NO];
            
        }
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
          }];
}
-(void)CallMyMethod1
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpNationality  = [[HttpWrapper alloc] init];
        httpNationality.delegate=self;
        httpNationality.getbool=NO;
        
        [httpNationality requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_nationality",MainUrl] param:@""];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpNationality && httpNationality != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrnationality = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
       
    }
    if(wrapper == httpCountry && httpCountry != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrCountry = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    else if (wrapper == httpcandidatesignin && httpcandidatesignin != nil){
        
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
            if ([message isEqualToString:@"Email id is already registered by another user. Please select a different email id"]){
                 _viewdifferentemail.hidden = NO;
            }
            else{
                
            }
            
        }
        else{
             showAlert(AlertTitle, @"Registered Successfully");
            ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:s animated:NO];
        }
    }
    
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
//- (IBAction)btnAccept:(id)sender {
//}

- (IBAction)btnCloseterms:(id)sender {
    _viewterms.hidden = YES;
    //_btnaccept.userInteractionEnabled = YES;
}
- (IBAction)BtnTermsnConditions:(id)sender {
    _viewterms.hidden = NO;
    _btnaccept.userInteractionEnabled = YES;
}
- (IBAction)BtnAcceptTerms:(id)sender {
    _viewterms.hidden = YES;
   // _btnaccept.userInteractionEnabled = YES;
    [_btnaccept setImage:[UIImage imageNamed:@"check-filled"] forState:UIControlStateNormal];
    ischecked= true;
    isAccept = [NSString stringWithFormat:@"1"];
    NSLog(@"CHEK");
}
- (IBAction)btnDifferentemailid:(id)sender {
    if ([_txtdifferentemail.text isEqualToString:@""]) {
        showAlert(AlertTitle, @"Please Enter Email id");
    }
    else{
        [_parameter setObject:_txtdifferentemail.text forKey:@"email"];
        [APP_DELEGATE showLoadingView:@""];
[self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
     
    }
   
}
- (IBAction)BtnDecline:(id)sender {
    ViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)BtnCountry:(id)sender {
    
    iscountry = YES;
    isrank = NO;
    [_txtCountry setTag:19];
    currentTextField = _txtCountry;
    // amountype = YES;
    [self showPopover:sender];
}
-(void)Country
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpCountry  = [[HttpWrapper alloc] init];
        httpCountry.delegate=self;
        httpCountry.getbool=NO;
        
        [httpCountry requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country_list",MainUrl] param:@""];
    });
    
    
}
- (IBAction)uploadcv_click:(UIButton *)sender {
    UIDocumentMenuViewController *importMenu = [[UIDocumentMenuViewController alloc] initWithDocumentTypes:@[(__bridge NSString*)kUTTypePDF] inMode:UIDocumentPickerModeImport];
    importMenu.delegate = self;
    importMenu.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:importMenu animated:YES completion:nil];
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url{
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        fileData = [NSData dataWithContentsOfURL:url];
        pdfname = [url lastPathComponent];
        NSLog(@"%@FILEDATA:",fileData);
        [_btnuploadcv setTitle:pdfname forState:UIControlStateNormal];
        NSString *alertMessage = [NSString stringWithFormat:@"Successfully imported %@", [url lastPathComponent]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Import"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        });
    }
}
- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}
- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    
}
@end
