//
//  SignupCandidateregister.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import <WebKit/WebKit.h>

@interface SignupCandidateregister : UIViewController<UITextFieldDelegate,HttpWrapperDelegate>{
    BOOL ischecked;
 NSString * isAccept;
    HttpWrapper *httpNationality,*httpcandidatesignin,*httpCountry;;
    NSString * NationalityId;
    BOOL isNationality;
    BOOL isrank;
    UITextField *currentTextField;
   
    NSMutableArray *marrnationality;
    IBOutlet UITextField *txtselectrank;
    
    IBOutlet UITextField *txtnoofchildren;
    IBOutlet UITextField *txtselectmatialstatus;
    NSMutableDictionary * dataid;

    
    IBOutlet UITextField *txtnationality;
    BOOL iscountry;
    NSString * countryId;
    NSMutableArray *marrCountry;
}
- (IBAction)BtnAcceptTerms:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnacceptterms;
- (IBAction)btnAccept:(id)sender;
- (IBAction)btnCloseterms:(id)sender;
//@property (strong, nonatomic) IBOutlet UIWebView *webview;
@property (strong, nonatomic) IBOutlet WKWebView *webview;
@property (strong, nonatomic) IBOutlet UIView *viewterms;

- (IBAction)BtnTermsnConditions:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lbltermsandconditions;
@property (strong , nonatomic) UIImage * image;
- (IBAction)BTnNationality:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnNAtionality;
- (IBAction)btnSignin:(id)sender;
- (IBAction)btnRegister:(id)sender;
- (IBAction)btnAccept:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnaccept;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtcity;
@property (strong, nonatomic) IBOutlet UITextView *txtaddress;
@property (strong, nonatomic) IBOutlet UITextField *txtzipcode;
@property (strong , nonatomic) NSMutableDictionary * parameter;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@property (weak, nonatomic) IBOutlet UIView *viewdifferentemail;
@property (weak, nonatomic) IBOutlet UITextField *txtdifferentemail;
- (IBAction)btnDifferentemailid:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnDecline;
- (IBAction)BtnDecline:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtCountry;
- (IBAction)BtnCountry:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnuploadcv;
- (IBAction)uploadcv_click:(UIButton *)sender;

@end
