//
//  AddSeamanDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface AddSeamanDetail : UIViewController<HttpWrapperDelegate>
{
    HttpWrapper *httadddetail,*httpeditdetail,*httpCountry;
  //    NSMutableArray* detail;
    NSMutableArray *marreditdetail,*marrcountry;
    NSMutableArray *marradddetail;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
     UITextField *currentTextField;
    NSString * countryId;
  
}
- (IBAction)btnCountry:(id)sender;
@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtnumber;
- (IBAction)btncalender:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtplaceofissue;
@property (strong, nonatomic) IBOutlet UIButton *btncalender;
@property (strong, nonatomic) IBOutlet UIButton *btnCalender;
@property (strong, nonatomic) IBOutlet UITextField *txtvalidity;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveseamandetail;
- (IBAction)btnSaveSeamanDetail:(id)sender;
- (IBAction)btnBack:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@end
