//
//  CompanyJobList.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/04/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "CompanyJobList.h"
#import "myModel.h"
#import "Constants.h"
#import "CompanyJobListcell.h"
#import "UIImageView+Haneke.h"
#import "JobDetails.h"
@interface CompanyJobList ()

@end

@implementation CompanyJobList

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (IsIphone6Plus ) {
        
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height);
    }
    else if (IsIphone6){
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height);
    }
    else{
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CompanyJobListcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"CompanyJobListcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return marrMatchjob.count;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(CompanyJobListcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    

    
    cell.LblName.text = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"JobTitle"]];
    
    cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"ShipType"]];
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [cell.imageview hnk_setImageFromURL:url];
    [APP_DELEGATE hideLoadingView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //        Myjoinedcell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //        if (cell.selected) {
    //            // ... Uncheck
    //            [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //        }
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
    
    [self.navigationController pushViewController:s animated:NO];
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];

    [parameters setValue:_Companyid forKey:@"company_id"];
     [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];

    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpMatchjob  = [[HttpWrapper alloc] init];
        httpMatchjob.delegate=self;
        httpMatchjob.getbool=NO;
        
        [httpMatchjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_job_list",MainUrl] param:[parameters copy]];
    });
    
    
}


-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpMatchjob && httpMatchjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrMatchjob = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrMatchjob);
            [self.tableview reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
@end
