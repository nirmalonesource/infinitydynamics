//
//  Locationcell.h
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompanyJobListcell: UITableViewCell{

    bool gradients;
}

@property (strong, nonatomic) IBOutlet UIImageView *imageview;

@property (strong, nonatomic) IBOutlet UILabel *LblName;
@property (strong, nonatomic) IBOutlet UILabel *lbldescription;
@property (strong, nonatomic) IBOutlet UILabel *lbldate;
@property (strong, nonatomic) IBOutlet UIButton *btnreadmore;




@end
