//
//  ExperienceDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"

@interface ExperienceDetail : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate>{
     HttpWrapper *httprank,*httpshiptype,*httpenginetype,*httpupdatedata,*httpaddData;
    HttpWrapper *httExperience;
    
    NSMutableArray *marrranklist;
    NSMutableArray *marrShiptype;
     NSMutableArray *marrEnginetype;
      NSMutableArray *marrupdate;
      NSMutableArray *marrEdit;
    NSString * shiptypeid;
    NSString * rankid;
    NSString * engineid;
    BOOL isenginetype;
    BOOL isrank;
     BOOL isshiptype;
      UITextField *currentTextField;
    
    
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    
    //////datepicker
    UIDatePicker *datePicker1;
    UIView *subView1;
    UIToolbar *toolbar1;
    BOOL isPickerLaunced11;
    BOOL isPickerLaunced12;
    NSString * endtime1;
    
}
- (IBAction)btnJoiningdate:(id)sender;
- (IBAction)btnBack:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtcompanyname;
@property (strong, nonatomic) IBOutlet UITextField *txtjoiningdate;
@property (strong, nonatomic) IBOutlet UITextField *txtLeaveingdate;
- (IBAction)btnLeavingDate:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtshipType;
- (IBAction)btnShiptype:(id)sender;
- (IBAction)btnRank:(id)sender;
- (IBAction)btnUpdateexp:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtRank;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;

@property (strong, nonatomic) IBOutlet UITextField *txtGrt;
@property (weak, nonatomic) IBOutlet UITextField *txtTonnageType;
@property (weak, nonatomic) IBOutlet UITextField *txtTonnageValue;

@property (strong, nonatomic) IBOutlet UITextField *txtenginetype;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtshipname;
- (IBAction)btnEngineType:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txttotalSeaDuration;
@end
