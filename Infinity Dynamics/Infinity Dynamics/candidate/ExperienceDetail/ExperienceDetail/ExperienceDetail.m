//
//  ExperienceDetail.m
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "ExperienceDetail.h"
#import "Constants.h"
#import "WYPopoverController.h"
#import "HcdDateTimePickerView.h"
#import "ExperienceDetailList.h"
#import "AppDelegate.h"

@interface ExperienceDetail ()<WYPopoverControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate>
{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
    HcdDateTimePickerView * dateTimePickerView;
    //NSArray * pickerData;
    __weak IBOutlet UIPickerView *myPickerView;
}
@property (nonatomic,retain) NSMutableArray * ExperienceArray;
@end

@implementation ExperienceDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //http://www.edumobile.org/ios/how-to-implement-pickerview-programmatically-in-iphone/
    
//    pickerData = [[NSArray alloc] init];
//    pickerData = @[@"GRT",@"DWT",@"TEUs",@"M3",@"CEUs",@"LIMS"];
    
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator = YES;
    
    myPickerView.hidden = YES;
    
    /*
     
     myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 200, 320, 200)];
     [self.view addSubview:myPickerView];
     myPickerView.delegate = self;
     myPickerView.showsSelectionIndicator = YES;
     
     
     doneButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
     [doneButton addTarget:self
     action:@selector(aMethod:)
     forControlEvents:UIControlEventTouchDown];
     doneButton.frame = CGRectMake(265.0,202.0, 52.0, 30.0);
     UIImage *img = [UIImage imageNamed:@"done.png"];
     [doneButton setImage:img forState:UIControlStateNormal];
    */
    
    
    
    
    
    
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 580);
    if (_Edit == YES) {
        _txtRank.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Rank"]];
        //_txtGrt.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"GRT"]];
        
        _txtTonnageType.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"TonnageType"]];
        _txtTonnageValue.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"TonnageValue"]];
        
//        _txtTonnageType.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"TonnageType"]];
//        _txtTonnageValue.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"TonnageValue"]];
        
        //_txtTonnageValue.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"GRT"]];
        _txttotalSeaDuration.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"TotalSeaDuration"]];
        _txtcompanyname.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CompanyName"]];
        _txtshipname.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShipName"]];
        _txtshipType.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShipType"]];
        if (![[NSString stringWithFormat:@"%@",[_detail valueForKey:@"EngineName"]]isEqualToString:@"<null>"])
        {
            _txtenginetype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"EngineName"]];
        }
        
        NSString * date = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"JoiningDate"]];
        
        NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
        [formatedt setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate=[formatedt dateFromString:date];
        [formatedt setDateFormat:@"dd-MMM-yyyy"];
        NSString * JoiningDate = [formatedt stringFromDate:fulldate];
        NSString * LeavingDate = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"LeavingDate"]];
        
        NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
        [formatedt1 setDateFormat:@"yyyy-MM-dd"];
        NSDate *fulldate1=[formatedt1 dateFromString:LeavingDate];
        [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
        NSString * LeavingDates = [formatedt1 stringFromDate:fulldate1];
        
        _txtjoiningdate.text = [NSString stringWithFormat:@"%@",JoiningDate];
        _txtLeaveingdate.text = [NSString stringWithFormat:@"%@",LeavingDates];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnJoiningdate:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
        // datePicker.minimumDate = [NSDate date];
        datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
}
-(void)datePickerCancelPressed
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd-MMM-yyyy"];//04-22-2015 3.00pm
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        _txtjoiningdate.text = eventDate;
        if ([_txtjoiningdate.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
        NSString *start = _txtjoiningdate.text;
        NSString *end = _txtLeaveingdate.text;
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"dd-MM-yyyy"];
        NSDate *startDate = [f dateFromString:start];
        
        
        if ([end isEqualToString:@""]) {
            
        }else{
            
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setTimeZone:[NSTimeZone localTimeZone]];
            [f setDateFormat:@"dd-MM-yyyy"];
            NSDate *startDate = [f dateFromString:start];
            NSDate *endDate1 = [f dateFromString:end];
            
            NSDate *endDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate: endDate1];
            
            
            if ([startDate compare:endDate1] == NSOrderedDescending) {
                
                //showAlert(@"Invalid date", @"check your date")
                
                [APP_DELEGATE AlertView:@"Invalid date" :@"check your date"];
                
                
                
                _txttotalSeaDuration.text =@"";
                _txtjoiningdate.text = @"";
                //"startDate is later than endDate
                
                
            } else if ([startDate compare:endDate1] == NSOrderedAscending) {
                //startDate is earlier than endDate
                NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
                
                // pass as many or as little units as you like here, separated by pipes
                NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                
                NSDateComponents *components = [gregorian components:units fromDate:startDate toDate:endDate options:0];
                
                NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                   fromDate: startDate
                                                                     toDate: endDate
                                                                    options: 0] month];
                
                NSLog(@"total months %ld",(long)month);
                
                NSLog(@"%ld", [components day]);
                NSLog(@"%ld", [components month]);
                NSLog(@"%ld", [components year]);
                NSString * year = [NSString stringWithFormat:@"%ld",[components year]];
                
                _txttotalSeaDuration.text = [NSString stringWithFormat:@"%ld Months %ld days",month,[components day]];
            }
            else{
                NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
                
                // pass as many or as little units as you like here, separated by pipes
                NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                
                NSDateComponents *components = [gregorian components:units fromDate:startDate toDate:endDate options:0];
                
                NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                   fromDate: startDate
                                                                     toDate: endDate
                                                                    options: 0] month];
                
                NSLog(@"total months %ld",(long)month);
                
                NSLog(@"%ld", [components day]);
                NSLog(@"%ld", [components month]);
                NSLog(@"%ld", [components year]);
                NSString * year = [NSString stringWithFormat:@"%ld",[components year]];
                
                _txttotalSeaDuration.text = [NSString stringWithFormat:@"%ld Months %ld days",month,[components day]];
            }
        }
        
    }
    else {
        if ([_txtjoiningdate.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"MM'/'dd'/'yyyy hh:mm:ss a"];//04-22-2015 3.00pm
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        endtime = [dateFormatter stringFromDate:selectedDate];
        _txtjoiningdate.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        
        NSString *start = _txtjoiningdate.text;
        NSString *end = _txtLeaveingdate.text;
        NSDateFormatter *f = [[NSDateFormatter alloc] init];
        [f setDateFormat:@"dd-MM-yyyy"];
        NSDate *startDate = [f dateFromString:start];
        
        
        if ([end isEqualToString:@""]) {
            
        }else{
            
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setTimeZone:[NSTimeZone localTimeZone]];
            [f setDateFormat:@"dd-MM-yyyy"];
            NSDate *startDate = [f dateFromString:start];
            NSDate *endDate1 = [f dateFromString:end];
            
            NSDate *endDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate: endDate1];
            
            
            if ([startDate compare:endDate1] == NSOrderedDescending) {
                
                showAlert(@"Invalid date", @"check your date")
                _txttotalSeaDuration.text =@"";
                _txtjoiningdate.text = @"";
                //"startDate is later than endDate
                
                
            } else if ([startDate compare:endDate1] == NSOrderedAscending) {
                //startDate is earlier than endDate
                NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
                
                // pass as many or as little units as you like here, separated by pipes
                NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                
                NSDateComponents *components = [gregorian components:units fromDate:startDate toDate:endDate options:0];
                
                NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                   fromDate: startDate
                                                                     toDate: endDate
                                                                    options: 0] month];
                
                NSLog(@"total months %ld",(long)month);
                
                NSLog(@"%ld", [components day]);
                NSLog(@"%ld", [components month]);
                NSLog(@"%ld", [components year]);
                NSString * year = [NSString stringWithFormat:@"%ld",[components year]];
                
                _txttotalSeaDuration.text = [NSString stringWithFormat:@"%ld Months %ld days",month,[components day]];
                
            }
        }
    }
}

- (IBAction)btnLeavingDate:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced1)
    {
        [self datePickerCancelPressed1];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        
        datePicker1 = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker1.datePickerMode=UIDatePickerModeDate;
        datePicker1.backgroundColor=[UIColor groupTableViewBackgroundColor];
        // datePicker.minimumDate = [NSDate date];
        datePicker1.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView1 = [[UIView alloc] initWithFrame:frame];
        [subView1 addSubview:datePicker1] ;
        subView1.backgroundColor=[UIColor redColor];
        subView1.hidden = NO;
        subView1.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar1 = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar1.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed1)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed1)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar1.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView1 addSubview:toolbar1];
        subView1.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView1.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView1];
        isPickerLaunced11=YES;
    }
}
-(void)datePickerCancelPressed1
{
    subView1.hidden=YES;
    toolbar1.hidden=YES;
    datePicker1.hidden=YES;
    isPickerLaunced11=NO;
}
-(void)datePickerDonePressed1
{
    if(isPickerLaunced11)
    {
        NSDate *selectedDate = datePicker1.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd-MMM-yyyy"];//04-22-2015 3.00pm
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime1 = [dateFormatter stringFromDate:selectedDate];
        _txtLeaveingdate.text = eventDate;
        if ([_txtLeaveingdate.text length] <=0)
        {
        }
        subView1.hidden=YES;
        toolbar1.hidden=YES;
        datePicker1.hidden=YES;
        isPickerLaunced11=NO;
        
        NSString *start = _txtjoiningdate.text;
        NSString *end = _txtLeaveingdate.text;
        
        if ([start isEqualToString:@""]) {
            
        }else{
            
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setDateFormat:@"dd-MM-yyyy"];
            [f setTimeZone:[NSTimeZone localTimeZone]];
            NSDate *startDate = [f dateFromString:start];
            //  NSDate *endDate1 = [f dateFromString:end];
            NSDate *endDate1 = [f dateFromString:end];
            
            NSDate *endDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate: endDate1];
            
            
            if ([startDate compare:endDate1] == NSOrderedDescending) {
                //"startDate is later than endDate
                
                showAlert(@"Invalid date", @"check your date")
                _txttotalSeaDuration.text =@"";
                _txtLeaveingdate.text = @"";
                
                
            } else if ([startDate compare:endDate1] == NSOrderedAscending) {
                //startDate is earlier than endDate
                NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
                
                // pass as many or as little units as you like here, separated by pipes
                NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                
                NSDateComponents *components = [gregorian components:units fromDate:startDate toDate:endDate options:0];
                NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                   fromDate: startDate
                                                                     toDate: endDate
                                                                    options: 0] month];
                
                NSLog(@"total months %ld",(long)month);
                NSLog(@"%ld", [components day]);
                NSLog(@"%ld", [components month]);
                _txttotalSeaDuration.text = [NSString stringWithFormat:@"%ld Months %ld days",month,[components day]];
                //   }
            }
            else{
                NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
                
                // pass as many or as little units as you like here, separated by pipes
                NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                
                NSDateComponents *components = [gregorian components:units fromDate:startDate toDate:endDate options:0];
                NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                   fromDate: startDate
                                                                     toDate: endDate
                                                                    options: 0] month];
                
                NSLog(@"total months %ld",(long)month);
                NSLog(@"%ld", [components day]);
                NSLog(@"%ld", [components month]);
                _txttotalSeaDuration.text = [NSString stringWithFormat:@"%ld Months %ld days",month,[components day]];
                
            }
        }
    }
    else {
        if ([_txtLeaveingdate.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker1.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"MM'/'dd'/'yyyy hh:mm:ss a"];//04-22-2015 3.00pm
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        endtime1 = [dateFormatter stringFromDate:selectedDate];
        
        _txtLeaveingdate.text = eventDate;
        subView1.hidden=YES;
        toolbar1.hidden=YES;
        datePicker1.hidden=YES;
        NSString *start = _txtjoiningdate.text;
        NSString *end = _txtLeaveingdate.text;
        
        if ([start isEqualToString:@""]) {
            
        }else{
            
            NSDateFormatter *f = [[NSDateFormatter alloc] init];
            [f setDateFormat:@"dd-MM-yyyy"];
            [f setTimeZone:[NSTimeZone localTimeZone]];
            NSDate *startDate = [f dateFromString:start];
            //  NSDate *endDate1 = [f dateFromString:end];
            NSDate *endDate1 = [f dateFromString:end];
            
            NSDate *endDate = [NSDate dateWithTimeInterval:(24*60*60) sinceDate: endDate1];
            
            
            if ([startDate compare:endDate] == NSOrderedDescending) {
                //"startDate is later than endDate
                
                showAlert(@"Invalid date", @"check your date")
                _txttotalSeaDuration.text =@"";
                _txtLeaveingdate.text = @"";
                
                
            } else if ([startDate compare:endDate] == NSOrderedAscending) {
                //startDate is earlier than endDate
                
                NSCalendar *gregorian = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
                
                // pass as many or as little units as you like here, separated by pipes
                NSUInteger units = NSYearCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit;
                
                NSDateComponents *components = [gregorian components:units fromDate:startDate toDate:endDate options:0];
                NSInteger month = [[[NSCalendar currentCalendar] components: NSCalendarUnitMonth
                                                                   fromDate: startDate
                                                                     toDate: endDate
                                                                    options: 0] month];
                
                NSLog(@"total months %ld",(long)month);
                
                
                NSLog(@"%ld", [components day]);
                NSLog(@"%ld", [components month]);
                
                _txttotalSeaDuration.text = [NSString stringWithFormat:@"%ld Months %ld days",month,[components day]];
                //   }
            }
        }
    }
}
- (IBAction)btnEngineType:(id)sender {
    [self.view endEditing:YES];
    isenginetype = YES;
    isshiptype = NO;
    isrank = NO;
    [_txtenginetype setTag:8];
    currentTextField = _txtenginetype;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnTonnageType:(id)sender
{
//    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"Actionsheet" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"GRT",@"DWT",@"TEUs",@"M3",@"CEUs",@"LIMS", nil];
//
//    UIPickerView *pickerView = [[UIPickerView alloc] init];
//    pickerView.delegate = self;
//    pickerView.dataSource = self;
//
//    [menu addSubview:pickerView];
//    [menu showInView:self.view];
//    [menu setBounds:CGRectMake(0,0,320, 500)];
//
//    CGRect pickerRect = pickerView.bounds;
//    pickerRect.origin.y = 35;
//    pickerView.frame = pickerRect;
    
    

    /*
   UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"Actionsheet" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"GRT",@"DWT",@"TEUs",@"M3",@"CEUs",@"LIMS", nil];
    // Add the picker
    UIPickerView *pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0,185,0,0)];
    
    pickerView.delegate = self;
    pickerView.showsSelectionIndicator = YES;    // note this is default to NO
    
    [menu addSubview:pickerView];
    [menu showInView:self.view];
    [menu setBounds:CGRectMake(0,0,320, 700)];
    */
    
    
    
    
    //myPickerView.hidden = NO;
    
    
    
    //_ExperienceArray
    UIActionSheet *menu2 = [[UIActionSheet alloc] initWithTitle:@"Actionsheet" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"GRT",@"DWT",@"TEUs",@"M3",@"CEUs",@"LIMS", nil];
    
    UIActionSheet *menu = [[UIActionSheet alloc] initWithTitle:@"Actionsheet" delegate:self                                                     cancelButtonTitle:nil destructiveButtonTitle:nil                                                     otherButtonTitles:nil];
    // ObjC Fast Enumeration
    for (NSString *title in _ExperienceArray)
    {
        [menu addButtonWithTitle:title];
    }
    menu.cancelButtonIndex = [menu addButtonWithTitle:@"Cancel"];
    
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.delegate = self;
    pickerView.dataSource = self;
    CGRect pickerRect = pickerView.bounds;
    pickerRect.origin.y = 35;
    pickerView.frame = pickerRect;
    
    UIButton *startbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    startbtn.frame = CGRectMake(80,230, 170, 73);
    [startbtn setBackgroundImage:[UIImage imageNamed:@"yourbutton.png"] forState:UIControlStateNormal];
    [startbtn addTarget:self action:@selector(pressedbuttonCancel:) forControlEvents:UIControlEventTouchUpInside];
    
    [menu addSubview:pickerView];
    [menu addSubview:startbtn];
    [menu showInView:self.view];
    [menu setFrame:CGRectMake(0,150,320, 350)];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if ([_ExperienceArray count] > 0 && buttonIndex < _ExperienceArray.count)
    {
        NSLog(@"buttonIndex = %@",[_ExperienceArray objectAtIndex:buttonIndex]);
        self.txtTonnageType.text = [NSString stringWithFormat:@"%@",[_ExperienceArray objectAtIndex:buttonIndex]];
    }
//    if(buttonIndex==1){
//        if(currentField == location){
//            if(!completedOnce){
//                [website becomeFirstResponder];
//            }
//        }else{
//            completedOnce = YES;
//        }
//    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger numRows = [_ExperienceArray count];
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [self.txtTonnageType setText:[NSString stringWithFormat:@"%@",[_ExperienceArray objectAtIndex:[myPickerView selectedRowInComponent:0]]]];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_ExperienceArray objectAtIndex:row];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    CGFloat componentWidth = 0.0;
    componentWidth = 135.0;
    
    return componentWidth;
    
}






- (IBAction)btnShiptype:(id)sender {
    [self.view endEditing:YES];
    isshiptype = YES;
    isenginetype = NO;
    isrank = NO;
    [_txtshipType setTag:7];
    currentTextField = _txtshipType;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnRank:(id)sender {
    [self.view endEditing:YES];
    isshiptype = NO;
    isenginetype = NO;
    isrank = YES;
    [_txtRank setTag:0];
    currentTextField = _txtRank;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnUpdateexp:(id)sender {
//    if ([_txtshipType.text isEqualToString:@""]||[_txtshipname.text isEqualToString:@""]||[_txtjoiningdate.text isEqualToString:@""]||[_txtLeaveingdate.text isEqualToString:@""]||[_txttotalSeaDuration.text isEqualToString:@""]||[_txtenginetype.text isEqualToString:@""]||[_txtcompanyname.text isEqualToString:@""]||[_txtRank.text isEqualToString:@""]||[_txtGrt.text isEqualToString:@""]) {
//    showAlert(AlertTitle, @"Please fill all the detail");
//}else{
    if ([_txtcompanyname.text isEqualToString:@""]) {
        showAlert(AlertTitle, @"Please enter Company name");
    }
    else{
        if ([_txtshipname.text isEqualToString:@""]) {
            showAlert(AlertTitle, @"Please enter Ship name");
        }
        else{
            if ([_txtshipType.text isEqualToString:@""]) {
                showAlert(AlertTitle, @"Please select Ship type");
            }
            else{
                if ([_txtRank.text isEqualToString:@""]) {
                    showAlert(AlertTitle, @"Select Rank");
                }
                else{
                    if ([_txtTonnageType.text isEqualToString:@""]) {
                        showAlert(AlertTitle, @"Please Select Tonnage/Engine Power Unit");
                    }
                    else{
                        if ([_txtTonnageValue.text isEqualToString:@""]) {
                            showAlert(AlertTitle, @"Please Enter Tonnage/Engine Power Value");
                        }
                        else{
                            if ([_txtjoiningdate.text isEqualToString:@""]) {
                                showAlert(AlertTitle, @"Please select sign-on date");
                            }
                            else{
                                if ([_txtLeaveingdate.text isEqualToString:@""]) {
                                    showAlert(AlertTitle, @"Please select sign-off date");
                                }
                                else{
                                    if ([_txttotalSeaDuration.text isEqualToString:@""]) {
                                        showAlert(AlertTitle, @"Please Select Sign-on and Sign-off date");
                                    }
                                    else{
                    
    if (_Edit == YES) {
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
    }
    else{
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
        
    }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
}
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self CallExperience];
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(Shiptype) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(enginetype) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(Ranklist) withObject:nil afterDelay:0.1];
    
    
}

-(void)CallExperience
{
   dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httExperience  = [[HttpWrapper alloc] init];
        httExperience.delegate=self;
        httExperience.getbool=NO;
        [httExperience requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_tonnage_type",MainUrl] param:@""];
    });
}

-(void)enginetype
{
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpenginetype  = [[HttpWrapper alloc] init];
        httpenginetype.delegate=self;
        httpenginetype.getbool=NO;
        
        [httpenginetype requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_engine_type",MainUrl] param:@""];
    });
}

-(void)Shiptype
{
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpshiptype  = [[HttpWrapper alloc] init];
        httpshiptype.delegate=self;
        httpshiptype.getbool=NO;
        
        [httpshiptype requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_ship_type",MainUrl] param:@""];
    });
}
-(void)Ranklist
{
  dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprank  = [[HttpWrapper alloc] init];
        httprank.delegate=self;
        httprank.getbool=NO;
        
        [httprank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:@""];
    });
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpshiptype && httpshiptype != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrShiptype = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    else if(wrapper == httprank && httprank != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrranklist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
    else if(wrapper == httpenginetype && httpenginetype != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrEnginetype = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
    else if(wrapper == httpaddData && httpaddData != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrupdate = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            showAlert(AlertTitle, @"Added Successfully");
            [self.navigationController popViewControllerAnimated:NO];
        }
        else
        {
            showAlert(AlertTitle, [dicsResponse valueForKey:@"message"]);
        }
    }
    else if(wrapper == httpupdatedata && httpupdatedata != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrEdit = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            showAlert(AlertTitle, @"Updated Successfully");
            [self.navigationController popViewControllerAnimated:NO];
        }
        else
        {
              showAlert(AlertTitle, [dicsResponse valueForKey:@"message"]);
        }
    }
    else if(wrapper == httExperience)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        _ExperienceArray = [[dicsResponse valueForKey:@"data"] valueForKey:@"TonnageType"];
        NSLog(@"_ExperienceArray ===>> %@",_ExperienceArray);
        
    }
    else
    {
         showAlert(AlertTitle, [dicsResponse valueForKey:@"message"]);
    }
    
    [APP_DELEGATE hideLoadingView];
}
- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    
    [APP_DELEGATE hideLoadingView];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        if (isshiptype == YES) {
            settingsViewController.marrshiptype = marrShiptype;
            settingsViewController.index = currentTextField.tag;
        }
        else if (isrank == YES){
            settingsViewController.marrData = marrranklist;
            settingsViewController.index = currentTextField.tag;
        }
        
        else if (isenginetype == YES){
            settingsViewController.marrenginetype = marrEnginetype;
            settingsViewController.index = currentTextField.tag;
        }
        else{
            settingsViewController.index = currentTextField.tag;
            // settingsViewController.marrnationality = marrnationality;
        }
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            //            if (amountype == YES) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
            //            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
            //            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    
}
- (IBAction)btnBack:(id)sender {
    //    [self.navigationController popViewControllerAnimated:NO];
    ExperienceDetailList *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ExperienceDetailList"];
    
    [self.navigationController pushViewController:s animated:NO];
}
-(void)CallMyMethod
{
    //    candidate_id
    //    company_name
    //    ship_name
    //    joining_date (yyyy-mm-dd)
    //    leaving_date (yyyy-mm-dd)
    //    ship_type
    //    engine_type
    //    rank
    //    grt
    //    total_sea_duration
    //    update = 0
    
    NSString *find = _txtshipType.text;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrShiptype filteredArrayUsingPredicate:predicate];
    shiptypeid= [NSString stringWithFormat:@"%@", [results  valueForKey:@"ShipID" ]] ;
    
    
    NSString * Uid1 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *find1 = _txtenginetype.text;
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"EngineType ==[c] %@", find1];
    NSArray *results1 = [marrEnginetype filteredArrayUsingPredicate:predicate1];
    engineid= [NSString stringWithFormat:@"%@", [results1  valueForKey:@"EngineTypeID" ]] ;
    
    
    NSString * Uid11 =   [[engineid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed1 = [Uid11 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *find2 = _txtRank.text;
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find2];
    NSArray *results2= [marrranklist filteredArrayUsingPredicate:predicate2];
    rankid= [NSString stringWithFormat:@"%@", [results2  valueForKey:@"RankID" ]] ;
    
    
    NSString * Uid12 =   [[rankid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed2 = [Uid12 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:_txtcompanyname.text forKey:@"company_name"];
    [parameters setValue:_txtshipname.text forKey:@"ship_name"];
    
    NSString * date = [NSString stringWithFormat:@"%@",_txtjoiningdate.text];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSString * joiningdate = [formatedt stringFromDate:fulldate];
    NSString * ldate = [NSString stringWithFormat:@"%@",_txtLeaveingdate.text];
    
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
    
    NSDate *leavedate=[formatedt1 dateFromString:ldate];
    [formatedt1 setDateFormat:@"yyyy-MM-dd"];
    NSString * leavedates = [formatedt1 stringFromDate:leavedate];
    [parameters setValue:joiningdate forKey:@"joining_date"];
    [parameters setValue:leavedates forKey:@"leaving_date"];
    //[parameters setValue:_txtGrt.text forKey:@"grt"];

    NSLog(@"txtTonnageType.text = %@",_txtTonnageType.text);
    NSLog(@"txtTonnageType.text = %@",_txtTonnageValue.text);
    
    [parameters setValue:_txtTonnageType.text forKey:@"tonnage_type"];
    [parameters setValue:_txtTonnageValue.text forKey:@"tonnage_value"];
    
    //[parameters setValue:_txtTonnageType.text forKey:@"tonnage_type"];
    //[parameters setValue:_txtTonnageValue.text forKey:@"tonnage_value"];

    [parameters setValue:_txttotalSeaDuration.text forKey:@"total_sea_duration"];
    [parameters setValue:trimmed2 forKey:@"rank"];
    [parameters setValue:trimmed1 forKey:@"engine_type"];
    [parameters setValue:trimmed forKey:@"ship_type"];
    [parameters setValue:[_detail valueForKey:@"ExperienceID"] forKey:@"experience_id"];
    
    [parameters setValue:@"1" forKey:@"update"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpupdatedata  = [[HttpWrapper alloc] init];
        httpupdatedata.delegate=self;
        httpupdatedata.getbool=NO;
        
        [httpupdatedata requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_experience_dtl",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)CallMyMethod1
{
    //    candidate_id
    //    company_name
    //    ship_name
    //    joining_date (yyyy-mm-dd)
    //    leaving_date (yyyy-mm-dd)
    //    ship_type
    //    engine_type
    //    rank
    //    grt
    //    total_sea_duration
    //    update = 0
    
    NSString *find = _txtshipType.text;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrShiptype filteredArrayUsingPredicate:predicate];
    shiptypeid= [NSString stringWithFormat:@"%@", [results  valueForKey:@"ShipID" ]] ;
    
    NSLog(@"shiptypeid = %@",shiptypeid);
    NSLog(@"results = %@",results);
    
    
    NSString * Uid1 =   [[shiptypeid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *find1 = _txtenginetype.text;
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"EngineType ==[c] %@", find1];
    NSArray *results1 = [marrEnginetype filteredArrayUsingPredicate:predicate1];
    engineid= [NSString stringWithFormat:@"%@", [results1  valueForKey:@"EngineTypeID" ]] ;
    
    
    NSString * Uid11 =   [[engineid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed1 = [Uid11 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    
    NSString *find2 = _txtRank.text;
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find2];
    NSArray *results2= [marrranklist filteredArrayUsingPredicate:predicate2];
    rankid= [NSString stringWithFormat:@"%@", [results2  valueForKey:@"RankID" ]] ;
    
    
    NSString * Uid12 =   [[rankid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed2 = [Uid12 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:_txtcompanyname.text forKey:@"company_name"];
    [parameters setValue:_txtshipname.text forKey:@"ship_name"];
    
    NSString * date = [NSString stringWithFormat:@"%@",_txtjoiningdate.text];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSString * joiningdate = [formatedt stringFromDate:fulldate];
    NSString * ldate = [NSString stringWithFormat:@"%@",_txtLeaveingdate.text];
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
    NSDate *leavedate=[formatedt1 dateFromString:ldate];
    [formatedt1 setDateFormat:@"yyyy-MM-dd"];
    NSString * leavedates = [formatedt1 stringFromDate:leavedate];
    [parameters setValue:joiningdate forKey:@"joining_date"];
    [parameters setValue:leavedates forKey:@"leaving_date"];
    
    //[parameters setValue:_txtGrt.text forKey:@"grt"];
    
    [parameters setValue:_txtTonnageType.text forKey:@"tonnage_type"];
    [parameters setValue:_txtTonnageValue.text forKey:@"tonnage_value"];
    
    [parameters setValue:_txttotalSeaDuration.text forKey:@"total_sea_duration"];
    [parameters setValue:trimmed2 forKey:@"rank"];
    [parameters setValue:trimmed1 forKey:@"engine_type"];
    [parameters setValue:trimmed forKey:@"ship_type"];
    [parameters setValue:@"0" forKey:@"update"];
//    [parameters setValue:_txtTonnageType.text forKey:@"TonnageType"];
//    [parameters setValue:_txtTonnageValue.text forKey:@"TonnageValue"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpaddData  = [[HttpWrapper alloc] init];
        httpaddData.delegate=self;
        httpaddData.getbool=NO;
        
        [httpaddData requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_experience_dtl",MainUrl] param:[parameters copy]];
    });
}
// NOTE: This code assumes you have set the UITextField(s)'s delegate property to the object that will contain this code, because otherwise it would never be called.
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // allow backspace
    if (!string.length)
    {
        return YES;
    }
    
    // Prevent invalid character input, if keyboard is numberpad
    if (textField.keyboardType == UIKeyboardTypeNumberPad)
    {
        if ([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound)
        {
            // BasicAlert(@"", @"This field accepts only numeric entries.");
            return NO;
        }
    }
    
    // verify max length has not been exceeded
    NSString *proposedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (proposedText.length > 6) // 4 was chosen for SSN verification
    {
        // suppress the max length message only when the user is typing
        // easy: pasted data has a length greater than 1; who copy/pastes one character?
        if (string.length > 1)
        {
            // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
        }
        
        return NO;
    }
    
    // only enable the OK/submit button if they have entered all numbers for the last four of their SSN (prevents early submissions/trips to authentication server)
   // self.answerButton.enabled = (proposedText.length == 4);
    
    return YES;
}
@end
