//
//  MyProfileCandidate.m
//  Infinity Dynamics
//
//  Created by My Mac on 10/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "MyProfileCandidate.h"
#import "SWRevealViewController.h"
#import "Constants.h"
#import "MyProfileCAndidateCell.h"
#import "UIImageView+WebCache.h"
#import "EditProfile.h"
#import "SeamenDetaillist.h"
#import "JobApplied.h"
#import "MatchedJob.h"
#import "PassportDetailList.h"
#import "COCDetails.h"
#import "DCEDetails.h"
#import "EducationDetailList.h"
#import "ExperienceDetailList.h"
#import "VisaDetails.h"
#import "STCWDetails.h"
#import "BuildResume.h"
@interface MyProfileCandidate ()

@end

@implementation MyProfileCandidate

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrname =  [[NSMutableArray alloc]initWithObjects:@"Edit Profile",@"Education Details",@"Experience Details",@"Seaman Book Details",@"Passport Details",@"Visa Details",@"COC/COP Details",@"DCE Details",@"STCW Certificates",@"History",@"Matching Jobs",@"Build Resume", nil];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
 
    _lblName.text = [NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"FirstName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"MiddleName"],[[NSUserDefaults standardUserDefaults] valueForKey:@"LastName"]];
    _lblRank.text = [NSString stringWithFormat:@"Rank:%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"RankName"]];
    _lblEmailid.text = [NSString stringWithFormat:@"Email:%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"EmailID"]];
    _lblIndosno.text = [NSString stringWithFormat:@"INDOS#%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"Indos"]];
    _lblMobileno.text = [NSString stringWithFormat:@"Mobile:%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"MobileNo"]];
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"ImageURL"]];
    if ([profilepicname  isEqualToString:@"http://test.infinitydynamics.in/uploads/user_profile_image/"]) {
        
        _myprofileimage.image = [UIImage imageNamed: @"user-1"];
    }else{
        NSString *fullpath = [NSString stringWithFormat:@"%@",profilepicname];
        [_myprofileimage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",fullpath]] placeholderImage:[UIImage imageNamed:@"wf.png"]];
        //   _Profileimage.image = [self getImageFromURL:fullpath];
    }
    
    _myprofileimage.layer.masksToBounds = YES;
    _myprofileimage.layer.cornerRadius = _myprofileimage.frame.size.width/2;
    
    
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyProfileCAndidateCell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MyProfileCAndidateCell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return arrname.count;
    
    
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(MyProfileCAndidateCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.lbldescription.text = [NSString stringWithFormat:@"%@",[arrname objectAtIndex:indexPath.section]];
    
   

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    
    if (indexPath.section == 0) {
        EditProfile *s =[self.storyboard instantiateViewControllerWithIdentifier:@"EditProfile"];
        s.profilecandidatebool = YES;
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 1) {
        EducationDetailList *s =[self.storyboard instantiateViewControllerWithIdentifier:@"EducationDetailList"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 2) {
        ExperienceDetailList *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ExperienceDetailList"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 3) {
        SeamenDetaillist *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SeamenDetaillist"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 4) {
        PassportDetailList *s =[self.storyboard instantiateViewControllerWithIdentifier:@"PassportDetailList"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 5) {
        VisaDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"VisaDetails"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 6) {
        COCDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"COCDetails"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 8) {
        STCWDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"STCWDetails"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 7) {
        DCEDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"DCEDetails"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    
    else  if (indexPath.section == 9) {
        JobApplied *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobApplied"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
  
    else  if (indexPath.section == 10) {
        MatchedJob *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MatchedJob"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    else  if (indexPath.section == 11) {
        BuildResume *s =[self.storyboard instantiateViewControllerWithIdentifier:@"BuildResume"];
        // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:s animated:NO];
    }
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}


@end
