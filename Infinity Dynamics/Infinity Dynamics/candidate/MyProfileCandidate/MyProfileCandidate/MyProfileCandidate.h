//
//  MyProfileCandidate.h
//  Infinity Dynamics
//
//  Created by My Mac on 10/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface MyProfileCandidate : UIViewController<HttpWrapperDelegate>{
     NSMutableArray* arrname;
       UIView *picker;   /////menu picker view
}
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UIImageView *myprofileimage;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblEmailid;
@property (strong, nonatomic) IBOutlet UILabel *lblRank;
@property (strong, nonatomic) IBOutlet UILabel *lblMobileno;
@property (strong, nonatomic) IBOutlet UILabel *lblIndosno;

@property (strong, nonatomic) IBOutlet UITableView *tableview;
@end
