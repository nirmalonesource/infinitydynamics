//
//  Aboutus.h
//  Infinity Dynamics
//
//  Created by My Mac on 10/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "TextFieldValidator.h"
@interface Aboutus : UIViewController<HttpWrapperDelegate,UITextViewDelegate>{
    HttpWrapper *httpaboutus;
  NSMutableArray *marraboutus;
      UIView *picker;
  
    
    
}
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmessage;
@property (weak, nonatomic) IBOutlet TextFieldValidator *txtsubject;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtphone;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtemail;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtname;
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
@property (strong, nonatomic) IBOutlet UILabel *lblcontactdetail;
- (IBAction)btnSendmessage:(id)sender;

@end
