//
//  Aboutus.m
//  Infinity Dynamics
//
//  Created by My Mac on 10/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Aboutus.h"
#import "Constants.h"
#import "HomeScreen.h"
#import "SWRevealViewController.h"
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{5,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{5,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]{10}"
@interface Aboutus ()

@end

@implementation Aboutus

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    [self setupAlerts];
}
-(void)setupAlerts{
    [_txtname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    [_txtemail addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController setDelegate:self];
    if ( revealViewController )
    {
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    }
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{
   // [APP_DELEGATE showLoadingView:@""];
}
-(void)Aboutus
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:_txtname.text forKey:@"name"];
      [parameters setValue:_txtemail.text forKey:@"email"];
      [parameters setValue:_txtphone.text forKey:@"phone"];
      [parameters setValue:_txtsubject.text forKey:@"subject"];
      [parameters setValue:_txtmessage.text forKey:@"message"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpaboutus  = [[HttpWrapper alloc] init];
        httpaboutus.delegate=self;
        httpaboutus.getbool=NO;
        [httpaboutus requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@contact_us",MainUrl] param:[parameters copy]];
    });
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpaboutus && httpaboutus != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marraboutus = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marraboutus);
            showAlert(AlertTitle,@"Message sent successfully Thank you for contacting us.");
            HomeScreen *s =[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
            [self.navigationController pushViewController:s animated:NO];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            [APP_DELEGATE hideLoadingView];
        }
        [APP_DELEGATE hideLoadingView];
    }
}
    - (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
    {
        NSLog(@"Fetch Data Fail Error:%@",error);
        [APP_DELEGATE hideLoadingView];
    }
- (IBAction)btnSendmessage:(id)sender {
    if ([_txtname validate])
    {
        if([_txtemail validate]){
            if([_txtphone.text isEqualToString:@""]){
                showAlert(AlertTitle,@"Please Enter Phone");
            }else{
                if([_txtsubject.text isEqualToString:@""]){
                    showAlert(AlertTitle,@"Please Enter Subject");
                }else{
                    if([_txtmessage.text isEqualToString:@""]){
                        showAlert(AlertTitle,@"Please Enter Message");
                    }else{
                        [self performSelector:@selector(Aboutus) withObject:nil afterDelay:0.1];
                    }
                }
            }
        }
        else{
            showAlert(AlertTitle,@"Please Enter Email");
        }
    }
    else{
         showAlert(AlertTitle,@"Please Enter Name");
    }
}
@end
