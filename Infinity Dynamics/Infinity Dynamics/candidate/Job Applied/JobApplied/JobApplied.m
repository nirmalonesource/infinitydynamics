//
//  JobApplied.m
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "JobApplied.h"
#import "Constants.h"
#import "JobAppliedcell.h"
#import "JobAppliedcellNew.h"

#import "AddSeamanDetail.h"
#import "UIImageView+Haneke.h"
#import "MyProfileCandidate.h"
@interface JobApplied ()

@end

@implementation JobApplied

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableview.estimatedRowHeight = 90; // or any other number that makes sense for your cells
    self.tableview.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JobAppliedcellNew * cell = [tableView dequeueReusableCellWithIdentifier:@"JobAppliedcellNew"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"JobAppliedcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return marrJobappliedlist.count;
    
    
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(JobAppliedcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    cell.lblNumber.text = [NSString stringWithFormat:@"%@",[[marrJobappliedlist  objectAtIndex:indexPath.section]valueForKey:@"JobTitle"] ];
    cell.lblshiptype.text = [NSString stringWithFormat:@"%@",[[marrJobappliedlist  objectAtIndex:indexPath.section] valueForKey:@"ShipType"]];
    
    NSString * date = [NSString stringWithFormat:@"%@",[[marrJobappliedlist  objectAtIndex:indexPath.section] valueForKey:@"ApplyDate"]];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    NSString * JoiningDate = [formatedt stringFromDate:fulldate];
    
    cell.lblapplied.text = [NSString stringWithFormat:@"Applied on %@",JoiningDate];
    
   // NSString * DescriptionString = [NSString stringWithFormat:@"%@",[[marrJobappliedlist  objectAtIndex:indexPath.section] valueForKey:@"Description"]];
        NSAttributedString * attrStr =
        [[NSAttributedString alloc] initWithData:[[[marrJobappliedlist  objectAtIndex:indexPath.section] valueForKey:@"Description"] dataUsingEncoding:NSUnicodeStringEncoding]
                                         options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                              documentAttributes:nil error:nil];
    cell.txtview.attributedText = attrStr;
    
//    cell.txtview.numberOfLines = 1;
    //[self getLabelHeight:cell.txtview];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrJobappliedlist objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [cell.imageview hnk_setImageFromURL:url];
    [APP_DELEGATE hideLoadingView];
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint                                                   options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:label.font} context:context].size;
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpJobappliedlist  = [[HttpWrapper alloc] init];
        httpJobappliedlist.delegate=self;
        httpJobappliedlist.getbool=NO;
        
        [httpJobappliedlist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_applied_job",MainUrl] param:[parameters copy]];
    });

}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpJobappliedlist && httpJobappliedlist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrJobappliedlist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
            [self.tableview reloadData];
        }
        else{
            
            
        }
    }
    
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (IBAction)btnBack:(id)sender {
    MyProfileCandidate *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
    // s.profilecandidatebool = YES;
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}

@end
