//
//  JobApplied.h
//  Infinity Dynamics
//
//  Created by My Mac on 12/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface JobApplied : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httdetailist,*httpJobappliedlist;
    NSMutableArray *marrJobappliedlist;
    NSString * seaman_id;
}
@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)btnBack:(id)sender;

@end
