//
//  Locationcell.h
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCEDetailscell: UITableViewCell{

    bool gradients;
}
@property (strong, nonatomic) IBOutlet UILabel *lbllevel;
@property (strong, nonatomic) IBOutlet UILabel *lblcertificatetype;

@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceofissue;
@property (strong, nonatomic) IBOutlet UILabel *lblvalidity;
@property (strong, nonatomic) IBOutlet UIButton *btnedit;
@property (strong, nonatomic) IBOutlet UIButton *btndelete;





@end
