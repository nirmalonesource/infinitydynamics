//
//  Locationcell.m
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import "MatchJobcell.h"
#import "UIColor+CL.h"
#import "UIImageView+Haneke.h"
@implementation MatchJobcell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    //    So it should have the Flex name, the progress bar, End date, amount collected/sold, and then the date contributed at the top instead of the organizer's name.
       
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}
- (void)prepareForReuse
{
    [self.imageView hnk_cancelSetImage];
    self.imageView.image = nil;
}

@end
