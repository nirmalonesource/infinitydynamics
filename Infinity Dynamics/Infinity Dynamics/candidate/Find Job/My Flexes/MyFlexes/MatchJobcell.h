//
//  Locationcell.h
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MatchJobcell: UITableViewCell{

    bool gradients;
}
@property (weak, nonatomic) IBOutlet UIProgressView *prog_prof;
@property (strong, nonatomic) IBOutlet UIImageView *imageview;

@property (strong, nonatomic) IBOutlet UILabel *lblstartdate;
@property (strong, nonatomic) IBOutlet UIButton *btnrequestmoney;

@property (strong, nonatomic) IBOutlet UIView *backgroundview;


@property (weak, nonatomic) IBOutlet UILabel *lblname;
@property (weak, nonatomic) IBOutlet UILabel *lblshiptype;

@property (strong, nonatomic) IBOutlet UILabel *lbldate;

@property (strong, nonatomic) IBOutlet UILabel *lblamount;




@end
