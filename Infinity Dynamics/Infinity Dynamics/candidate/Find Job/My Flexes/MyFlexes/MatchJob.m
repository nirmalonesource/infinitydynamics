//
//  MyFlexes.m
//  FlexCash
//
//  Created by My Mac on 21/11/17.
//  Copyright © 2017 Rishi. All rights reserved.
//

#import "MatchJob.h"
#import "myModel.h"
#import "Constants.h"
#import "MatchJobcell.h"
#import "UIImageView+Haneke.h"
#import "JobDetails.h"
@interface MatchJob () <UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

@end

@implementation MatchJob
@synthesize  Search_View;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (IsIphone6Plus ) {
        
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height);
    }
    else if (IsIphone6){
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height);
    }
    else{
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height);
    }
//    Search_View.hidden = NO;
//    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, Search_View.frame.size.width+0, Search_View.frame.size.height+5)];
//    searchBar.autoresizingMask =0;
//    searchBar.delegate = self;
//    searchBar.placeholder = @"Ship Type";
//    searchBar.showsScopeBar = YES;
//    
//    [searchBar setBackgroundColor:[UIColor clearColor]];
//    searchBar.layer.borderWidth = 1.0f;
//    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
//    [searchBar setBarTintColor:[UIColor whiteColor]]; //this is what you want
//    [searchBar setShowsCancelButton:YES animated:YES];
//    
//    [Search_View addSubview:searchBar];
//   // [searchBar becomeFirstResponder];
//    
//    UITextField *searchField = [searchBar valueForKey:@"searchField"];
//    
//    // To change background color
//    searchField.backgroundColor = [UIColor whiteColor];
//    
//    // To change text color
//    searchField.textColor = [UIColor blackColor];
//    searchField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
//    // To change placeholder text color
//    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Company Name"];
//    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
//    placeholderLabel.textColor = [UIColor grayColor];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MatchJobcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"MatchJobcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isFilterd == YES)
    {
        return filterdarray.count;
    }
    else
    {
        return marrMatchjob.count;
    }// in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(MatchJobcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isFilterd == YES)
    {
        cell.lblname.text = [NSString stringWithFormat:@"%@",[filterdarray objectAtIndex:indexPath.section]  ];
        
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"CompanyName"] ];
    }
    else
    {
        cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"CompanyName"] ];
    }
    
    cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"JobTitle"] ];
    cell.lblshiptype.text = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"ShipType"] ];
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
  //  [cell.imageview hnk_setImageFromURL:url];
    [cell.imageview hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];

    [APP_DELEGATE hideLoadingView];
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self callcountapi:@"0" companyid:[[marrMatchjob objectAtIndex:indexPath.section] valueForKey:@"JobID"]];
    
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [marrMatchjob objectAtIndex:indexPath.section] ;
    
    [self.navigationController pushViewController:s animated:NO];
    
}

-(void)callcountapi:(NSString *)type companyid:(NSString *)companyid
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:type forKey:@"Type"];
    [AddPost setValue:companyid forKey:@"ID"];
    [AddPost setValue:[myModel getIPAddress] forKey:@"IPAddress"];
    
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@ship_shore_job_visit_count",MainUrl] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}



///searchbar
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isFilterd = NO;
    [self SearchBarDismiss];
    [self.tableview reloadData];
}

//SearchBarDismiss
#pragma mark - SearchBar Delegate

- (void)SearchBarDismiss
{
    isFilterd = NO;
    [self.view endEditing:YES];
    //  Btn_Search.hidden = NO;
    // Search_View.hidden = YES;
}

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        isFilterd = NO;
    }
    else
    {
        isFilterd = YES;
        
        filterdarray = [[NSMutableArray alloc]init];
        
        //https://stackoverflow.com/questions/10611362/ios-coredata-nspredicate-to-query-multiple-properties-at-once
        
        filterdarray = [[NSMutableArray alloc]init];
        
        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"CompanyName contains[cd] %@ OR JobTitle contains[cd] %@", searchText, searchText];
        
        filterdarray = [NSMutableArray arrayWithArray:[marrMatchjob  filteredArrayUsingPredicate:predicate]];
        
        [self.tableview reloadData];
        
        
        
        /*
         
         NSPredicate *predicate =[NSPredicate predicateWithFormat:@"CompanyName contains[cd] %@ OR JobTitle contains[cd] %@", searchText, searchText];
         
         //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"CompanyName contains[cd] %@ OR JobTitle contains[cd] %@", searchText, searchText];
         
         filterdarray = [NSMutableArray arrayWithArray:[marrRecentjob  filteredArrayUsingPredicate:predicate]];
         
         [self.tableview reloadData];
         
        */
        
        
        

//        NSPredicate *bPredicate =
//        [NSPredicate predicateWithFormat:@"SELF JobTitle[c] 'b'"];
//        [[filterdarray valueForKey:@"JobTitle"] filteredArrayUsingPredicate:bPredicate];
//        // beginWithB contains { @"Bill", @"Ben" }.
//        NSLog(@"marrRecentjob = %@",filterdarray);
//
//
//        NSPredicate *sPredicate =
//        [NSPredicate predicateWithFormat:@"SELF CompanyName[c] 's'"];
//        [[filterdarray valueForKey:@"CompanyName"] filteredArrayUsingPredicate:sPredicate];
//        NSLog(@"marrRecentjob = %@",filterdarray);

        
    }
    [self.tableview reloadData];
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  isFilterd = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    // isFilterd = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
}

- (IBAction)btnsearch:(id)sender {
    //////////for searching
    //Create SearchBar
//    Search_View.hidden = NO;
//    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, Search_View.frame.size.width+0, Search_View.frame.size.height+5)];
//    searchBar.autoresizingMask =0;
//    searchBar.delegate = self;
//    searchBar.placeholder = @"Ship Type";
//    searchBar.showsScopeBar = YES;
//
//    [searchBar setBackgroundColor:[UIColor clearColor]];
//    searchBar.layer.borderWidth = 1.0f;
//    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
//    [searchBar setBarTintColor:[UIColor whiteColor]]; //this is what you want
//    [searchBar setShowsCancelButton:YES animated:YES];
//
//    [Search_View addSubview:searchBar];
//    [searchBar becomeFirstResponder];
//
//    UITextField *searchField = [searchBar valueForKey:@"searchField"];
//
//    // To change background color
//    searchField.backgroundColor = [UIColor whiteColor];
//
//    // To change text color
//    searchField.textColor = [UIColor blackColor];
//    searchField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
//    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
//    // To change placeholder text color
//    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Company Name"];
//    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
//    placeholderLabel.textColor = [UIColor grayColor];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpMatchjob  = [[HttpWrapper alloc] init];
        httpMatchjob.delegate=self;
        httpMatchjob.getbool=NO;
        
        [httpMatchjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_match_job",MainUrl] param:[parameters copy]];
    });
    
}


-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpMatchjob && httpMatchjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrMatchjob = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrMatchjob);
            [self.tableview reloadData];
            [APP_DELEGATE hideLoadingView];
        }
        else{
            
            [APP_DELEGATE hideLoadingView];
        }
          [APP_DELEGATE hideLoadingView];
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

@end
