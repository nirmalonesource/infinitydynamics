//
//  Profileview.m
//  FlexCash
//
//  Created by My Mac on 21/11/17.
//  Copyright © 2017 Rishi. All rights reserved.
//

#import "FindJob.h"
#import "Recentjob.h"

#import "UIColor+CL.h"

#import "Constants.h"
#import "myModel.h"

#import "UIImageView+WebCache.h"
#import "AsyncImageDetail.h"
#import "MatchJob.h"
#import "SWRevealViewController.h"
#import "MyProfileCandidate.h"
@interface FindJob ()

@end

@implementation FindJob
@synthesize startStopButtonIsActive = _startStopButtonIsActive;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
      appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
    _myjoined.tintColor = [UIColor colorWithHex:0xDD1700];
    _myFlexes.tintColor = [UIColor blackColor];
    
    [self performSegueWithIdentifier:@"Myjoined" sender:nil];

}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
   
   
}
- (IBAction)btnmyjoined:(id)sender {
    _myjoined.tintColor = [UIColor colorWithHex:0xDD1700];
    _myFlexes.tintColor = [UIColor blackColor];
    [self performSegueWithIdentifier:@"Myjoined" sender:nil];
}
- (IBAction)btnMyflexes:(id)sender {
    _myFlexes.tintColor = [UIColor colorWithHex:0xDD1700];
    _myjoined.tintColor = [UIColor blackColor];
    [self performSegueWithIdentifier:@"Myflexes" sender:nil];
}


-(void)logout{
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Logout Alert"
                                                      message:@"Do You Want To Exit?"
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:@"Cancel",nil];
    message.tag = 0;
    [message show];
}
- (IBAction)BtnLogout:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Logout Alert"
                                                      message:@"Do You Want To Exit?"
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:@"Cancel",nil];
    [message show];
}

- (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}
- (IBAction)BtnProfile:(id)sender {
    MyProfileCandidate *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
@end
