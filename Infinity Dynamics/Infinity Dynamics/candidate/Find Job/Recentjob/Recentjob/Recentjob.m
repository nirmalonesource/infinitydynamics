//
//  Myjoined.m
//  FlexCash
//
//  Created by My Mac on 21/11/17.
//  Copyright © 2017 Rishi. All rights reserved.
//

#import "Recentjob.h"
#import "myModel.h"
#import "Constants.h"
#import "Recentjobcell.h"
#import "UIImageView+Haneke.h"
#import "JobDetails.h"
#import "NSPredicate+Search.h"


@interface Recentjob () <UISearchBarDelegate>
{
    UITextField *searchField;
}
@end

@implementation Recentjob
@synthesize  Search_View;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        appdelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
     self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    if (IsIphone6Plus ) {
        
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height-125);
    }
    else if (IsIphone6){
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height-125);
    }
    else{
        self.tableview.frame = CGRectMake(self.view.frame.origin.x,self.tableview.frame.origin.y,self.view.frame.size.width,self.tableview.frame.size.height-125);
    }
    Search_View.hidden = NO;
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, Search_View.frame.size.width+0, 40)];
    searchBar.autoresizingMask =0;
    searchBar.delegate = self;
    searchBar.placeholder = @"Search";
    searchBar.showsScopeBar = YES;
    
    [searchBar setBackgroundColor:[UIColor clearColor]];
    searchBar.layer.borderWidth = 1.0f;
    searchBar.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [searchBar setBarTintColor:[UIColor whiteColor]]; //this is what you want
    [searchBar setShowsCancelButton:YES animated:YES];
    [Search_View addSubview:searchBar];
  //  [searchBar becomeFirstResponder];
    searchField = [searchBar valueForKey:@"searchField"];
    // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
//    [searchField addTarget:self
//                    action:@selector(textFieldChange:)
//          forControlEvents:UIControlEventEditingChanged];
    // To change text color
    searchField.textColor = [UIColor blackColor];
    searchField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
    // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
    
//    UIButton *searchby = [[UIButton alloc] initWithFrame:CGRectMake(10, 50, Search_View.frame.size.width-20, 40)];
//    [searchby addTarget:self
//               action:@selector(aMethod:)
//     forControlEvents:UIControlEventTouchUpInside];
//    [Search_View addSubview:searchby];
        searchtype = @"";
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == searchField)
    {
        if (string.length == 0)
        {
            isFilterd = NO;
            [self SearchBarDismiss];
            [self.tableview reloadData];
        }
    }
    
    return YES;
}

-(IBAction)textFieldChange:(id)sender
{
//    isFilterd = NO;
//    [self SearchBarDismiss];
//    [self.tableview reloadData];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Recentjobcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"Recentjobcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (isFilterd == YES)
    {
        return filterdarray.count;
    }
    else
    {
    return marrRecentjob.count;
    }// in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(Recentjobcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isFilterd == YES)
    {
         cell.lblname.text = [NSString stringWithFormat:@"%@",[filterdarray objectAtIndex:indexPath.section]  ];
         cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"CompanyName"] ];
    }
    else
    {
          cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"CompanyName"] ];
        
    }
  
    cell.lblamount.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"JobTitle"] ];
     cell.lblshiptype.text = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"ShipType"] ];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
   // [cell.imageview hnk_setImageFromURL:url];
    [cell.imageview hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"no_image"]];
    [APP_DELEGATE hideLoadingView];
 
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self callcountapi:@"0" companyid:[[marrRecentjob objectAtIndex:indexPath.section] valueForKey:@"JobID"]];
    
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [marrRecentjob objectAtIndex:indexPath.section] ;
    
    [self.navigationController pushViewController:s animated:NO];
    
}

-(void)callcountapi:(NSString *)type companyid:(NSString *)companyid
{
    
    NSMutableDictionary *AddPost = [[NSMutableDictionary alloc]init];
    
    [AddPost setValue:type forKey:@"Type"];
    [AddPost setValue:companyid forKey:@"ID"];
    [AddPost setValue:[myModel getIPAddress] forKey:@"IPAddress"];
    
    NSLog(@"PARAM ---%@",AddPost);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"%@ship_shore_job_visit_count",MainUrl] parameters:[AddPost copy] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[responseObject valueForKey:@"data"];
        
        NSString *msg=[NSString stringWithFormat:@"%@",[responseObject valueForKey:@"message"]];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [APP_DELEGATE hideLoadingView];
    }];
}

-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    return result;
}



///searchbar
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    isFilterd = NO;
    [self SearchBarDismiss];
    [self.tableview reloadData];
}

//SearchBarDismiss
#pragma mark - SearchBar Delegate

- (void)SearchBarDismiss
{
    isFilterd = NO;
    [self.view endEditing:YES];
    //  Btn_Search.hidden = NO;
   // Search_View.hidden = YES;
}

//-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    filterdarray = [[NSMutableArray alloc]init];
//    if (searchText.length == 0)
//    {
//        filterdarray = [[NSMutableArray alloc] initWithArray:marrRecentjob];
//
//        NSLog(@"filterdarray AAA = %@",filterdarray);
//        [self.tableview reloadData];
//        return;
//    }
//
//    NSPredicate *predicate = [NSPredicate predicateWithSearch:searchText searchTerm:@"searchTerms"];
//    filterdarray = [[NSMutableArray alloc] initWithArray:[marrRecentjob filteredArrayUsingPredicate:predicate]];
//    NSLog(@"filterdarray BBB = %@",filterdarray);
//    [self.tableview reloadData];
//
//}


- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText
{
//    if (searchText.length == 0)
//    {
//        isFilterd = NO;
//    }
//    else
//    {
//        //https://stackoverflow.com/questions/10611362/ios-coredata-nspredicate-to-query-multiple-properties-at-once
//
//        isFilterd = YES;
//
//        //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@",searchText];
//
////https://stackoverflow.com/questions/18822777/nspredicate-on-an-nsarray-to-search-for-any-object
//
//        filterdarray = [[NSMutableArray alloc]init];
//
//        NSPredicate *predicate =[NSPredicate predicateWithFormat:@"CompanyName contains[cd] %@ OR JobTitle contains[cd] %@", searchText, searchText];
//
//        filterdarray = [NSMutableArray arrayWithArray:[marrRecentjob filteredArrayUsingPredicate:predicate]];
//
//        [self.tableview reloadData];
//    }
    
}


-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    //  isFilterd = YES;
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    // isFilterd = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
}

- (IBAction)btnsearch:(id)sender {
    //////////for searching
    //Create SearchBar
    Search_View.hidden = NO;
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, Search_View.frame.size.width+0, Search_View.frame.size.height+5)];
    searchBar.autoresizingMask =0;
    searchBar.delegate = self;
    searchBar.placeholder = @"Search";
    searchBar.showsScopeBar = YES;
    
    [searchBar setBackgroundColor:[UIColor clearColor]];
    searchBar.layer.borderWidth = 1.0f;
    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
    [searchBar setBarTintColor:[UIColor whiteColor]]; //this is what you want
    [searchBar setShowsCancelButton:YES animated:YES];
    
    [Search_View addSubview:searchBar];
    [searchBar becomeFirstResponder];
    
    searchField = [searchBar valueForKey:@"searchField"];
    
    // To change background color
    searchField.backgroundColor = [UIColor whiteColor];
    
    // To change text color
    searchField.textColor = [UIColor blackColor];
    searchField.font = [UIFont fontWithName:@"Roboto-Regular" size:15];
    searchField.autocorrectionType = UITextAutocapitalizationTypeAllCharacters;
    // To change placeholder text color
    searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Search"];
    UILabel *placeholderLabel = [searchField valueForKey:@"placeholderLabel"];
    placeholderLabel.textColor = [UIColor grayColor];
    
}
-(void)viewWillAppear:(BOOL)animated{
    
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    //candidate_id:1
    //keyword:oil
    //searchtype:3
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:searchField.text forKey:@"keyword"];
    [parameters setValue:searchtype forKey:@"searchtype"];


    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpRecentjob  = [[HttpWrapper alloc] init];
        httpRecentjob.delegate=self;
        httpRecentjob.getbool=NO;
        
        [httpRecentjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@search_job",MainUrl] param:[parameters copy]];
    });
    
    
}


-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpRecentjob && httpRecentjob != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrRecentjob = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSLog(@"%@",marrRecentjob);
            [self.tableview reloadData];
            [APP_DELEGATE hideLoadingView];

        }
        else{
            [APP_DELEGATE hideLoadingView];
            
        }
        [APP_DELEGATE hideLoadingView];
    }
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
       
            settingsViewController.index = currentTextField.tag;
        
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            //            if (amountype == YES) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
            //            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
            //            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    
    if ([_txtfindjobs.text isEqualToString:@""]) {
        searchtype = @"";
    }
    else{
        if ([_txtfindjobs.text isEqualToString:@"Jobs by Company"]) {
            searchtype= [NSString stringWithFormat:@"1" ] ;
        }
        else if ([_txtfindjobs.text isEqualToString:@"Jobs as per Rank"]){
            searchtype= [NSString stringWithFormat:@"2" ];
        }
        else if ([_txtfindjobs.text isEqualToString:@"Jobs by Shiptype"]){
            searchtype= [NSString stringWithFormat:@"3" ];
        }
        else if ([_txtfindjobs.text isEqualToString:@"Featured Jobs"]){
            searchtype= [NSString stringWithFormat:@"4" ];
        }
        else if ([_txtfindjobs.text isEqualToString:@"Latest Jobs"]){
            searchtype= [NSString stringWithFormat:@"5" ];
        }
        else if ([_txtfindjobs.text isEqualToString:@"Shore Jobs"]){
            searchtype= [NSString stringWithFormat:@"6" ];
        }
    }
}

- (IBAction)findjobs_click:(UIButton *)sender {
    
    [_txtfindjobs setTag:21];
    currentTextField = _txtfindjobs;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)ok_click:(UIButton *)sender {
    [APP_DELEGATE showLoadingView:@""];
     [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
}
@end
