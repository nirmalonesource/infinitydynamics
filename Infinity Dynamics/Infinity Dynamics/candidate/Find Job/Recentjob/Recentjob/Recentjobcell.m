//
//  Locationcell.m
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import "Recentjobcell.h"
#import "UIColor+CL.h"
#import "UIImageView+Haneke.h"
@implementation Recentjobcell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    _prog_prof.progressTintColor = [UIColor colorWithHex:0xF68B1E];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 5.0f);
    _prog_prof.transform = transform;


}
- (void)prepareForReuse
{
    [self.imageView hnk_cancelSetImage];
    self.imageView.image = nil;
}

@end
