//
//  Myjoined.h
//  FlexCash
//
//  Created by My Mac on 21/11/17.
//  Copyright © 2017 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
#import "WYPopoverController.h"

@interface Recentjob : UIViewController<HttpWrapperDelegate,WYPopoverControllerDelegate>{
  
    WYPopoverController* popoverController;
    HttpWrapper *httpRecentjob;
    NSMutableArray *marrRecentjob;
    AppDelegate * appdelegate;
    UITextField *currentTextField;
    NSString *searchtype;
    ///////for search bar
    //BoolCreation
    BOOL isFilterd;
    //ArrayCreation
    NSMutableArray *filterdarray;

}
@property (strong, nonatomic) IBOutlet UIView *Search_View;
@property (strong, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UITextField *txtfindjobs;

@property (strong, nonatomic) IBOutlet UITableView *tableview;
- (IBAction)findjobs_click:(UIButton *)sender;
- (IBAction)ok_click:(UIButton *)sender;

@end
