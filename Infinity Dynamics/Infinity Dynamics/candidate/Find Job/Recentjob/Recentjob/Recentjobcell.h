//
//  Locationcell.h
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Recentjobcell: UITableViewCell{

    bool gradients;
}
@property (weak, nonatomic) IBOutlet UIProgressView *prog_prof;
@property (strong, nonatomic) IBOutlet UIImageView *imageview;

@property (strong, nonatomic) IBOutlet UILabel *lblshiptype;
@property (strong, nonatomic) IBOutlet UILabel *lblamount;

@property (strong, nonatomic) IBOutlet UIButton *btnjoined;
@property (strong, nonatomic) IBOutlet UIButton *btnrefund;

@property (weak, nonatomic) IBOutlet UILabel *lblname;

@property (strong, nonatomic) IBOutlet UILabel *lbldate;





@end
