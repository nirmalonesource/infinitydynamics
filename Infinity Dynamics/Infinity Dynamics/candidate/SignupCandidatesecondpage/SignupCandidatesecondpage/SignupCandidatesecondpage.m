//
//  SignupCandidatesecondpage.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "SignupCandidatesecondpage.h"
#import "WYPopoverController.h"
#import "Constants.h"
#import "SignupCandidateregister.h"
@interface SignupCandidatesecondpage()<WYPopoverControllerDelegate,UITextFieldDelegate,UIActionSheetDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
}
@property (nonatomic,retain) NSString * CalcValueStr;

@end

@implementation SignupCandidatesecondpage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.CalcValueStr = [[NSString alloc] init];
    
    NSLog(@"parameters = %@",_parameter);
    
    self.txtHeight.text = @"";
    self.txtWeight.text = @"";
    self.txtBMI.text = @"";
  
    
    //self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 490);
    
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 700);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        if (isNationality == YES) {
           settingsViewController.index = currentTextField.tag;
        }
        else if (isrank == YES){
            settingsViewController.marrData = marrranklist;
            
        }
        else if (isGender == YES){
            settingsViewController.index = currentTextField.tag;
            
        }
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
//            if (amountype == YES) {
                settingsViewController.preferredContentSize = CGSizeMake(200, 200);
//            }
//            else{
//                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
//            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
//            }
//            else{
//                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
//            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}

-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    
    currentTextField.text = strValue;
    if ([txtselectrank.text isEqualToString:strValue])
    {
        NSString *findrank = txtselectrank.text;
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", findrank];
        NSArray *resultsrank = [marrranklist filteredArrayUsingPredicate:predicate1];
        
        
        rankId= [NSString stringWithFormat:@"%@", [resultsrank  valueForKey:@"RankID" ]] ;
        NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        selectedrankid = [Uid1 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(selectedRanklist) withObject:nil afterDelay:0.1];
       
    }

}

-(void)Popvalueselected:(NSString*)strID:(NSString*)strValue
{
    NSLog(@"strID = %@",strID);
    NSLog(@"strValue = %@",strValue);
    
    currentTextField.text = strValue;
    if ([txtselectrank.text isEqualToString:strValue])
    {
        NSString *findrank = txtselectrank.text;
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", findrank];
        NSArray *resultsrank = [marrranklist filteredArrayUsingPredicate:predicate1];
        
        
        rankId= [NSString stringWithFormat:@"%@", [resultsrank  valueForKey:@"RankID" ]] ;
        NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
        selectedrankid = [Uid1 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(ShipType) withObject:nil afterDelay:0.1];
    }
}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnNextStep:(id)sender {
    if ([txtselectrank.text isEqualToString:@""])
    {
        showAlert(AlertTitle, @"Please select Current rank");
    }
    else
    {
        if ([_txtGender.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please select Gender");
        }
        
        else if ([_txtHeight.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please Enter Height in CMS");
        }
        else if ([_txtHeight.text length] < 3)
        {
            showAlert(AlertTitle, @"Height should be of minimum 3 digits");
        }
        else if ([_txtHeight.text integerValue] < 100)
        {
            showAlert(AlertTitle, @"Height should be of minimum 100 CMS");
        }
        else if ([_txtWeight.text isEqualToString:@""])
        {
                showAlert(AlertTitle, @"Please Enter Weight in KGS");
        }
        else if ([_txtWeight.text length] < 2)
        {
            showAlert(AlertTitle, @"Weight should be minimum 2 digits");
        }
        else if ([_txtWeight.text integerValue] < 40)
        {
            showAlert(AlertTitle, @"Weight should be of minimum 40 KGS");
        }
        else
        {
            if ([txtselectmatialstatus.text isEqualToString:@""])
            {
                showAlert(AlertTitle, @"Please select Marital Status");
            }
            else{
                if ([_txtjoining.text isEqualToString:@""])
                {
                    showAlert(AlertTitle, @"Please select Joining type");
                }
                else
                {
                    if ([_txtjoining.text isEqualToString:@"Not Available"])
                    {
                        if ([_txtmultiplerank.text isEqualToString:@""]) {
                            showAlert(AlertTitle, @"Please select Applied Rank");
                        }
                        else
                        {
                            if ([_txtshiptype.text isEqualToString:@""]) {
                                showAlert(AlertTitle, @"Please Select Applied Ship Type");
                            }
                            else
                            {
                                NSString *find = txtselectrank.text;
                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
                                NSArray *results = [marrranklist filteredArrayUsingPredicate:predicate];
                                rankId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"RankID" ]] ;
                                NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                dataid = [[NSMutableDictionary alloc]init];
                                [dataid addEntriesFromDictionary:_parameter];
                                [dataid setValue:trimmed forKey:@"rank_id"];
                                [dataid setValue:_txtGender.text forKey:@"gender"];
                                [dataid setValue:_txtHeight.text forKey:@"height"];
                                [dataid setValue:_txtWeight.text forKey:@"weight"];
                                [dataid setValue:_txtBMI.text forKey:@"bmi"];
                                [dataid setValue:txtnoofchildren.text forKey:@"no_of_children"];
                                [dataid setValue:txtselectmatialstatus.text forKey:@"marital_status"];
                                [dataid setValue:txtselectmatialstatus.text forKey:@"marital_status"];
                                [dataid setValue:txtselectmatialstatus.text forKey:@"marital_status"];
                                [dataid setValue:RankID forKey:@"applied_ranks"];
                                [dataid setValue:ShipID forKey:@"applied_ships"];
                                [dataid setValue:endtime forKey:@"available_from"];
                                [dataid setValue:_txtjoining.text forKey:@"joining_type"];
                                SignupCandidateregister *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SignupCandidateregister"];
                                s.parameter = dataid;
                                s.image = _Image;
                                [self.navigationController pushViewController:s animated:NO];
                            }
                        }
                    }
                    else{
                        if ([_txtAvailable.text isEqualToString:@""]) {
                            showAlert(AlertTitle, @"Select Available from/before date");
                        }
                        else{
                            if ([_txtmultiplerank.text isEqualToString:@""]) {
                                showAlert(AlertTitle, @"Please select Applied Rank");
                            }
                            else{
                                if ([_txtshiptype.text isEqualToString:@""]) {
                                    showAlert(AlertTitle, @"Please Select Applied Ship Type");
                                }
                                else{
                                    NSString *find = txtselectrank.text;
                                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
                                    NSArray *results = [marrranklist filteredArrayUsingPredicate:predicate];
                                    rankId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"RankID" ]] ;
                                    NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
                                    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                    dataid = [[NSMutableDictionary alloc]init];
                                    [dataid addEntriesFromDictionary:_parameter];
                                    [dataid setValue:trimmed forKey:@"rank_id"];
                                    [dataid setValue:_txtGender.text forKey:@"gender"];
                                    [dataid setValue:_txtHeight.text forKey:@"height"];
                                    [dataid setValue:_txtWeight.text forKey:@"weight"];
                                    [dataid setValue:_txtBMI.text forKey:@"bmi"];
                                    [dataid setValue:txtnoofchildren.text forKey:@"no_of_children"];
                                    [dataid setValue:txtselectmatialstatus.text forKey:@"marital_status"];
                                    [dataid setValue:txtselectmatialstatus.text forKey:@"marital_status"];
                                    [dataid setValue:txtselectmatialstatus.text forKey:@"marital_status"];
                                    [dataid setValue:RankID forKey:@"applied_ranks"];
                                    [dataid setValue:ShipID forKey:@"applied_ships"];
                                    [dataid setValue:endtime forKey:@"available_from"];
                                    [dataid setValue:_txtjoining.text forKey:@"joining_type"];
                                    SignupCandidateregister *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SignupCandidateregister"];
                                    s.parameter = dataid;
                                    s.image = _Image;
                                    [self.navigationController pushViewController:s animated:NO];
                                }
                            }
                        }
                    }
                }
            }
        }
        }
}
                

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtHeight)
    {
       /* if ([self.txtHeight.text isEqualToString:@""]) {
            self.txtHeight=@"";
        }
        */

        self.txtBMI.text=@"0";

        /*  heightString = [self.txtHeight.text stringByReplacingCharactersInRange:range withString:string];
        //NSLog(@"heightString = %@",heightString);
      */
    }
    else if (textField == self.txtWeight)
    {

        NSString * proposedNewString=[_txtWeight.text stringByAppendingString:string];
        
        int jk=[_txtHeight.text intValue];
        
        int wi=[proposedNewString intValue];
       
        _txtBMI.text=[NSString stringWithFormat:@"%.0f",(float)wi/(jk * jk)*10000];
    }
    // Hight=>180 , Width=>68  === 21
    return YES;
}


-(void)updateTextLabelsWithText:(float)string
{
    //[self.txtBMI setText:string];
}

- (IBAction)btnselectrank:(id)sender {
    isNationality = NO;
    isrank = YES;
     isGender = NO;
    [txtselectrank setTag:0];
    currentTextField = txtselectrank;
   // amountype = YES;
    [self showPopover:sender];
}


- (IBAction)btnSelectGender:(id)sender {
    isNationality = NO;
    isrank = NO;
    isGender = YES;
    
    [_txtGender setTag:16];
    currentTextField = _txtGender;
    // amountype = YES;
    [self showPopover:sender];
}

- (IBAction)btnselectmartialstatus:(id)sender {
    isNationality = YES;
    isrank = NO;
       isGender = NO;
    [txtselectmatialstatus setTag:2];
    currentTextField = txtselectmatialstatus;
    // amountype = YES;
    [self showPopover:sender];
 
}

- (IBAction)btnMartialstatus:(id)sender {
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(ShipType) withObject:nil afterDelay:0.1];
   
}
-(void)ShipType
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpShip  = [[HttpWrapper alloc] init];
        httpShip.delegate=self;
        httpShip.getbool=NO;
        
        [httpShip requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_ship_type",MainUrl] param:@""];
    });
    
    
}
-(void)CallMyMethod1
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprank  = [[HttpWrapper alloc] init];
        httprank.delegate=self;
        httprank.getbool=NO;
        
        [httprank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:@""];
    });

    
}
-(void)CallMyMethod2
{
      
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpNationality  = [[HttpWrapper alloc] init];
        httpNationality.delegate=self;
        httpNationality.getbool=NO;
        
        [httpNationality requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_nationality",MainUrl] param:@""];
    });
    
    
    
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httprank && httprank != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrranklist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
       
            
        }
    }
    else if(wrapper == httpselectedrank && httpselectedrank != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrrankselectedlist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
   else if(wrapper == httpShip && httpShip != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrshiplist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
            
            
        }
    }
    else if (wrapper == httpNationality && httpNationality != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrnationality = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, message);
        }
        else{
            
            
        }
        
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}


// MULTISELECTION DROPDOWN
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSMutableArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    if (shiptype == YES) {
        Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
        Dropobj.delegate = self;
        [Dropobj showInView:self.view animated:YES];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDown_R:20.0 G:35.0 B:97.0 alpha:0.70];
        
    }
    if (ranktype == YES) {
        Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
        Dropobj.delegate = self;
        [Dropobj showInView:self.view animated:YES];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDown_R:20.0 G:35.0 B:97.0 alpha:0.70];
        
    }

    
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    if (ranktype == YES) {
    
    
    for (int i=0; i<marrranklist.count; i++) {
        _txtmultiplerank.text=[[marrranklist objectAtIndex:i] valueForKey:@"Name"];
    }
    _txtmultiplerank.text=[[marrranklist objectAtIndex:anIndex] valueForKey:@"Name"];
    }
    if (shiptype == YES) {
        for (int i=0; i<marrshiplist.count; i++) {
            _txtshiptype.text=[[marrshiplist objectAtIndex:i] valueForKey:@"ShipType"];
        }
        _txtshiptype.text=[[marrshiplist objectAtIndex:anIndex] valueForKey:@"ShipType"];
        
    }
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (ranktype == YES) {
    if (ArryData.count>0) {
        _txtmultiplerank.text=[ArryData componentsJoinedByString:@","];
        NSMutableArray *NewArr = [[NSMutableArray alloc] init];
        NSMutableArray *myarr = [[NSMutableArray alloc] init];
        
        for (int i=0; i<marrranklist.count; i++) {
            [NewArr addObject:[[marrranklist objectAtIndex:i] valueForKey:@"Name"]];
        }
        
        for (int i=0; i<ArryData.count; i++) {
            NSUInteger index = [NewArr indexOfObject:ArryData[i]];
            NSLog(@"index %lu",(unsigned long)index);
            [myarr addObject:[[marrranklist objectAtIndex:index] valueForKey:@"RankID"]];
        }
        NSLog(@"my arr %@",myarr);
        
        RankID = [myarr componentsJoinedByString:@","];
        
        NSLog(@"RankID %@",RankID);
        //CGSize size=[self GetHeightDyanamic:_txt_profession];
        //_txt_profession.frame=CGRectMake(16, 240, 287, size.height);
    }
    else{
        _txtmultiplerank.text=@"";
    }
    }
    if (shiptype == YES) {
        if (ArryData.count>0) {
            _txtshiptype.text=[ArryData componentsJoinedByString:@","];
            NSMutableArray *NewArr = [[NSMutableArray alloc] init];
            NSMutableArray *myarr = [[NSMutableArray alloc] init];
            
            for (int i=0; i<marrshiplist.count; i++) {
                [NewArr addObject:[[marrshiplist objectAtIndex:i] valueForKey:@"ShipType"]];
            }
            
            for (int i=0; i<ArryData.count; i++) {
                NSUInteger index = [NewArr indexOfObject:ArryData[i]];
                NSLog(@"index %lu",(unsigned long)index);
                [myarr addObject:[[marrshiplist objectAtIndex:index] valueForKey:@"ShipID"]];
            }
            NSLog(@"my arr %@",myarr);
            
            ShipID = [myarr componentsJoinedByString:@","];
            
            NSLog(@"RankID %@",ShipID);
            //CGSize size=[self GetHeightDyanamic:_txt_profession];
            //_txt_profession.frame=CGRectMake(16, 240, 287, size.height);
        }
        else{
            _txtshiptype.text=@"";
        }
        
    }
}
- (void)DropDownListViewDidCancel{
    
}

- (IBAction)btnSelectJoining:(id)sender {
    
    isNationality = YES;
    isrank = NO;
    [_txtjoining setTag:17];
    currentTextField = _txtjoining;
    // amountype = YES;
    [self showPopover:sender];
    
}
-(void)datePickerCancelPressed1
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed1
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        _txtAvailable.text = eventDate;
        if ([_txtAvailable.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([_txtAvailable.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        
        _txtAvailable.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}

- (IBAction)BtnShipType:(id)sender {
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    ranktype = NO;
    shiptype = YES;
    for (int i=0; i<marrshiplist.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[marrshiplist objectAtIndex:i] valueForKey:@"ShipType"]];
        
    }
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Ship type" withOption:arrteam xy:CGPointMake((self.view.frame.size.width-280)/2, (self.view.frame.size.height-300)/2) size:CGSizeMake(280, 300) isMultiple:YES];
}

- (IBAction)btnMultiplerank:(id)sender
{
    if ([txtselectrank.text isEqualToString:@""]) {
         showAlert(AlertTitle, @"Select Current Rank in Above Field First");
    }else{
    
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    ranktype = YES;
    shiptype = NO;
    for (int i=0; i<marrrankselectedlist.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[marrrankselectedlist objectAtIndex:i] valueForKey:@"Name"]];
        
    }
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Rank Type" withOption:arrteam xy:CGPointMake((self.view.frame.size.width-280)/2, (self.view.frame.size.height-300)/2) size:CGSizeMake(280, 300) isMultiple:YES];
    }
}

- (IBAction)btnAvailable:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed1];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setDay:3];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
        // datePicker.minimumDate = [NSDate date];
        // datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed1)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed1)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
    
}
-(void)selectedRanklist
{
    NSMutableDictionary *updateparameters = [[NSMutableDictionary alloc]init];
    
    [updateparameters setValue: selectedrankid forKey:@"rank_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpselectedrank  = [[HttpWrapper alloc] init];
        httpselectedrank.delegate=self;
        httpselectedrank.getbool=NO;
        
        [httpselectedrank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:[updateparameters copy]];
    });
    
}
@end
