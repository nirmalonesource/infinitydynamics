//
//  SignupCandidatesecondpage.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "DropDownListView.h"
@interface SignupCandidatesecondpage : UIViewController<UITextFieldDelegate,HttpWrapperDelegate,kDropDownListViewDelegate,UIScrollViewDelegate>
{
    HttpWrapper *httprank,*httpNationality,*httpselectedrank,*httpShip;
    NSString * rankId;
    BOOL isNationality;
    BOOL isrank;
    BOOL isGender;
      UITextField *currentTextField;
        NSMutableArray *marrranklist,*marrshiplist;
    NSMutableArray *marrrankselectedlist;
    NSMutableArray *marrnationality;
    IBOutlet UITextField *txtselectrank;
    
    IBOutlet UITextField *txtnoofchildren;
    IBOutlet UITextField *txtselectmatialstatus;
    NSMutableDictionary * dataid;
    
    NSMutableArray *arryList;
    DropDownListView * Dropobj;
    NSString * RankID;
     NSString * ShipID;
    
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    
    BOOL shiptype;
    BOOL ranktype;
     NSString * selectedrankid;
    
    
}
@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBMI;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
- (IBAction)btnBack:(id)sender;
- (IBAction)btnNextStep:(id)sender;
@property (strong , nonatomic) NSMutableDictionary * parameter;
@property (strong , nonatomic) UIImage * Image;

- (IBAction)btnselectrank:(id)sender;
- (IBAction)btnselectmartialstatus:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalueselected:(NSString*)strID:(NSString*)strValue;

-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@property (strong, nonatomic) IBOutlet UITextField *txtGender;
- (IBAction)BtnShipType:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtshiptype;

- (IBAction)btnSelectGender:(id)sender;

- (IBAction)btnMultiplerank:(id)sender;
- (IBAction)btnAvailable:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtAvailable;
@property (strong, nonatomic) IBOutlet UITextField *txtjoining;
- (IBAction)btnSelectJoining:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtmultiplerank;
@end
