//
//  AddEducationDetail.m
//  Infinity Dynamics
//
//  Created by My Mac on 21/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "AddEducationDetail.h"
#import "Constants.h"
#import "HttpWrapper.h"
#import "WYPopoverController.h"
@interface AddEducationDetail ()<WYPopoverControllerDelegate>{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
}


@end

@implementation AddEducationDetail

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_Edit == YES) {
        _txtInstitutename.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"InstituteName"]];
        _txtplaceofissue.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Place"]];
        _txtdegree.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Degree"]];
        _txtvalidity.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"PassingYear"]];
        _txteducationdetail.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"EducationType"]];
    }
   
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    
    
    //Create Years Array from 1960 to This year
    years = [[NSMutableArray alloc] init];
    for (int i=1970; i<=i2; i++) {
        [years addObject:[NSString stringWithFormat:@"%d",i]];
    }
    years=[[[years reverseObjectEnumerator] allObjects] mutableCopy];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSaveSeamanDetail:(id)sender {
    [self.view endEditing:YES];
//    if ([_txtInstitutename.text isEqualToString:@""]||[_txtdegree.text isEqualToString:@""]||[_txtvalidity.text isEqualToString:@""]||[_txtplaceofissue.text isEqualToString:@""]) {
//        showAlert(AlertTitle, @"Please fill all the detail");
//    }else{
    if ([_txteducationdetail.text isEqualToString:@""]){
        showAlert(AlertTitle, @"Please Select Education Type");
    }else{
        
        if ([_txtInstitutename.text isEqualToString:@""]){
            showAlert(AlertTitle, @"Please Enter Institute name");
        }else{
        
            if ([_txtdegree.text isEqualToString:@""]){
                showAlert(AlertTitle, @"Please Enter Type of Certificate");
            }else{
                if ([_txtplaceofissue.text isEqualToString:@""]){
                    showAlert(AlertTitle, @"Please Enter Place");
                }else{
                    if ([_txtvalidity.text isEqualToString:@""]){
                        showAlert(AlertTitle, @"Please Select Year of Passing");
                    }else{
        
        if (_Edit == YES) {
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(CallMyMethod) withObject:nil afterDelay:0.1];
        }
        else{
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
        }
                }
        }
            }
        }
        
    }
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)btncalender:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
   
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 50 , self.view.frame.size.width , 300 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 350 );
        }
     
        
        yearpicker = [[UIPickerView alloc] initWithFrame:pickerFrame];
        yearpicker.delegate = self; // Also, can be done from IB, if you're using
        yearpicker.dataSource = self;
        yearpicker.showsSelectionIndicator = YES;
        yearpicker.backgroundColor = [UIColor whiteColor];
      //  int i = years.count/2;
       // [yearpicker selectRow:i inComponent:0 animated:YES];
      
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,300);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
    //    yearpicker.frame = subView.frame;
        [subView addSubview:yearpicker] ;
        subView.backgroundColor=[UIColor whiteColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
}
-(void)viewWillAppear:(BOOL)animated{

}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component
{
    return [years count];
}
- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
   
    // _txtvalidity.text = [NSString stringWithFormat:@"%@",[years objectAtIndex:row]];
    
    return [years objectAtIndex:row];
}
- (void)pickerView:(UIPickerView *)thePickerView
      didSelectRow:(NSInteger)row
       inComponent:(NSInteger)component {
    
    //Here, like the table view you can get the each section of each row if you've multiple sections
    
    //Now, if you want to navigate then;
    // Say, OtherViewController is the controller, where you want to navigate:
   selectedyear = [NSString stringWithFormat:@"%@",[years objectAtIndex:row]];
    NSLog(@"Selected Color: %@. Index of selected color: %li",
          [years objectAtIndex:row], (long)row);
}
-(void)datePickerCancelPressed
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    yearpicker.hidden = YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed
{
    if(isPickerLaunced)
    {
       
   _txtvalidity.text = [NSString stringWithFormat:@"%@",selectedyear];
        subView.hidden=YES;
        toolbar.hidden=YES;
       yearpicker.hidden = YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        _txtvalidity.text = [NSString stringWithFormat:@"%@",selectedyear];
      
        subView.hidden=YES;
        toolbar.hidden=YES;
       yearpicker.hidden = YES;
        datePicker.hidden=YES;
    }
}
-(void)CallMyMethod
{
//    candidate_id
//    education_type ('Academic Education', 'Pre-Sea Training')
//    institute_name
//    degree
//    place
//    passing_year (yyyy)
//    update = 1
//    education_id
    
 
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:_txtdegree.text forKey:@"degree"];
    [parameters setValue:_txtvalidity.text forKey:@"passing_year"];
    [parameters setValue:_txtplaceofissue.text forKey:@"place"];
    [parameters setValue:_txtInstitutename.text forKey:@"institute_name"];
    [parameters setValue:[_detail valueForKey:@"CanEducationID"] forKey:@"education_id"];
       [parameters setValue:_txteducationdetail.text forKey:@"education_type"];
    
    [parameters setValue:@"1" forKey:@"update"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpeditdetail  = [[HttpWrapper alloc] init];
        httpeditdetail.delegate=self;
        httpeditdetail.getbool=NO;
        
        [httpeditdetail requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_education_dtl",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)CallMyMethod1
{
    //    candidate_id
    //    number
    //    proficiency
    //    place_of_issue
    //    level ('Management', 'Operational', 'support')
    //    validity (yyyy-mm-dd)
    //    update = 0
    
  
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [parameters setValue:_txtdegree.text forKey:@"degree"];
    [parameters setValue:_txtvalidity.text forKey:@"passing_year"];
    [parameters setValue:_txtplaceofissue.text forKey:@"place"];
    [parameters setValue:_txtInstitutename.text forKey:@"institute_name"];
       [parameters setValue:_txteducationdetail.text forKey:@"education_type"];
    [parameters setValue:@"0" forKey:@"update"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httadddetail  = [[HttpWrapper alloc] init];
        httadddetail.delegate=self;
        httadddetail.getbool=NO;
        
        [httadddetail requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@add_update_education_dtl",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httadddetail && httadddetail != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marradddetail = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
             showAlert(AlertTitle, @"Added Successfully");
            
           
            [self.navigationController popViewControllerAnimated:NO];
        }
        else{
            
            
        }
    }
    else if (wrapper == httpcertificate && httpcertificate != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marreditdetail = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            marrProficiency = [dicsResponse valueForKey:@"data"] ;
            
            
        }
    }
    else if (wrapper == httpeditdetail && httpeditdetail != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marreditdetail = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            
              showAlert(AlertTitle, @"Updated Successfully");
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        else{
            
            
        }
        
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}



- (IBAction)btnSelecteducationdetail:(id)sender {
    [_txteducationdetail setTag:10];
    currentTextField = _txteducationdetail;
    // amountype = YES;
    [self showPopover:sender];
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
    
            settingsViewController.index = currentTextField.tag;
            // settingsViewController.marrproficiency = marrProficiency;
        
        
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    
}
@end
