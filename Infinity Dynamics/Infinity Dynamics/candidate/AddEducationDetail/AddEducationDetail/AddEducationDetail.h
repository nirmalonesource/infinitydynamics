//
//  AddEducationDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 21/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface AddEducationDetail : UIViewController<HttpWrapperDelegate,UIPickerViewDataSource, UIPickerViewDelegate>{
    HttpWrapper *httadddetail,*httpeditdetail,*httpcertificate;
    //    NSMutableArray* detail;
    NSMutableArray *marreditdetail;
    NSMutableArray *marradddetail;
    //////datepicker
      UIPickerView *yearpicker;
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    UITextField *currentTextField;
    NSMutableArray *marrProficiency;
    NSString * CertificateID;
    IBOutlet UITextField *txtlevl;
    
    BOOL isProficiency;
    BOOL isLevel;
    NSMutableArray * years;
    NSString * selectedyear;
}
- (IBAction)btnLevel:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtInstitutename;
- (IBAction)btnSelecteducationdetail:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txteducationdetail;


@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtdegree;
- (IBAction)btncalender:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtplaceofissue;
@property (strong, nonatomic) IBOutlet UIButton *btncalender;
@property (strong, nonatomic) IBOutlet UIButton *btnCalender;
@property (strong, nonatomic) IBOutlet UITextField *txtvalidity;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveseamandetail;
- (IBAction)btnSaveSeamanDetail:(id)sender;
- (IBAction)btnBack:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;


@end
