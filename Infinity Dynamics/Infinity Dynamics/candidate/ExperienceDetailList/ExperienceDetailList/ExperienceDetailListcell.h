//
//  Locationcell.h
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExperienceDetailListcell: UITableViewCell{

    bool gradients;
}

@property (strong, nonatomic) IBOutlet UIButton *btnedit;
@property (strong, nonatomic) IBOutlet UIButton *btndelete;
@property (strong, nonatomic) IBOutlet UILabel *lblcompanyname;
@property (strong, nonatomic) IBOutlet UILabel *lblrank;
@property (strong, nonatomic) IBOutlet UILabel *lblshiptype;
@property (strong, nonatomic) IBOutlet UILabel *lblsignon;
@property (strong, nonatomic) IBOutlet UILabel *lblsignoff;
@property (strong, nonatomic) IBOutlet UILabel *lblenginetype;
@property (strong, nonatomic) IBOutlet UILabel *lblshipname;
@property (strong, nonatomic) IBOutlet UILabel *lblGRT;
@property (weak, nonatomic) IBOutlet UILabel *lblTonnageType;
@property (weak, nonatomic) IBOutlet UILabel *lblTonnageValue;


@property (strong, nonatomic) IBOutlet UILabel *lbltotalseaduration;


@end
