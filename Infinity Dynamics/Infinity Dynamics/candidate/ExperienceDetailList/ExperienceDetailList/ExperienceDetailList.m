//
//  ExperienceDetailList.m
//  Infinity Dynamics
//
//  Created by My Mac on 23/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "ExperienceDetailList.h"
#import "Constants.h"
#import "ExperienceDetailListcell.h"
#import "ExperienceDetail.h"
#import "MyProfileCandidate.h"
@interface ExperienceDetailList ()

@end

@implementation ExperienceDetailList

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExperienceDetailListcell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"ExperienceDetailListcell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return marreducationlist.count;
    
    
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(ExperienceDetailListcell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString * Type= [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section]valueForKey:@"TonnageType"]];
    
    NSString * Value= [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section]valueForKey:@"TonnageValue"]];
    
    //NSString * KeyAndVal = [NSString stringWithFormat:@"%@ %@",Value,Type];
    
    //cell.lblGRT.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section]valueForKey:@"GRT"]];
    
    //cell.lblGRT.text = [NSString stringWithFormat:@"%@",KeyAndVal];
    
    cell.lblTonnageType.text = [NSString stringWithFormat:@"%@",Type];
    
    cell.lblTonnageValue.text = [NSString stringWithFormat:@"%@",Value];
    
    cell.lblrank.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section]valueForKey:@"Rank"] ];
    
    NSString * date = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"JoiningDate"]];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    NSString * JoiningDate = [formatedt stringFromDate:fulldate];
    
    cell.lblsignon.text = [NSString stringWithFormat:@"%@",JoiningDate];
    NSString * LeavingDate = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"LeavingDate"]];
    
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate1=[formatedt1 dateFromString:LeavingDate];
    [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
    NSString * LeavingDates = [formatedt1 stringFromDate:fulldate1];
    
    cell.lblsignoff.text = [NSString stringWithFormat:@"%@",LeavingDates];
     cell.lblshipname.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"ShipName"]];
     cell.lblshiptype.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"ShipType"]];
    if (![[NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"EngineName"]]isEqualToString:@"<null>"])
    {
         cell.lblenginetype.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"EngineName"]];
    }
    else
    {
        cell.lblenginetype.text = @"";
    }

    
     cell.lblcompanyname.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"CompanyName"]];
     cell.lbltotalseaduration.text = [NSString stringWithFormat:@"%@",[[marreducationlist  objectAtIndex:indexPath.section] valueForKey:@"TotalSeaDuration"]];
    [cell.btnedit setTag:indexPath.section];
    [ cell.btnedit addTarget:self action:@selector(switchpress:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btndelete setTag:indexPath.section];
    [ cell.btndelete addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)switchpress:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    NSLog(@"here :: %@",[marreducationlist objectAtIndex:indexPath.row]  );
    ExperienceDetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ExperienceDetail"];
    s.detail = [marreducationlist objectAtIndex:indexPath.row] ;
    s.Edit = YES;
    [self.navigationController pushViewController:s animated:NO];
    
}
- (IBAction)buttonPressed:(UIButton *)sender {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag]inSection:0];
    NSLog(@"here :: %@",[[marreducationlist objectAtIndex:indexPath.row] valueForKey:@"ExperienceID"] );
    
    seaman_id = [NSString stringWithFormat:@"%@",[[marreducationlist objectAtIndex:indexPath.row] valueForKey:@"ExperienceID"] ];
    [self performSelector:@selector(delete) withObject:nil afterDelay:0.1];
}
-(void)delete
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Delete" message:@"Want to Delete This Record?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                {
                                    /** What we write here???????? **/
                                    [APP_DELEGATE showLoadingView:@""];

                                    NSLog(@"you pressed OK, button");
                                    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
                                    
                                    [parameters setValue:seaman_id forKey:@"experience_id"];
                                    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                                        httpdelete  = [[HttpWrapper alloc] init];
                                        httpdelete.delegate=self;
                                        httpdelete.getbool=NO;
                                        
                                        [httpdelete requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@delete_experience",MainUrl] param:[parameters copy]];
                                         /** What we write here???????? **/
                                    });
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"CANCEL"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                               {
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed Cancel, button");
                                   // call method whatever u need
                               }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)CallTonngeApi
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        htteducationlist  = [[HttpWrapper alloc] init];
        htteducationlist.delegate=self;
        htteducationlist.getbool=NO;
        
        [htteducationlist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_experience_dtl",MainUrl] param:[parameters copy]];
    });
}

-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        htteducationlist  = [[HttpWrapper alloc] init];
        htteducationlist.delegate=self;
        htteducationlist.getbool=NO;
        
        [htteducationlist requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_experience_dtl",MainUrl] param:[parameters copy]];
    });
}

-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == htteducationlist && htteducationlist != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marreducationlist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {

            [self.tableview reloadData];
        }
        else{
            
            
        }
    }
    else if (wrapper == httpdelete && httpdelete != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marreducationlist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            [APP_DELEGATE showLoadingView:@""];
            [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
            showAlert(AlertTitle, @"Deleted Successfully");

        }
        else{
            
            
        }
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (IBAction)btnBack:(id)sender {
    MyProfileCandidate *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
    
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)btnPlusbutton:(id)sender {
    ExperienceDetail *s =[self.storyboard instantiateViewControllerWithIdentifier:@"ExperienceDetail"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}

@end
