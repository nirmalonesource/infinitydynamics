//
//  Signupoption.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "Signupoption.h"
#import "SignupCompany.h"
#import "SignupCandidatefirstpage.h"
#import "InstituteRegistrationVC.h"

@interface Signupoption ()

@end

@implementation Signupoption

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnAsaCandidate:(id)sender {
    SignupCandidatefirstpage *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SignupCandidatefirstpage"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)btnAsaComapny:(id)sender {
    SignupCompany *s =[self.storyboard instantiateViewControllerWithIdentifier:@"SignupCompany"];
    // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:s animated:NO];
}
- (IBAction)BTNNSTITUTEREGISTRATIONClick:(id)sender
{
    InstituteRegistrationVC *s =[self.storyboard instantiateViewControllerWithIdentifier:@"InstituteRegistrationVC"];
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)BackViewBTNClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}


@end
