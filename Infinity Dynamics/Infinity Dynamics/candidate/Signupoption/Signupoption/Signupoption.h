//
//  Signupoption.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Signupoption : UIViewController

- (IBAction)btnAsaCandidate:(id)sender;

- (IBAction)btnAsaComapny:(id)sender;

- (IBAction)BTNNSTITUTEREGISTRATIONClick:(id)sender;

@end
