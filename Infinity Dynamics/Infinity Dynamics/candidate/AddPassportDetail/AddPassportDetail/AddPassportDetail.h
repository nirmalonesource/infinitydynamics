//
//  AddPassportDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 13/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface AddPassportDetail : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httadddetail,*httpeditdetail,*httpgetdetail,*httpCountry;
    //    NSMutableArray* detail
     NSMutableArray *marrgetdetail,*marrcountry;
    NSMutableArray *marreditdetail;
    NSMutableArray *marradddetail;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    
    NSString * PassportDetID;
    NSString *  ExpiryDate;
    NSString * PlaceofIssue;
    NSString * PassportNo;
    
    NSString * PassportDetID2;
    NSString *  ExpiryDate2;
    NSString * PlaceofIssue2;
    NSString * PassportNo2;
    
    UITextField *currentTextField;
    NSString * countryId;
    
}
- (IBAction)btnCountry:(id)sender;
@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtnumber;
- (IBAction)btncalender:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtplaceofissue;
@property (strong, nonatomic) IBOutlet UIButton *btncalender;
@property (strong, nonatomic) IBOutlet UIButton *btnCalender;
@property (strong, nonatomic) IBOutlet UITextField *txtvalidity;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveseamandetail;
- (IBAction)btnSaveSeamanDetail:(id)sender;
- (IBAction)btnBack:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@end
