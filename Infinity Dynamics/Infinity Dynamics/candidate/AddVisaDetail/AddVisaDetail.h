//
//  AddVisaDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 15/03/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "PopOverView.h"

@interface AddVisaDetail : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httadddetail,*httpeditdetail,*httpcertificates1,*httpCountry;
    //    NSMutableArray* detail;1
    NSMutableArray *marreditdetail,*marrcountry;
    NSMutableArray *marradddetail;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    UITextField *currentTextField;
    NSMutableArray *marrProficiency;
    NSString * CertificateID;
      BOOL isCountry;
     NSString * countryId;
}
- (IBAction)btncalender:(id)sender;
@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtnumber;
@property (strong, nonatomic) IBOutlet UITextField *txtplaceofissue;
@property (strong, nonatomic) IBOutlet UITextField *txtvalidity;
- (IBAction)btnSaveSeamanDetail:(id)sender;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtvisatype;
- (IBAction)btnVisaType:(id)sender;
- (IBAction)btnCountry:(id)sender;

-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
@end
