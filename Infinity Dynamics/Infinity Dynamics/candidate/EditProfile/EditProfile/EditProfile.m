//
//  EditProfile.m
//  Infinity Dynamics
//
//  Created by My Mac on 11/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "EditProfile.h"
#import "TextFieldValidator.h"
#import "Constants.h"
#import "WYPopoverController.h"
#import "UIImageView+Haneke.h"
#import "ViewController.h"
#import "SeamenDetaillist.h"
#import "PCCPViewController.h"
#import "MyProfileCandidate.h"
#import "WebViewController.h"
#define REGEX_USER_NAME_LIMIT @"^.{3,10}$"
#define REGEX_USER_NAME @"[A-Za-z0-9]{3,10}"
#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT @"^.{5,20}$"
#define REGEX_PASSWORD @"[A-Za-z0-9]{5,20}"
#define REGEX_PHONE_DEFAULT @"[0-9]"
@interface EditProfile ()<WYPopoverControllerDelegate,UITextFieldDelegate,UIActionSheetDelegate,UIDocumentPickerDelegate,UIDocumentMenuDelegate>
{
    WYPopoverController* popoverController;
    UIActionSheet *actionSheet;
    NSMutableData * webData1;
    NSURLConnection *connection1;
    NSString *currentData;
    NSData *fileData;
    NSString *pdfname;
}

@end

@implementation EditProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.txtWeight.text = @"";
    self.txtWeight.text = @"";
    self.txtBMI.text = @"";
    
    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width, 1630);
    _imgeview.layer.masksToBounds = YES;
    _imgeview.layer.cornerRadius = _imgeview.frame.size.width/2;
    
    _btnimage.layer.masksToBounds = YES;
    _btnimage.layer.cornerRadius = _btnimage.frame.size.width/2;
    
    [self setupAlerts];
    if (_cocdetail == YES) {
        [_txtindusno becomeFirstResponder];
    }
    [self Nationality];
    [self countryofres];
    if (imagepicker == YES) {
        
    }
    else{
       
    }
    
    if (_isfrompopup) {
       // [self btnAvailable:self];
        CGPoint bottomOffset = CGPointMake(0, self.scrollview.contentSize.height - self.scrollview.bounds.size.height + self.scrollview.contentInset.bottom);
        [self.scrollview setContentOffset:bottomOffset animated:YES];
        _vwavailabledt.layer.cornerRadius = 5;
        _vwavailabledt.layer.masksToBounds = true;
        _vwavailabledt.layer.borderColor = [[UIColor redColor] CGColor];
        _vwavailabledt.layer.borderWidth = 2;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.txtHeight)
    {
        NSString * proposedNewString=[_txtHeight.text stringByAppendingString:string];
        
        int jk=[proposedNewString intValue];
        
        int wi=[_txtWeight.text intValue];
        
        _txtBMI.text=[NSString stringWithFormat:@"%.0f",(float)((wi*10000)/(jk * jk))];
        NSLog(@"TOTAL:%d",((wi*10000)/(jk * jk)));
        //self.txtBMI.text=@"0";
        
        /*  heightString = [self.txtHeight.text stringByReplacingCharactersInRange:range withString:string];
         //NSLog(@"heightString = %@",heightString);
         */
    }
    else if (textField == self.txtWeight)
    {
        NSString * proposedNewString=[_txtWeight.text stringByAppendingString:string];
        
        int jk=[_txtHeight.text intValue];
        
        int wi=[proposedNewString intValue];
        
        _txtBMI.text=[NSString stringWithFormat:@"%.0f",(float)((wi*10000)/(jk * jk))];
        NSLog(@"TOTAL:%d",((wi*10000)/(jk * jk)));

    }
    // Hight=>180 , Width=>68  === 21
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupAlerts{
    [_txtfirstname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    [_txtfirstname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    _txtfirstname.validateOnResign=NO;
    [_txtlastname addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    [_txtlastname addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    _txtlastname.validateOnResign=NO;
    [_txtmiddlename addRegx:REGEX_USER_NAME_LIMIT withMsg:@"User name charaters limit should be come between 3-10"];
    [_txtmiddlename addRegx:REGEX_USER_NAME withMsg:@"Only alpha numeric characters are allowed."];
    _txtmiddlename.validateOnResign=NO;
    
    [_txtemail addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];
    [_txtemail2 addRegx:REGEX_EMAIL withMsg:@"Enter valid email."];

   // [_txtmobileno addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper format (eg. ##########)"];
  //  _txtmobileno.isMandatory=NO;
  // _txtmobileno2 addRegx:REGEX_PHONE_DEFAULT withMsg:@"Phone number must be in proper \\format (eg. ##########)"];
 // xtmobileno2.isMandatory=NO;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]]) {
        [Dropobj fadeOut];
    }
}
- (IBAction)btnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)btnNextStep:(id)sender {
    if ([_txtfirstname.text isEqualToString:@""]) {
        showAlert(AlertTitle,@"Please Enter First Name");
    }else if ([_txtlastname.text isEqualToString:@""]) {
            showAlert(AlertTitle,@"Please Enter Last Name");
    }else if (![_txtemail validate])
    {
        showAlert(AlertTitle,@"Please enter email id");
    } else {
        if (![_txtemail2.text isEqualToString:@""] && ![_txtemail2 validate]) {
            showAlert(AlertTitle,@"Please enter valid email2");
        }else{
            [self performSelector:@selector(contactValidations) withObject:nil afterDelay:0.1];
        }
    }
}
-(void)contactValidations {
    if ([txtnationality.text isEqualToString:@""]) {
        showAlert(AlertTitle,@"Please Select Nationality");
    }
//    else{
//        if ([_txtmobileno.text isEqualToString:@""] || ![_txttelephoneno.text isEqualToString:@""]) {//////for mobile no
//            if ([_txttelephoneno.text isEqualToString:@""])
//            {
//                showAlert(AlertTitle,@"Please Enter Telphone or Mobile No.");
//            }
//            else {
//                if ([_txttelephonecode.text isEqualToString:@""]){
//                    showAlert(AlertTitle,@"Please enter ISD");
//                }else{
//                    if ([_txtstdcode.text isEqualToString:@""]){
//                        showAlert(AlertTitle,@"Enter STD Code");
//                    } else{
//                        if ([_txtemail.text isEqualToString:_txtemail2.text]) {
//                            showAlert(AlertTitle,@"Both Email id should not be same");
//                        }else{
//                            [self performSelector:@selector(nextValidation) withObject:nil afterDelay:0.1];
//                        }
//                    }
//                }
//            }
//        } else {
//            if (![_txtmobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
//            {
//                UIAlertController * alert = [UIAlertController  alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                    //do something when click button
//                }];
//                [alert addAction:okAction];                        [self presentViewController:alert animated:YES completion:nil];
//            }
//            else if ([_txtmobilecode.text isEqualToString:@"91"] && _txtmobileno.text.length != 10)
//            {
//                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
//                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                    //do something when click button
//                }];
//                [alert addAction:okAction];                        [self presentViewController:alert animated:YES completion:nil];
//            } else {
//
//                if(![_txtmobileno2.text isEqualToString:@""] &&[_txtmobilecode2.text isEqualToString:@"91"] && _txtmobileno2.text.length != 10){
//                    showAlert(AlertTitle,@"Mobile2 should be 10 digits for India");
//                }else{
//                    if(![_txtmobileno2.text isEqualToString:@""] && [_txtmobilecode2.text isEqualToString:@""]){
//                        showAlert(AlertTitle,@"Please Enter Mobile2 ISD Code.");
//                    }else if ([_txtmobileno.text isEqualToString:_txtmobileno2.text]) {
//                            showAlert(AlertTitle,@"Both Mobile No. should not be same");
//                        }else{
//                            [self performSelector:@selector(nextValidation) withObject:nil afterDelay:0.1];
//                        }
//                }
////                if ([_txtmobilecode2.text isEqualToString:@"91"] && _txtmobileno2.text.length != 10)
////                {
////                    showAlert(AlertTitle,@"Mobile No2 should be 10 digits for India");
////                }
////                else if ([_txtemail.text isEqualToString:_txtemail2.text]) {
////                    showAlert(AlertTitle,@"Both Email id should not be same");
////                } else{
////                    if ([_txtmobileno.text isEqualToString:_txtmobileno2.text]) {
////                        showAlert(AlertTitle,@"Both Mobile No. should not be same");
////                    }else{
////                        [self performSelector:@selector(nextValidation) withObject:nil afterDelay:0.1];
////                    }
////                }
//            }
//        }
//    }
    else{
        if ([_txtmobileno.text isEqualToString:@""]) {//////for mobile no
            if ([_txttelephoneno.text isEqualToString:@""])
            {
                showAlert(AlertTitle,@"Please Enter Telphone or Mobile No.");
            }
            else
            {
                if ([_txttelephonecode.text isEqualToString:@""]){
                    showAlert(AlertTitle,@"Please enter ISD");
                }
                else{
                    if ([_txtstdcode.text isEqualToString:@""]){
                        showAlert(AlertTitle,@"Enter STD Code");
                    }
                    else if (![_txtmobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
                    {
                        UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            //do something when click button
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else if ([_txtmobilecode.text isEqualToString:@"91"] && _txtmobileno.text.length != 10)
                    {
                        UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            //do something when click button
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    }else if(![_txtmobileno2.text isEqualToString:@""] && [_txtmobilecode2.text isEqualToString:@""]){
                        UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile 2 ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            //do something when click button
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else if ([_txtmobilecode2.text isEqualToString:@"91"] && _txtmobileno2.text.length != 10)
                    {
                        UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            //do something when click button
                        }];
                        [alert addAction:okAction];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    else{
                        if ([_txtemail.text isEqualToString:_txtemail2.text]) {
                            showAlert(AlertTitle,@"Both Email id should not be same");
                        }
                        else{
                          
                            ////////
                             [self performSelector:@selector(nextValidation) withObject:nil afterDelay:0.1];
                        }
                    }
                }
            }
        }
        else
        {
            if (![_txtmobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
            {
                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    //do something when click button
                }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else if ([_txtmobilecode.text isEqualToString:@"91"] && _txtmobileno.text.length != 10)
            {
                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Mobile No should be 10 digits for India" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    //do something when click button
                }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }else if(![_txtmobileno2.text isEqualToString:@""] && [_txtmobilecode2.text isEqualToString:@""]){
                UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile 2 ISD Code." preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    //do something when click button
                }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            else
            {
                if ([_txtmobilecode2.text isEqualToString:@"91"] && _txtmobileno2.text.length != 10)
                {
                    showAlert(AlertTitle,@"Mobile No2 should be 10 digits for India");
                }
                else if ([_txtemail.text isEqualToString:_txtemail2.text]) {
                    showAlert(AlertTitle,@"Both Email id should not be same");
                }
                else
                {
                    if ([_txtmobileno.text isEqualToString:_txtmobileno2.text]) {
                        showAlert(AlertTitle,@"Both Mobile No. should not be same");
                    }
                    else{
                        //////
                         [self performSelector:@selector(nextValidation) withObject:nil afterDelay:0.1];
                    }
                }
            }
        }
    }

}
-(void)nextValidation {
     if (![_txtmobileno.text isEqualToString:@""] && [_txtmobilecode.text isEqualToString:@""])
    {
        UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            
            //do something when click button
        }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
     else if (![_txtmobileno2.text isEqualToString:@""] && [_txtmobilecode2.text isEqualToString:@""])
     {
         UIAlertController * alert = [UIAlertController                                              alertControllerWithTitle:@"" message:@"Please Enter Mobile ISD Code." preferredStyle:UIAlertControllerStyleAlert];
         
         UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
             
             //do something when click button
         }];
         [alert addAction:okAction];
         [self presentViewController:alert animated:YES completion:nil];
     }
   else if ([txtselectrank.text isEqualToString:@""])
    {
        showAlert(AlertTitle, @"Please select Current rank");
    }else if ([_txtGender.text isEqualToString:@""])
    {
        showAlert(AlertTitle, @"Please select Gender");
    }
   
    else if ([_txtHeight.text isEqualToString:@""])
    {
        showAlert(AlertTitle, @"Please Enter Height in CMS");
    }
    else if ([_txtHeight.text length] < 3)
    {
        showAlert(AlertTitle, @"Height should be of minimum 3 digits");
    }
    else if ([_txtHeight.text integerValue] < 100)
    {
        showAlert(AlertTitle, @"Height should be of minimum 100 CMS");
    }
    else if ([_txtWeight.text isEqualToString:@""])
    {
        showAlert(AlertTitle, @"Please Enter Weight in KGS");
    }
    else if ([_txtWeight.text length] < 2)
    {
        showAlert(AlertTitle, @"Weight should be minimum 2 digits");
    }
    else if ([_txtWeight.text integerValue] < 40)
    {
        showAlert(AlertTitle, @"Weight should be of minimum 40 KGS");
    }else{
        
        if (![_txtemail2.text isEqualToString:@""] && ![_txtemail2 validate]) {
            showAlert(AlertTitle,@"Please enter second valid email");
        }else{
            
        }
        
        if ([txtselectmatialstatus.text isEqualToString:@""])
        {
            showAlert(AlertTitle, @"Please select Marital Status");
        }else if([_txtcity.text isEqualToString:@""]){
            showAlert(AlertTitle, @"Please enter City");
        }
        else if([_txtzipcode.text isEqualToString:@""]){
            showAlert(AlertTitle, @"Please enter Zipcode");
        }else{
            if ([_txtjoining.text isEqualToString:@""])
            {
                showAlert(AlertTitle, @"Please select Joining type");
            }else{
                if ([_txtjoining.text isEqualToString:@"Not Available"])
                {
                    if ([_txtmultiplerank.text isEqualToString:@""]) {
                        showAlert(AlertTitle, @"Please select Applied Rank");
                    }else{
                        if ([_txtshiptype.text isEqualToString:@""]) {
                            showAlert(AlertTitle, @"Please Select Applied Ship Type");
                        }else{
                            [APP_DELEGATE showLoadingView:@""];
                            [self performSelector:@selector(getupdatedata) withObject:nil afterDelay:0.1];
                        }
                    }
                }else{
                    if ([_txtAvailable.text isEqualToString:@""]) {
                        showAlert(AlertTitle, @"Select Available from/before date");
                    }else{
                        if ([_txtmultiplerank.text isEqualToString:@""]) {
                            showAlert(AlertTitle, @"Please select Applied Rank");
                        }
                        else{
                            if ([_txtshiptype.text isEqualToString:@""]) {
                                showAlert(AlertTitle, @"Please Select Applied Ship Type");
                            }
                            else{
                                [APP_DELEGATE showLoadingView:@""];
                                [self performSelector:@selector(getupdatedata) withObject:nil afterDelay:0.1];
                                
                            }
                        }
                    }
                }
            }
            
        }
    }
}
- (IBAction)btnImage:(id)sender {
    [self createActionSheet];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [_imgeview setImage:chosenImage];
    //  _img_choose_img.image = [info objectForKey:UIImagePickerControllerOriginalImage];
  //  image = [info objectForKey:UIImagePickerControllerOriginalImage];
 //  image = [self scaleAndRotateImage:[info objectForKey:UIImagePickerControllerOriginalImage]];
     image = [self scaleAndRotateImage:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    imagepicker = YES;
}
- (UIImage *) scaleAndRotateImage: (UIImage *)image
{
    int kMaxResolution = 3000; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = bounds.size.height * ratio;
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef),      CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient)
    {
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft)
    {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
-(void)createActionSheet
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:self
                                     cancelButtonTitle:@"Cancel"
                                destructiveButtonTitle:nil
                                     otherButtonTitles:@"Camera", @"Library",nil];
    [actionSheet showInView:self.view];
    actionSheet.tag = 100;
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (actionSheet.tag == 100)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"Edit Profile");
            [self cameraPictureButtonClicked];
        }
        else if (buttonIndex ==1)
        {
            NSLog(@"Camera");
            [self libraryButtonClicked];
        }
    }
    NSLog(@"Index = %d - Title = %@", buttonIndex, [actionSheet buttonTitleAtIndex:buttonIndex]);
}
- (void)cameraPictureButtonClicked
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
    }
}
- (void)libraryButtonClicked
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
}
- (void)showPopover:(UIView*)btn;
{
    if (popoverController == nil)
    {
        PopOverView *settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PopOverView"];
        
        if (isNationality == YES) {
            settingsViewController.index = currentTextField.tag;
        }
        else if (isrank == YES){
            settingsViewController.marrData = marrranklist;
            
        }
        else if (isGender == YES){
          settingsViewController.index = currentTextField.tag;
            
        }
        else if (iscontryofres == YES)
        {
            settingsViewController.index = currentTextField.tag;
            settingsViewController.marrnationality = countryofresidencelist;

        }
        else{
            settingsViewController.index = currentTextField.tag;
            settingsViewController.marrnationality = marrnationality;
        }
        //settingsViewController.marrData = marrranklist;
        if ([settingsViewController respondsToSelector:@selector(setPreferredContentSize:)]) {
            //            if (amountype == YES) {
            settingsViewController.preferredContentSize = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.preferredContentSize = CGSizeMake(200, 100);
            //            }
            // iOS 7
        }
        else {
#pragma clang diagnostic pushc
#pragma GCC diagnostic ignored "-Wdeprecated"
            //if (amountype == YES) {
            settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 200);
            //            }
            //            else{
            //                settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 100);
            //            }
            //   settingsViewController.contentSizeForViewInPopover = CGSizeMake(200, 150);      // iOS < 7
#pragma clang diagnostic pop
        }
        settingsViewController.delegates = self;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        settingsViewController.modalInPopover = NO;
        popoverController = [[WYPopoverController alloc] initWithContentViewController:settingsViewController];
        popoverController.delegate = self;
        popoverController.passthroughViews = @[btn];
        popoverController.popoverLayoutMargins = UIEdgeInsetsMake(0, 10, 10 , 10);
        popoverController.wantsDefaultContentAppearance = YES;
        [popoverController presentPopoverFromRect:btn.bounds
                                           inView:btn
                         permittedArrowDirections:WYPopoverArrowDirectionAny
                                         animated:YES
                                          options:WYPopoverAnimationOptionFadeWithScale];
    }
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}
- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller
{
    popoverController.delegate = nil;
    popoverController = nil;
}
-(void)dismissPopUpViewController
{
    [popoverController dismissPopoverAnimated:YES];
    popoverController.delegate = nil;
    popoverController = nil;
}
- (IBAction)BtnShipType:(id)sender {
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    ranktype = NO;
    shiptype = YES;
    for (int i=0; i<marrshiplist.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[marrshiplist objectAtIndex:i] valueForKey:@"ShipType"]];
        
    }
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Rank Type" withOption:arrteam xy:CGPointMake(35, 58) size:CGSizeMake(300, 330) isMultiple:YES];
}
- (IBAction)btnMultiplerank:(id)sender {
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    ranktype = YES;
    shiptype = NO;
    for (int i=0; i<marrrankselectedlist.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[marrrankselectedlist objectAtIndex:i] valueForKey:@"Name"]];
        
    }
    
    [Dropobj fadeOut];
//    [self showPopUpWithTitle:@"Select Rank Type" withOption:arrteam xy:CGPointMake(35, 58) size:CGSizeMake(300, 330) isMultiple:YES];
     [self showPopUpWithTitle:@"Select Rank Type" withOption:arrteam xy:CGPointMake((self.view.frame.size.width-300)/2, 58) size:CGSizeMake(300, 330) isMultiple:YES];
}

- (IBAction)btnAvailable:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed1];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setDay:3];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
         datePicker.minimumDate = [NSDate date];
        // datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed1)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
   
}
-(void)Popvalueselected:(NSString*)strValue{
    NSLog(@"POP:%@",strValue);
    currentTextField.text = strValue;
    if ([txtselectrank.text isEqualToString:strValue]) {
        NSString *findrank = txtselectrank.text;
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", findrank];
        NSArray *resultsrank = [marrranklist filteredArrayUsingPredicate:predicate1];
        
        
        rankId= [NSString stringWithFormat:@"%@", [resultsrank  valueForKey:@"RankID" ]] ;
        NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
      selectedrankid = [Uid1 stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        [APP_DELEGATE showLoadingView:@""];
        [self performSelector:@selector(ShipType) withObject:nil afterDelay:0.1];
        [self performSelector:@selector(selectedRanklist) withObject:nil afterDelay:0.1];
    }
    
}
- (IBAction)btnselectrank:(id)sender {
    isNationality = NO;
    isrank = YES;
      isGender = NO;
    [txtselectrank setTag:0];
    currentTextField = txtselectrank;
    // amountype = YES;
    [self showPopover:sender];
    _txtmultiplerank.text = @"";
    RankID =@"";
}

- (IBAction)btnselectmartialstatus:(id)sender {
    isNationality = YES;
    isrank = NO;
      isGender = NO;
    [txtselectmatialstatus setTag:2];
    currentTextField = txtselectmatialstatus;
    // amountype = YES;
    [self showPopover:sender];
    
}
- (IBAction)BTnNationality:(id)sender {
    isNationality = NO;
    isrank = NO;
      isGender = NO;
    iscontryofres = NO;
    [txtnationality setTag:1];
    currentTextField = txtnationality;
    // amountype = YES;
    [self showPopover:sender];
}
-(void)viewWillAppear:(BOOL)animated{
    if (_profilecandidatebool == YES) {
        
        _profilecandidatebool = NO;
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(Nationality) withObject:nil afterDelay:0.1];
     [self performSelector:@selector(Ranklist) withObject:nil afterDelay:0.1];
         [self performSelector:@selector(ShipType) withObject:nil afterDelay:0.1];
    }
   
}
-(void)viewDidAppear:(BOOL)animated{
}

-(void)Nationality
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpNationality  = [[HttpWrapper alloc] init];
        httpNationality.delegate=self;
        httpNationality.getbool=NO;
        
        [httpNationality requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_nationality",MainUrl] param:@""];
    });
    
    
}
-(void)Ranklist
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httprank  = [[HttpWrapper alloc] init];
        httprank.delegate=self;
        httprank.getbool=NO;
        
        [httprank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:@""];
    });

}
-(void)selectedRanklist
{
    NSMutableDictionary *updateparameters = [[NSMutableDictionary alloc]init];
    
    [updateparameters setValue: selectedrankid forKey:@"rank_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpselectedrank  = [[HttpWrapper alloc] init];
        httpselectedrank.delegate=self;
        httpselectedrank.getbool=NO;
        
        [httpselectedrank requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_rank",MainUrl] param:[updateparameters copy]];
    });
}
-(void)getuserdata
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpuserdata  = [[HttpWrapper alloc] init];
        httpuserdata.delegate=self;
        httpuserdata.getbool=NO;
        
        [httpuserdata requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_candidate_data",MainUrl] param:[parameters copy]];
    });
    
    
}

-(void)getupdatedata
{
    NSString *find = txtnationality.text;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find];
    NSArray *results = [marrnationality filteredArrayUsingPredicate:predicate];
    
    
    NationalityId= [NSString stringWithFormat:@"%@", [results  valueForKey:@"NationalID" ]] ;
    NSString * Uid11 =   [[NationalityId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed1 = [Uid11 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //
    NSString *find1 = _txtcountryofresidence.text;
    
    NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", find1];
    NSArray *results1 = [countryofresidencelist filteredArrayUsingPredicate:predicate12];
    
    
    NSString * countryresid = [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryID" ]] ;
    NSString * Uid111 =   [[countryresid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed11 = [Uid111 stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    //
    
    NSString *findrank = txtselectrank.text;
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", findrank];
    NSArray *resultsrank = [marrranklist filteredArrayUsingPredicate:predicate1];
    
    
    rankId= [NSString stringWithFormat:@"%@", [resultsrank  valueForKey:@"RankID" ]] ;
    NSString * Uid1 =   [[rankId stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSString *trimmed = [Uid1 stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSMutableDictionary *updateparameters = [[NSMutableDictionary alloc]init];
    
    [updateparameters setValue: _txtfirstname.text forKey:@"first_name"];
    [updateparameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    [updateparameters setValue:_txtmiddlename.text forKey:@"middle_name"];
    [updateparameters setValue:_txtlastname.text forKey:@"last_name"];
    [updateparameters setValue:_txtemail.text forKey:@"email"];
    [updateparameters setValue:_txtemail2.text forKey:@"email2"];
    [updateparameters setValue:txtselectmatialstatus.text forKey:@"marital_status"];
    [updateparameters setValue:txtnoofchildren.text forKey:@"no_of_children"];
    [updateparameters setValue:_txtaddress.text forKey:@"address"];
    [updateparameters setValue:_txtcity.text forKey:@"city_name"];
    [updateparameters setValue:_txtzipcode.text forKey:@"zip_code"];
    [updateparameters setValue:trimmed1 forKey:@"nationality"];
    [updateparameters setValue:_txttelephoneno.text forKey:@"phone_no"];
    [updateparameters setValue:trimmed forKey:@"rank_id"];
    [updateparameters setValue:_txtmobileno.text forKey:@"mobile_no"];
    [updateparameters setValue:_txtmobileno2.text forKey:@"mobile_no2"];
    [updateparameters setValue:_txtindusno.text forKey:@"indos"];
    [updateparameters setValue:trimmed11 forKey:@"CountryofResidenceID"];
// _txtcountryofresidence.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CountryofResidenceID"]];
    
    
    [updateparameters setValue:_txtGender.text forKey:@"gender"];
    [updateparameters setValue:_txtHeight.text forKey:@"height"];
    [updateparameters setValue:_txtWeight.text forKey:@"weight"];
    [updateparameters setValue:_txtBMI.text forKey:@"bmi"];
    
    NSLog(@"height = %@",_txtHeight.text);
    NSLog(@"weight = %@",_txtHeight.text);
    NSLog(@"bmi = %@",_txtBMI.text);
    
    
    NSString * date = [NSString stringWithFormat:@"%@",txtdatofbirth.text];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSString * dob = [formatedt stringFromDate:fulldate];
    
    
    [updateparameters setValue:dob forKey:@"dob"];
    [updateparameters setValue:RankID forKey:@"applied_ranks"];
    
    [updateparameters setValue:ShipID forKey:@"applied_ships"];
    
    NSString * date1 = [NSString stringWithFormat:@"%@",_txtAvailable.text];
    NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
    [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
    
    NSDate *fulldate1=[formatedt1 dateFromString:date1];
    [formatedt1 setDateFormat:@"yyyy-MM-dd"];
    NSString * Available = [formatedt1 stringFromDate:fulldate1];
    
    [updateparameters setValue:Available forKey:@"available_from"];
    [updateparameters setValue:_txtjoining.text forKey:@"joining_type"];
    [updateparameters setValue:_txtmobilecode.text forKey:@"mcountry_code"];
    [updateparameters setValue:_txtmobilecode2.text forKey:@"mcountry_code2"];
    [updateparameters setValue:_txttelephonecode.text forKey:@"country_code"];
    
    [updateparameters setValue:_txtstdcode.text forKey:@"std_code"];
    
    NSLog(@"updateparameters = %@",updateparameters);
    
    if (image>0) {
        //Create manager
        [self uploadfile:updateparameters];

    }
    else
    {
        NSLog(@" url is update");
       
        if (fileData > 0)
        {
            [self uploadfile:updateparameters];
        }
        else
        {
             [updateparameters setObject:[marruserdata valueForKey:@"ImageUrl"] forKey:@"candidate_image"];
            dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
                httpupdatedata  = [[HttpWrapper alloc] init];
                httpupdatedata.delegate=self;
                httpupdatedata.getbool=NO;
                
                [httpupdatedata requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@update_candidate_data",MainUrl] param:[updateparameters copy]];
            });
        }
       
        
    }
    
    
}

-(void)uploadfile:(NSMutableDictionary *)updateparameters
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    //Now post
    NSLog(@"AddPost %@",updateparameters);
    NSString *url = [NSString stringWithFormat:@"%@update_candidate_data",MainUrl];
    [manager POST:url parameters:[updateparameters copy] constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //add img data one by one
        if (image>0) {
            NSData *imageData = UIImageJPEGRepresentation(image,0.5);
            [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"candidate_image"] fileName:[NSString stringWithFormat:@"user_image.jpg"] mimeType:@"image/jpeg"];
        }
        if (fileData>0)
        {
             [formData appendPartWithFileData:fileData name:[NSString stringWithFormat:@"CV"] fileName:[NSString stringWithFormat:@"%@",pdfname] mimeType:@"application/pdf"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id dicsResponse) {
        // [(AppDelegate *)[[UIApplication sharedApplication] delegate] hideProgress];
        NSLog(@"Success: %@", dicsResponse);
        NSLog(@"sign up---%@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marruserdata = [dicsResponse valueForKey:@"data"];
        NSString *msg=[NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
        if ([msg isEqualToString:@""])
        {
            msg=@"Updated Successfully";
        }
        if ([[dicsResponse valueForKey:@"status"] integerValue] == 0) {
            showAlert(AlertTitle, msg);
            [APP_DELEGATE hideLoadingView];
        }
        else
        {
            showAlert(AlertTitle, msg);
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Address"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CCode"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CandidateID"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CityName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Created"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DOB"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"EmailID"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"EmailID2"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FirstName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ImageURL"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Indos"];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Gender"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Weight"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"height"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bmi"];
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IsActive"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LastName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MaritalStatus"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MiddleName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MobileNo"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Nationality"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"NoofChildren"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"OtherMobileNo"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RankID"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RankName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TelephoneNo"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Zipcode"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"NationalName"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"JoiningType"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AvailableFrom"];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppliedRank"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            
            ///moreinfo
            [standardUserDefaults setObject:[marruserdata valueForKey:@"AppliedRank"] forKey:@"AppliedRank"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"Address"] forKey:@"Address"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"AvailableFrom"] forKey:@"AvailableFrom"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"CCode"] forKey:@"CCode"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"CandidateID"] forKey:@"CandidateID"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"CityName"] forKey:@"CityName"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"Created"] forKey:@"Created"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"DOB"] forKey:@"DOB"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"EmailID"] forKey:@"EmailID"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"EmailID2"] forKey:@"EmailID2"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"FirstName"] forKey:@"FirstName"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"ImageUrl"] forKey:@"ImageURL"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"Indos"] forKey:@"Indos"];
            
            [standardUserDefaults setObject:[marruserdata valueForKey:@"Gender"] forKey:@"Gender"];
            [standardUserDefaults setValue:_txtHeight.text forKey:@"height"];
            [standardUserDefaults setValue:_txtWeight.text forKey:@"weight"];
            [standardUserDefaults setValue:_txtBMI.text forKey:@"bmi"];
            
            [standardUserDefaults setObject:[marruserdata valueForKey:@"IsActive"] forKey:@"IsActive"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"LastName"] forKey:@"LastName"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"MaritalStatus"] forKey:@"MaritalStatus"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"MiddleName"] forKey:@"MiddleName"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"MobileNo"] forKey:@"MobileNo"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"Nationality"] forKey:@"Nationality"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"NoofChildren"] forKey:@"NoofChildren"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"OtherMobileNo"] forKey:@"OtherMobileNo"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"RankID"] forKey:@"RankID"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"RankName"] forKey:@"RankName"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"TelephoneNo"] forKey:@"TelephoneNo"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"JoiningType"] forKey:@"JoiningType"];
            
            [standardUserDefaults setObject:[marruserdata valueForKey:@"Zipcode"]forKey:@"Zipcode"];
            [standardUserDefaults setObject:[marruserdata valueForKey:@"NationalName"]forKey:@"NationalName"];
            
            [self.navigationController popViewControllerAnimated:NO];
            [APP_DELEGATE hideLoadingView];
        }
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              [APP_DELEGATE hideLoadingView];
          }];
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpNationality && httpNationality != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrnationality = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        
    }
    else if(wrapper == httpcountryofres && httpcountryofres != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        countryofresidencelist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
         [self performSelector:@selector(getuserdata) withObject:nil afterDelay:0.1];
    }
    else if(wrapper == httpShip && httpShip != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrshiplist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"0"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
           // showAlert(AlertTitle, message);
        }
        else{
            
            
        }
    }
   else if(wrapper == httprank && httprank != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrranklist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
    }
   else if(wrapper == httpselectedrank && httpselectedrank != nil)
   {
       NSLog(@"dicresponce %@",dicsResponse);
       NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
       dic=[dicsResponse valueForKey:@"data"];
       marrrankselectedlist = [dicsResponse valueForKey:@"data"];
       NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
   }
   else if(wrapper == httpuserdata && httpuserdata != nil)
   {
       NSLog(@"dicresponce %@",dicsResponse);
       NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
       dic=[dicsResponse valueForKey:@"data"];
       marruserdata = [dicsResponse valueForKey:@"data"];
       NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
       if ([status isEqualToString:@"1"])
       {
           NSLog(@"Userdata == %@",marruserdata);

//           CCode = "";
//           DOB = "0000-00-00";
//           ImageUrl = "http://clientsdemoarea.com/projects/mariexeltime/uploads/user_profile_image/";

           NSString* profilepicname = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"ImageUrl"]];
           
         
           if ([profilepicname  isEqualToString:@"http://test.infinitydynamics.in/uploads/user_profile_image/"])
           {
               
               _imgeview.image = [UIImage imageNamed: @"user-1"];
           }else{
               NSURL *url = [NSURL URLWithString:profilepicname];
               [_imgeview hnk_setImageFromURL:url];
           }
           _txtcity.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CityName"]];
_txtemail.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"EmailID"]];
_txtemail2.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"EmailID2"]];
txtselectmatialstatus.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"MaritalStatus"]];
_txtindusno.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Indos"]];

_txtGender.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Gender"]];
_txtHeight.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Height"]];
_txtWeight.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Weight"]];
_txtBMI.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"BMI"]];

           
_txtfirstname.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"FirstName"]];
           _txtfirstname.userInteractionEnabled = NO;
_txtmiddlename.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"MiddleName"]];
           _txtmiddlename.userInteractionEnabled = NO;

_txtlastname.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"LastName"]];
           _txtlastname.userInteractionEnabled = NO;

_txtmobileno.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"MobileNo"]];
txtnationality.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"NationalName"]];
txtnoofchildren.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"NoofChildren"]];
_txtmobileno2.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"OtherMobileNo"]];
            txtselectrank.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"RankName"]];
 _txtaddress.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Address"]];
 _txttelephoneno.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"TelephoneNo"]];
           _txtzipcode.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Zipcode"]];
         
           [_btnuploadcv setTitle:[NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"ResumeFile"]] forState:UIControlStateNormal];
           //
           NSString *find1 = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CountryofResidenceID"]];
           
           NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"CountryID ==[c] %@", find1];
           NSArray *results1 = [countryofresidencelist filteredArrayUsingPredicate:predicate12];
           
           
           NSString * countryresid = [NSString stringWithFormat:@"%@", [results1  valueForKey:@"CountryName" ]] ;
           NSString * Uid111 =   [[countryresid stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
           NSString *trimmed11 = [Uid111 stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceAndNewlineCharacterSet]];
           //
           
           NSString *s = trimmed11;
           
           
           NSString *r = [s stringByReplacingOccurrencesOfString:@"\"" withString:@""];
           
           _txtcountryofresidence.text = r;

           NSString * date = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"DOB"]];
           
           NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
           [formatedt setDateFormat:@"yyyy-MM-dd"];
           NSDate *fulldate=[formatedt dateFromString:date];
           [formatedt setDateFormat:@"dd-MMM-yyyy"];
           NSString * dob = [formatedt stringFromDate:fulldate];
           
           txtdatofbirth.text = [NSString stringWithFormat:@"%@",dob];
           txtdatofbirth.userInteractionEnabled = NO;
           _btndob.userInteractionEnabled = NO;

           _txtjoining.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"JoiningType"]];
           
           
           NSString * date1 = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"AvailableFrom"]];
           
           NSDateFormatter *formatedt1=[[NSDateFormatter alloc]init];
           [formatedt1 setDateFormat:@"yyyy-MM-dd"];
           NSDate *fulldate1=[formatedt1 dateFromString:date1];
           [formatedt1 setDateFormat:@"dd-MMM-yyyy"];
           NSString * Available = [formatedt1 stringFromDate:fulldate1];
           
           _txtAvailable.text = [NSString stringWithFormat:@"%@",Available];
           NSString *stdCode =[marruserdata valueForKey:@"STDCode"];
           if ([stdCode isEqualToString:@"0"] )
           {
               stdCode=@"";
           }
           
            _txtstdcode.text = [NSString stringWithFormat:@"%@",stdCode];
             _txtmobilecode.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CCode"]];
             _txtmobilecode2.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"OtherCCode"]];
           NSString *telCode =[marruserdata valueForKey:@"TelCountryCode"];
           if ([telCode isEqualToString:@"0"] )
           {
               telCode=@"";
           }
           

            _txttelephonecode.text = [NSString stringWithFormat:@"%@",telCode];
           
           NSMutableArray * ArryData = [[NSMutableArray alloc] init];
             NSMutableArray *NewArr = [[NSMutableArray alloc] init];
            NSMutableArray *myarr = [[NSMutableArray alloc] init];
              NSMutableArray *myrankname = [[NSMutableArray alloc] init];
           ArryData = [marruserdata valueForKey:@"AppliedRank"];
           for (int i=0; i<ArryData.count; i++) {
               NSUInteger index = [NewArr indexOfObject:ArryData[i]];
               NSLog(@"index %lu",(unsigned long)index);
               [myarr addObject:[[ArryData objectAtIndex:i] valueForKey:@"RankID"]];
                [myrankname addObject:[[ArryData objectAtIndex:i] valueForKey:@"Name"]];
           }
           NSLog(@"my arr %@",myarr);
           
           RankID = [myarr componentsJoinedByString:@","];
           _txtmultiplerank.text = [myrankname componentsJoinedByString:@","];
           
           
           
           
           
           NSMutableArray * ArryData1 = [[NSMutableArray alloc] init];
           NSMutableArray *NewArr1 = [[NSMutableArray alloc] init];
           NSMutableArray *myarr1 = [[NSMutableArray alloc] init];
           NSMutableArray *myrankname1 = [[NSMutableArray alloc] init];
           ArryData1 = [marruserdata valueForKey:@"AppliedShipType"];
           for (int i=0; i<ArryData1.count; i++) {
               NSUInteger index1 = [NewArr1 indexOfObject:ArryData1[i]];
               NSLog(@"index %lu",(unsigned long)index1);
               [myarr1 addObject:[[ArryData1 objectAtIndex:i] valueForKey:@"ShipTypeID"]];
               [myrankname1 addObject:[[ArryData1 objectAtIndex:i] valueForKey:@"ShipType"]];
           }
           NSLog(@"my arr %@",myarr1);
           
           ShipID = [myarr1 componentsJoinedByString:@","];
          _txtshiptype.text = [myrankname1 componentsJoinedByString:@","];
           
           
           
           
           selectedrankid  = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"RankID"]];
           [self performSelector:@selector(ShipType) withObject:nil afterDelay:0.1];
             [self performSelector:@selector(selectedRanklist) withObject:nil afterDelay:0.1];
           
          
            [APP_DELEGATE hideLoadingView];
           
         //   [self.navigationController popViewControllerAnimated:NO];
         
       }
       else{
           
           
       }
   }
   else if(wrapper == httpupdatedata && httpupdatedata != nil)
   {
       NSLog(@"dicresponce %@",dicsResponse);
       NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
       dic=[dicsResponse valueForKey:@"data"];
       marruserdata = [dicsResponse valueForKey:@"data"];
       NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
       if ([status isEqualToString:@"1"]) {
           NSLog(@"Userdata == %@",marruserdata);
           
           //           CCode = "";
           
           //           DOB = "0000-00-00";
           //           ImageUrl = "http://clientsdemoarea.com/projects/mariexeltime/uploads/user_profile_image/";
           
           NSString* profilepicname = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"ImageUrl"]];
           NSURL *url = [NSURL URLWithString:profilepicname];
           [_imgeview hnk_setImageFromURL:url];

           _txtcity.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CityName"]];
           _txtemail.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"EmailID"]];
           _txtemail2.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"EmailID2"]];
           txtselectmatialstatus.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"MaritalStatus"]];
           _txtindusno.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Indos"]];
           
           _txtGender.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Gender"]];
           _txtHeight.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Height"]];
           _txtWeight.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Weight"]];
           _txtBMI.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"BMI"]];
           
           
           
           _txtfirstname.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"FirstName"]];
           _txtmiddlename.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"MiddleName"]];
           _txtlastname.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"LastName"]];
           _txtmobileno.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"MobileNo"]];
           txtnationality.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Nationality"]];
           txtnoofchildren.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"NoofChildren"]];
           _txtmobileno2.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"OtherMobileNo"]];
           txtselectrank.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"RankName"]];
           _txtaddress.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Address"]];
           _txttelephoneno.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"TelephoneNo"]];
           _txtzipcode.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"Zipcode"]];
           _txtjoining.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"JoiningType"]];
           _txtAvailable.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"AvailableFrom"]];
           _txtcountryofresidence.text = [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CountryofResidenceID"]];

           NSMutableArray * ArryData = [[NSMutableArray alloc] init];
           NSMutableArray *NewArr = [[NSMutableArray alloc] init];
           NSMutableArray *myarr = [[NSMutableArray alloc] init];
           NSMutableArray *myrankname = [[NSMutableArray alloc] init];
           ArryData = [marruserdata valueForKey:@"AppliedRank"];
           for (int i=0; i<ArryData.count; i++) {
               NSUInteger index = [NewArr indexOfObject:ArryData[i]];
               NSLog(@"index %lu",(unsigned long)index);
               [myarr addObject:[[ArryData objectAtIndex:i] valueForKey:@"RankID"]];
               [myrankname addObject:[[ArryData objectAtIndex:i] valueForKey:@"Name"]];
           }
           NSLog(@"my arr %@",myarr);
           
           RankID = [myarr componentsJoinedByString:@","];
           _txtmultiplerank.text = [myrankname componentsJoinedByString:@","];
           
           
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Address"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CCode"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CandidateID"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CityName"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Created"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"DOB"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"EmailID"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"EmailID2"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FirstName"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ImageURL"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Indos"];
           
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Gender"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"height"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"weight"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"bmi"];
           
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"IsActive"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LastName"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MaritalStatus"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MiddleName"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"MobileNo"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Nationality"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"NoofChildren"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"OtherMobileNo"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RankID"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RankName"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TelephoneNo"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Zipcode"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"NationalName"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"JoiningType"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AvailableFrom"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppliedRank"];
           [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CountryofResidenceID"];

           [[NSUserDefaults standardUserDefaults] synchronize];
           
           NSUserDefaults* standardUserDefaults = [NSUserDefaults standardUserDefaults];
           
           
           ///moreinfo
           [standardUserDefaults setObject:[marruserdata valueForKey:@"AppliedRank"] forKey:@"AppliedRank"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Address"] forKey:@"Address"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"AvailableFrom"] forKey:@"AvailableFrom"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"CCode"] forKey:@"CCode"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"CandidateID"] forKey:@"CandidateID"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"CityName"] forKey:@"CityName"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Created"] forKey:@"Created"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"DOB"] forKey:@"DOB"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"EmailID"] forKey:@"EmailID"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"EmailID2"] forKey:@"EmailID2"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"FirstName"] forKey:@"FirstName"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"ImageUrl"] forKey:@"ImageURL"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Indos"] forKey:@"Indos"];
           
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Gender"] forKey:@"Gender"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Height"] forKey:@"height"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Weight"] forKey:@"weight"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"BMI"] forKey:@"bmi"];
           
           [standardUserDefaults setObject:[marruserdata valueForKey:@"IsActive"] forKey:@"IsActive"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"LastName"] forKey:@"LastName"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"MaritalStatus"] forKey:@"MaritalStatus"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"MiddleName"] forKey:@"MiddleName"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"MobileNo"] forKey:@"MobileNo"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Nationality"] forKey:@"Nationality"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"NoofChildren"] forKey:@"NoofChildren"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"OtherMobileNo"] forKey:@"OtherMobileNo"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"RankID"] forKey:@"RankID"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"RankName"] forKey:@"RankName"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"TelephoneNo"] forKey:@"TelephoneNo"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"JoiningType"] forKey:@"JoiningType"];
           
           [standardUserDefaults setObject:[marruserdata valueForKey:@"Zipcode"]forKey:@"Zipcode"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"NationalName"]forKey:@"NationalName"];
           [standardUserDefaults setObject:[marruserdata valueForKey:@"CountryofResidenceID"]forKey:@"CountryofResidenceID"];

          

           NSString * message =  [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
            showAlert(AlertTitle, @"Updated Successfully");
           MyProfileCandidate *s =[self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileCandidate"];
          // s.profilecandidatebool = YES;
           // s.mdiccomplainlistid = [marrComplaintListData objectAtIndex:indexPath.row];
           [self.navigationController pushViewController:s animated:NO];
       }
       else{
           NSString * message =  [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
           if ([message isEqualToString:@""])
           {
               message=@"Updated Successfully";
           }
           showAlert(AlertTitle, message );
            [APP_DELEGATE hideLoadingView];
           
       }
   }
    
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
- (IBAction)BtnDateofbirth:(id)sender {
    NSLog(@"date Button Clicked");
    [self.view endEditing:YES];
    
    if(isPickerLaunced)
    {
        [self datePickerCancelPressed];
    }
    else
    {
        CGRect pickerFrame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            pickerFrame = CGRectMake(0 , 84 , self.view.frame.size.width , 200 );
        }
        else
        {
            pickerFrame = CGRectMake(0 , 40 , self.view.frame.size.width , 250 );
        }
//        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
//        NSDate * currentDate = [NSDate date];
//        NSDateComponents * comps = [[NSDateComponents alloc] init];
//        [comps setDay:3];
//        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
        datePicker.datePickerMode=UIDatePickerModeDate;
        datePicker.backgroundColor=[UIColor groupTableViewBackgroundColor];
        NSCalendar * gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
        NSDate * currentDate = [NSDate date];
        NSDateComponents * comps = [[NSDateComponents alloc] init];
        [comps setYear: -18];
        NSDate * maxDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        [comps setYear: -70];
        NSDate * minDate = [gregorian dateByAddingComponents: comps toDate: currentDate options: 0];
        
        self->datePicker.minimumDate = minDate;
        self->datePicker.maximumDate = maxDate;
        self->datePicker.date = maxDate;
        
       // datePicker.minimumDate = [NSDate date];
          //      datePicker.maximumDate = [NSDate date];
        CGRect frame;
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            frame = CGRectMake(0,150,self.view.frame.size.width,200);
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 812){
            
            ///this is iphone x
            frame = CGRectMake(0,400,self.view.frame.size.width,350);
            
        }
        else if ([[UIScreen mainScreen] bounds].size.height == 736){
            
            ///this is iphone x
            frame = CGRectMake(0,325,self.view.frame.size.width,350);
            
        }
        else
        {
            frame = CGRectMake(0,250,self.view.frame.size.width,350);
        }
        subView = [[UIView alloc] initWithFrame:frame];
        [subView addSubview:datePicker] ;
        subView.backgroundColor=[UIColor redColor];
        subView.hidden = NO;
        subView.backgroundColor = [UIColor clearColor];
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            //this is iphone 5 xib
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 40)];
        }
        else
        {
            toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
            
        }
        toolbar.tintColor=[UIColor blackColor];
        UIBarButtonItem *toolbarItem1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(datePickerCancelPressed)];
        UIBarButtonItem *toolbarItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(datePickerDonePressed)];
        UIBarButtonItem *flexibleWidth = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] ;
        toolbar.items = [[NSArray alloc] initWithObjects:toolbarItem1,flexibleWidth , toolbarItem, nil];
        [subView addSubview:toolbar];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 482 );
        [UIView beginAnimations:@"start" context:nil];
        [UIView setAnimationDuration:0.6];
        [UIView setAnimationDidStopSelector:@selector(animationFinished)];
        [UIView setAnimationDelegate:self];
        subView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0 , 125 );
        [UIView commitAnimations];
        [self.view addSubview:subView];
        isPickerLaunced=YES;
    }
}
-(void)datePickerCancelPressed
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MMM'-'yyyy"];//04-22-2015 3.00pm

        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        txtdatofbirth.text = eventDate;
        if ([txtdatofbirth.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([txtdatofbirth.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MMM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter stringFromDate:selectedDate];
        
        endtime = [dateFormatter stringFromDate:selectedDate];
        
        txtdatofbirth.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}
- (IBAction)btn_SPROFESSION:(id)sender {
    /* [_txt_profession setTag:3];
     currentTextField = _txt_profession;
     [self showPopover:sender];*/
    
    NSMutableArray *arrteam=[[NSMutableArray alloc] init];
    
    for (int i=0; i<marrranklist.count; i++) {
        //arrteam=[[arryList objectAtIndex:i] valueForKey:@"Profession"];
        [arrteam addObject:[[marrranklist objectAtIndex:i] valueForKey:@"Name"]];
        
    }

    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Rank Type" withOption:arrteam xy:CGPointMake((self.view.frame.size.width-300)/2, 58) size:CGSizeMake(300, 330) isMultiple:YES];
    
    
}
// MULTISELECTION DROPDOWN
-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSMutableArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    if (shiptype == YES) {
        Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
        Dropobj.delegate = self;
        [Dropobj showInView:self.view animated:YES];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDown_R:20.0 G:35.0 B:97.0 alpha:0.70];
        
    }
    if (ranktype == YES) {
        Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
        Dropobj.delegate = self;
        [Dropobj showInView:self.view animated:YES];
        
        /*----------------Set DropDown backGroundColor-----------------*/
        [Dropobj SetBackGroundDropDown_R:20.0 G:35.0 B:97.0 alpha:0.70];
        
    }
    
//    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
//    Dropobj.delegate = self;
//    [Dropobj showInView:self.view animated:YES];
//
//    /*----------------Set DropDown backGroundColor-----------------*/
//    [Dropobj SetBackGroundDropDown_R:20.0 G:35.0 B:97.0 alpha:0.70];
//
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
//    for (int i=0; i<marrranklist.count; i++) {
//        _txtmultiplerank.text=[[marrrankselectedlist objectAtIndex:i] valueForKey:@"Name"];
//    }
//    _txtmultiplerank.text=[[marrrankselectedlist objectAtIndex:anIndex] valueForKey:@"Name"];
    if (ranktype == YES) {
        
        
        for (int i=0; i<marrranklist.count; i++) {
            _txtmultiplerank.text=[[marrranklist objectAtIndex:i] valueForKey:@"Name"];
        }
        _txtmultiplerank.text=[[marrranklist objectAtIndex:anIndex] valueForKey:@"Name"];
    }
    if (shiptype == YES) {
        for (int i=0; i<marrshiplist.count; i++) {
            _txtshiptype.text=[[marrshiplist objectAtIndex:i] valueForKey:@"ShipType"];
        }
        _txtshiptype.text=[[marrshiplist objectAtIndex:anIndex] valueForKey:@"ShipType"];
        
    }
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (ranktype == YES) {
        if (ArryData.count>0) {
            _txtmultiplerank.text=[ArryData componentsJoinedByString:@","];
            NSMutableArray *NewArr = [[NSMutableArray alloc] init];
            NSMutableArray *myarr = [[NSMutableArray alloc] init];
            
            for (int i=0; i<marrranklist.count; i++) {
                [NewArr addObject:[[marrranklist objectAtIndex:i] valueForKey:@"Name"]];
            }
            
            for (int i=0; i<ArryData.count; i++) {
                NSUInteger index = [NewArr indexOfObject:ArryData[i]];
                NSLog(@"index %lu",(unsigned long)index);
                [myarr addObject:[[marrranklist objectAtIndex:index] valueForKey:@"RankID"]];
            }
            NSLog(@"my arr %@",myarr);
            
            RankID = [myarr componentsJoinedByString:@","];
            
            NSLog(@"RankID %@",RankID);
            //CGSize size=[self GetHeightDyanamic:_txt_profession];
            //_txt_profession.frame=CGRectMake(16, 240, 287, size.height);
        }
        else{
            _txtmultiplerank.text=@"";
        }
    }
    if (shiptype == YES) {
        if (ArryData.count>0) {
            _txtshiptype.text=[ArryData componentsJoinedByString:@","];
            NSMutableArray *NewArr = [[NSMutableArray alloc] init];
            NSMutableArray *myarr = [[NSMutableArray alloc] init];
            
            for (int i=0; i<marrshiplist.count; i++) {
                [NewArr addObject:[[marrshiplist objectAtIndex:i] valueForKey:@"ShipType"]];
            }
            
            for (int i=0; i<ArryData.count; i++) {
                NSUInteger index = [NewArr indexOfObject:ArryData[i]];
                NSLog(@"index %lu",(unsigned long)index);
                [myarr addObject:[[marrshiplist objectAtIndex:index] valueForKey:@"ShipID"]];
            }
            NSLog(@"my arr %@",myarr);
            
            ShipID = [myarr componentsJoinedByString:@","];
            
            NSLog(@"RankID %@",ShipID);
            //CGSize size=[self GetHeightDyanamic:_txt_profession];
            //_txt_profession.frame=CGRectMake(16, 240, 287, size.height);
        }
        else{
            _txtshiptype.text=@"";
        }
        
    }
    
}
- (void)DropDownListViewDidCancel{
    
}

- (IBAction)btnSelectJoining:(id)sender {
    
    isNationality = YES;
    isrank = NO;
      isGender = NO;
    [_txtjoining setTag:17];
    currentTextField = _txtjoining;
    // amountype = YES;
    [self showPopover:sender];

}
- (IBAction)btnSelectGender:(id)sender {
    isNationality = NO;
    isrank = NO;
    isGender = YES;
    
    [_txtGender setTag:16];
    currentTextField = _txtGender;
    // amountype = YES;
    [self showPopover:sender];
}
- (IBAction)btntelephonecountrycode:(id)sender {
    ismobilecountrycode = NO;
    ismobile2countrycode = NO;
    istelephonecountrycode = YES;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}
-(void)datePickerCancelPressed1
{
    subView.hidden=YES;
    toolbar.hidden=YES;
    datePicker.hidden=YES;
    isPickerLaunced=NO;
}
-(void)datePickerDonePressed1
{
    if(isPickerLaunced)
    {
        NSDate *selectedDate = datePicker.date;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //  [dateFormatter setDateFormat:@"MM'-'dd'-'yyyy"];//04-22-2015
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd"];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MMM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter1 stringFromDate:selectedDate];
        _txtAvailable.text = eventDate;
        if ([_txtAvailable.text length] <=0)
        {
        }
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
        isPickerLaunced=NO;
    }
    else {
        if ([_txtAvailable.text length] <=0)
        {
            
        }
        NSDate *selectedDate = datePicker.date;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy'-'MMM'-'dd"];
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        [dateFormatter1 setDateFormat:@"dd'-'MMM'-'yyyy"];//04-22-2015 3.00pm
        
        
        NSString *eventDate=[dateFormatter1 stringFromDate:selectedDate];
        
        endtime = [dateFormatter1 stringFromDate:selectedDate];
        
        _txtAvailable.text = eventDate;
        subView.hidden=YES;
        toolbar.hidden=YES;
        datePicker.hidden=YES;
    }
}
- (IBAction)btnMobilecountrycode:(id)sender {
    ismobilecountrycode = YES;
    ismobile2countrycode = NO;
    istelephonecountrycode = NO;
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
    
}
- (void)updateViewsWithCountryDic:(NSDictionary*)countryDic{
    if (ismobilecountrycode == YES) {
         [_txtmobilecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
        
    }
    else if (ismobile2countrycode == YES){
      [_txtmobilecode2 setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    else{
         [_txttelephonecode setText:[NSString stringWithFormat:@"%@",countryDic[@"phone_code"]]];
    }
    
}
- (IBAction)btnmobile2countrycode:(id)sender {
    ismobilecountrycode = NO;
    ismobile2countrycode = YES;
    istelephonecountrycode = NO;
    
    PCCPViewController * vc = [[PCCPViewController alloc] initWithCompletion:^(id countryDic) {
        [self updateViewsWithCountryDic:countryDic];
    }];
    [vc setIsUsingChinese:[_langSwitch selectedSegmentIndex] == 1];
    UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:naviVC animated:YES completion:NULL];
}
-(void)ShipType
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpShip  = [[HttpWrapper alloc] init];
        httpShip.delegate=self;
        httpShip.getbool=NO;
        
        [httpShip requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_ship_type",MainUrl] param:@""];
    });
    
    
}
- (IBAction)btncountryofres_click:(UIButton *)sender {
    
    iscontryofres = YES;
    isNationality = NO;
    isrank = NO;
    isGender = NO;
    [_txtcountryofresidence setTag:20];
    currentTextField = _txtcountryofresidence;
    // amountype = YES;
    [self showPopover:sender];
  
}
-(void)countryofres
{
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpcountryofres  = [[HttpWrapper alloc] init];
        httpcountryofres.delegate=self;
        httpcountryofres.getbool=NO;
        
        [httpcountryofres requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_country_list",MainUrl] param:@""];
    });
}
- (IBAction)uploadcv_click:(UIButton *)sender {
    
    UIDocumentMenuViewController *importMenu = [[UIDocumentMenuViewController alloc] initWithDocumentTypes:@[(__bridge NSString*)kUTTypePDF] inMode:UIDocumentPickerModeImport];
    importMenu.delegate = self;
    importMenu.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:importMenu animated:YES completion:nil];
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url{
  //  [APP_DELEGATE hideLoadingView];
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        fileData = [NSData dataWithContentsOfURL:url];
        pdfname = [url lastPathComponent];
        NSLog(@"%@FILEDATA:",fileData);
          [_btnuploadcv setTitle:pdfname forState:UIControlStateNormal];
        NSString *alertMessage = [NSString stringWithFormat:@"Successfully imported %@", [url lastPathComponent]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Import"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        });
    }
}
- (void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker {
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}
- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
  // [APP_DELEGATE showLoadingView:@""];
}
- (IBAction)previewcv_click:(UIButton *)sender {
    if ([[marruserdata valueForKey:@"CVFile"] isEqualToString:@""])
    {
       showAlert(AlertTitle, @"No CV Found" );
    }
    else
    {
        WebViewController *s =[self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
        s.website =   [NSString stringWithFormat:@"%@",[marruserdata valueForKey:@"CVFile"]];
        s.strheader = @"Preview";
        [self.navigationController pushViewController:s animated:NO];
    }
}
@end
