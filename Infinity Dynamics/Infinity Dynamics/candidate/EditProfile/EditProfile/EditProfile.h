//
//  EditProfile.h
//  Infinity Dynamics
//
//  Created by My Mac on 11/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextFieldValidator.h"
#import "PopOverView.h"
#import "HttpWrapper.h"
#import "DropDownListView.h"
@interface EditProfile : UIViewController<UIScrollViewDelegate,UITextViewDelegate,HttpWrapperDelegate,kDropDownListViewDelegate>{
     HttpWrapper *httprank,*httpNationality,*httpuserdata,*httpupdatedata,*httpselectedrank,*httpShip,*httpcountryofres;
    UIImage * image;
      NSMutableArray *marruserdata,*marrshiplist;
    NSString * isAccept;
    NSString * NationalityId;
    BOOL isNationality;
    BOOL iscontryofres;

    BOOL isrank;
    BOOL isGender;
    BOOL imagepicker;
    UITextField *currentTextField;
    
    NSMutableArray *marrranklist;
    NSMutableArray *marrrankselectedlist;
    NSMutableArray *marrnationality;
    NSMutableArray *countryofresidencelist;

    IBOutlet UITextField *txtselectrank;
    
    IBOutlet UITextField *txtnoofchildren;
    IBOutlet UITextField *txtselectmatialstatus;
    
    IBOutlet UITextField *txtnationality;
    
      NSString * rankId;
    IBOutlet TextFieldValidator *txtdatofbirth;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
   NSString * endtime;
   
    NSMutableArray *arryList;
    DropDownListView * Dropobj;
    NSString * RankID;
     NSString * ShipID;
    BOOL ismobilecountrycode;
      BOOL ismobile2countrycode;
      BOOL istelephonecountrycode;
    
    NSString * selectedrankid;
    
    BOOL shiptype;
    BOOL ranktype;
    
}
@property (assign, nonatomic) BOOL cocdetail;
@property (assign, nonatomic) BOOL profilecandidatebool;
@property (assign, nonatomic) BOOL isfrompopup;
@property (strong, nonatomic) IBOutlet UIView *vwavailabledt;
@property (weak, nonatomic) IBOutlet UISegmentedControl *langSwitch;
- (IBAction)BtnDateofbirth:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnimage;
@property (strong, nonatomic) IBOutlet UIImageView *imgeview;
- (IBAction)btnImage:(id)sender;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtfirstname;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmiddlename;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtlastname;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtemail;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtemail2;

@property (strong, nonatomic) IBOutlet TextFieldValidator *txtindusno;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txttelephoneno;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txttelephonecode;
@property (strong, nonatomic) IBOutlet UITextField *txtmobilecode;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmobileno;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmobilecode2;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtmobileno2;
@property (strong, nonatomic) IBOutlet UITextField *txtmultiplerank;


@property (weak, nonatomic) IBOutlet UITextField *txtHeight;
@property (weak, nonatomic) IBOutlet UITextField *txtWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtBMI;



- (IBAction)btnNextStep:(id)sender;

- (IBAction)btnBack:(id)sender;
- (IBAction)btnselectrank:(id)sender;
- (IBAction)btnselectmartialstatus:(id)sender;

- (IBAction)BTnNationality:(id)sender;
@property (strong, nonatomic) IBOutlet TextFieldValidator *txtstdcode;

@property (strong, nonatomic) IBOutlet UITextField *txtcity;
@property (strong, nonatomic) IBOutlet UITextView *txtaddress;
@property (strong, nonatomic) IBOutlet UITextField *txtzipcode;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;
- (IBAction)btnMultiplerank:(id)sender;
- (IBAction)btnAvailable:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtAvailable;
@property (strong, nonatomic) IBOutlet UITextField *txtjoining;
- (IBAction)btnSelectJoining:(id)sender;
- (IBAction)btntelephonecountrycode:(id)sender;

- (IBAction)btnMobilecountrycode:(id)sender;
- (IBAction)btnmobile2countrycode:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtGender;

- (IBAction)btnSelectGender:(id)sender;
- (IBAction)BtnShipType:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtshiptype;
@property (weak, nonatomic) IBOutlet UITextField *txtcountryofresidence;
- (IBAction)btncountryofres_click:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UIButton *btndob;
@property (strong, nonatomic) IBOutlet UIButton *btnuploadcv;
- (IBAction)uploadcv_click:(UIButton *)sender;

@end
