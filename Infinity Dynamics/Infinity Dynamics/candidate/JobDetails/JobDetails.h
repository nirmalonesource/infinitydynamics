//
//  JobDetails.h
//  Infinity Dynamics
//
//  Created by My Mac on 23/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
@interface JobDetails : UIViewController<UITextFieldDelegate,HttpWrapperDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>{
    HttpWrapper *httplatestjob,*httpFeaturedjob;
    NSMutableArray *marrlatestjob;
    NSMutableArray *marrhttpFeaturedjob;
    NSString * companyid;
     NSString * selectedjobid;
    UIView *picker;   /////menu picker view
 
}
@property (weak, nonatomic) IBOutlet UILabel *txtvacancy;
@property (weak, nonatomic) IBOutlet UILabel *txtExperiencerange;
@property (strong, nonatomic) IBOutlet UIView *afterdescview;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;
@property (strong, nonatomic) IBOutlet UILabel *lblshiptype;
@property (strong, nonatomic) IBOutlet UILabel *lblcompanytype;
//@property (strong, nonatomic) IBOutlet UILabel *lbldescription;
@property (strong, nonatomic) IBOutlet UITextView *lbldescription;
@property (strong, nonatomic) IBOutlet UILabel *lblcompanyotherjobs;

@property (strong, nonatomic) IBOutlet UILabel *lbljobtype;
@property (strong, nonatomic) IBOutlet UIImageView *companyimage;
- (IBAction)btnApplyNow:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnAPPLYNOW;
@property (strong, nonatomic)  NSString *isshoreselect;
@property (strong, nonatomic) IBOutlet UILabel *lblshiptpe;

@end
