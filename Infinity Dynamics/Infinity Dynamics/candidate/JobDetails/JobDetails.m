//
//  JobDetails.m
//  Infinity Dynamics
//
//  Created by My Mac on 23/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "JobDetails.h"
#import "homecollectionCell.h"
#import "Constants.h"
#import "UIImageView+Haneke.h"
#import "JobApply.h"
@interface JobDetails ()

@end

@implementation JobDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"details === %@",_detail);
    NSString * descript = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"Description"]];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [_companyimage hnk_setImageFromURL:url];
  
    NSString*text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"JobTitle"]];
    
    [_lbljobtype setText:[text uppercaseString]];
    _lblcompanyotherjobs.text = [NSString stringWithFormat:@"OTHER JOB OF %@",[_detail valueForKey:@"CompanyName"]];
       // _lbldescription.text = [NSString stringWithFormat:@"%@",descript];
    NSString * htmlString = [NSString stringWithFormat:@"%@",descript];

    NSAttributedString * attrStr =
    [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                          documentAttributes:nil error:nil];
    
    _lbldescription.text = attrStr.string;
    
      _lblshiptype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"ShipType"]];
      _lblcompanytype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CompanyName"]];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date  = [dateFormatter dateFromString:[_detail valueForKey:@"JobEndDate"]];
    
    // Convert to new Date Format
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
    NSString *newDate = [dateFormatter stringFromDate:date];
    _txtvacancy.text = [NSString stringWithFormat:@"%@",newDate];
    _txtExperiencerange.text = [NSString stringWithFormat:@"%@ upto %@",[_detail valueForKey:@"FromExperience"],[_detail valueForKey:@"ToExperience"]];
    
   // _lbldescription.frame = CGRectMake(_lbldescription.frame.origin.x, _lbldescription.frame.origin.y, _lbldescription.frame.size.width, [self heightForText:descript]);
    
//    [_lbldescription sizeToFit];
//    [_lbldescription setAdjustsFontSizeToFitWidth:YES];
   // CGRect afterdescview = _afterdescview.frame;
   // afterdescview.origin.y = _lbldescription.frame.origin.y+_lbldescription.frame.size.height+5;
   // _afterdescview.frame = afterdescview;
     NSString * IsApplied = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"IsApplied"]];
    if ([IsApplied isEqualToString:@"Applied"]) {
        _btnAPPLYNOW.enabled = NO;
        _btnAPPLYNOW.userInteractionEnabled = NO;
        _btnAPPLYNOW.alpha = 0.5;
    }
    else{
        _btnAPPLYNOW.enabled = YES;
        _btnAPPLYNOW.userInteractionEnabled = YES;
        _btnAPPLYNOW.alpha = 1;
        
    }

    self.scrollview.contentSize =CGSizeMake(self.view.frame.size.width,_scrollview.frame.size.height+_lbldescription.frame.size.height-30);
    
    if ([_isshoreselect isEqualToString:@"1"])
    {
        _afterdescview.hidden = YES;
        _lblshiptpe.text = @"Designation";
        _lblshiptype.text = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"DesignationName"]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForReuse
{
    [self.companyimage hnk_cancelSetImage];
    self.companyimage.image = nil;
}
- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}
-(CGFloat)heightForText:(NSString *)text
{
    NSInteger MAX_HEIGHT = 2000;
    UITextView * textView = [[UITextView alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width-80, MAX_HEIGHT+150)];
    textView.text = text;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        textView.font = [UIFont fontWithName:@"Lato-Regular" size:13.0];
    }
    else
    {
        textView.font = [UIFont fontWithName:@"Lato-Regular" size:14.0];
    }
    
    [textView sizeToFit];
    
    return textView.frame.size.height;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return marrhttpFeaturedjob.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    
    homecollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.lblname.text = [NSString stringWithFormat:@"%@",[[marrhttpFeaturedjob valueForKey:@"JobTitle"]objectAtIndex:indexPath.row]];
    cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[marrhttpFeaturedjob valueForKey:@"ShipType"]objectAtIndex:indexPath.row]];
    
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[marrhttpFeaturedjob objectAtIndex:indexPath.row] valueForKey:@"LogoURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [cell.imageview hnk_setImageFromURL:url];
    [APP_DELEGATE hideLoadingView];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // [self SelectedMenu:indexPath.row];
    JobDetails *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobDetails"];
    s.detail = [marrhttpFeaturedjob objectAtIndex:indexPath.row] ;
    
    [self.navigationController pushViewController:s animated:NO];
}

- (IBAction)btnApplyNow:(id)sender {
    [self.view endEditing:YES];
    JobApply *s =[self.storyboard instantiateViewControllerWithIdentifier:@"JobApply"];
    s.isshoreselect = _isshoreselect;
    s.detail = _detail;
    
    [self.navigationController pushViewController:s animated:NO];
}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    companyid = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"CompanyID"]];
    selectedjobid = [NSString stringWithFormat:@"%@",[_detail valueForKey:@"JobID"]];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];

}
-(void)CallMyMethod1
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:companyid forKey:@"company_id"];
    [parameters setValue:selectedjobid forKey:@"selected_job_id"];

    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"] forKey:@"candidate_id"];
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httplatestjob  = [[HttpWrapper alloc] init];
        httplatestjob.delegate=self;
        httplatestjob.getbool=NO;
        
        [httplatestjob requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_job_list",MainUrl] param:[parameters copy]];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
     if (wrapper == httplatestjob && httplatestjob != nil){
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        marrhttpFeaturedjob = [dicsResponse valueForKey:@"data"] ;
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            if (marrhttpFeaturedjob.count == 0) {
                _lblcompanyotherjobs.hidden = YES;
            }

            [self.collectionview reloadData];
            
        }
        else{
    
        }
    }
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}
@end
