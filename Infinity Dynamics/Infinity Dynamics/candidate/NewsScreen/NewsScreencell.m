//
//  Locationcell.m
//  CustomCellTutorial
//
//  Created by rishi on 10/17/13.
//  Copyright (c) 2013 rishi. All rights reserved.
//

#import "NewsScreencell.h"
#import "UIColor+CL.h"
#import "UIImageView+Haneke.h"
@implementation NewsScreencell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
       
    }
    return self;
}
-(void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                  byRoundingCorners:corners
                                                        cornerRadii:CGSizeMake(8.0, 8.0)];
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    view.layer.mask = shape;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

   [self setMaskTo:self.btnreadmore byRoundingCorners:UIRectCornerTopRight | UIRectCornerTopLeft];
   _imageview.layer.masksToBounds = YES;
  _imageview.layer.cornerRadius = 10;

}
- (void)prepareForReuse
{
    [self.imageView hnk_cancelSetImage];
    self.imageView.image = nil;
}

@end
