//
//  NewsScreen.m
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import "NewsScreen.h"
#import "Constants.h"
#import "UIImageView+Haneke.h"
#import "SWRevealViewController.h"
@interface NewsScreen ()

@end

@implementation NewsScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.navigationController.navigationBarHidden=YES;
    [self customSetup];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    //self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-228;
    [revealViewController setDelegate:self];
    
    if ( revealViewController )
    {
        
        [_btnmenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [picker addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        
    }
    
    if ( IsIphone6Plus ) {
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-150;
    }
    else{
        self.revealViewController.rearViewRevealWidth = self.view.frame.size.width-100;
    }
}
- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    
    if (revealController.frontViewPosition==FrontViewPositionRight)
    {
        picker.hidden=NO;
    }
    if (revealController.frontViewPosition==FrontViewPositionLeft)
    {
        picker.hidden=YES;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewsScreencell * cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"NewsScreencell" bundle:nil] forCellReuseIdentifier:@"myCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    }
    return cell;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return newslist.count;
    
    
    // return dicOfferList.count; // in your case, there are 3 cells
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.; // you can have your own choice, of course
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(NewsScreencell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.LblName.text = [NSString stringWithFormat:@"%@",[[newslist objectAtIndex:indexPath.section] valueForKey:@"NewsTitle"]];
    
    NSString * date = [NSString stringWithFormat:@"%@",[[newslist objectAtIndex:indexPath.section] valueForKey:@"NewsDate"]];
    
    NSDateFormatter *formatedt=[[NSDateFormatter alloc]init];
    [formatedt setDateFormat:@"yyyy-MM-dd"];
    NSDate *fulldate=[formatedt dateFromString:date];
    [formatedt setDateFormat:@"dd-MMM-yyyy"];
    NSString * JoiningDate = [formatedt stringFromDate:fulldate];
     cell.lbldate.text = [NSString stringWithFormat:@"%@",JoiningDate];
    cell.lbldescription.text = [NSString stringWithFormat:@"%@",[[newslist objectAtIndex:indexPath.section] valueForKey:@"Description"]];
    NSString* profilepicname = [NSString stringWithFormat:@"%@",[[newslist objectAtIndex:indexPath.section] valueForKey:@"NewsImageURL"]];
    NSURL *url = [NSURL URLWithString:profilepicname];
    [cell.imageview hnk_setImageFromURL:url];
    [APP_DELEGATE hideLoadingView];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"I'm selected %ld",(long)indexPath.section);
    //self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}
-(void)viewWillAppear:(BOOL)animated{
    [APP_DELEGATE showLoadingView:@""];
    [self performSelector:@selector(CallMyMethod1) withObject:nil afterDelay:0.1];
   
}

-(void)CallMyMethod1
{
    
    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
        httpnews  = [[HttpWrapper alloc] init];
        httpnews.delegate=self;
        httpnews.getbool=NO;
        
        [httpnews requestWithMethod:@"POST" url:[NSString stringWithFormat:@"%@get_news_list",MainUrl] param:@""];
    });
    
    
}
-(void)HttpWrapper:(HttpWrapper *)wrapper fetchDataSuccess:(NSMutableDictionary *)dicsResponse
{
    if(wrapper == httpnews && httpnews != nil)
    {
        NSLog(@"dicresponce %@",dicsResponse);
        NSMutableDictionary *dic=[[NSMutableDictionary alloc]init];
        dic=[dicsResponse valueForKey:@"data"];
        newslist = [dicsResponse valueForKey:@"data"];
        NSString * status = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"status"]];
        if ([status isEqualToString:@"1"]) {
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
           [self.tableview reloadData];
           
        }
        else{
            NSString * message = [NSString stringWithFormat:@"%@",[dicsResponse valueForKey:@"message"]];
      showAlert(AlertTitle, message);
            
        }
    }
 
    [APP_DELEGATE hideLoadingView];
    
}

- (void) HttpWrapper:(HttpWrapper *)wrapper fetchDataFail:(NSError *)error
{
    
    NSLog(@"Fetch Data Fail Error:%@",error);
    [APP_DELEGATE hideLoadingView];
}

@end
