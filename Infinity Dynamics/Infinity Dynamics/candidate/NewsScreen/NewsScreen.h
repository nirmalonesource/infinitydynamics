//
//  NewsScreen.h
//  Infinity Dynamics
//
//  Created by My Mac on 05/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HttpWrapper.h"
#import "NewsScreencell.h"
@interface NewsScreen : UIViewController<UITextFieldDelegate,HttpWrapperDelegate>{
    HttpWrapper *httpnews;
      NSMutableArray *  newslist;
    
       UIView *picker;   /////menu picker view
}
@property (strong, nonatomic) IBOutlet UIButton *btnmenu;
- (IBAction)btnBack:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableview;

@end
