//
//  AddDCEDetail.h
//  Infinity Dynamics
//
//  Created by My Mac on 21/02/18.
//  Copyright © 2018 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "HttpWrapper.h"
#import "PopOverView.h"
@interface AddDCEDetail : UIViewController<HttpWrapperDelegate>{
    HttpWrapper *httadddetail,*httpeditdetail,*httpcertificate,*httpCountry;
    //    NSMutableArray* detail;
    NSMutableArray *marreditdetail,*marrcountry;
    NSMutableArray *marradddetail;
    //////datepicker
    UIDatePicker *datePicker;
    UIView *subView;
    UIToolbar *toolbar;
    BOOL isPickerLaunced;
    BOOL isPickerLaunced1;
    NSString * endtime;
    UITextField *currentTextField;
    NSMutableArray *marrProficiency;
    NSString * CertificateID;
    IBOutlet UITextField *txtlevl;
    
    BOOL isProficiency;
    BOOL isLevel;
    
    BOOL isCountry;
    NSString * countryId;
}
- (IBAction)btnCountry:(id)sender;
- (IBAction)btnLevel:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtproficiency;
- (IBAction)btnProficiency:(id)sender;
@property (nonatomic, assign) BOOL Edit;
@property (nonatomic, strong) NSMutableArray *detail;
@property (strong, nonatomic) IBOutlet UITextField *txtnumber;
- (IBAction)btncalender:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtplaceofissue;
@property (strong, nonatomic) IBOutlet UIButton *btncalender;
@property (strong, nonatomic) IBOutlet UIButton *btnCalender;
@property (strong, nonatomic) IBOutlet UITextField *txtvalidity;
@property (strong, nonatomic) IBOutlet UIButton *btnSaveseamandetail;
- (IBAction)btnSaveSeamanDetail:(id)sender;
- (IBAction)btnBack:(id)sender;
-(void)Popvalueselected:(NSString*)strValue;
-(void)Popvalveselected:(NSString*)strValve;
-(void)dismissPopUpViewController;

@end
