//
//  PopOverView.m
//  f2f
//
//  Created by Parth Patoliya on 11/05/16.
//  Copyright (c) 2016 rishi. All rights reserved.
//

#import "PopOverView.h"

@interface PopOverView ()
{
    NSIndexPath* checkedIndexPath;
}
@property (nonatomic, retain) NSIndexPath* checkedIndexPath;
@property (nonatomic, retain) NSMutableArray* cellSelected;
@property (nonatomic, retain) NSMutableArray* CountryIdSelected;
@property (nonatomic, retain) NSMutableArray* CountryNameSelected;

@property (nonatomic, retain) NSString * ClientIdArrayString;
@property (nonatomic, retain) NSString * ClientNameArrayString;

@end

@implementation PopOverView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
    [CountryDataSave setValue:@"" forKey:@"CountryIDSelected"];
    [CountryDataSave setValue:@"" forKey:@"CountryNameSelected"];
    [CountryDataSave synchronize];
    
    
    self.cellSelected = [NSMutableArray array];
    self.CountryIdSelected = [NSMutableArray array];
    self.CountryNameSelected = [NSMutableArray array];
    
    
    self.ClientIdArrayString = [[NSString alloc] init];
    self.ClientIdArrayString = @"";
    
    self.ClientNameArrayString = [[NSString alloc] init];
    self.ClientNameArrayString = @"";
    
    
    _tblPopOver.delegate = self;
    _tblPopOver.dataSource = self;
    
  //  arrHostel = [[NSMutableArray alloc]initWithObjects:@"HOSTELS",@"MUMBAI - ANDHERI BOYS",@"MUMBAI-SANDHRUST ROAD BOYS",@"VADODARA BOYS",@"VALLABH VIDYANAGAR BOYS",@"VALLABH VIDYANAGAR GIRLS",@"AMEHADABAD BOYS",@"AMEHADABAD GIRLS",@"PUNE BOYS",@"PUNE GIRLS",@"BHAVNAGAR BOYS",@"UDAIPUR BOYS",nil];
    if ([_isfrom isEqualToString:@"AddVacancy"])
    {
         arrfromexperience = [[NSMutableArray alloc]initWithObjects:@"On Promotion",@"Less than 6 Months",@"6 Months",@"12 Months",@"18 Months",@"24 Months", nil];
    }
    else
    {
         arrfromexperience = [[NSMutableArray alloc]initWithObjects:@"On Promotion",@"Less than 6 Months",@"6 Months",@"12 Months",@"18 Months",@"24 Months",@"36 Months", nil];
    }
   
    arrtoexperience = [[NSMutableArray alloc]initWithObjects:@"6 Months",@"12 Months",@"18 Months",@"24 Months",@"24+ Months", nil];
     arrtodays = [[NSMutableArray alloc]initWithObjects:@"Available Immediately",@"15 Days-30 Days",@"30 Days-45 Days",@"45 Days-60 Days",@"All Available", nil];
     arraccount = [[NSMutableArray alloc]initWithObjects:@"Bank Account",@"Debit Card", nil];
    arrMonth = [[NSMutableArray alloc]initWithObjects:@"JANUARY",@"FEBRUARY",@"MARCH",@"APRIL",@"MAY",@"JUNE",@"JULY",@"AUGUST",@"SEPTEMBER",@"OCTOBER",@"NOVEMBER",@"DECEMBER", nil];
    
    //arrAvailable = [[NSMutableArray alloc]initWithObjects:@"Available with in",@"Urgent Joining Before",@"Not Available", nil];
    arrAvailable = [[NSMutableArray alloc]initWithObjects:@"Available with in",@"Urgent Joining Before", nil];
    
    //arrAvailablein = [[NSMutableArray alloc]initWithObjects:@"Available After",@"Urgent Joining Before",@"Not Available", nil];
    
    arrAvailablein = [[NSMutableArray alloc]initWithObjects:@"Available After",@"Urgent Joining Before", nil];
    
    arrflextype = [[NSMutableArray alloc]initWithObjects:@"Tourist Visa",@"Student Visa",@"Business Visa",@"Seaman Visa",@"USA B1/B2",@"USA C1/D",nil];
      arrdcepr = [[NSMutableArray alloc]initWithObjects:@"Oil",@"Chemical",@"Gas",nil];
    
     arreducationtype = [[NSMutableArray alloc]initWithObjects:@"Academic Education",@"Pre-Sea Training",nil];
    
 
      arrtype = [[NSMutableArray alloc]initWithObjects:@"Single", @"Married",@"Divorced",@"Widow",@"Widower",nil];
     arrLevel = [[NSMutableArray alloc]initWithObjects:@"Management",@"Operational",@"Support",nil];
    arrGender = [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",@"Decline to declare",@"Others",nil];
    arrFindjobs = [[NSMutableArray alloc]initWithObjects:@"Jobs by Company",@"Jobs as per Rank",@"Jobs by Shiptype",@"Featured Jobs",@"Latest Jobs",nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    switch (self.index) {
        case 0:
      arrCurrent = _marrData;
            //arrCurrent = arrtype;
            ischecked = true;
            break;
        case 1:
            arrCurrent = _marrnationality;
            isnationality = true;
            break;
        case 2:
            arrCurrent = arrtype;
            break;
        case 3:
            arrCurrent = arrflextype;
            break;
        case 4:
            arrCurrent = arrLevel;
            break;
        case 5:
            arrCurrent = _marrproficiency;
            isnproficiency = true;
            break;
        case 6:
            arrCurrent = arrdcepr;
            break;
        case 7:
            arrCurrent = _marrshiptype;
            isshiptype = YES;
            break;
        case 8:
            arrCurrent = _marrenginetype;
             isEngineType = YES;
            break;
        case 9:
            arrCurrent = arrdcepr;
            break;
        case 10:
            arrCurrent = arreducationtype;
            break;
        case 11:
            arrCurrent = _stwccertificate;
            isstwcertificate = YES;
            break;
        case 12:
            arrCurrent = arrAvailable;
          
            break;
        case 13:
            arrCurrent = arrfromexperience;
            
            break;
        case 14:
            arrCurrent = arrtoexperience;
            
            break;
        case 15:
            arrCurrent = arrtodays;
            
            break;
        case 16:
            arrCurrent = arrGender;
            
            break;
        case 17:
            arrCurrent = arrAvailablein;
            
            break;
        case 18:
            arrCurrent = _marrPlan;
            isplan = YES;
            break;
        case 19:
            arrCurrent = _marrcountry;
            iscountry = YES;
            break;
        case 20:
            arrCurrent = _marrnationality;
            iscountryofres = YES;
            break;
        case 21:
            arrCurrent = arrFindjobs;
            break;
    }

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrCurrent.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableView.separatorColor = [UIColor clearColor];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.tag = indexPath.row;
    cell.textLabel.font = [UIFont fontWithName: cell.textLabel.font.fontName size:15];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    if (ischecked == true) {
       // cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        
        ischecked = true;
               
        NSLog(@"CountryName = %@",cell.textLabel.text);
        
        if ([_isfrom isEqualToString:@"shipjob"])
        {
            cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"Name"];
        }
        else if ([_isfrom isEqualToString:@"shorejob"])
        {
            cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"DesignationName"];
            
        }
        else
        {
            cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"Name"];
        }
       
    }
    else if (isnproficiency == true){
        iscountryofres = false;

        ischecked = false;
        isnationality = false;
        isEngineType = false;
        isshiptype = false;
         isstwcertificate=false;
        iscountry = false;
        isplan = false;
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CertificateType"];
    }
    else if (isplan == true){
        iscountryofres = false;

        ischecked = false;
        isnationality = false;
        isEngineType = false;
        isshiptype = false;
        isstwcertificate=false;
        iscountry = false;
       cell.textLabel.font=[UIFont systemFontOfSize:10.0f];
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"PlanTitle"];
    }
    else if (isnationality == true){
        iscountryofres = false;

        ischecked = false;
        iscountry = false;
        isnproficiency = false;
        isEngineType = false;
        isshiptype = false;
         isstwcertificate=false;
        isplan = false;
         cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"NationalName"];
    }
    else if (iscountryofres == YES){
        ischecked = false;
        iscountry = false;
        isnproficiency = false;
        isEngineType = false;
        isshiptype = false;
        isstwcertificate=false;
        isplan = false;
        isnationality = false;
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryName"];
    }
    else if (isnationality == true){
        iscountryofres = false;

        ischecked = false;
        iscountry = false;
        isnproficiency = false;
        isEngineType = false;
        isshiptype = false;
        isstwcertificate=false;
        isplan = false;
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"NationalName"];
    }
    else if (isshiptype == true){
        iscountryofres = false;

        iscountry = false;
        ischecked = false;
        isnproficiency = false;
        isEngineType = false;
        isstwcertificate=false;
        isplan = false;
        if ([_isfrom isEqualToString:@"shipapp"])
        {
            cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"ShipType"];
        }
        else if ([_isfrom isEqualToString:@"shoreapp"])
        {
            cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"DesignationName"];

        }
        else
        {
            cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"ShipType"];

        }
    }
    else if (isEngineType == true){
        iscountryofres = false;

        iscountry = false;
        ischecked = false;
        isnproficiency = false;
        isstwcertificate=false;
        isshiptype = false;
        isplan = false;
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"EngineType"];
    }
    else if (isstwcertificate == true){
        iscountryofres = false;

        iscountry = false;
        ischecked = false;
        isnproficiency = false;
        isEngineType = false;
        isshiptype = false;
        isplan = false;
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"STCWCertificateType"];
    }
    else if (iscountry == true){
        iscountryofres = false;

        isstwcertificate = false;
        ischecked = false;
        isnproficiency = false;
        isEngineType = false;
        isshiptype = false;
        isplan = false;
        
//        if([self.checkedIndexPath isEqual:indexPath])
//        {
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }
//        else
//        {
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        
        if ([self.cellSelected containsObject:indexPath])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        
        cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"CountryName"];
     }
    else
    {
            cell.textLabel.text = [arrCurrent objectAtIndex:indexPath.row];
        
    }
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    /*
    if([self.delegates respondsToSelector:@selector(Popvalueselected:)])
    {
        [self.delegates dismissPopUpViewController];
        
        if (ischecked == true) {
   
           [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"Name"]];
        }
        else if (isnationality == true){
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"NationalName"]];
        }
        else if (isnproficiency == true){
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"CertificateType"]];
        }
        else if (isshiptype == true){
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"ShipType"]];
        }
        else if (isEngineType == true){
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"EngineType"]];
        }
        else if (isstwcertificate == true){
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"STCWCertificateType"]];
        }
        else if (isplan == true){
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"PlanTitle"]];
        }
        else if (iscountry == true)
        {
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"CountryName"]];
        }
        else
        {
        
            [self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
        }
    }
    */
 
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if([self.delegates respondsToSelector:@selector(Popvalueselected:)])
    {
        //[self.delegates dismissPopUpViewController];
        
        if (ischecked == true) {
            if ([_isfrom isEqualToString:@"shipjob"])
            {
                [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"Name"]];
            }
            else if ([_isfrom isEqualToString:@"shorejob"])
            {
                 [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"DesignationName"]];
               // cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"DesignationName"];
            }
            else
            {
                 [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"Name"]];
            }
            [self.delegates dismissPopUpViewController];
           
        }
        else if (isnationality == true){
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"NationalName"]];
        }
        else if (iscountryofres == true){
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"CountryName"]];
        }
        else if (isnproficiency == true){
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"CertificateType"]];
        }
        else if (isshiptype == true){
           
           
            if ([_isfrom isEqualToString:@"shipapp"])
            {
               [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"ShipType"]];
            }
            else if ([_isfrom isEqualToString:@"shoreapp"])
            {
               // cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"DesignationName"];
                 [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"DesignationName"]];
                
            }
            else
            {
                [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"ShipType"]];
                
            }
             [self.delegates dismissPopUpViewController];
        }
        else if (isEngineType == true){
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"EngineType"]];
        }
        else if (isstwcertificate == true){
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"STCWCertificateType"]];
        }
        else if (isplan == true){
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"PlanTitle"]];
        }
        else if (iscountry == true)
        {
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[[arrCurrent objectAtIndex:indexPath.row]valueForKey:@"CountryName"]];

            //cell.textLabel.text = [[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryName"];
            NSLog(@"self.cellSelected = %@",self.cellSelected);
            
//            if ([self.cellSelected containsObject:indexPath])
//            {
//                UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
//                uncheckCell.accessoryType = UITableViewCellAccessoryNone;
//
//                [self.cellSelected removeObject:indexPath];
//
//                NSString * CountryId = [NSString stringWithFormat:@"%@",[[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryID"]];
//                [self.CountryIdSelected removeObject:CountryId];
//
//                 NSString * CountryName = [NSString stringWithFormat:@"%@",[[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryName"]];
//                 [self.CountryNameSelected removeObject:CountryName];
//
//                //[tableView reloadData];
//            }
//            else
//            {
//                UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
//                cell.accessoryType = UITableViewCellAccessoryCheckmark;
//
//                [self.cellSelected addObject:indexPath];
//
//                NSString * CountryId = [NSString stringWithFormat:@"%@",[[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryID"]];
//                [self.CountryIdSelected addObject:CountryId];
//
//                NSString * CountryName = [NSString stringWithFormat:@"%@",[[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryName"]];
//                [self.CountryNameSelected addObject:CountryName];
//
//            }
//            NSLog(@"self.CountryIdSelected = %@",self.CountryIdSelected);
//            NSLog(@"self.CountryNameSelected = %@",self.CountryNameSelected);
//
//
//            NSLog(@"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//            self.ClientIdArrayString = [_CountryIdSelected componentsJoinedByString:@","];
//            NSLog(@"ClientIdArrayString = %@",self.ClientIdArrayString);
//            NSLog(@"@******************************");
//            self.ClientNameArrayString= [_CountryNameSelected componentsJoinedByString:@","];
//            NSLog(@"_CountryNameSelected = %@",self.ClientNameArrayString);
//            NSLog(@"@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
//
//            //[[arrCurrent objectAtIndex:indexPath.row] valueForKey:@"CountryName"];
//            //ClientNameArrayString
//            [tableView reloadData];
//
//            //[self.delegates Popvalueselected:[[arrCurrent  objectAtIndex:indexPath.row]valueForKey:@"CountryName"]];
//
//            if ([_CountryIdSelected count] > 0 && [_CountryNameSelected count] > 0)
//            {
//                NSLog(@"YES YES YES YES YES YES YES YES YES YES ");
//                NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
//                [CountryDataSave setValue:self.ClientIdArrayString forKey:@"CountryIDSelected"];
//                [CountryDataSave setValue:self.ClientNameArrayString forKey:@"CountryNameSelected"];
//                [CountryDataSave synchronize];
//            }
//            else
//            {
//                NSLog(@"NO NO NO NO NO NO NO NO NO NO NO NO NO ");
//                NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
//                [CountryDataSave setValue:@"" forKey:@"CountryIDSelected"];
//                [CountryDataSave setValue:@"" forKey:@"CountryNameSelected"];
//                [CountryDataSave synchronize];
//            }
//
//            if(self.ClientIdArrayString.length > 0 && self.ClientNameArrayString.length > 0)
//            {
//                [self.delegates Popvalueselected:self.ClientIdArrayString];
//                [self.delegates Popvalueselected:self.ClientNameArrayString];
//            }
//            else
//            {
//                [self.delegates dismissPopUpViewController];
//
//                NSUserDefaults * CountryDataSave = [NSUserDefaults standardUserDefaults];
//                [CountryDataSave setValue:@"" forKey:@"CountryIDSelected"];
//                [CountryDataSave setValue:@"" forKey:@"CountryNameSelected"];
//                [CountryDataSave synchronize];
//            }
            
            /*
             // Uncheck the previous checked row
             if(self.checkedIndexPath)
             {
             UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:self.checkedIndexPath];
             uncheckCell.accessoryType = UITableViewCellAccessoryNone;
             }
             if([self.checkedIndexPath isEqual:indexPath])
             {
                 self.checkedIndexPath = nil;
             }
             else
             {
                 UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
                 cell.accessoryType = UITableViewCellAccessoryCheckmark;
                 self.checkedIndexPath = indexPath;
                 
                 NSLog(@"self.checkedIndexPath = %ld",(long)self.checkedIndexPath.row);
             
             }
             */
        }
        else
        {
            [self.delegates dismissPopUpViewController];
            [self.delegates Popvalueselected:[arrCurrent objectAtIndex:indexPath.row]];
        }
    }
    
}

@end
