//
//  HttpWrapper.m
//  Towing Service
//
//  Created by RAHUL MEHTA on 03/03/17.
//  Copyright © 2017 RAHUL MEHTA. All rights reserved.
//

#import "HttpWrapper.h"
#import "AppDelegate.h"

@implementation HttpWrapper

AppDelegate *appDelegate;

@synthesize requestMain;
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        });
        
    }
    return self;
}

-(void) requestWithMethod:(NSString*)method url:(NSString*)strUrl param:(NSString*)dictParam
{
    NSLog(@"dict is %@",dictParam);
    if(requestMain)
    {
        requestMain = nil;
    }
  
    operation = [AFHTTPSessionManager manager];
       operation.requestSerializer = [AFHTTPRequestSerializer serializer];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    operation.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    operation.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    if (_getbool == YES)
    {
        [operation GET:strUrl parameters:dictParam success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (delegate != nil)
            {
                NSLog(@"dictparam %@",dictParam);
                
                if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                {
                    [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                }
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                {
                    [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (delegate == nil)
                return;
            
            if([delegate respondsToSelector:@selector(fetchDataFail:)])
                [delegate performSelector:@selector(fetchDataFail:) withObject:error];
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                [delegate HttpWrapper:self fetchDataFail:error];
        }];
    }
    else
    {
        [operation POST:strUrl parameters:dictParam success:^(NSURLSessionDataTask *  task, id   responseObject) {
            if (delegate != nil)
            {
                NSLog(@"dictparam %@",dictParam);
                
                if([delegate respondsToSelector:@selector(fetchDataSuccess:)])
                {
                    [delegate performSelector:@selector(fetchDataSuccess:) withObject:responseObject];
                }
                
                if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataSuccess:)])
                {
                    [delegate HttpWrapper:self fetchDataSuccess:responseObject];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            if (delegate == nil)
                return;
            
            if([delegate respondsToSelector:@selector(fetchDataFail:)])
                [delegate performSelector:@selector(fetchDataFail:) withObject:error];
            
            if([delegate respondsToSelector:@selector(HttpWrapper:fetchDataFail:)])
                [delegate HttpWrapper:self fetchDataFail:error];
        }];
    }
}

-(void) cancelRequest
{
//    [operation.operationQueue cancelAllOperations];
//    requestMain.delegate = nil;
//    [requestMain cancel];
}
@end
