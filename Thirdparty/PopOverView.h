//
//  PopOverView.h
//  f2f
//
//  Created by Parth Patoliya on 11/05/16.
//  Copyright (c) 2016 rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignupCandidatesecondpage.h"

@class SignupCandidatesecondpage;


@interface PopOverView : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray * arrAvailable;
    NSMutableArray * arrAvailablein;
    NSMutableArray* arrHostel;
    NSMutableArray* arrfromexperience;
      NSMutableArray* arrtoexperience;
     NSMutableArray* arrtodays;
    NSMutableArray* arraccount;
    NSMutableArray *arrMonth;
    NSMutableArray *arrCurrent;
    NSMutableArray *arrflextype;
      NSMutableArray *arrdcepr;
      NSMutableArray *arreducationtype;
     NSMutableArray *arrGender;
     NSMutableArray *arrtype;
     NSMutableArray *arrLevel;
    NSMutableArray *arrFindjobs;

    BOOL ischecked;
      BOOL isnationality;
    BOOL iscountryofres;

    BOOL isnproficiency;
    BOOL  isEngineType;
    BOOL  isshiptype;
      BOOL isstwcertificate;
    BOOL isplan;
     BOOL iscountry;
}
@property(nonatomic,assign) NSMutableArray *stwccertificate;
@property(nonatomic,assign) NSMutableArray *marrcountry;
@property(nonatomic,assign) NSMutableArray *marrproficiency;
@property(nonatomic,assign) NSMutableArray *marrData;
@property(nonatomic,assign) NSMutableArray *marrPlan;
@property(nonatomic,assign) NSMutableArray *marrnationality;
@property(nonatomic,assign) NSMutableArray *marrshiptype;
@property(nonatomic,assign) NSMutableArray *marrenginetype;
@property(nonatomic,assign) NSMutableArray *marrBranch;
@property (weak, nonatomic) IBOutlet UITableView *tblPopOver;
@property(nonatomic,assign)SignupCandidatesecondpage *delegates;

@property (nonatomic) NSInteger index;
@property (strong, nonatomic) NSString *isfrom;

-(void)Popvalueselected:(NSString*)strValue;
-(void)dismissPopUpViewController;

@end
