//
//  ShowAlert.h
//  DIGGEST
//
//  Created by cdp on 10/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowAlert : NSObject

-(void)showMyAlert:(NSString *)title :(NSString *)message;

@end
