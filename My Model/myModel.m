//
//  MyModel.m
//  DAYT
//
//  Created by Amit Ajmera on 10/16/15.
//  Copyright © 2015 The FreeBird. All rights reserved.
//

#import "myModel.h"
#import "Constants.h"
#import <Foundation/Foundation.h>
#include <ifaddrs.h>
#include <arpa/inet.h>

@interface myModel ()

@end

@implementation myModel

Helper* helper;

+ (void) helperAllocInit
{
    helper = [[Helper alloc]init];
    
}

#pragma mark EditProfile
+(BOOL) editprofile:(NSString *)strURL{
    [APP_DELEGATE hideLoadingView];
    NSString *myRequestString = @"";
    
    NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
   
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:strURL]];
    
    NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    NSLog(@"ReturnData:%@",returnData);
    
    NSError* error;
    
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableArray* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                               options:kNilOptions
                                                                 error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"error"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            NSLog(@"SignIn Succeed");
           
            NSUserDefaults *userLoginCredentials = [NSUserDefaults standardUserDefaults];
            
          //  [userLoginCredentials setObject:[[json valueForKey:@"data"] valueForKey:@"email"] forKey:@"Email"];
            //[userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"lastname"] forKey:@"LASTNAME"];
            [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"state"] forKey:@"STATE"];
            [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"city"] forKey:@"CITY"];
            [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"zip"] forKey:@"ZIPCODE"];
            [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"phone"] forKey:@"PHONENUMBER"];
            //[userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"uid"] forKey:@"Id"];
            [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"name"] forKey:@"Name"];
            [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"addr"] forKey:@"ADDRESS"];
            
            
            return @"SUCCESS";
        }else{
            [APP_DELEGATE hideLoadingView];
            NSLog(@"SignIn Failed");
            showAlert(AlertTitle, @"SignIn Failed");
            return @"Failed";
        }
        
    }else {
        [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        
        return @"Check internet Connection";
    }
    


}
#pragma mark SignUP

+(NSString*)userSignUp:(NSString *)strURL{
    
     [APP_DELEGATE showLoadingView:@""];
    //NSString *myRequestString = [NSString stringWithFormat:@"v=login&user=%@&pass=%@",strUser,strPassword];
    NSString *myRequestString = @"";
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    //NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"http://hollybollygossip.com/dayt/login.php"]]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:strURL]];
    
    NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    NSLog(@"ReturnData:%@",returnData);
    
    NSError* error;
    
    if(returnData != nil)
    {
        NSMutableArray* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                               options:kNilOptions
                                                                 error:&error];
        NSLog(@"Json :%@",json);
        
       
         NSString *strval = [NSString stringWithFormat:@"%@",[json valueForKey:@"message"] ];
        NSLog(@"val:%@",strval);
        if ( ![strval isEqualToString:@"Email is already used by another user. Please choose another email."] )
        {
            
                
           
                [APP_DELEGATE hideLoadingView];
                NSLog(@"SignIn Succeed");
           //     showAlert(AlertTitle, @"Register Success");
                NSUserDefaults *userLoginCredentials = [NSUserDefaults standardUserDefaults];
                
                [userLoginCredentials setObject:[[json valueForKey:@"data"] valueForKey:@"email"] forKey:@"Email"];
                [userLoginCredentials setObject:[[json valueForKey:@"data"] valueForKey:@"password"] forKey:@"password"];
                [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"id"] forKey:@"Id"];
                [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"name"] forKey:@"Name"];
                [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"payment_id"] forKey:@"payment_id"];
                
                return json;

            
            }
                
        else{
            [APP_DELEGATE hideLoadingView];
            NSLog(@"SignIn Failed");
         //   showAlert(AlertTitle, @"SignIn Failed");
            return @"Failed";
            }
        
    }
    else {
        [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        
        return @"Check internet Connection";
    }
    
}

+(void)saveUserDetails
{
    NSUserDefaults* autoLogin = [[NSUserDefaults alloc]init];
    [helper.mdictUserData setValue:[autoLogin valueForKey:@"Id"] forKey:@"Id"];
    [helper.mdictUserData setValue:[autoLogin valueForKey:@"Name"] forKey:@"Name"];
    [helper.mdictUserData setValue:[autoLogin valueForKey:@"password"] forKey:@"password"];
    [helper.mdictUserData setValue:[autoLogin valueForKey:@"email"] forKey:@"Email"];
    [helper.mdictUserData setValue:[autoLogin valueForKey:@"imagefile"] forKey:@"imagefile"];
    [helper.mdictUserData setValue:[autoLogin valueForKey:@"imagefile1"] forKey:@"imagefile1"];
    
}

#pragma mark SignIn

+(NSString*)userSignIn:(NSString *)strUser Password:(NSString *)strPassword
{
    [APP_DELEGATE showLoadingView:@""];

       NSString *myRequestString = @"";
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
   
     NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@login?&email=%@&password=%@",MainUrl,strUser,strPassword]]];
    
    NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"GET"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    NSLog(@"ReturnData:%@",returnData);
    
    NSError* error;
    
    NSString *charSendString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"charlieSendString:%@",charSendString);
    
    if(returnData != nil )
    {
        NSMutableArray* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                               options:kNilOptions
                                                                 error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"status"]];
        NSString *strdata = [NSString stringWithFormat:@"%@",[json valueForKey:@"data"]];
        if (![strdata isEqualToString:@"<null>"]) {
        
            if ([strSuccess isEqualToString:@"1"]) {
            
                
                NSLog(@"SignIn Succeed");
                [APP_DELEGATE hideLoadingView];
                NSUserDefaults *userLoginCredentials = [NSUserDefaults standardUserDefaults];
                [userLoginCredentials setObject:[[json valueForKey:@"data"] valueForKey:@"data"] forKey:@"data"];
                [userLoginCredentials setObject:[[json valueForKey:@"data"] valueForKey:@"EmailID"] forKey:@"EmailID"];
                [userLoginCredentials setObject:strUser forKey:@"User"];
                [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"ClientID"] forKey:@"ClientID"];
                [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"ContactNo"] forKey:@"ContactNo"];
                [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"FirstName"] forKey:@"FirstName"];
                 [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"LastName"] forKey:@"LastName"];
                 [userLoginCredentials setValue:[[json valueForKey:@"data"] valueForKey:@"DeviceToken"] forKey:@"DeviceToken"];
                [userLoginCredentials setValue:strPassword forKey:@"strPassword"];
                [userLoginCredentials setValue:@"1" forKey:@"Session"];
                [userLoginCredentials synchronize];
                
                return json;

            }
               
            return json;
            
        }
        else
        {
            NSLog(@"SignIn Failed");
             [APP_DELEGATE hideLoadingView];
           // showAlert(AlertTitle, @"SignIn Failed");
            return @"Failed";
        }
        
    }else {
         [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        
        return @"Check internet Connection";
    }
}

+ (NSDictionary*) userProfileData {
    
    return helper.mdictUserData;
}

+(NSMutableDictionary*)getDictResponse:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"GET"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
   // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);
    
    NSError* error;
    NSMutableDictionary* marr = [[NSMutableDictionary alloc]init];
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                               options:kNilOptions
                                                                 error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"success"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            

            
            return json;
        }else{
            [APP_DELEGATE hideLoadingView];
            return marr;
        }
        
    }else {
        [APP_DELEGATE hideLoadingView];
        return marr;
    }
}
+(NSMutableDictionary*)getDictResponse1:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [strURL UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);
    
    NSError* error;
   
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"success"]];
               
        
        if ([strSuccess isEqualToString:@"1"]) {
              [APP_DELEGATE hideLoadingView];
            
            return json;
        }else{
            [APP_DELEGATE hideLoadingView];
            return [json valueForKey:@"message"];
        }
        
    }else {[APP_DELEGATE hideLoadingView];
        
        return @"FAILED";
    }
}
+(NSMutableDictionary*)getDictResponse2:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [strURL UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"GET"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);
    
    NSError* error;
    
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"success"]];
        
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            
            return json;
        }else{
            [APP_DELEGATE hideLoadingView];
            return json ;
        }
        
    }else {[APP_DELEGATE hideLoadingView];
        return @"FAILED";
    }
}
+(NSMutableDictionary*)profile:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",strURL);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [strURL UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);
    
    NSError* error;
    
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"success"]];
        
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            
            return json;
        }else{
            [APP_DELEGATE hideLoadingView];
            return json ;
        }
        
    }else {[APP_DELEGATE hideLoadingView];
        return @"FAILED";
    }
}


+(NSString*)encodeURL:(NSString*)strURL
{
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)strURL,
                                                                                 NULL,
                                                                                 (CFStringRef)@"~",
                                                                                 kCFStringEncodingUTF8 ));
    
    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                 NULL,
                                                                                 (CFStringRef)strURL,
                                                                                 NULL,
                                                                                 (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                 kCFStringEncodingUTF8 ));
}
+(BOOL)requestUrl:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);

    NSError* error;
    
    if(returnData != nil)
    {[APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"error"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            return YES;
        }else{
            [APP_DELEGATE hideLoadingView];
            showAlert(AlertTitle, [json valueForKey:@"message"]);
            return NO;
        }
        
    }else {
        [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        return NO;
    }
}

+(BOOL)requestUrl1:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", returnString);
    
    NSString *charSendString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"charlieSendString:%@",charSendString);
    
    NSError* error;
    
    if(returnData != nil)
    {[APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"error"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            
            return YES; 
        }else{
            [APP_DELEGATE hideLoadingView];
            showAlert(AlertTitle, [json valueForKey:@"message"]);
            return NO;
        }
        
    }else {
        [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        return NO;
    }
}

+(BOOL)requestUrl2:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@?%@",strURL,myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"returnString:%@", returnString);
    
    NSError* error;
    
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"error"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            
            return YES;
        }else{
            [APP_DELEGATE hideLoadingView];
            return NO;
        }
    }else {
        [APP_DELEGATE hideLoadingView];
        return NO;
    }
}

+(BOOL)addInvitationUrl:(NSString *)strURL RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
    NSString *myRequestString = strRequestData;
    
    NSLog(@"myRequestString:%@",myRequestString);
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]]];
    //NSLog(@"myRequestString:%@",request);
    
    [request setTimeoutInterval:60];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
    // NSLog(@"ReturnData:%@",returnData);
    
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", returnString);
    
    NSString *charSendString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"charlieSendString:%@",charSendString);
    
    NSError* error;
    
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"error"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            
            helper.marrNewlyAddedInvitation = [json valueForKey:@"invitation_list"];
            return YES;
        }else{
            [APP_DELEGATE hideLoadingView];
            showAlert(AlertTitle, [json valueForKey:@"message"]);
            return NO;
        }
        
    }else {
        [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        return NO;
    }
}

+(BOOL)requestURL:(NSString*)strURL withRequestParams:(NSMutableDictionary*)params ImageNSData:(NSData*)imgNSData
{
    [APP_DELEGATE showLoadingView:@""];
    NSURL *url = [[NSURL alloc] initWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in params) {
        NSLog(@"Param:%@",param);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // Add __VIEWSTATE
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"__VIEWSTATE\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"/wEPDwUKLTQwMjY2MDA0M2RkXtxyHItfb0ALigfUBOEHb/mYssynfUoTDJNZt/K8pDs=" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // add image data
    NSData *imageData = imgNSData;
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Disposition: form-data; name=\"friend_image\"; filename=\"image.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSLog(@"Check response if image was uploaded after this log");
    //return and test

    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
     
    NSLog(@"returnString:%@", returnString);
    
    // Log Response
    NSError* error;
    
    if(returnData != nil)
    {
        [APP_DELEGATE hideLoadingView];
        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                    options:kNilOptions
                                                                      error:&error];
        NSLog(@"Json :%@",json);
        
        NSString *strSuccess = [NSString stringWithFormat:@"%@",[json valueForKey:@"success"]];
        
        if ([strSuccess isEqualToString:@"1"]) {
            [APP_DELEGATE hideLoadingView];
            
            return true;
        }else{
            [APP_DELEGATE hideLoadingView];
            showAlert(AlertTitle, [json valueForKey:@"message"]);
            return false;
        }
        
    }else {
        [APP_DELEGATE hideLoadingView];
        [self checkIfInternetConection];
        return false;
        
    }
}

#pragma mark Internet Connection

+(BOOL)checkIfInternetConection
{
   
    //1
    /*Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS No internet connection");
        showAlert(AlertTitle, AlertInternet);
        return NO;
        
    } else
    {
        NSLog(@"There IS internet connection");
      
        showAlert(AlertTitle, AlertServerError);
        return YES;
    }
    */
    //2
    Reachability* wifiReach = [Reachability reachabilityWithHostName: @"www.google.com"];
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            NSLog(@"Access Not Available");
            showAlert(AlertTitle, AlertInternet);
            return NO;
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"Reachable WWAN");
            [self checkServerReachability];

            return YES;
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"Reachable WiFi");
            [self checkServerReachability];

            return YES;
            break;
        }
    }
}



+(BOOL)checkIfInternetConection1
{
  
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS No internet connection");
        showAlert(AlertTitle, AlertInternet);
        return NO;
        
    }else {
        return YES;
    }
    
}
//+(NSString*)SignIn:(NSString *)strUser Password:(NSString *)strPassword RequestData:(NSString *)strRequestData
//{
//    [APP_DELEGATE showLoadingView:@""];
//  
//  NSMutableDictionary* json = [[NSMutableDictionary alloc] init];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    
//    //Set Params
//    [request setHTTPShouldHandleCookies:NO];
//    [request setTimeoutInterval:60];
//    [request setHTTPMethod:@"POST"];
//    
//    //Create boundary, it can be anything
//    NSString *boundary = @"------VohpleBoundary4QuqLuM1cE5lMwCy";
//    
//    // set Content-Type in HTTP header
//    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
//    
//    // post body
//    NSMutableData *body = [NSMutableData data];
//    
//    //Populate a dictionary with all the regular values you would like to send.
//    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//
//    //////////////
//    
//    [parameters setValue:strUser forKey:@"email"];
//    
//    [parameters setValue:strPassword forKey:@"password"];
//
//    // add params (all params are strings)
//    for (NSString *param in parameters) {
//        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
//        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [parameters objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
//    }
//    
//    //Close off the request with the boundary
//    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    // setting the body of the post to the request
//    [request setHTTPBody:body];
//    NSString * str;
//    str = [NSString stringWithFormat:@"%@login?",MainUrl];  //,userid];
//    // set URL
//    [request setURL:[NSURL URLWithString:str]];
//     NSOperationQueue *queue = [[NSOperationQueue alloc] init];
////    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
//// 
////    {
////                                 NSMutableDictionary* json = [[NSMutableDictionary alloc] init];
////                               NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
////                               
////                               if ([httpResponse statusCode] == 200) {
////                                   
////                                   NSLog(@"success");
////                                   
////                                    json = [NSJSONSerialization JSONObjectWithData:data
////                                                                                               options:kNilOptions
////                                                                                                 error:&error];
////                                   [APP_DELEGATE hideLoadingView];
////                                   
////                                   NSLog(@"%@",json);
////                                   NSLog(@"%@",[json valueForKey:@"data"]);
////                                   NSString * success = [NSString stringWithFormat:@"%@",[json valueForKey:@"status"]];
////                                   if ([success isEqualToString:@"1"]) {
////                                       NSString * message = [NSString stringWithFormat:@"%@",[json valueForKey:@"message"]];
////                                       showAlert(AlertTitle,message);
////                                       
////                                       return json;
////                                       
////                                   } else{
////                                       // newsarray1 = nil;
////                                       NSString * message = [NSString stringWithFormat:@"%@",[json valueForKey:@"message"]];
////                                       showAlert(AlertTitle,message);
////                                       return json;
////                                   }
////                                  
////                                   [APP_DELEGATE hideLoadingView];
////                               }
////                               [APP_DELEGATE hideLoadingView];
////                                return json;
////                           }];
//   
//}
+(NSString*)userSignIn:(NSString *)strURL  email:(NSString *)email password:(NSString *)password RequestData:(NSString *)strRequestData
{
    [APP_DELEGATE showLoadingView:@""];
   // NSDictionary *headers = @{ @"cache-control": @"no-cache",
                      //         @"postman-token": @"a1cc34fb-683d-8261-32a7-bbe469a626ef" };
    
    NSDictionary *headers = @{ @"content-type": @"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                               @"sid": @"103",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"96b9c8f0-01f5-664a-9063-1ae5cf0fba0d" };
    // NSData *postData = [[NSData alloc] initWithData:[newOutput dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSData *myRequestData = [NSData dataWithBytes: [strRequestData UTF8String] length: [strRequestData length]];
    NSArray *parameters = @[ @{ @"name": @"email", @"value": email
                                },
                             @{ @"name": @"password", @"value": password } ];
    NSString *boundary = @"----WebKitFormBoundary7MA4YWxkTrZu0gW";
    
    NSError *error;
    NSMutableString *body = [NSMutableString string];
    for (NSDictionary *param in parameters) {
        [body appendFormat:@"--%@\r\n", boundary];
        if (param[@"fileName"]) {
            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"; filename=\"%@\"\r\n", param[@"name"], param[@"fileName"]];
            [body appendFormat:@"Content-Type: %@\r\n\r\n", param[@"contentType"]];
            [body appendFormat:@"%@", [NSString stringWithContentsOfFile:param[@"fileName"] encoding:NSUTF8StringEncoding error:&error]];
            if (error) {
                NSLog(@"%@", error);
            }
        } else {
            [body appendFormat:@"Content-Disposition:form-data; name=\"%@\"\r\n\r\n", param[@"name"]];
            [body appendFormat:@"%@", param[@"value"]];
        }
    }
    [body appendFormat:@"\r\n--%@--\r\n", boundary];
    NSData *postData = [body dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://clientsdemoarea.com/projects/mariexeltime/web_services/version_2/web_services/login"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                       
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSMutableDictionary* json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                    options:kNilOptions
                                                                                                                      error:&error];
                                                        NSLog(@"Json :%@",json);
                                                        
                                                    }
                                                    
                                                }];
    [dataTask resume];
    return @"SUCCESS";
    
//    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:nil error: nil];
//    NSLog(@"ReturnData:%@",returnData);
//
// //   NSError* error;
//
//    NSString *charSendString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
//    NSLog(@"charlieSendString:%@",charSendString);
//
//    if(returnData != nil )
//    {
//        NSMutableArray* json = [NSJSONSerialization JSONObjectWithData:returnData
//                                                               options:kNilOptions
//                                                                 error:&error];
//        NSLog(@"Json :%@",json);
//
//        NSString * sucess = [json valueForKey:@"status"];
//
//        if (![sucess  isEqualToString:@"0"]) {
//            NSUserDefaults *userLoginCredentials = [NSUserDefaults standardUserDefaults];
//
//
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"EmailID"] forKey:@"EmailID"];
//            NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"EmailID"]);
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"AppUserID"] forKey:@"AppUserID"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"UserCode"] forKey:@"UserCode"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"UserName"] forKey:@"UserName"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"branchname"] forKey:@"branchname"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"rolename"] forKey:@"rolename"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"BranchID"] forKey:@"BranchID"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"contactNo"] forKey:@"contactNo"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"RoleID"] forKey:@"RoleID"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"rolecode"] forKey:@"rolecode"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"UPassword"] forKey:@"UPassword"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"PhotoURL"] forKey:@"PhotoURL"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"BAddress"] forKey:@"BAddress"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"BContactNo"] forKey:@"BContactNo"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"BEmail"] forKey:@"BEmail"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"BLocation"] forKey:@"BLocation"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"DOB"] forKey:@"DOB"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"address"] forKey:@"address"];
//            [userLoginCredentials setObject:[[json valueForKey:@"records"] valueForKey:@"userregistrationid"] forKey:@"userregistrationid"];
//
//            [userLoginCredentials setValue:@"1" forKey:@"Sessions"];
//            [userLoginCredentials synchronize];
//
//
//
//            NSLog(@"SignIn Succeed");
//            [APP_DELEGATE hideLoadingView];
//
//
//            return @"SUCCESS";
//
//        }
//        else
//        {
//            NSLog(@"SignIn Failed");
//            [APP_DELEGATE hideLoadingView];
//            // showAlert(AlertTitle, @"SignIn Failed");
//            return @"Failed";
//        }
//
//    }else {
//        [APP_DELEGATE hideLoadingView];
//        [self checkIfInternetConection];
//
//        return @"Check internet Connection";
//    }
}

+ (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}
@end
