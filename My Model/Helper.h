//
//  Helper.h
//  DAYT
//
//  Created by Amit Ajmera on 10/17/15.
//  Copyright © 2015 The FreeBird. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Helper : NSObject

@property (nonatomic,retain) NSMutableDictionary *mdictUserData,*mdictTempData,*mdictPending,*mdictSelectedFriends;
@property (nonatomic,retain) NSString *strToken,*strProfileImgURL,*strisFromBackground;

@property (nonatomic,retain) NSMutableArray *marrScheluedIds,*marrtempScheluedIds,*marrContactsDayUser,*marrContactsNotDayUser,*marrToday,*marrTmrw,*marrSchedule,*marrNewInvitation,*marrCancelDecilined,*marrNewlyAddedInvitation,*marrFriends;

@property (nonatomic,assign) int scrollPadding;


@end
