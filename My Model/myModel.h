//
//  MyModel.h
//  DAYT
//
//  Created by Amit Ajmera on 10/16/15.
//  Copyright © 2015 The FreeBird. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "Helper.h"
#import "AFNetworking.h"
#import "AppDelegate.h"
@interface myModel : UIViewController


//+(void)AllocInitSchedules;
+(NSString*)userSignIn:(NSString *)strURL  email:(NSString *)email password:(NSString *)password RequestData:(NSString *)strRequestData;
+(NSString*)SignIn:(NSString *)strUser Password:(NSString *)strPassword RequestData:(NSMutableDictionary *)strRequestData;
+(BOOL) editprofile:(NSString *)strURL;
//+(BOOL) userSignUp:(NSString *)strURL;
+(NSString*)userSignUp:(NSString *)strURL;
// SignIn
+(NSString*)userSignIn:(NSString *)strUser Password:(NSString *)strPassword;

+ (NSDictionary*) userProfileData;
// Get Response in Array
+(NSMutableDictionary*)getDictResponse:(NSString *)strURL RequestData:(NSString *)strRequestData;
+(NSMutableDictionary*)getDictResponse1:(NSString *)strURL RequestData:(NSString *)strRequestData;
+(NSMutableDictionary*)getDictResponse2:(NSString *)strURL RequestData:(NSString *)strRequestData;
+(NSMutableDictionary*)profile:(NSString *)strURL RequestData:(NSString *)strRequestData;
+(BOOL)requestUrl1:(NSString *)strURL RequestData:(NSString *)strRequestData;

+(BOOL)requestUrl2:(NSString *)strURL RequestData:(NSString *)strRequestData;

+(BOOL)addInvitationUrl:(NSString *)strURL RequestData:(NSString *)strRequestData;

+(BOOL)requestUrl:(NSString *)strURL RequestData:(NSString *)strRequestData;

+(BOOL)requestURL:(NSString*)strURL withRequestParams:(NSMutableDictionary*)params ImageNSData:(NSData*)imgNSData;
+(NSString*)encodeURL:(NSString*)strURL;
#pragma mark Local DB
//+(void)openDB;
//Contact List
//+(void)insertNewContact:(NSMutableArray*)marrContacts;
//+(NSMutableArray*)getContactList:(NSString*)strIsDayUser;
//+(NSMutableArray*)getContactNumber;
//+(void)UpdateDayUserAvailable:(NSMutableDictionary*)mdictUsers;

//Friend List
//+(void)createFriendTable;
//+(NSMutableArray*)getFreindList;
//+(void)deleteFriend:(NSString*)strFriendId;
//+(void)insertNewFreind:(NSMutableArray*)marrFriendData;


// Internet Connectivity
+(BOOL)checkIfInternetConection;
+(BOOL)checkIfInternetConection1;
+(void)checkServerReachability;
+(NSString *)getIPAddress;
// Alloc Init
//+ (void) helperAllocInit;

@end
